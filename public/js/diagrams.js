$(document).ready(function(){
    //-----------------------------------Диаграмма Статусы оборудования-------------------------------------------------
    let totalItem    = 0;
    let unitsBefore  = $('.caption-item_before');
    let unitsList    = $('.unit');
    let unitsListArr = [];
    let percentItem  = 0;
    let firstParty   = '';
    let secondParty  = '';
    let inTotalVal   = $('.in-total-val');
    let inTotalValContainer = $('.in-total-val-container');

    // Вычисление и заполнение массива процентами
    unitsList.each(function(i) {
        let $unit = $(this);
        if (i==0) { totalItem = $unit.attr('data-count'); }
        let volumeItem = $unit.attr('data-count');
        if (totalItem == 0) {
            percentItem = 0;
        } else {
            percentItem = parseFloat(((volumeItem*100)/totalItem).toFixed(1));
        }
        unitsListArr.push(percentItem);
    });

    // Добавление html тега
    let itemCount= $('.caption-item_count');
    itemCount.each(function(i) {
        $(this).append('<span class="caption-item_percent"></span>');
    });

    // Добавление к каждому html тегу значений в процентах
    let itemPercent = $('.caption-item_percent');
    itemPercent.each(function (i) {
        if(i==0) {
            secondParty = (totalItem +' ' + '('+unitsListArr[i]+'%'+')');
        } else {
            $(this).text(' '+'('+unitsListArr[i]+'%'+')');
            //$('#list__count-vertical').append(' '+'('+unitsListArr[i]+'%'+')');
        }
    });

    //-----------------------------------ChartJS Статусы оборудования---------------------------------------------------
    let unitColorArr   = [];
    let unitDescrArr   = [];
    let unitCountArr   = [];

    unitsList.each(function(i) {
        let $unit = $(this);
        if (i==0) { totalItems = $unit.attr('data-count'); }
        let colorItem = $unit.attr('data-color');
        let descrItem = $unit.attr('data-descr');
        let countItem = $unit.attr('data-count');
        if (i == 0) {
            inTotalVal.css('background', colorItem);
            firstParty = descrItem;
        } else {
            unitColorArr.push(colorItem);
            unitDescrArr.push(descrItem);
            unitCountArr.push(+countItem);
        }
    });

    // Складываем для вывода параметра "Всего"
    inTotalValContainer.append('<span class="in-total-val-offset"></span>');
    inTotalValContainer.append(firstParty + '-' + ' ' + secondParty);


    //Chart.defaults.color = '#ccc';
    // Chart JS #4
    var ctx = document.getElementById("myChart4").getContext('2d');
    const getSuitableY = (y, yArray = [], direction) => {
        let result = y;
        yArray.forEach((existedY) => {
            if (existedY - 14 < result && existedY + 14 > result) {
                if (direction === "right") {
                    result = existedY + 14;
                } else {
                    result = existedY - 14;
                }
            }
        });
        return result;
    };
    const getOriginPoints = (source, center, l) => {
        let a = {x: 0, y: 0};
        var dx = (center.x - source.x) / l;
        var dy = (center.y - source.y) / l;
        a.x = center.x - l * dx;
        a.y = center.y - l * dy;
        return a;
    };
    const options = {
        plugins: {
            legend: {
                display: false
            }
        },
        layout: {
            padding: {
                top: 40,
                left: 40,
                right: 40,
                bottom: 40
            }
        },
    };
    const plugins = [
        {
            afterDraw: (chart) => {
                const ctx = chart.ctx;
                ctx.save();
                ctx.font = "12px Arial";
                const leftLabelCoordinates = [];
                const rightLabelCoordinates = [];
                const chartCenterPoint = {
                    x: (chart.chartArea.right - chart.chartArea.left) / 2 + chart.chartArea.left,
                    y: (chart.chartArea.bottom - chart.chartArea.top) / 2 + chart.chartArea.top
                };
                i = toogleItem;
                chart.config.data.labels[i];
                const meta = chart.getDatasetMeta(0);
                const arc = meta.data[i];
                const centerPoint = arc.getCenterPoint();
                const dataset = chart.config.data.datasets[0];
                let color = chart.config._config.data.datasets[0].backgroundColor[i];
                let labelColor = chart.config._config.data.datasets[0].backgroundColor[i];
                // Prepare data to draw
                const angle = Math.atan2(
                    centerPoint.y - chartCenterPoint.y,
                    centerPoint.x - chartCenterPoint.x
                );
                let originPoint = getOriginPoints(chartCenterPoint, centerPoint, arc.outerRadius)
                const point2X = chartCenterPoint.x + Math.cos(angle) * (centerPoint.x < chartCenterPoint.x ? arc.outerRadius + 10 : arc.outerRadius + 10);
                let point2Y = chartCenterPoint.y + Math.sin(angle) * (centerPoint.y < chartCenterPoint.y ? arc.outerRadius + 15 : arc.outerRadius + 15);
                const point3X = chartCenterPoint.x + Math.cos(angle) * arc.outerRadius;
                let point3Y = chartCenterPoint.y + Math.sin(angle) * arc.outerRadius;
                let suitableY;
                if (point2X < chartCenterPoint.x) {
                    // on the left
                    suitableY = getSuitableY(point2Y, leftLabelCoordinates, "left");
                } else {
                    // on the right
                    suitableY = getSuitableY(point2Y, rightLabelCoordinates, "right");
                }
                point2Y = suitableY;
                let value = Math.ceil((dataset.data[i]*100)/totalItem);
                let edgePointX = point2X < chartCenterPoint.x ? chartCenterPoint.x - arc.outerRadius - 10 : chartCenterPoint.x + arc.outerRadius + 10;
                if (point2X < chartCenterPoint.x) {
                    leftLabelCoordinates.push(point2Y);
                } else {
                    rightLabelCoordinates.push(point2Y);
                }
                // first line: connect between arc's center point and outside point
                ctx.lineWidth = 1;
                ctx.strokeStyle = color;
                ctx.beginPath();
                if (flagEmptyArr) {
                    ctx.moveTo(0, 0);
                    ctx.lineTo(0, 0);
                } else {
                    ctx.moveTo(point3X, point3Y);
                    ctx.lineTo(point2X, point2Y);
                }
                ctx.stroke();
                // second line: connect between outside point and chart's edge
                ctx.beginPath();
                if (flagEmptyArr) {
                    ctx.moveTo(0, 0);
                    ctx.lineTo(0, 0);
                } else {
                    ctx.moveTo(point2X, point2Y);
                    ctx.lineTo(edgePointX, point2Y);
                }
                ctx.stroke();
                //fill custom label
                const labelAlignStyle = edgePointX < chartCenterPoint.x ? "right" : "left";
                const labelX = edgePointX < chartCenterPoint.x ? edgePointX : edgePointX + 0;
                const labelY = point2Y + 7;
                ctx.textAlign = labelAlignStyle;
                ctx.textBaseline = "bottom";
                ctx.font = "12px Arial";
                if ($("body").hasClass("dark-layout")) {
                    ctx.fillStyle = 'white';
                } else {
                    ctx.fillStyle = 'black';
                }
                if (flagEmptyArr) {
                    value='';
                    ctx.fillText(value, labelX, labelY);
                } else {
                    ctx.fillText(value + '%', labelX, labelY);
                }
                ctx.restore();
            }
        }
    ];
    var myChart4 = new Chart(ctx, {
        type: 'doughnut',
        plugins: plugins,
        options: options,
        data: {
            labels: unitDescrArr,
            datasets: [
                {
                    label: "Статусы оборудования",
                    data: unitCountArr,
                    backgroundColor: unitColorArr,
                    borderColor: '#ffffff',
                    borderWidth: 1,
                    cutout: '60%',
                    rotation: 0,
                    borderAlign: 'inner',
                    spacing: 1,
                }
            ]
        }
    });

    // Назначение номеров для выбора сноски при клике в круговой диаграмме
    unitsBefore.each(function(i) {
        if (i != 0) {
            $(this).attr("data-toggle-item", i-1);
        }
    });
    // Если данных для диаграммы нет, не рисуем линии и метки
    let flagEmptyArr = false;
    if (unitsListArr[0] == 0) {
        flagEmptyArr = true;
    }
    // Обработка Click в диаграмме
    let indexOfMaxValue = unitCountArr.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);
    let toogleItem = indexOfMaxValue;
    unitsBefore.each(function(i) {
        let self = $(this);
        if (i !=0) {
            self.on("click", function(){
                toogleItem = self.attr('data-toggle-item');
                myChart4.update();
            });
        }
    });


    // Chart JS #5
    const ctx5 = document.getElementById("myChart5").getContext('2d');
    const options1 = {
        plugins: {
            legend: {
                // display: false,
                onClick: (evt, legendItem, legend) => {
                    const index = legend.chart.data.labels.indexOf(legendItem.text);
                    legend.chart.toggleDataVisibility(index);
                    legend.chart.getDataVisibility(index);
                    legend.chart.update();
                },
                position: 'right',
                labels: {
                    font: {
                        family: "'Manrope','Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                        size: 14,
                        //lineHeight: 1.65,
                    },
                    usePointStyle: false,
                    //pointStyle: 'rect',
                    //pointStyleWidth: '17px',
                    boxWidth: 16,
                    boxHeight: 16,
                    useBorderRadius: true,
                    borderRadius: 3,
                    padding: 6,
                    //color: '#ccc',
                    generateLabels: (chart) => {
                        let visibility = [];
                        for (let i = 0; i < chart.data.labels.length; i++) {
                            if(chart.getDataVisibility(i) === true) {
                                visibility.push(false)
                            } else {
                                visibility.push(true)
                            }
                        };
                        //console.table(visibility);
                        //console.log(chart);
                        return chart.data.labels.map(
                            (label, index) => ({
                                text: label,
                                strokeStyle: chart.data.datasets[0].borderColor[index],
                                fillStyle: chart.data.datasets[0].backgroundColor[index],
                                hidden: visibility[index]
                            })
                        )
                    }
                }
            }
        },
        maintainAspectRatio: false,
        layout: {
            padding: {
                top: 20,
                left: 20,
                right: 20,
                bottom: 20
            }
        },
        scales: {
            x: {
                display: false,
                grid: {
                    drawOnChartArea: false
                },
                ticks: {
                    display: false
                }
            },
            y: {
                display: false,
                // beginAtZero: true,
                grid: {
                    drawOnChartArea: false
                },
                ticks: {
                    display: false
                }
            }
        }
    };

    let label_FillStyle = '';
    if ($("body").hasClass("dark-layout")) {
        label_FillStyle = 'white';
    } else {
        label_FillStyle = 'black';
    }

    const myChart5 = new Chart(ctx5, {
        type: 'bar',
        plugins: [ChartDataLabels],
        options: options1,
        data: {
            labels: unitDescrArr,
            datasets: [
                {
                    label: "Статусы оборудования",
                    data: unitCountArr,
                    backgroundColor: unitColorArr,
                    borderColor: unitColorArr,
                    borderRadius: 6,
                    datalabels: {
                        color: label_FillStyle,
                        align: 'top',
                        anchor: 'end',
                        offset: 0
                    }
                }
            ]
        }
    });

    // Добавление к каждому html тегу значений в процентах для правой диаграммы
    let cList = $('ul.ui__list')
    $.each(unitsListArr, function(i)
    {
        if(i!=0) {
            let li = $('<li/>')
                .addClass('ui__list-item')
                .appendTo(cList);
            let item_p = $('<p></p>')
                .addClass('ui__list-item-p')
                .text(' '+unitsListArr[i]+'%'+' '+'('+unitCountArr[i-1]+')')
                .appendTo(li);
        }
    });

    //-----------------------------Работа с таблицей статусов оборудования----------------------------------------------
    $('.table__new').on('click', '.div__open-table', function() {
        onClickOfCustomStatesByTypes();
    });

    $('.table__new tr:not(:first-child)').hover(function(){
        $(this).addClass('hover');
    }, function() {
        $(this).removeClass('hover');
    });

    $('.table__new tr:not(:first-child)').click(function(){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    });

});
