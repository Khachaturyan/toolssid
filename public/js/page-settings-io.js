
        //--- функция сохраняет настройки привязанные к конкретному пользователю ---------------------------------------
        function saveUserSettings(pageName,uid,key,value,token){
            let post ={};
            post['_token'] = token;
            post['label' ] = pageName + "." + uid + "." + key;
            post['data'  ] = value;
            $.post( "/set-any-data", post, function(data, status) {
                if (status === 'success'){
                    if ("true" === data){
                        console.log(data);
                    }else{
                        alert("ошибка сохранения данных - " + data);
                    }
                }
            });
        }

        //--- загружаются персональные настройки страницы и пользователя -----------------------------------------------
        function loadAnyPageData(pageName,uid,callback){
            let bSaveSettingsUser = false; //признак нового пользователя
            $.get( "/get-any-data/" + pageName + "/" + uid, function(data, status) {
                if (status === 'success'){
                    let arrData = [];
                    let parsed = JSON.parse(data);
                    for (let c in parsed) {
                        arrData.push(parsed[c]);
                    }
                    if (arrData.length==0) {
                        bSaveSettingsUser = true;
                    }
                    let pageSettings = {};
                    let dat = JSON.parse(data);
                    for (let i = 0; i < dat.length; i++){
                        pageSettings[dat[i].label.replace(pageName + "." + uid + ".","")] = dat[i].data;
                    }
                    callback(pageSettings, bSaveSettingsUser);
                }
            });
        }
