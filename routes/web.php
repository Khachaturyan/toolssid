<?php

use App\Http\Controllers\SettingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\TerminalController;
use App\Http\Controllers\TerminalGroupController;
use App\Http\Controllers\TerminalMessageController;
use App\Http\Controllers\ApiCellController;
use App\Http\Controllers\MenImportExcelController;
use App\Http\Controllers\EquImportExcelController;
use App\Http\Controllers\StageController;
use App\Http\Controllers\UseManualController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CellSizeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ApiEquipmentController;
use App\Models\CustomData;
use App\Http\Controllers\ProjectSettingsController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ApiManController;
use App\Http\Controllers\AndCommController;
use App\Http\Controllers\EquMenRigidLigamentController;
use App\Http\Controllers\ApiTestController;
use App\Http\Controllers\StatsController;
use App\Http\Controllers\EquController;
use App\Http\Controllers\TypeLoadController;
use App\Http\Controllers\FailureTypeController;
use App\Http\Controllers\AuthCustomController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\StatStateController;
use App\Http\Controllers\BreakingStateController;
use App\Http\Controllers\EquStatusController;
use App\Http\Controllers\BlockController;
use App\Http\Controllers\MenCellLinkController;
use App\Http\Controllers\ActController;
use App\Http\Controllers\ApiSigurController;
use App\Http\Controllers\ApiLogController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\AlertMailPersonalController;

//----------------------------------------------------------------------------------------------------------------------
$cdDisableRegister = Schema::hasTable('custom_data') &&
                                        CustomData::where('label','reg')->where('data','false')->first()!= null;
if ($cdDisableRegister){
    Auth::routes(['register' => false]);
}else{
    Auth::routes();
}
//----------------------------------------------------------------------------------------------------------------------
Route::get('/', function () { return redirect()->route('main'); });
Route::get('/change-reg-lock-mode-action',   [ UserController::class, 'changeRegLockMode'         ]);
Route::get('/confirm/{hash}',                [ UserController::class, 'confirmUser'               ]);
Route::post('/register-new',                 [ RegisterController::class,'registerNew'            ]);
Route::post('/check-user-auth',              [ UserController::class,'checkUserAuth'              ]);
Route::get('/home',                          [ HomeController::class, 'index'                     ])->name('home');
Route::get('/events',                        [ HomeController::class, 'index'                     ]);
Route::get('/events-all',                    [ HomeController::class, 'eventsAll'                 ]);
Route::get('/events/{bid}/{mid}',            [ HomeController::class, 'events'                    ]);
Route::get('/events/{eid}',                  [ HomeController::class, 'eventsEq'                  ]);
Route::get('/events-men/{mid}',              [ HomeController::class, 'eventsMen'                 ]);
Route::get('/events-cell/{cid}',             [ HomeController::class, 'eventsCell'                ]);
Route::get('/events-ser/{ser}',              [ HomeController::class, 'eventsSer'                 ]);
Route::get('/events-inv/{inv}',              [ HomeController::class, 'eventsInv'                 ]);
Route::get('/events-find/{dat}',             [ HomeController::class, 'eventsFind'                ]);
Route::get('/users',                         [ HomeController::class, 'users'                     ])->name('users');

Route::get('/mans',                          [ HomeController::class, 'mans'                      ])->name('mans');
Route::get('/men-fired',                     [ HomeController::class, 'menFired'                  ])->name('men-fired');

Route::post('/check-auth',                   [ HomeController::class, 'checkAuth'                 ]);

Route::post('/men-prs-import',               [ ApiManController::class, 'parsecImport'            ]);
Route::post('/men-ori-import',               [ ApiManController::class, 'orionImport'             ]);
Route::post('/seven-seals-import',           [ ApiManController::class, 'sevenSealsImport'        ]);
Route::post('/men-revert-dia-state',         [ ApiManController::class, 'revertState'             ]);
Route::get('/men-gen-lab',                   [ ApiManController::class, 'genLabel'                ]);
Route::get('/men-data/{eid}',                [ ApiManController::class, 'menDataForEqu'           ]);
Route::get('/men-select/{sel_data}',         [ ApiManController::class, 'menSelect'               ]);
Route::get('/men-data-cell/{cid}',           [ ApiManController::class, 'menDataForCell'          ]);
Route::get('/men-for-alert/{estate}',        [ ApiManController::class, 'menForAlertEstate'       ]);
Route::get('/hw-men-data/{pi}/{sid}',        [ ApiManController::class, 'hardworkingMenData'      ]);
Route::get('/b-men-data/{pi}/{sid}',         [ ApiManController::class, 'breakingMenData'         ]);
Route::get('/del-men/{any}',                 [ HomeController::class, 'manDel'                    ])->name('del-men');
Route::get('/men-for-alert-person',          [ ApiManController::class, 'menForAlertPerson'       ]);
Route::get('/men-for-alert-person/{mid}',    [ AlertMailPersonalController::class, 'alertForMen'  ]);
Route::post('/personal-alert-create',        [ AlertMailPersonalController::class, 'paCreate'     ]);
Route::post('/personal-alert-remove',        [ AlertMailPersonalController::class, 'paRemove'     ]);

Route::get('/roles',                         [ HomeController::class, 'roles'                     ])->name('roles');
Route::get('/roles-arc',                     [ HomeController::class, 'rolesArc'                  ])->name('roles-arc');
Route::get('/operations',                    [ HomeController::class, 'operations'                ])->name('operations');

Route::get('/equipment',                     [ EquController::class, 'equipment'                  ])->name('equipment');
Route::get('/equipment-archive',             [ EquController::class, 'equipmentArchive'           ])->name('equipment-archive');
Route::get('/equipment-not-working',         [ EquController::class, 'equipmentNotWorking'        ])->name('equipment-not-working');
//Route::get('/equ-update-estat/{id}/{st}',    [ EquController::class, 'equEstatUpdate'             ]);
Route::post('/equipment-import-action',      [ EquImportExcelController::class, 'import'          ]);
Route::post('/equipment-create-action',      [ EquController::class, 'equipmentCreateAction'      ]);
Route::post('/equ-create-web-action',        [ ApiEquipmentController::class, 'createFromWeb'     ]);
Route::post('/equ-edit-web-action',          [ ApiEquipmentController::class, 'editFromWeb'       ]);
Route::post('/equipment-create-action-cons', [ EquController::class, 'equipmentCreateActionCons'  ]);
Route::post('/equ-find-rdp-link',            [ EquController::class, 'getRdpLink'                 ]);
Route::post('/equ-clear-favorites',          [ EquController::class, 'clearFavorites'             ]);
Route::get('/equ-eject/{id}',                [ EquController::class, 'equEject'                   ]);
Route::get('/type-has-equ/{tid}',            [ EquController::class, 'isTypeHasEqu'               ]);
Route::get('/equ-take-apart/{tid}',          [ EquController::class, 'takeApartEqu'               ]);
Route::get('/equ-kit-items/{kid}',           [ EquController::class, 'getKitItems'                ]);

Route::get('/equ-states',                    [ EquStatusController::class, 'show'                 ])->name('equ-states');
Route::get('/equ-states-items',              [ EquStatusController::class, 'itemsData'            ]);
Route::post('/equ-states-item-edit',         [ EquStatusController::class, 'itemEdit'             ]);
Route::get('/equ-states-item-del/{id}',      [ EquStatusController::class, 'itemDelete'           ]);
Route::get('/add-equipment-count/{id}',      [ EquController::class, 'addEquipmentCount'          ])->name('add-equipment-count');
Route::get('/equ-rigt-data',                 [ EquController::class, 'equRigitData'               ]);
Route::get('/equ-types-des/{kid}',           [ EquController::class, 'getKitItemsDes'             ]);

Route::get('/mce-add/{mid}/{cid}',           [ MenCellLinkController::class, 'AddLink'            ]);
Route::get('/mce-remove/{mid}/{cid}',        [ MenCellLinkController::class, 'RemoveLink'         ]);

Route::get('/cells',                         [ HomeController::class, 'cells'                     ])->name('cells');
Route::get('/et-select/{cd}',                [ HomeController::class, 'etypesSelect'              ]);
Route::get('/etypes',                        [ HomeController::class, 'etypes'                    ])->name('etypes');
Route::get('/etypes-all',                    [ HomeController::class, 'etypesAll'                 ])->name('etypes-all');

Route::get('/stages',                        [ StageController::class, 'show'                     ])->name('stages');
Route::get('/stages-arc',                    [ StageController::class, 'showArc'                  ])->name('stages-arc');
Route::post('/stage-action',                 [ StageController::class, 'stageAction'              ]);

Route::get('/cells/{bid}',                   [ HomeController::class, 'cellsblock'                ])->name('cellsblock');
Route::get('/cell-clear/{cid}',              [ HomeController::class, 'cellsClear'                ]);
Route::post('/cell-edit-action',             [ ApiCellController::class, 'cellEditAction'         ]);
Route::post('/cell-sizes-all-action',        [ HomeController::class, 'changeAllCellsSize'        ]);

Route::get('/add-role-item/{id}',            [ HomeController::class, 'addRoleItem'               ])->name('add-role-item');

Route::get('/blocks',                        [ BlockController::class, 'blocks'                   ])->name('blocks');
Route::post('/block-action',                 [ BlockController::class, 'blockAction'              ]);
Route::get('/block-equipment',               [ BlockController::class, 'blockEquipment'           ])->name('blocks-equipment');

Route::get('/logs-clear/{dat}',              [ ApiLogController::class, 'clearLogs'               ]);
Route::post('/etype-create-action',          [ HomeController::class, 'etypeCreateAction'         ]);
Route::post('/etype-attache-action',         [ HomeController::class, 'etypeAttacheAction'        ]);
Route::post('/role-action',                  [ HomeController::class, 'roleCreateAction'          ]);
Route::post('/role-attache-type',            [ RoleController::class, 'attacheRoleToTypeState'    ]);

Route::post('/man-action',                   [ HomeController::class, 'manAction'                 ]);
Route::post('/add-equipment-action',         [ EquController::class, 'equipmentAddAction'         ]);

Route::get('/qr/{tid}',                      [ TerminalController::class,    'qr'                 ])->name('qr');

Route::get('/terminals-message',             [ TerminalMessageController::class, 'index'          ])->name('terminals-message');
Route::get('/tm-create/{tid}',               [ TerminalMessageController::class, 'tmCreate'       ])->name('tm-create');
Route::get('/tm-men',                        [ TerminalMessageController::class, 'tmMen'          ])->name('tm-men');
Route::get('/tm-men-send-all',               [ TerminalMessageController::class, 'tmMenActionAll' ]);
Route::get('/tm-men-send-sel/{ids}',         [ TerminalMessageController::class, 'tmMenActionSel' ]);
Route::post('/tm-create-action',             [ TerminalMessageController::class, 'tmAction'       ]);
Route::post('/tm-men-send-action',           [ TerminalMessageController::class, 'tmActionSend'   ]);

Route::post('/men-excel-import',             [ MenImportExcelController::class, 'import'          ])->name('men-excel-import');

Route::get('/manual',                        [ UseManualController::class,      'show'            ])->name('manual');
Route::post('/manual-android-save',          [ UseManualController::class,      'saveManual'      ]);

Route::get('/feedbacks',                     [ FeedbackController::class,      'feedbacks'        ])->name('feedbacks');

Route::get('/cell-sizes',                    [ CellSizeController::class, 'index'                 ])->name('cell-sizes');
Route::post('/cell-size-create-action',      [ CellSizeController::class, 'create'                ]);

Route::get('/user-remove/{id}',              [ UserController::class, 'remove'                    ])->name('remove');

Route::get('/mail-settings',                 [ ProjectSettingsController::class, 'mailSettings'   ])->name('mail-settings');
Route::get('/design-switch',                 [ ProjectSettingsController::class, 'desSwitch'      ]);
Route::get('/project-settings2',             [ ProjectSettingsController::class, 'index'          ])->name('project-settings2');
Route::post('/ps-mail-send',                 [ ProjectSettingsController::class, 'sendMail'       ]);
Route::post('/settings-sigur-act',           [ ProjectSettingsController::class, 'sigurAction'    ]);
Route::post('/settings-sigur-act2',          [ ProjectSettingsController::class, 'sigurAction2'   ]);
Route::post('/settings-sap-ewm-act',         [ ProjectSettingsController::class, 'sapEwmAction'   ]);
Route::post('/soti-save-action',             [ ProjectSettingsController::class, 'saveSoti'       ]);
Route::post('/p-settings2-save-action',      [ ProjectSettingsController::class, 'saveAction'     ]);
Route::post('/ps-save-mail-action',          [ ProjectSettingsController::class, 'saveMailAction' ]);
Route::post('/add-new-mail-alert',           [ ProjectSettingsController::class, 'addNewMail'     ]);
Route::post('/remove-mail-alert',            [ ProjectSettingsController::class, 'removeMail'     ]);
Route::post('/set-sync-tim',                 [ ProjectSettingsController::class, 'setSyncTimEn'   ]);
Route::get('/get-any-data/{page}/{uid}',     [ ProjectSettingsController::class, 'getAnyData'     ]);
Route::post('/set-any-data',                 [ ProjectSettingsController::class, 'setAnyData'     ]);
Route::get('/alert-mails',                   [ ProjectSettingsController::class, 'getAlertMails'  ]);
Route::post('/alert-mail-update-state',      [ ProjectSettingsController::class, 'updAlertEstate' ]);
Route::get('/set-autoupdate/{st}',           [ ProjectSettingsController::class, 'setAutoupdate'  ]);
Route::get('/get-autoupdate',                [ ProjectSettingsController::class, 'getAutoupdate'  ]);
Route::post('/clear-base',                   [ ProjectSettingsController::class, 'clearBase'      ]);
Route::post('/update-release-note-max-id',   [ ProjectSettingsController::class, 'updRnMaxId'     ]);
Route::post('/update-manual-max-id',         [ ProjectSettingsController::class, 'updManualMaxId' ]);
Route::get('/sync',                          [ ProjectSettingsController::class, 'showSync'       ])->name('sync');
Route::get('/pack-prj',                      [ ProjectSettingsController::class, 'packDcuProject' ]);

Route::post('/and-comm',                     [ AndCommController::class, 'add'                    ]);

Route::get('/me-rigid-remove/{eid}',         [ EquMenRigidLigamentController::class, 'remove'     ]);
Route::get('/me-rigid-remove2/{eid}/{uid}',  [ EquMenRigidLigamentController::class, 'remove2'    ]);
Route::get('/me-rigid-add/{eid}/{uid}',      [ EquMenRigidLigamentController::class, 'add'        ]);
Route::get('/me-rigid-add2/{eid}/{uid}',     [ EquMenRigidLigamentController::class, 'add2'       ]);
Route::get('/me-rigid-data/{eid}',           [ EquMenRigidLigamentController::class, 'getData'    ]);

Route::post('/api-test',                     [ ApiTestController::class, 'apiTest'                ]);

Route::get('/stats/{mid}',                   [ StatsController::class, 'stats'                    ]);
Route::get('/stats-all',                     [ StatsController::class, 'statsAll'                 ]);

Route::get('/user-thema',                    [ UserController::class, 'thema'                     ])->name('user-thema');

Route::get('/type-load',                     [ TypeLoadController::class, 'show'                  ])->name('type-load');
Route::get('/type-load-list/{tf}/{sid}',     [ TypeLoadController::class, 'listItems'             ]);
Route::get('/type-unlim-list/{sid}',         [ TypeLoadController::class, 'getUnlimitUseItems'    ]);
Route::get('/not-working-list/{sid}',        [ TypeLoadController::class, 'getNotWorkingItems'    ]);

Route::get('/failure-type',                  [ FailureTypeController::class, 'index'              ])->name('failure-type');
Route::get('/failure-type-arc',              [ FailureTypeController::class, 'showArchive'        ])->name('failure-type-arc');
Route::get('/failure-change-arc-state/{id}', [ FailureTypeController::class, 'changeArcState'     ]);
Route::post('/failure-type-action',          [ FailureTypeController::class, 'action'             ]);

Route::post('/pass-reset-action',            [ AuthCustomController::class, 'passResetAction'     ]);
Route::post('/pass-email',                   [ AuthCustomController::class, 'passEmail'           ]);
Route::get('/pass-reset/{pass}',             [ AuthCustomController::class, 'passReset'           ]);

Route::get('/rep/{ds}/{de}/{mids}/{eid}',    [ ReportController::class, 'reportCreate'            ]);
Route::get('/rep-xls/{ds}/{de}/{mids}/{eid}',[ ReportController::class, 'reportXlsx'              ]);
Route::get('/rep-equ-xls/{ds}/{de}/{sid}',   [ ReportController::class, 'reportEquXlsx'           ]);
Route::get('/rep-equ-xls2',                  [ ReportController::class, 'equExcelMaxReport'       ]);
Route::get('/rep-equ-xls3/{sid}',            [ ReportController::class, 'equExcelReport3'         ]);

Route::get('/reports',                       [ ReportController::class, 'reports'                 ])->name('reports');
Route::get('/rep-not-wrk/{ds}/{de}/{sid}',   [ ReportController::class, 'createRepNotWorking'     ]);
Route::get('/rep-men',                       [ ReportController::class, 'reportMenXlsx'           ]);
Route::get('/rep-day-log/{date}/{sid}',      [ ReportController::class, 'reportDayLogXlsx'        ]);

Route::get('/block-data/{archive}',          [ TypeLoadController::class, 'blockData'             ]);
Route::get('/main',                          [ TypeLoadController::class, 'main'                  ])->name('main');
Route::get('/main/{st_id}',                  [ TypeLoadController::class, 'mainStage'             ]);

Route::get('/stst-items/{ds}/{de}/{sid}',    [ StatStateController::class, 'itemsLoad'            ]);

Route::get('/rigid-links',                   [ EquMenRigidLigamentController::class, 'show'       ])->name('rigid-links');

Route::get('/breaking-stats/{i}/{sid}',      [ BreakingStateController::class, 'getFailStat'      ]);

Route::get('/any-cols/{t}/{c}',              [ HomeController::class, 'getAnyColValues'           ]);
Route::get('/any-cols/{t}/{c}/{p}',          [ HomeController::class, 'getAnyColParamValues'      ]);

Route::get('/acts',                          [ ActController::class, 'index'                      ]);
Route::get('/act-list',                      [ ActController::class, 'list'                       ]);
Route::get('/act-create',                    [ ActController::class, 'create'                     ]);
Route::get('/act-equ-lst/{aid}',             [ ActController::class, 'equList'                    ]);
Route::get('/act-change/{aid}/{eid}',        [ ActController::class, 'change'                     ]);
Route::get('/act-select-all/{aid}',          [ ActController::class, 'selectAll'                  ]);
Route::get('/act-unselect-all/{aid}',        [ ActController::class, 'unselectAll'                ]);
Route::get('/act-save/{aid}',                [ ActController::class, 'save'                       ]);
Route::get('/act-xlsx-out/{aid}',            [ ActController::class, 'reportRapairActOut'         ]);
Route::get('/act-xlsx-in/{aid}',             [ ActController::class, 'reportRapairActIn'          ]);

Route::get('/jsigur-on',                     [ ApiSigurController::class, 'startServer'           ]);
Route::get('/jsigur-off',                    [ ApiSigurController::class, 'stopServer'            ]);
Route::get('/jsigur-check',                  [ ApiSigurController::class, 'checkServer'           ]);
Route::get('/sigur-empl-test',               [ ApiSigurController::class, 'getListEmployees'      ]);
Route::get('/sigur-get-token',               [ ApiSigurController::class, 'getJwtToken'           ]);

Route::get('/archive-logs',                  [ ApiLogController::class, 'showArchiveLogs'         ]);
