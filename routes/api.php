<?php


use App\Http\Controllers\AndCommController;
use App\Http\Controllers\ApiBlockController;
use App\Http\Controllers\ApiCellController;
use App\Http\Controllers\ApiEquipmentController;
use App\Http\Controllers\ApiLogController;
use App\Http\Controllers\ApiManController;
use App\Http\Controllers\ApiProjectSettingsController;
use App\Http\Controllers\ApiSigurController;
use App\Http\Controllers\ApiTypeController;
use App\Http\Controllers\FailureTypeController;
use App\Http\Controllers\FbTokenController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\TerminalController;
use App\Http\Controllers\AutostartController;
use App\Http\Controllers\CellBusyController;
use App\Http\Controllers\TimServerController;
use App\Http\Controllers\LogTakeItemController;
use App\Http\Controllers\MenBlockUseController;
use App\Http\Controllers\EquRateController;
use App\Http\Controllers\MenLockController;
use App\Http\Controllers\EquExpiredController;
use App\Http\Controllers\EquFavoriteController;
use App\Http\Controllers\ParsecController;
use App\Http\Controllers\OrionController;
use App\Http\Controllers\StatStateController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ApiTypeLoadController;
use App\Http\Controllers\ApiBreakingStateController;
use App\Http\Controllers\AlertMailPersonalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login',                        [ RegisterController::class,          'login'                    ]);
Route::get('tick',                          [ ApiLogController::class,            'onTick'                   ]);
Route::get('get-ping',                      [ ApiLogController::class,            'getPing'                  ]);
Route::get('on-start',                      [ AutostartController::class,         'onStart'                  ]);
Route::get('check-apk',                     [ TimServerController::class,         'checkApk'                 ]);
Route::get('apk-find/{ver}',                [ TimServerController::class,         'getFile'                  ]);
Route::get('sync-tim',                      [ TimServerController::class,         'syncTimSrv'               ]);
Route::get('clear-log-take-item',           [ LogTakeItemController::class,       'clearItems'               ]);
Route::get('update-equ-rate',               [ EquRateController::class,           'updateEquRate'            ]);
Route::get('update-lock-status',            [ MenLockController::class,           'updateLockStatus'         ]);
Route::get('check-expired',                 [ EquExpiredController::class,        'checkExpired'             ]);
Route::get('check-favorite',                [ EquFavoriteController::class,       'checkFavorite'            ]);
Route::get('check-parsec',                  [ ParsecController::class,            'checkParsec'              ]);
Route::get('check-orion',                   [ OrionController::class,             'checkOrion'               ]);
Route::get('check-stat-state',              [ StatStateController::class,         'checkStatState'           ]);
Route::get('check-one-day-report',          [ ReportController::class,            'checkOneDayReport'        ]);
Route::get('check-one-day-report-equ-work', [ ReportController::class,            'checkOneDayReportEquWork' ]);
Route::get('check-type-load',               [ ApiTypeLoadController::class,       'checkTypeLoad'            ]);
Route::get('check-bs',                      [ ApiBreakingStateController::class,  'checkBs'                  ]);
Route::get('check-sigur',                   [ ApiSigurController::class,          'checkSigur'               ]);
Route::get('check-alert-mail-personal',     [ AlertMailPersonalController::class, 'checkAmp'                 ]);

Route::post('sigur-delegation',      [ ApiSigurController::class, 'delegation'                   ]);

Route::middleware('auth:api')->group( function () {
    Route::get('block',                  [ ApiBlockController::class, 'index'                    ]);
    Route::get('block-id/{id}',          [ ApiBlockController::class, 'find'                     ]);
    Route::get('cell/{bid}',             [ ApiCellController::class, 'show'                      ]);
    Route::get('cell-all/{bid}',         [ ApiCellController::class, 'showAll'                   ]);
    Route::post('cell-update',           [ ApiCellController::class, 'update'                    ]);
    Route::get('cell-busy/{bid}',        [ ApiCellController::class, 'busy'                      ]);
    Route::get('cell-by-id/{id}',        [ ApiCellController::class, 'cellById'                  ]);
    Route::get('cell-sizes',             [ ApiCellController::class, 'sizes'                     ]);
    Route::get('cell-free/{bid}',        [ ApiCellController::class, 'free'                      ]);
    Route::get('cell-free-owned/{bid}',  [ ApiCellController::class, 'freeOwned'                 ]);
    Route::get('cell-nw/{bid}',          [ ApiCellController::class, 'notWorking'                ]);
    Route::get('cell-men/{bid}/{uid}',   [ ApiCellController::class, 'cellMen'                   ]);
    Route::get('cell-etype/{bid}/{tid}', [ ApiCellController::class, 'cellByEtype'               ]);
    Route::get('cell-info/{bid}',        [ ApiCellController::class, 'info'                      ]);
    Route::post('cell-state',            [ ApiCellController::class, 'setState'                  ]);
    Route::post('cell-lock',             [ ApiCellController::class, 'setLock'                   ]);
    Route::post('cell-color-update',     [ ApiCellController::class, 'updateCellColors'          ]);

    Route::get('equipment-kits',         [ ApiEquipmentController::class, 'itemsKits'            ]);
    Route::get('equipment-cons',         [ ApiEquipmentController::class, 'itemsConsumable'      ]);
    Route::post('equipment',             [ ApiEquipmentController::class, 'itemsByIds'           ]);
    Route::post('equipment-create',      [ ApiEquipmentController::class, 'create'               ]);
    Route::post('equ-kit-create',        [ ApiEquipmentController::class, 'kitCreate'            ]);
    Route::post('equipment-update',      [ ApiEquipmentController::class, 'update'               ]);
    Route::post('equipment-import',      [ ApiEquipmentController::class, 'import'               ]);
    Route::get('equipment/{elabel}',     [ ApiEquipmentController::class, 'itemByLabel'          ]);
    Route::get('equipment-by-man/{uid}', [ ApiEquipmentController::class, 'itemByUserIgnoreCons' ]);

    Route::get('man/{mlabel}',           [ ApiManController::class, 'show'                       ]);
    Route::get('man-by-id/{id}',         [ ApiManController::class, 'menById'                    ]);
    Route::get('man-by-pin/{pin}',       [ ApiManController::class, 'menByPin'                   ]);
    Route::post('man-action',            [ ApiManController::class, 'manAction'                  ]);
    Route::post('man-with-equ',          [ ApiManController::class, 'menWithEquResp'             ]);

    Route::get('type/{label}',           [ ApiTypeController::class, 'check'                     ]);
    Route::get('type/{label}/{bid}',     [ ApiTypeController::class, 'check'                     ]);
    Route::get('type-men-info/{id}',     [ ApiTypeController::class, 'menEquInfo'                ]);

    Route::get('check-auth',             [ TerminalController::class, 'checkAuth'                ]);
    Route::post('terminal-state',        [ TerminalController::class, 'state'                    ]);
    Route::post('fb-update',             [ FbTokenController::class, 'update'                    ]);

    Route::post('log',                   [ ApiLogController::class, 'addLog'                     ]);
    Route::get('log-men/{id}',           [ ApiLogController::class, 'logByMan'                   ]);
    Route::get('log-block/{id}',         [ ApiLogController::class, 'logByBlock'                 ]);
    Route::get('log-cell/{id}',          [ ApiLogController::class, 'logByCell'                  ]);

    Route::get('and-com/{bid}',          [ AndCommController::class, 'get'                       ]);
    Route::get('custom-data',            [ ApiProjectSettingsController::class, 'data'           ]);

    Route::get('apk-max-version',        [ ApiLogController::class, 'getMaxVersion'              ]);

    Route::get('failure-types',          [ FailureTypeController::class, 'list'                  ]);

    Route::post('product-create',        [ ProductController::class, 'create'                    ]);
    Route::post('product-delete',        [ ProductController::class, 'delete'                    ]);

    Route::post('cell-busy-update',      [ CellBusyController::class, 'update'                   ]);

    Route::post('men-block-use-state',   [ MenBlockUseController::class, 'setState'              ]);
});
