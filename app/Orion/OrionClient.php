<?php

namespace App\Orion;
use App\Http\Controllers\ApiLogController;
use App\Http\Controllers\ProjectSettingsController;
use App\Imports\MenImport;
use App\Models\OrionSendItem;
use SoapClient;
use SoapFault;

class OrionClient
{
    public static $posts = array(
        "Бригадир",
        "Бригадир атрибута",
        "Бригадир комплектация",
        "Бригадир отгрузки",
        "Бригадир приемки",
        "Водитель УПШ",
        "Дежурный администратор",
        "Директор департамента информационных технологий",
        "Кладовщик",
        "Кладовщик (НУПЗ)",
        "Кладовщик (отбор НУПЗ)",
        "Кладовщик (УПШ)",
        "Кладовщик комплектации",
        "Кладовщик приемки",
        "Кладовщик СХТ",
        "Комплектовщик",
        "Маркировщица",
        "Начальник отдела контроля и качества",
        "Начальник смены",
        "Сборщик-маркировщик",
        "Сотрудник склада",
        "Сток-менеджер",
        "Упаковщик-маркировщик"
    );

    private static function loadPersons($_ip = null,$_port = null)
    {
        $ip = $_ip != null ? $_ip : ProjectSettingsController::getSettings('orion_ip','')->data;
        if (!filter_var($ip, FILTER_VALIDATE_IP)){
            return 'no orion ip in settings';
        }
        $port = $_port != null ? $_port : ProjectSettingsController::getSettings('orion_port','8090')->data;
        if (intval($port) == 0){
            return 'no orion port in settings';
        }
        $soapURL = "http://$ip:$port/wsdl/IOrionPro";
        $soapClient = new SoapClient($soapURL);
        $soapResult = $soapClient->__soapCall("GetKeys", Array('codeType' => 4, 'offset' => 0, 'count' => 0));
        $keys = array();
        if ($soapResult != null && is_array($soapResult->OperationResult)){
            foreach ($soapResult->OperationResult as $key){
                $keys['' . $key->PersonId] = $key->Code;
            }
        }else{
            return 'no key data';
        }
        $menCounter = 0;
        $soapResult = $soapClient->__soapCall("GetPersons", Array('withoutPhoto' => true, 'offset' => 0, 'count' => 0));
        if ($soapResult != null && is_array($soapResult->OperationResult)){
            $menImportHelper = new MenImport();
            foreach ($soapResult->OperationResult as $men){
                if (!$men->IsInArchive && $men->Id != null && array_key_exists('' . $men->Id,$keys)){
                    $label = $menImportHelper->orionHackConvert($keys[$men->Id]);
                    if (in_array($men->Position, self::$posts)){
                        $newMen = $menImportHelper->parseFormat($men->Position, $men->Id, $men->FirstName,
                            $men->LastName, $men->MiddleName, $label,$keys[$men->Id],0,'');
                        if ($newMen != null){
                            $newMen->save();
                        }
                        $menCounter++;
                    }
                }
            }
            $menImportHelper->moveToFired();
        }else{
            return 'no person data';
        }
        return $menCounter;
    }

    public static function toImportLogResult($_ip = null,$_port = null){
        $res = self::loadPersons($_ip,$_port);
        if (is_int($res)){
            return 'import from orion ' . $res . ' items';
        }
        return $res;
    }

    private static function changeCardLockStatus($cardNo,$state) : bool{
        $ip = ProjectSettingsController::getSettings('orion_ip','')->data;
        if (!filter_var($ip, FILTER_VALIDATE_IP)){
            return false;
        }
        $port = ProjectSettingsController::getSettings('orion_port','8090')->data;
        if (intval($port) == 0){
            return false;
        }
        $soapURL = "http://$ip:$port/wsdl/IOrionPro";
        try {
            $soapClient = new SoapClient($soapURL);
            $soapResult = $soapClient->__soapCall("BlockPass", Array('cardNo' => $cardNo, 'blocked' => $state));
        } catch (SoapFault $e) {
            $soapResult = null;
        }
        return $soapResult != null && $soapResult->Success;
    }

    public static function uploadOrionSendItems(){
        $items = OrionSendItem::where('send_state',0)->get();
        foreach ($items as $item){
            $sendRes = self::changeCardLockStatus($item->raw_label,$item->blocked);
            $item->send_state = $sendRes ? 1 : 0;
            $item->update();
            if ($sendRes){
                if ($item->blocked === 1){
                    $str = __('str.mens_card') . ' ' . $item->mlabel  . ' ' .  __('str.mens_will_lock_on_ok');
                }else{
                    $str = __('str.mens_card') . ' ' . $item->mlabel  . ' ' .  __('str.mens_will_lock_off_ok');
                }
            }else{
                $str = __('str.mens_card') . ' ' . $item->mlabel  . ' ' .  __('str.mens_will_lock_error');
            }
            ApiLogController::addLogText($str);
        }
    }

}
