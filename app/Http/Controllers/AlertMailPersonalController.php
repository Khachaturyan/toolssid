<?php

namespace App\Http\Controllers;

use App\Exports\StatEquExport2;
use App\Exports\StatEquExport4;
use App\Models\AlertMailPersonal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;
use App\Mail\MailSandler;

class AlertMailPersonalController extends Controller
{

    public static function strTimeToInt($str) : int{
        if ($str == null) return 0;
        $arr = explode(':',$str);
        if (count($arr) != 2) return 0;
        return intval($arr[0]) * 3600 + intval($arr[1]) * 60;
    }

    public static function intTimeToStr($t) : string{
        if ($t == null) return "00:00";
        $t = intval($t) % 86400;
        $c = intval($t / 3600);
        $m = intval(($t % 3600) / 60);
        if ($c < 10) $c = '0' . $c;
        if ($m < 10) $m = '0' . $m;
        return $c . ':' . $m;
    }

    public static function checkAlertMailPersonalAsyncRequest(){
        $url = "http://127.0.0.1/api/check-alert-mail-personal";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function checkAmp(){
        if (request()->ip() == "127.0.0.1"){
            $nowTimeLocalSec = (Carbon::now()->timestamp + ProjectSettingsController::getTimeOffset()) % 86400;
            AlertMailPersonal::where('state',1)->where('time','>',$nowTimeLocalSec)->update(['state' => 0]);
            $items = AlertMailPersonal::where('alert_mail_personals.state',0)
                ->where('alert_mail_personals.time','<',$nowTimeLocalSec)
                ->leftJoin("users", "users.id","=", "alert_mail_personals.uid")
                ->select('users.email as mail','alert_mail_personals.*')
                ->get();
            $estateArr = array();
            foreach ($items as $item){
                if (!array_key_exists($item->estate,$estateArr)){
                    $estateArr[$item->estate] = array();
                }
                $estateArr[$item->estate][] = $item;
            }
            foreach($estateArr as $estate => $alerts){
                switch($estate)
                {
                    case ProjectSettingsController::$MAIL_ESTATE_EQU_IN_HAND:
                        $items = ReportController::equExcelMaxReportItemsStatic(0,true);
                        Excel::store(new StatEquExport4($items), '/public/files/device_return_time_report.xlsx');
                        $strDate = date("d.m.y h:i",Carbon::now()->timestamp + ProjectSettingsController::getTimeOffset());
                        $sbj = "Отчёт о возврате оборудования";
                        $body = "Добрый день! Коллеги, во вложении отчёт по состоянию ТСД СГП на " . $strDate;
                        foreach ($alerts as $alert){
                            MailSandler::send2($alert->mail,
                                $sbj,$body,'../storage/app/public/files/device_return_time_report.xlsx');
                            $alert->update(['state' => 1]);
                        }
                        break;
                    case ProjectSettingsController::$MAIL_ESTATE_CURRENT_EQU_STATE:
                        $items = ReportController::equExcelMaxReportItemsStatic(0,false);
                        Excel::store(new StatEquExport2($items), '/public/files/device_work_report.xlsx');
                        $strDate = date("d.m.y h:i",Carbon::now()->timestamp + ProjectSettingsController::getTimeOffset());
                        $sbj = "Отчёт по текущему состоянию ТСД на " . $strDate;
                        $body = "Добрый день! Коллеги, во вложении отчёт по состоянию ТСД СГП на " . $strDate;
                        foreach ($alerts as $alert){
                            //ApiLogController::addLogText('эмуляция отправки отчёта ' . $sbj . ' ' . $body);
                            MailSandler::send2($alert->mail,
                                $sbj,$body,'../storage/app/public/files/device_work_report.xlsx');
                            $alert->update(['state' => 1]);
                        }
                        break;
                }

            }
        }
        return "ok.";
    }

    public function paCreate(Request $request){
        if (AlertMailPersonal::where('uid',$request->uid)->count() >= 6){
            return 'максимальное количество персональных уведомлений - 6';
        }
        $item = new AlertMailPersonal();
        $item->uid    = $request->uid;
        $item->time   = self::strTimeToInt($request->time);
        $item->estate = $request->estate;
        $item->save();
        return 'true';
    }

    public static function paRemove(Request $request){
        AlertMailPersonal::whereId($request->id)->delete();
        return 'true';
    }

    public function alertForMen($mid){
        $query = AlertMailPersonal::query()
            ->leftJoin("users", "users.id","=", "alert_mail_personals.uid")
            ->where("alert_mail_personals.uid", $mid);
        $query = $query->orderBy('alert_mail_personals.time')
            ->select('users.email','alert_mail_personals.*');
        return Datatables::of($query)
            ->addColumn('remove', function ($row){
                return '<img src="/img/delete_black_24dp.svg" width="20px" height="20px">';
            })
            ->addColumn('time_des', function ($row){
                return self::intTimeToStr($row->time);
            })
            ->addColumn('estate_des', function ($row){
                return ProjectSettingsController::getMailEstateDescription($row->estate);
            })
            ->rawColumns(['remove','time_des','estate_des'])
            ->make(true);
    }
}
