<?php

namespace App\Http\Controllers;


use App\DataTables\CellDataTable;
use App\DataTables\LogDataTable;
use App\DataTables\ManDataTable;
use App\DataTables\RolesDataTable;
use App\DataTables\EtypeDataTable;
use App\Mail\MailSandler;
use App\Models\CellSize;
use App\Models\CustomData;
use App\Models\Product;
use App\Models\Block;
use App\Models\Cell;
use App\Models\Etype;
use App\Models\Equipment;
use App\Models\Role;
use App\Models\Man;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Auth\RegisterController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    private function eventsDataArray($bid, $mid, $eid){
        $blocks = Block::all();
        $allBlock = new Block();
        $allBlock->id = 0;
        $allBlock->name = 'Любой блок';
        $blocks->splice(0, 0, [$allBlock]);
        $mens = Man::all();
        $allMen = new Man();
        $allMen->id = 0;
        $allMen->mname = "Все сотрудники";
        $mens->splice(0, 0, [$allMen]);
        $info = array();
        $notWorking = 0;
        $inWork     = 0;
        $consCount  = 0;
        $busy       = 0;
        foreach ($blocks as $block){
            $arrItem = array();
            $arrItem['notWorking'] = DB::table('equipment')->where('equipment.cid', '>', '0')
                ->where('cells.block', $block->id)
                ->where('equipment.is_work', false)
                ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                ->leftJoin("cells", "cells.id","=", "equipment.cid")->count();
            $notWorking += $arrItem['notWorking'];
            $arrItem['inWork'] = DB::table('equipment')->where('equipment.cid', '>', '0')
                ->where('cells.block', $block->id)
                ->where('equipment.uid', '>', '0')
                ->where('etypes.is_cons', false)
                ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                ->leftJoin("cells", "cells.id","=", "equipment.cid")->count();
            $inWork += $arrItem['inWork'];
            $arrItem['consCount'] = DB::table('equipment')->where('equipment.cid', '>', '0')
                ->where('cells.block', $block->id)
                ->where('etypes.is_cons', true)
                ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                ->leftJoin("cells", "cells.id","=", "equipment.cid")->count();
            $consCount += $arrItem['consCount'];
            $arrItem['busy'] = DB::table('equipment')->where('equipment.cid', '>', '0')
                ->where('cells.block', $block->id)
                ->where('etypes.is_cons', false)
                ->where('equipment.uid', 0)
                ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                ->leftJoin("cells", "cells.id","=", "equipment.cid")->count();
            $busy += $arrItem['busy'];
            $info[$block->id] = $arrItem;
        }
        $arrItem = array();
        $arrItem['notWorking'] = $notWorking;
        $arrItem['inWork']     = $inWork;
        $arrItem['consCount']  = $consCount;
        $arrItem['busy']       = $busy;
        $info[-1] = $arrItem;
        return [
            'blocks' => $blocks, 'selectedBlockId' => $bid,
            'mens'   => $mens,   'selectedMenId'   => $mid,
            'info'   => $info
        ];
    }

    public function index(LogDataTable $dataTable)
    {
        $dataTable->bid = 0;
        $dataTable->mid = 0;
        $dataTable->eid = 0;
        $dataTable->cid = 0;
        return $dataTable->render('home',$this->eventsDataArray(0,0, 0));
    }

    public function eventsAll(LogDataTable $dataTable){
        $dataTable->ignoreWebLogs = false;
        $dataTable->bid = 0;
        $dataTable->mid = 0;
        $dataTable->eid = 0;
        $dataTable->cid = 0;
        return $dataTable->render('home',$this->eventsDataArray(0,0, 0));
    }

    public function events(LogDataTable $dataTable, $bid, $mid)
    {
        $dataTable->bid = $bid;
        $dataTable->mid = $mid;
        $dataTable->eid = 0;
        $dataTable->cid = 0;
        return $dataTable->render('events',$this->eventsDataArray($bid,$mid,0));
    }

    public function eventsEq(LogDataTable $dataTable, $eid)
    {
        $dataTable->bid = 0;
        $dataTable->mid = 0;
        $dataTable->eid = $eid;
        $dataTable->cid = 0;
        return $dataTable->render('events',$this->eventsDataArray(0,0, $eid));
    }

    public function eventsMen(LogDataTable $dataTable, $mid)
    {
        $dataTable->bid = 0;
        $dataTable->mid = $mid;
        $dataTable->eid = 0;
        $dataTable->cid = 0;
        return $dataTable->render('events',$this->eventsDataArray(0,$mid, 0));
    }

    public function eventsCell(LogDataTable $dataTable, $cid)
    {
        $dataTable->bid = 0;
        $dataTable->mid = 0;
        $dataTable->eid = 0;
        $dataTable->cid = $cid;
        $dataTable->ignoreWebLogs = false;
        return $dataTable->render('events',$this->eventsDataArray(0,0, 0));
    }

    public function eventsSer(LogDataTable $dataTable, $ser)
    {
        $dataTable->ser = $ser;
        return $dataTable->render('events',$this->eventsDataArray(0,0, 0));
    }

    public function eventsInv(LogDataTable $dataTable, $inv)
    {
        $dataTable->inv = $inv;
        return $dataTable->render('events',$this->eventsDataArray(0,0, 0));
    }

    public function eventsFind(LogDataTable $dataTable, $dat)
    {
        $dataTable->ser = $dat;
        $dataTable->ignoreWebLogs = false;
        return $dataTable->render('events',$this->eventsDataArray(0,0, 0));
    }

    public function users()
    {
        return view('users');
    }

    public function roles(RolesDataTable $dataTable)
    {
        $dataTable->showArcOnly = false;
        return $dataTable->render('roles',['etypes' => Etype::where('hide',0)->get()]);
    }

    public function rolesArc(RolesDataTable $dataTable)
    {
        $dataTable->showArcOnly = true;
        return $dataTable->render('roles',['etypes' => Etype::where('hide',0)->get()]);
    }

    public function operations()
    {
        return view('operations');
    }

    public function etypesSelect($cd)
    {
        $arr = explode(",",$cd);
        $ids = array();
        foreach ($arr as $item){
            if (strlen($item) > 0) $ids[] = intval($item);
        }
        $query = Etype::query()
            ->where("hide",false)
            ->where('is_comp',false);
        if (count($ids) > 0){
            $str = "";
            foreach ($ids as $id){
                if (strlen($str) > 0) $str .= ",";
                $str .= $id;
            }
            $str = 'id IN (' . $str . ') DESC';
            $query = $query->orderByRaw($str);
        }
        return Datatables::of($query)
            ->addColumn('is_checked', function ($row) use ($ids){
                return in_array($row->id, $ids) ? '<img src="/img/done_green_24dp.svg" style="width : 24px; height: 24px; padding: 6px"/>' : "";
            })
            ->rawColumns(['is_checked'])
            ->make(true);
    }

    public function etypes(EtypeDataTable $dataTable)
    {
        $dataTable->hide = false;
        return $dataTable->render('etypes', ["table_header" => __('str.etypes_main_header'), 'cellSizes' => CellSize::all()]);
    }

    public function etypesAll(EtypeDataTable $dataTable)
    {
        $dataTable->hide = true;
        return $dataTable->render('etypes',["table_header" => __('str.etypes_hide_main_header'), 'cellSizes' => CellSize::all()]);
    }

    public function cells(CellDataTable $dataTable)
    {
        $etypesAll = DB::table('etypes')->where('hide',false)->orderBy("is_cons")->get();
        return $dataTable->render('cells',['cellSizes' => CellSize::all(),
            'blocks' => Block::where('is_arc',false)->get(),"etypesAll"    => $etypesAll,]);
    }

    public function cellsblock(CellDataTable $dataTable,$bid)
    {
        $dataTable->bid = $bid;
        $etypesAll = DB::table('etypes')->where('hide',false)->orderBy("is_cons")->get();
        return $dataTable->render('cells',['cellSizes' => CellSize::all(),
            'blocks' => Block::where('is_arc',false)->get(),"etypesAll"    => $etypesAll,]);
    }

    public function cellsClear($id){
        $equ = Equipment::where('cid',$id)->first();
        $isEqu = true;
        if ($equ == null) {
            $equ = Product::where('cid',$id)->first();
            if ($equ == null) return "equ with cell id - $id not found";
            $isEqu = false;
        }
        $estr = $isEqu ? "устройство" : "продукт";
        if ($isEqu){
            $equ->cid = 0;
            if ($equ->estat <= 0){
                $equ->estat_old = $equ->estat;
                $equ->estat = ApiEquipmentController::getEstatFromEquipment($equ);
                $equ->estat_des = ApiEquipmentController::getStatDescription($equ->estat);
            }
            $equ->update();
        }else{
            $equ->delete();
        }
        ApiLogController::addLogText('Пользователь: ' . '<a href="/events-find/' . auth()->user()->email . '">' .
            auth()->user()->email . '</a> отвязал ' . $estr . ' id: ' .  $equ->id . ' от ячейки id: ' . $id);
        return "true";
    }

    public function changeAllCellsSize(Request $request){
        Cell::where('block',$request->bid)->update(['csid' => $request->csid]);
        return redirect()->route("type-load");
    }

    public function addRoleItem($id){
        $roles = Role::where('is_arc',false)->where('is_admin',false)->get();
        $etype = Etype::where('etypes.id', "=", $id)->first();
        if ($etype == null){
            return abort(404);
        }
        return view('attache-etypes',["roles" => $roles, "etype" => $etype]);
    }

    public function roleCreateAction(Request $request){
        $lim_items = $request->lim_items != null ? $request->lim_items : 1;
        $show_pages = 1 == $request->show_pages;
        $create_user = 1 == $request->create_user;
        $create_role = 1 == $request->create_role;
        $create_unit_type = 1 == $request->create_unit_type;
        $change_status = 1 == $request->change_status;
        $change_object = 1 == $request->change_object;
        $change_box = 1 == $request->change_box;
        $change_cell = 1 == $request->change_cell;
        $show_logs = 1 == $request->show_logs;
        $rigit_men = 1 == $request->rigit_men;
        $create_failure_types = 1 == $request->create_failure_types;
        $export_report = 1 == $request->export_report;
        $access_settings = 1 == $request->access_settings;
        $create_ticket = 1 == $request->create_ticket;
        $more_one_type = 1 == $request->more_one_type;
        if ($request->input("id") == null){
            $role = new Role();
            $role->rname = $request->input("rname");
            $role->is_admin = $request->input("is_admin");
            $role->is_arc = $request->input("is_arc");
            $role->etypes = '';
            $role->lim_items = $lim_items;
            $role->show_pages = $show_pages;
            $role->create_user = $create_user;
            $role->create_role = $create_role;
            $role->create_unit_type = $create_unit_type;
            $role->change_status = $change_status;
            $role->change_object = $change_object;
            $role->change_box = $change_box;
            $role->change_cell = $change_cell;
            $role->show_logs = $show_logs;
            $role->rigit_men = $rigit_men;
            $role->create_failure_types = $create_failure_types;
            $role->export_report = $export_report;
            $role->access_settings = $access_settings;
            $role->create_ticket = $create_ticket;
            $role->more_one_type = $more_one_type;
            $role->save();
            ApiLogController::addLogText('Создана роль: id=' . $role->id . ' (' . $role->rname .
                ')' . ', Пользователь: ' .
                '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>');
        }else{
            Role::whereId($request->input("id"))->update([
                    'rname'                => $request->input("rname"),
                    'is_admin'             => $request->input("is_admin"),
                    'is_arc'               => $request->input("is_arc"),
                    'show_pages'           => $show_pages,
                    'create_user'          => $create_user,
                    'create_role'          => $create_role,
                    'create_unit_type'     => $create_unit_type,
                    'change_status'        => $change_status,
                    'change_object'        => $change_object,
                    'change_box'           => $change_box,
                    'change_cell'          => $change_cell,
                    'show_logs'            => $show_logs,
                    'rigit_men'            => $rigit_men,
                    'create_failure_types' => $create_failure_types,
                    'export_report'        => $export_report,
                    'access_settings'      => $access_settings,
                    'create_ticket'        => $create_ticket,
                    'lim_items'            => $lim_items,
                    'more_one_type'        => $more_one_type
                ]);
            ApiLogController::addLogText('Изменена роль: id=' . $request->id . ' (' . $request->rname .
                ')' . ', Пользователь: ' .
                '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>');
        }
        return redirect()->route("roles");
    }


    public function etypeCreateAction(Request $request)
    {
        $arcStr = "";
        $request->validate([
            "tname" => "required|min:1|max:512",
            "tmark" => "required|min:1|max:512"
        ]);
        if ($request->input("id") != null){
            $etype = Etype::whereId($request->input("id"))->first();
        }else{
            $etype = new Etype();
        }
        if ($etype == null){
            abort(404);
        }
        $etype->tname     = $request->input("tname"    );
        $etype->tmark     = $request->input("tmark"    );
        $etype->tmodel    = $request->input("tmodel"   ) != null ? $request->input("tmodel" ) : "";
        $etype->is_cons   = $request->input("is_cons"  );
        $etype->is_comp   = $request->input("is_comp"  );
        $etype->comp_data = $request->input("comp_data");
        if ($request->input("id") != null && $request->input("hide") && !$etype->hide){
            $arcStr = " тип устройства отправлен в архив";
        }
        $etype->hide    = $request->input("hide"   );
        $etype->csid    = $request->input("csid"   ) != null ? $request->input("csid"   ) : 0;
        if ($request->input("id") != null){
            $etype->update();
        }else{
            $etype->save();
            $rid = Role::where('perm_index',RoleController::SCLAD_MEN_INDEX)->first()->id;
            RoleController::changeRoleTypeAttacheState(true,$rid,$etype->id);
            $rid = Role::where('perm_index',RoleController::IT_MEN_INDEX)->first()->id;
            RoleController::changeRoleTypeAttacheState(true,$rid,$etype->id);
            $roles= Role::where('perm_index',RoleController::ADMIN_INDEX)->get();
            foreach ($roles as $role){
                RoleController::changeRoleTypeAttacheState(true,$role->id,$etype->id);
            }
        }
        if ($request->file('file') != null){
            $path = Storage::disk('public')->putFile("etimg/" . $etype->id, $request->file('file'));
            $etype->img = $path;
            $etype->update();
        }
        ApiLogController::addLogText('Обновлен тип устройства: id=' . $etype->id . ' (' . $etype->tname . ')' .
            ', Пользователь: ' . '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email .
            '</a>' . $arcStr);
        return redirect()->route("etypes");
    }

    public function etypeAttacheAction(Request $request)
    {
        $tid = $request->input("id");
        $roles = Role::where('roles.is_admin', "=", false)->where('is_arc',0)->get();
        $reqArr = $request->all();
        foreach($roles as $role) {
            if (!str_contains($role->etypes, '#' . $tid . '#') && array_key_exists ( "role" . $role->id , $reqArr )){
                // add etype
                if (strlen($role->etypes) == 0){
                    $role->etypes = "#" ;
                }
                $role->etypes .= $tid . "#";
                $role->save();
            } else if (str_contains($role->etypes, '#' . $tid . '#') && !array_key_exists ( "role" . $role->id , $reqArr )) {
                // remove etype
                $parts = explode("#",$role->etypes);
                $str = "#";
                foreach ($parts as $part) if (strlen($part) > 0){
                    if (strcmp($part,"".$tid) != 0){
                        $str .= $part . "#";
                    }
                }
                $role->etypes = $str;
                $role->save();
            }
        }
        return redirect()->route("etypes");
    }

    //----- men region -------------------------------------------------------------------------------------------------

    private function manTable(ManDataTable $dataTable, $eid, $equ_text){
        return $dataTable->render('mans',[
            "roles"         => Role::where('is_arc',0)->orderBy('perm_index', 'asc')->get(),
            'eid'           => $eid,
            'equ_text'      => $equ_text,
            'is_fired_only' => $dataTable->isFired]);
    }

    public function mans(ManDataTable $dataTable)
    {
        $dataTable->isFired = false;
        return $this->manTable($dataTable, '', '');
    }


    public function manDel($id){
        //$man = Man::where('id', $id)->first();
        //$man->delete();
        return redirect('/mans');
    }

    public function menFired(ManDataTable $dataTable)
    {
        $dataTable->isFired = true;
        return $this->manTable($dataTable, '', '');
    }

    public static function createUpdateMen($id,$rid,$pin,$mname,$msurname,$mpatronymic,$fired,$mlabel,$post,
                                           $str_ext_id, $can_dia, $str_ext_login, $web_uid) : array {
        $man = $id != null ? Man::where('id', $id)->first() : new Man();
        if ($man == null){
            return array(-1, null);
        }

        $man->pin = $pin != null ? $pin : "";
        if($can_dia){
            $man->can_dia = $can_dia;
        }
        else{
            $man->can_dia = 0;
        }
        $man->mname = $mname;
        $man->msurname = $msurname;
        $man->mpatronymic = $mpatronymic != null ? $mpatronymic : '';
        $man->str_ext_login = $str_ext_login != null ? $str_ext_login : '';
        if ($id == null) {
            $man->en_items = '';
        }
        $arcStr = "";
        if ($id != null && $man->fired != 1 && 1 == $fired){
            $arcStr = " сотрудник отправлен в архив (уволен) ";
        }
        $roleStr = "";
        if ($id != null && $man->rid != $rid){
            $oldName = Role::whereId($man->rid)->first()->rname;
            $newName = Role::whereId($rid)->first()->rname;
            $roleStr = " обновлена роль c $oldName на $newName ";
        }
        $man->fired = $fired != null ? $fired : 0;
        $man->rid   = $rid;
        $man->post  = $post  != null ? $post : '';
        $man->str_ext_id = $str_ext_id != null ? $str_ext_id : "";
        if ($web_uid != null){
            $man->web_uid = $web_uid;
        }
        if ($man->fired == 1){
            $man->mlabel = '';
        }else{
            $man->mlabel = $mlabel;
            $labelIsUnique = $man->fired == 2 || Man::where('mlabel',$man->mlabel)->where('id', '!=', $man->id)->get()->count() == 0;
            if (!$labelIsUnique){
                return array(-2, 'Метка должна быть уникальной');
            }
        }
        $user = auth()->user();
        if ($id == null){
            $man->save();
            if ($user != null){
                ApiLogController::addLogText('Создан сотрудник метка: <a href="/events/0/' . $man->id . '">' . $man->mlabel . '</a> id=' . $man->id . ', Пользователь: ' .
                    '<a href="/events-find/' . $user->email . '">' . $user->email . '</a>');
            }
        }else{
            $man->update();
            if ($user != null){
                ApiLogController::addLogText('Обновлены данные по сотруднику ' . $man->mname . ' ' .
                    $man->msurname . ' метка: <a href="/events/0/' . $man->id . '">' . $man->mlabel . '</a> Обновил: ' .
                    '<a href="/events-find/' . $user->email . '">' . $user->email . '</a>' . $arcStr . $roleStr,ApiLogController::CHANGE_MEN_DATA_FAKE_BID_ID, $man->id);
            }
        }
        return array($man->id, null);
    }

    private function generate_password($length = 8){
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|';
        $str = '';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++){
            $str .= $chars[random_int(0, $max)];
        }
        return $str;
    }

    public function manAction(Request $request){

        $res = self::createUpdateMen($request->id,$request->rid,$request->pin,$request->mname,$request->msurname,
                                        $request->mpatronymic,$request->fired,$request->mlabel,
                                            $request->post, $request->str_ext_id, $request->can_dia,
                                                $request->str_ext_login,null);
        if ($res[0] === -1){
            return 'error 404';
        } else if ($res[0] === -2){
            return '' . $res[1];
        } else if ($res[0] > 0){
            if ($request->email != null && filter_var($request->email, FILTER_VALIDATE_EMAIL)){
                $men = Man::whereId($res[0])->first();
                $mailIsChanged = $request->old_mail != null && $request->old_mail !== $request->email;
                //die($request->old_mail . '   ' . $request->email . ' ' . ($mailIsChanged ? 'true' : 'false'));
                if ($men->web_uid == 0){
                    $user = User::where('email',$request->email)->first();
                    if ($user == null){
                        $password = $this->generate_password();
                        $user = User::create([
                            'name' => $men->msurname . ' ' . $men->mname,
                            'email' => $request->email,
                            'password' => Hash::make($password),
                        ]);
                    }else{
                        $password = '';
                        $checkMen = Man::where('web_uid',$user->id)->first();
                        if ($checkMen != null){
                            return "Пользователь с почтой " . $request->email . " уже есть.";
                        }
                    }
                    $men->web_uid = $user->id;
                    $men->update();
                    $url = CustomData::firstOrCreate(['label' => 'srv_adr'],['data' => $_SERVER['SERVER_ADDR']])->data;
                    if (strlen($url) > 0){
                        if (!str_starts_with($url, "http")){
                            $url = "http://" . $url;
                        }
                        if (!str_ends_with($url, "/")){
                            $url = $url . "/";
                        }
                        $url = $url . "login";
                        $url = '<a href="' . $url . '">' . $url . '</a>';
                    }
                    $body = __('str.mens_user_create_body');
                    $body = str_replace('LOGIN',$request->email,$body);
                    $body = strlen($password) > 0 ? str_replace('PASSWORD',$password,$body) :
                        str_replace('Пароль: «PASSWORD»<br>','',$body);
                    $body = str_replace('URL', $url, $body);
                    MailSandler::send2($request->email,__('str.mens_user_create_subject'),$body);
                } else if ($mailIsChanged){
                    $mailIsUniq = User::where('email',$request->email)->first() == null;
                    if ($mailIsUniq){
                        $user = User::where('id',$men->web_uid)->first();
                        $user->email = $request->email;
                        $user->is_confirm = false;
                        $user->update();
                        RegisterController::sendConfirmMail($user);
                        ApiLogController::addLogText('Изменена почта сотрудника ' . $men->mname . ' ' .
                            $men->msurname . ' метка: <a href="/events/0/' . $men->id . '">' . $men->mlabel .
                            '</a> id=' . $men->id . ' с ' . $request->old_mail . ' на ' . $request->email . ', обновил: ' .
                            '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . ' ' .
                            auth()->user()->name . '</a>');
                    }else{
                        return "Вы пытаетесь изменить почту, но в системе пользователь с такой почтой уже существует";
                    }
                } //else {
                    //return "ошибка пользователь с ид " . $men->web_uid . " уже существует";
                //}
            }
            return "true";
        }else{
            return "error men action";
        }
    }



    public function getAnyColValues($t,$c){
        $items = DB::table($t)->pluck($c)->toArray();
        return json_encode($items);
    }

    public function getAnyColParamValues($t,$c,$_params){
        $params = explode(",",urldecode($_params));
        $builder = DB::table($t);
        foreach ($params as $param){
            $p = explode(" ",$param);
            if (count($p) != 3) continue;
            $builder = $builder->where($p[0],$p[1],$p[2]);
        }
        $items = $builder->pluck($c)->toArray();
        return json_encode($items);
    }

    public function checkAuth(Request $request){
        return "auth ok.";
    }
}
