<?php

namespace App\Http\Controllers;

use App\Models\BreakingState;
use Carbon\Carbon;

class ApiBreakingStateController extends Controller
{
    public static function checkBreakingStatesAsyncRequest(){
        $url = "http://127.0.0.1/api/check-bs";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function checkBs(){
        if (request()->ip() == "127.0.0.1"){
            $breakingStateUpdateTime = intval(ProjectSettingsController::getSettings('bs_update_time',0)->data);
            $LOAD_TYPES_PERIOD_UPDATE = 1800;
            if ($breakingStateUpdateTime + $LOAD_TYPES_PERIOD_UPDATE < Carbon::now()->timestamp){
                BreakingState::update();
                ProjectSettingsController::getSettings('bs_update_time',0)->update(['data' => Carbon::now()->timestamp]);
            }
        }
        return "ok.";
    }
}
