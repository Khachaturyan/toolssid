<?php

namespace App\Http\Controllers;

use App\Models\BreakingState;
use App\Models\FailureType;
use Carbon\Carbon;

class BreakingStateController extends Controller
{


    public static function update(){
        $SEC_IN_DAY = 86400;
        $timeOffset = $SEC_IN_DAY * 30;
        BreakingState::where('created_at','<',Carbon::now()->addSeconds(-$timeOffset))->delete();
    }

    public static function addNewBreakingStateItem($item, $uid){
        $itm = new BreakingState();
        $itm->cid = $item->cid;
        $itm->eid = $item->id;
        $itm->mid = $uid;
        $itm->ftid = $item->ftid;
        $itm->save();
    }

    public function getFailStat($periodIndex,$sid) : string{
        $SEC_IN_DAY = 86400;
        $periodSec = $periodIndex == 4 ? $SEC_IN_DAY * 7 : $SEC_IN_DAY * 30;
        $time = Carbon::now()->addSeconds(-$periodSec);
        $builder = BreakingState::where('breaking_states.created_at','>',$time);
        if ($sid > 0){
            $builder = $builder->leftJoin("cells", "cells.id","=", "breaking_states.cid")
                ->leftJoin("blocks", "blocks.id", "=", "cells.block")
                ->where("blocks.st_id",$sid);
        }
        $items = $builder->get();
        $arr = array();
        foreach ($items as $item){
            if (!array_key_exists($item->ftid,$arr)){
                $arr[$item->ftid] = 1;
            }else{
                $arr[$item->ftid]++;
            }
        }
        $items = FailureType::where('is_archive',0)->get();
        for($i = 0; $i < count($items); $i++){
            $items[$i]->cnt = !array_key_exists($items[$i]->id,$arr) ? 0 : $arr[$items[$i]->id];
        }
        return json_encode($items);
    }
}
