<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CloudVpsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getMainDomainOfSubdomain(){
        $root = Request::root();
        if (starts_with($root, 'http://'))
        {
            $domain = substr($root, 7);
        }else if (starts_with($root, 'https://'))
        {
            $domain = substr($root, 8);
        }else{
            return "";
        }
        $arr = explode('.', $domain);
        if (count($arr) <= 2){
            return "";
        }
        if (count($arr) == 3 && intval($arr[0]) > 0){
            return "";
        }
        $dom = "";
        foreach ($arr as $part){
            if (strlen($dom) > 0) $dom .= ".";
            $dom .= $part;
        }
        return $dom;
    }

    public function test(){
        //$dom = $this->getMainDomainOfSubdomain();
        return request()->getHttpHost();
        //return $dom != null ? $dom : "no dom";
    }
}
