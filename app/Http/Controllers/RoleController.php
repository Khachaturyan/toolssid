<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public const SCLAD_MEN         = 'Сотрудник склада';
    public const BOX_MANAGER       = 'Менеджер шкафа';
    public const IT_MEN            = 'Сотрудник ИТ';
    public const ADMIN             = 'Администратор';
    public const SECURE            = 'Служба безопасности';

    public const SCLAD_MEN_INDEX   = -1;
    public const BOX_MANAGER_INDEX = -2;
    public const IT_MEN_INDEX      = -3;
    public const ADMIN_INDEX       = -4;
    public const SECURE_INDEX      = -5;

    public static function disableLock($perm_index){
        return $perm_index == self::ADMIN_INDEX || $perm_index == self::SECURE_INDEX ||
                    $perm_index == self::IT_MEN_INDEX || $perm_index == self::BOX_MANAGER_INDEX;
    }

    public static function getAdminId() {
        return Role::where('rname',self::ADMIN)->first()->id;
    }

    public static function getRoleStorekeeperId(){
        $storekeeper = Role::where('rname',self::SCLAD_MEN)->first();
        if ($storekeeper == null){
            $storekeeper = new Role();
            $storekeeper->lim_items = 1;
            $storekeeper->rname = self::SCLAD_MEN;
            $storekeeper->is_admin = false;
            $storekeeper->is_arc   = 0;
            $storekeeper->etypes = '';
            $storekeeper->perm_index = self::SCLAD_MEN_INDEX;
            $storekeeper->save();
        }
        return $storekeeper->id;
    }

    public static function changeRoleTypeAttacheState(bool $hasTypeId, $roleId, $eid) : bool {
        $role = Role::whereId($roleId)->first();
        if ($role == null){
            return false;
        }
        if ($hasTypeId){
            if (!str_contains($role->etypes, '#' . $eid . '#')){
                if (strlen($role->etypes) == 0){
                    $role->etypes = "#";
                }
                $role->etypes .= $eid . "#";
            }
        }else{
            if (strpos($role->etypes, '#' . $eid . '#') >= 0){
                $arr = explode("#",$role->etypes);
                $role->etypes = "";
                foreach ($arr as $s){
                    if (strlen($s) > 0 && $s !== $eid){
                        $role->etypes .= "#" . $s;
                    }
                }
                if (strlen($role->etypes) > 0){
                    if (!str_starts_with($role->etypes, '#')){
                        $role->etypes = "#" . $role->etypes;
                    }
                    if (!str_ends_with($role->etypes, '#')){
                        $role->etypes = $role->etypes . "#" ;
                    }
                }
            }
        }
        $role->update();
        ApiLogController::addLogText('Изменена роль: id=' . $role->id . ' (' . $role->rname .
            ')' . ', Пользователь: ' .
            '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>');
        return true;
    }

    public function attacheRoleToTypeState(Request $request){
        return self::changeRoleTypeAttacheState('true' === $request->input("state"),
            $request->input("rid"),
            $request->input("eid")) ? "true" : "Роль не найдена";
    }
}
