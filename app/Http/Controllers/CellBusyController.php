<?php

namespace App\Http\Controllers;

use App\Mail\MailSandler;
use App\Models\AlertMail;
use App\Models\Cell;
use App\Models\CellBusy;
use App\Models\Man;
use Illuminate\Http\Request;

class CellBusyController extends BaseController
{
    public function update(Request $request){
        $data = $request->all();
        CellBusy::firstOrCreate(['cid' => $request->cid],['uid' => 0 , 'state' => 0])
            ->update(['uid' => $request->uid , 'state' => $request->state]);
        if (array_key_exists('uid_to',$data) && 1 == $request->state) {
            $menTo = Man::where("men.id", $data['uid_to'])
                ->leftJoin("roles", "roles.id", "=", "men.rid")
                ->leftJoin("users", "users.id", "=", "men.web_uid")
                ->select("roles.rname", "roles.is_admin", "men.*", "users.email", "users.created_at as ucr")->first();
            //$enableSend = $menTo != null && (AlertMail::where('mail', $menTo->email)->first() == null || AlertMail::where('mail', $menTo->email)
            //            ->where('estate', 'like', '%' . ProjectSettingsController::$MAIL_ESTATE_SET_PARCELABLE_BOX . '%')
            //            ->first() != null);
            $enableSend = $menTo != null;
            $cell = Cell::where('cells.id',$request->cid)
                ->leftJoin("blocks", "blocks.id","=", "cells.block")
                ->leftJoin("stages", "stages.id","=", "blocks.st_id")
                ->select("blocks.name as bname","cells.*","stages.name as sname")->first();
            if ($enableSend && $cell != null) {
                $sbj = 'Для Вас оставлено отправление в системе TOOLSiD';
                $str = $sbj . ' Объект: ' . $cell->sname . ', Блок:' . $cell->bname . ' Ячейка:' . $cell->block_pos;
                MailSandler::send2($menTo->email,$sbj,$str);
            }
        }
        return $this->sendResponse(true, 'cell state update successfully.');
    }
}
