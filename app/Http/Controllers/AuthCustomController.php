<?php

namespace App\Http\Controllers;

use App\Mail\MailSandler;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthCustomController extends Controller
{
    public function passEmail(Request $request){
        $user = User::where('email',$request->email)->first();
        if ($user == null){
            return __('str.user_with_email_not_found');
        }
        $host = substr(url()->current(), 0, strrpos(url()->current(), "/"));
        $link = $host . '/pass-reset/' . md5($user->email . $user->updated_at);
        $res = MailSandler::send2($request->email,__('str.restore_pass_subj'),__('str.change_pass_link') . ' ' . $link);
        return $res ? __('str.email_with_reset_link_sanded') : __('str.email_with_reset_link_error');
    }

    public function passResetAction(Request $request){
        if ($request->password !== $request->password_confirmation){
            return __('str.pass_confirm_error');
        }
        $user = User::where('email',$request->email)->first();
        if ($user == null){
            return __('str.user_with_email_not_found');
        }
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->route("home");
    }

    public function passReset($pass){
        $users = User::all();
        $usr = null;
        foreach ($users as $user){
            if (md5($user->email . $user->updated_at) === $pass){
                $usr = $user;
                break;
            }
        }
        if ($usr == null){
            return __('str.user_not_found');
        }
        return view('auth.pass-reset',['email' => $user->email]);
    }
}
