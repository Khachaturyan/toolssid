<?php

namespace App\Http\Controllers;

use App\Models\LogTakeItem;
use Carbon\Carbon;

class LogTakeItemController extends Controller
{
    public static function clearItemsAsyncRequest(){
        $url = "http://127.0.0.1/api/clear-log-take-item";
        $cmd = "curl -i -X GET $url    --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function clearItems(){
        if (request()->ip() == "127.0.0.1"){
            $SEC_IN_DAY = 86400;
            $timeOffset = $SEC_IN_DAY * 30;
            LogTakeItem::where('created_at','<',Carbon::now()->addSeconds(-$timeOffset))->delete();
        }
        return "ok.";
    }
}
