<?php

namespace App\Http\Controllers;

use App\Exports\ActOutputExport;
use App\Models\Act;
use App\Models\ActItem;
use App\Models\CustomData;
use App\Models\Equipment;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class ActController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('acts');
    }

    //------------------------------------------------------------------------------------------------------------------

    private static function createEmptyAct() : int{
        //$SEC_IN_DAY = 86400;
        $emptyActs = Act::where("state",0)
            //->where("created_at","<",date("Y-m-d H:i:s", time() - $SEC_IN_DAY))
            ->get();
        foreach ($emptyActs as $act){
            ActItem::where("aid",$act->id)->delete();
            $act->delete();
        }
        $act = new Act(["num" => 0]);
        $act->save();
        return $act->id;
    }

    private static function changeActItem($aid,$eid){
        $item = ActItem::where("aid",$aid)->where("eid",$eid)->first();
        if ($item == null){
            $item = new ActItem(["aid" => $aid, "eid" => $eid, "compl" => "", "des" => "", "state" => 0]);
            $item->save();
        }else{
            $item->delete();
        }
    }

    private static function updateActItemNum($aid){
        $act = Act::whereId($aid)->first();
        if ($act != null){
            $act->num = Act::max('num') + 1;
            $act->state = 1;
            $act->update();
        }
        return $act;
    }

    //------------------------------------------------------------------------------------------------------------------

    public function create(){
        return "true" . self::createEmptyAct();
    }

    public function change($aid,$eid){
        self::changeActItem($aid,$eid);
        return "true";
    }

    public function save($aid){
        self::updateActItemNum($aid);
        return "true";
    }

    public function list(){
        $query = Act::query()->where("num",'>', 0);
        return Datatables::of($query)
            ->addColumn('count', function ($row){
                return ActItem::where("aid",$row->id)->count();
            })
            ->addColumn('export', function ($row){
                return '<a href="/act-xlsx-out/' . $row->id . '" target="_blank"><img src="/img/icons8-export-excel-48.png" style="width : 48px; height: 48px; padding: 6px"/></a>';
            })
            ->editColumn('created_at', function ($row){
                return ProjectSettingsController::toLocalTime($row->created_at);
            })
            ->rawColumns(['export'])
            ->make(true);
    }

    public function equList($aid){
        $query = Equipment::query()
            ->whereIn("equipment.estat",[ApiEquipmentController::$NOT_WORKING_INDEX, ApiEquipmentController::$REPAIR_INDEX])
            ->where('equipment.archive','=',false)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin("failure_types", "failure_types.id","=", "equipment.ftid")
            ->select("etypes.tname","etypes.tmark","etypes.tmodel", "equipment.*","failure_types.name as ft_name");
        return Datatables::of($query)
            ->addColumn('is_checked', function ($row) use ($aid){
                $isChecked = ActItem::where("aid",$aid)->where("eid",$row->id)->first() != null;
                return $isChecked ? '<img src="/img/done_green_24dp.svg" style="width : 24px; height: 24px; padding: 6px"/>' : "";
            })
            ->rawColumns(['is_checked'])
            ->make(true);
    }

    public function selectAll($aid){
        $items = Equipment::query()
            ->where("equipment.estat",ApiEquipmentController::$REPAIR_INDEX)
            ->where('equipment.archive','=',false)
            ->get();
        foreach ($items as $item){
            $isChecked = ActItem::where("aid",$aid)->where("eid",$item->id)->first() != null;
            if (!$isChecked){
                $item = new ActItem(["aid" => $aid, "eid" => $item->id, "compl" => "", "des" => "", "state" => 0]);
                $item->save();
            }
        }
        return "true";
    }

    public function unselectAll($aid){
        ActItem::where("aid",$aid)->delete();
        return "true";
    }

    public function reportRapairActIn(){
        return "ok";
    }

    public static function createActFileForMailReport($eid){
        $aid = self::createEmptyAct();
        self::changeActItem($aid,$eid);
        $act = self::updateActItemNum($aid);
        $items = ActItem::where('act_items.aid',$aid)
            ->leftJoin("equipment", "equipment.id","=", "act_items.eid")
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin("failure_types", "failure_types.id","=", "equipment.ftid")
            ->select("etypes.tname","etypes.tmark","etypes.tmodel", "equipment.*","failure_types.name as ft_name","act_items.compl")
            ->get();
        Excel::store(new ActOutputExport($items,$act->num,
            ProjectSettingsController::toLocalTime($act->created_at),
            CustomData::firstOrCreate(['label' => 'org_yr_name'],['data' => ''])->data,
            CustomData::firstOrCreate(['label' => 'org_yr_adr' ],['data' => ''])->data,
            CustomData::firstOrCreate(['label' => 'org_yr_inn' ],['data' => ''])->data,
            CustomData::firstOrCreate(['label' => 'org_yr_kpp' ],['data' => ''])->data),
            '/public/files/act.xlsx');
    }

    public function reportRapairActOut($aid){
        $act = Act::whereId($aid)->first();
        $items = ActItem::where('act_items.aid',$aid)
            ->leftJoin("equipment", "equipment.id","=", "act_items.eid")
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin("failure_types", "failure_types.id","=", "equipment.ftid")
            ->select("etypes.tname","etypes.tmark","etypes.tmodel", "equipment.*","failure_types.name as ft_name","act_items.compl")
            ->get();
        //return json_encode($items);
        return Excel::download(new ActOutputExport($items,$act->num,
            ProjectSettingsController::toLocalTime($act->created_at),
            CustomData::firstOrCreate(['label' => 'org_yr_name'],['data' => ''])->data,
            CustomData::firstOrCreate(['label' => 'org_yr_adr' ],['data' => ''])->data,
            CustomData::firstOrCreate(['label' => 'org_yr_inn' ],['data' => ''])->data,
            CustomData::firstOrCreate(['label' => 'org_yr_kpp' ],['data' => ''])->data), 'act.xlsx');
    }

}
