<?php

namespace App\Http\Controllers;

use App\Models\CustomData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UseManualController extends Controller
{

    public function show()
    {
        $manual = CustomData::where("label","=","android-manual")->first();
        $manualData = $manual == null ? "" : $manual->data;
        return view('manual',["manual" => $manualData, "isAuth" => Auth::check()]);
    }

    public function saveManual(Request $request){
        $request->validate([
            "manual-android" => "required|min:1"
        ]);
        $manual = CustomData::where("label","=","android-manual")->first();
        if ($manual == null){
            $manual = new CustomData();
            $manual->label = "android-manual";
            $manual->data  = $request->input("manual-android");
            $manual->save();
        }else{
            $manual->update(["data" => $request->input("manual-android")]);
        }
        return redirect()->route("manual");
    }
}
