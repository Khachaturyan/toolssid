<?php

namespace App\Http\Controllers;

use App\Models\CustomData;
use Illuminate\Http\Request;

class ApiProjectSettingsController extends BaseController
{
    public function data(){
        return $this->sendResponse(CustomData::all(), 'custom data retrieved successfully.',array('time' => time()));
    }
}
