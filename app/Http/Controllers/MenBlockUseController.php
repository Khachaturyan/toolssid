<?php

namespace App\Http\Controllers;


use App\Models\MenBlockUse;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MenBlockUseController extends Controller
{
    public static function isBind($mid) : bool {
        $SEC_IN_5MIN = 300;
        return MenBlockUse::where('mid',$mid)
                ->where('created_at','>',Carbon::now()->addSeconds(-$SEC_IN_5MIN))
                ->first() != null;
    }

    public function setState(Request $request){
        if ($request->state != 1){
            MenBlockUse::where('mid',$request->mid)->where('bid',$request->bid)->delete();
        }else{
            MenBlockUse::firstOrCreate(['mid' => $request->mid, 'bid' => $request->bid],['state' => 1])->update(['state' => 1]);
        }
        return 'true';
    }
}
