<?php

namespace App\Http\Controllers;

use App\Models\CustomData;
use App\Models\Equipment;
use App\Models\EquRate;
use App\Models\LogTakeItem;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EquRateController extends Controller
{
    public static function checkForUpdate(){
        $customData = CustomData::firstOrCreate(['label' => 'equ_rate_update_time'],['data' => 0]);
        $lastUpdate = intval($customData->data);
        $PERIOD_UPDATE = 900;
        $currentTime = Carbon::now()->timestamp;
        if ($lastUpdate + $PERIOD_UPDATE < $currentTime){
            $url = "http://127.0.0.1/api/clear-log-take-item";
            $cmd = "curl -i -X GET $url    --insecure  > /dev/null 2>&1 &";
            exec($cmd, $output, $exit);
            $customData->update(['data' => $currentTime]);
        }
    }

    public function updateEquRate(){
        if (request()->ip() == "127.0.0.1"){
            EquRate::truncate();
            $eIds = LogTakeItem::where('eid','>',0)->select('eid')->distinct()->pluck('eid');
            $eIds2 = Equipment::where('cid','>',0)->whereNotIn('id',$eIds)->select('id')->distinct()->pluck('id');
            $arr = array();
            $sum = 0;
            foreach ($eIds as $eid){
                $count = LogTakeItem::where('eid',$eid)->count();
                $arr[$eid] = $count;
                $sum += $count;
            }
            foreach ($eIds2 as $eid){
                $count = 0;
                if (!array_key_exists($eid,$arr)) {
                    $arr[$eid] = $count;
                }
            }
            $sra = ($sum / (count($eIds) + count($eIds2)));
            //die(json_encode(array($sra,$arr)));
            foreach ($arr as $key => $value){
                if ($value <= $sra){
                    EquRate::create(['eid' => $key, 'rate' => $value])->save();
                }
            }
        }
        return "ok.";
    }
}
