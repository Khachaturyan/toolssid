<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

class ApiTypeLoadController extends Controller
{
    public static function checkTypeLoadAsyncRequest(){
        $url = "http://127.0.0.1/api/check-type-load";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function checkTypeLoad(){
        if (request()->ip() == "127.0.0.1"){
            $loadTypeUpdateTime = intval(ProjectSettingsController::getSettings('lt_update_time',0)->data);
            $LOAD_TYPES_PERIOD_UPDATE = 300;
            if ($loadTypeUpdateTime + $LOAD_TYPES_PERIOD_UPDATE < Carbon::now()->timestamp){
                TypeLoadController::update();
                ProjectSettingsController::getSettings('lt_update_time',0)->update(['data' => Carbon::now()->timestamp]);
            }
        }
        return "ok.";
    }
}
