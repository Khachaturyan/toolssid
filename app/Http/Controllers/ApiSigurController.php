<?php

namespace App\Http\Controllers;

use App\Models\CustomData;
use App\Models\MenLock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ApiSigurController extends Controller
{

    private static $LIMIT = 100;

    public static function checkSigurAsyncRequest(){
        $url = "http://127.0.0.1/api/check-sigur";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function checkSigur(){
        if (request()->ip() == "127.0.0.1"){
            $sigurSyncEnable2 = intval(CustomData::firstOrCreate(['label' => 'sigur_sync_en2' ],['data' => 0])->data) == 1;
            if ($sigurSyncEnable2){
                $res = self::updateServerData();
                ApiLogController::addLogText("sigur update data =" . $res);
            }
            $sigur_sync_employees = intval(CustomData::firstOrCreate(['label' => 'sigur_sync_employees' ],['data' => 0])->data) == 1;
            if ($sigur_sync_employees){
                $last_update_sigur_employees_time = intval(ProjectSettingsController::getSettings('last_update_sigur_employees_time',0)->data);
                $SIGUR_PERIOD_UPDATE = 900;
                if ($last_update_sigur_employees_time + $SIGUR_PERIOD_UPDATE < Carbon::now()->timestamp){
                    ProjectSettingsController::getSettings('last_update_sigur_employees_time',0)->update(['data' => Carbon::now()->timestamp]);
                }
            }
        }
        return "ok.";
    }

    private static function putReq($req, $timeOut, $headers){
        $ch = curl_init($req);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeOut);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeOut); //timeout in seconds
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array, '', '&'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $httpcode;
    }

    private static function getReq($req, $timeOut, $headers) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$req);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeOut);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeOut); //timeout in seconds
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        $sOutput = curl_exec($ch);
        curl_close($ch);
        return $sOutput;
    }

    private static function postReq($url, $data, $header){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $sOutput = curl_exec($ch);
        curl_close($ch);
        return $sOutput;
    }

//--- api sigur --------------------------------------------------------------------------------------------------------

    private static function getSigurHost(){
        $host = CustomData::firstOrCreate(['label' => 'sigur_host'],['data' => ''])->data;
        if (strlen($host) == 0){
            return null;
        }
        if (!str_starts_with($host,'http://')){
            $host = 'http://' . $host;
        }
        if (!str_ends_with($host,'/')){
            $host = $host . '/';
        }
        return $host;
    }

    private static function getApiSigurHeader() : array{
        $header = 'Authorization: Bearer ' .
            CustomData::firstOrCreate(['label' => 'sigur_jwt_token'],['data' => ''])->data;
        return array($header);
    }

    public function getJwtToken() : string{
        $host = self::getSigurHost();
        $pas = CustomData::firstOrCreate(['label' => 'sigur_api_pas'  ],['data' => ''])->data;
        $sys = CustomData::firstOrCreate(['label' => 'sigur_sys_int'  ],['data' => ''])->data;
        if ($host == null || strlen($pas) == 0 || strlen($sys) == 0){
            return '';
        }
        $host = $host . 'api/v1/application-keys/auth';
        $resp = self::postReq($host,array('system' => $sys,'password' => $pas),array('Content-Type: application/json'));
        return $resp != null ? str_replace('"', '', $resp) : '';
    }

    //------------------------------------------------------------------------------------------------------------------

    private static function getSigurLockReq($id, bool $isLock){
        $req = self::getSigurHost();
        if ($req == null){
            return null;
        }
        return $req . "api/v1/employees/$id/" . ($isLock ? "block" : "unblock");
    }

    private function changeLockStatus($id, bool $isLock) : bool{
        $req = self::getSigurLockReq($id,$isLock);
        $header = self::getApiSigurHeader();
        if ($req == null){
            return false;
        }
        $code = self::putReq($req,60,$header);
        return 204 == $code;
    }

    //------------------------------------------------------------------------------------------------------------------

    private static function getListCardReq($skip){
        $req = self::getSigurHost();
        if ($req == null){
            return null;
        }
        return $req . "api/v1/cards?includeFields=formattedValue,holder&offset=" . $skip . "&limit=" . self::$LIMIT;
    }

    public function getListCards(array $header){
        $skip = 0;
        $req = self::getListCardReq($skip);
        if ($req == null){
            return null;
        }
        $resp = self::getReq($req,60,$header);
        $exit = false;
        $items = array();
        while ($resp != null && !$exit){
            $arr = json_decode($resp, true);
            foreach ($arr as $item){
                $uid = $item['holder']['holderId'];
                $items[$uid] = $item['formattedValue'];
            }
            $exit = count($arr) < self::$LIMIT;
            if (!$exit){
                $skip += count($arr);
                $resp = self::getReq(self::getListCardReq($skip),60,$header);
            }
        }
        return $items;
    }

    //------------------------------------------------------------------------------------------------------------------

    private static function getListEmployeesReq($skip){
        $req = self::getSigurHost();
        if ($req == null){
            return null;
        }
        return $req . "api/v1/employees?includeFields=id,name,isBlocked,tabId&offset=" . $skip . "&limit=" . self::$LIMIT;
    }

    public function getListEmployees(){
        $skip = 0;
        $req = self::getListEmployeesReq($skip);
        if ($req == null){
            return 'false';
        }
        $header = self::getApiSigurHeader();
        $cards = $this->getListCards($header);
        if ($cards == null){
            return 'false';
        }
        $resp = self::getReq($req,60,$header);
        $exit = false;
        $items = array();
        while ($resp != null && !$exit){
            $arr = json_decode($resp, true);
            foreach ($arr as $item){
                $item['card'] = $cards[$item['id']];
                $items[] = $item;
            }
            $exit = count($arr) < self::$LIMIT;
            if (!$exit){
                $skip += count($arr);
                $resp = self::getReq(self::getListEmployeesReq($skip),60,$header);
            }
        }
        $resp = json_encode($items);
        Storage::put('public/employees.txt', $resp);
        return $resp != null ? $resp : 'false';
    }

//--- jSigur -----------------------------------------------------------------------------------------------------------

    public static function staticStartServer(){
        $resp = self::getReq("http://127.0.0.1:5961/api",1, array());
        if ("true" != $resp){
            if ($resp != null){
                return "false";
            }
            exec('bash -c "exec nohup setsid java -jar java/jsigur.jar > /dev/null 2>&1 &"');
            sleep(1);
            return "true" == self::getReq("http://127.0.0.1:5961/api",1, array()) ? "true" : "false";
        }
        return "true";
    }

    public static function staticStopServer(){
        $resp = self::getReq("http://127.0.0.1:5961/api",1, array());
        if ("true" == $resp){
            return "ok." == self::getReq("http://127.0.0.1:5961/api?kill=now",1, array()) ? "true" : "false";
        }
        return $resp == null ? "true" : "false";
    }

    public static function updateServerData(){
        return self::postReq("http://127.0.0.1:5961/api",array('com' => 1, 'data' => MenLock::all()),array());
    }

    public function startServer() {
        return self::staticStartServer();
    }

    public function stopServer() {
        return self::staticStopServer();
    }

    public function checkServer(){
        return "true" == self::getReq("http://127.0.0.1:5961/api",1, array()) ? "true" : "false";
    }

//----------------------------------------------------------------------------------------------------------------------

    public function delegation(Request $request){
        $logEnable = intval(CustomData::firstOrCreate(['label' => 'sigur_log_en'],['data' => 0 ])->data) == 1;
        if ($logEnable){
            ApiLogController::addLogText("Запрос делегации от SIGUR type=" . $request->type . " keyHex=" .
                $request->keyHex . " direction=" . $request->direction . " accessPoint=" . $request->accessPoint);
        }
        $res = array();
        if ($request->type == null || $request->keyHex == null ||
            $request->direction == null || $request->accessPoint == null){
            $res["allow"  ] = false;
            $res["message"] = "Доступ запрещён так как отсутствует одно из полей: type,keyHex,direction,accessPoint";
            return json_encode($res);
        }
        $syncEnable = intval(CustomData::firstOrCreate(['label' => 'sigur_sync_en'],['data' => 0 ])->data) == 1;
        if (!$syncEnable){
            $res["allow"  ] = false;
            $res["message"] = "Доступ запрещён так как в настройках не включена синхронизация с SIGUR";
            return json_encode($res);
        }
        $sigurMode = intval(CustomData::firstOrCreate(['label' => 'sigur_mode'],['data' => 0 ])->data);
        $lockAll = $sigurMode == 2;
        if ($lockAll){
            $res["allow"  ] = false;
        } else {
            $lockNone = $sigurMode == 1;
            if ($lockNone){
                $res["allow"  ] = true;
            } else {
                $lockItem = MenLock::where('label',$request->keyHex)->first();
                $res["allow"  ] = $lockItem == null || !$lockItem->lock;
            }
        }
        return json_encode($res);
    }

    public function events(Request $request){
        $res = array();
        $res["confirmedLogId"] = $request->logs == null ? 0 : ApiLogController::addLogText("Запрс проходов от SIGUR=" . urlencode(json_encode($request->logs)));
        return json_encode($res);
    }
}

/*
 *
6CAF5B
1BAB47
537A76
C8FE87
67DA6F
C8BE37
5301EF
5301EF
53257F
5301EF
6D44A8
6804B7
1A6E20
9D5131
677D4E
9D64E1
679A54
7F3482
1632C8
53009A
7E30C7
1632C8
2782BE
 */


