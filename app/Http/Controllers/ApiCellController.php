<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\CellColor;
use App\Models\CellSize;
use App\Models\EquFavorite;
use App\Models\Equipment;
use App\Models\EquMenRigidLigament;
use App\Models\Man;
use App\Models\MenCellLink;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Cell;
use DB;

class ApiCellController extends BaseController
{

    public static function staticCellById($id){
        return Cell::where('cells.id',$id)
            ->leftJoin('blocks','blocks.id','=','cells.block')
            ->select('cells.*','blocks.name')->first();
    }

    public function cellById($id){
        return $this->sendResponse(self::staticCellById($id), 'cell load successfully.');
    }

    public function cellMen($bid,$uid){
        $men = DB::table('men')->where('men.id', $uid)
            ->leftJoin("roles", "roles.id", "=", "men.rid")
            ->select("roles.etypes","roles.is_admin","roles.rname","men.*")->first();
        if ($men == null){
            return $this->sendResponse(false, 'Men not found.');
        }
        $etypes = explode(" ",trim(str_replace("#", " ", $men->etypes), " "));
        $cellsArr = Cell::where("block",$bid)->where("is_lock",'!=',true)->get();
        $ids   = array();
        $cells = array();
        for ($i = 0; $i < count($cellsArr); $i++){
            $ids[] = $cellsArr[$i]->id;
            $cells[$cellsArr[$i]->id] = $cellsArr[$i];
        }
        $eqsArr = DB::table('equipment')->whereIn('equipment.cid', $ids)
            ->whereIn('equipment.tid', $etypes)->where("equipment.uid",0)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->select("etypes.tname","etypes.csid","etypes.img","etypes.tmark","etypes.tmodel","etypes.is_cons","equipment.*")->get();
        $cellsOut = array();
        for ($i = 0; $i < count($eqsArr); $i++){
            if (!array_key_exists($eqsArr[$i]->tid, $cellsOut)){
                $cell = $cells[$eqsArr[$i]->cid];
                $cell['equ'] = $eqsArr[$i];
                $cellsOut[$eqsArr[$i]->tid] = $cell;
            }
        }
        return $this->sendResponse(array_values($cellsOut), 'cell update successfully.');
    }

    public function cellByEtype($bid,$tid){
        $cellsArr = Cell::where("block",$bid)->where("is_lock",'=',false)->get();
        $ids   = array();
        $cells = array();
        for ($i = 0; $i < count($cellsArr); $i++){
            $ids[] = $cellsArr[$i]->id;
            $cells[$cellsArr[$i]->id] = $cellsArr[$i];
        }
        $eqsArr = DB::table('equipment')
            ->whereIn('equipment.cid', $ids)
            ->where("equipment.uid",0)
            ->where("equipment.tid",$tid)
            ->where("equipment.is_work",true)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->select("etypes.tname","etypes.csid","etypes.img","etypes.tmark","etypes.tmodel","etypes.is_cons","equipment.*")->get();
        $cellsOut = array();
        for ($i = 0; $i < count($eqsArr); $i++){
            $cell = $cells[$eqsArr[$i]->cid];
            $cell['equ'] = $eqsArr[$i];
            $cellsOut[] = $cell;
        }
        return $this->sendResponse($cellsOut, 'cell load successfully.');
    }

    public function infoArray($bid){
        $info = array();
        $info['lock'] = count(JsonResource::collection(Cell::where("block",$bid)->where("is_lock",'=',true)->get()));
        $info['free'] = count($this->freeItems($bid,false));
        $info['busy'] = count($this->busyItems($bid));
        $info['notWorking'] = count($this->notWorkingItems($bid));
        return $info;
    }

    public function info($bid){
        return $this->sendResponse($this->infoArray($bid), 'Info retrieved successfully.');
    }

    public function showAll($bid){
        return $this->show($bid,false);
    }

    public function show($bid,$withoutLock = true)
    {
        // обновляем данные блока
        $headers = getallheaders();
        if (array_key_exists('ver',$headers)){
            Block::whereId($bid)->update(['last_update' => time(), 'android_ver' => $headers['ver']]);
        }
        // получаем ячейки
        $builder = Cell::where("cells.block",$bid)->leftJoin('cell_busies','cells.id','=','cell_busies.cid');
        if ($withoutLock){
            $builder = $builder->where("cells.is_lock", '!=', true);
        }
        $cells = $builder->select("cells.*","cell_busies.state as cb_state")->get();
        // получаем айдишники ячеек
        $ids = array();
        for ($i = 0; $i < count($cells); $i++){
            $ids[] = $cells[$i]->id;
        }
        // получаем всё оборудование по айдишникам ячеек
        $eqsArr = DB::table('equipment')->whereIn('equipment.cid', $ids)->where('equipment.comp_id',0)
                ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                ->select("etypes.tname","etypes.csid","etypes.img","etypes.tmark",
                            "etypes.tmodel","etypes.is_cons","equipment.*")->get();

        $productsArr = Product::whereIn('cid',$ids)->get();
        $products = array();
        foreach ($productsArr as $p){
            $products[$p->cid] = $p;
        }

        $ids = array(); // массив айдиников людей на которых числится оборудование
        $eqs = array(); // массив "ид ячейки" => оборудование
        for ($i = 0; $i < count($eqsArr); $i++){
            if ($eqsArr[$i]->uid > 0  && !in_array($eqsArr[$i]->uid, $ids)){
                $ids[] = $eqsArr[$i]->uid;
            }
            if (array_key_exists($eqsArr[$i]->cid,$eqs)){
                $equ = $eqs[$eqsArr[$i]->cid];
                $equ->count++;
            }else{
                $equ = $eqsArr[$i];
                $equ->count = 1;
                $eqs[$eqsArr[$i]->cid] = $equ;
            }
        }
        //--- добавляем связанных с ячейкой сотрудников ----------------------------------------------------------------
        for ($i = 0; $i < count($cells); $i++){
            $cells[$i]->men_cell_links = MenCellLink::where('men_cell_links.cid',$cells[$i]->id)
                ->leftJoin("men", "men.id", "=", "men_cell_links.mid")
                ->leftJoin("roles", "roles.id", "=", "men.rid")
                ->where("men.fired","!=",1)
                ->select("roles.etypes","roles.is_admin","roles.rname","men.*")->get();
        }
        // выборка людей по массиву ид
        $mensArr = DB::table('men')->whereIn('men.id', $ids)
            ->leftJoin("roles", "roles.id", "=", "men.rid")
            ->select("roles.etypes","roles.is_admin","roles.rname","men.*")->get();
        $mens = array();
        for ($i = 0; $i < count($mensArr); $i++){
            $mens[$mensArr[$i]->id] = $mensArr[$i];
        }
        for ($i = 0; $i < count($cells); $i++){
            if (array_key_exists($cells[$i]->id, $eqs)){
                $equ = $eqs[$cells[$i]->id];
                $ef = EquFavorite::where('eid',$equ->id)->first();
                $equ->fid = $ef != null ? $ef->uid : 0; // ид человека у которого девайс в фаворе если не в фаворе то - 0
                $er = EquMenRigidLigament::where('eid',$equ->id)->get();
                $rid = array(); // ид сотрудников с которыми устройство жёстко связано
                for ($j = 0; $j < count($er); $j++){
                    $rid[] = $er[$j]->uid;
                }
                $equ->rid = $rid;
                $equ->estat_color = ApiEquipmentController::getStatColor($equ->estat);
                $cells[$i]['equ'] = $equ;
                if (array_key_exists($equ->uid, $mens)){
                    $cells[$i]['men'] = $mens[$equ->uid];
                }
            }
            if (array_key_exists($cells[$i]->id, $products)){
                $cells[$i]['product'] = $products[$cells[$i]->id];
            }
        }
        return $this->sendResponse(JsonResource::collection($cells), 'Cells retrieved successfully.');
    }

    public function notFree($bid)
    {
        $equipment = DB::table('equipment')->where('cid', '>', '0')->where('uid', '>', '0')->get();
        if (count($equipment) == 0){
            return $this->sendResponse(JsonResource::collection(array()), 'Cells retrieved successfully.');
        }
        $ids = array();
        foreach ($equipment as $equ){
            $ids[] = $equ->cid;
        }
        $cells = Cell::where("block",$bid)->whereIn('id', $ids)->get();
        return $this->sendResponse(JsonResource::collection($cells), 'Cells retrieved successfully.');
    }

    private function notWorkingItems($bid){
        $eqsArr = DB::table('equipment')->where('equipment.cid', '>', '0')
            ->where('cells.block', $bid)
            ->where('equipment.is_work', false)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin("cells", "cells.id","=", "equipment.cid")
            ->leftJoin("failure_types", "failure_types.id","=", "equipment.ftid")
            ->select("etypes.tname","etypes.csid","etypes.img","etypes.tmark","etypes.tmodel","etypes.is_cons",
                "equipment.*","cells.block","failure_types.name as ft_name")->get();
        $ids = array();
        $eqs = array();
        for ($i = 0; $i < count($eqsArr); $i++){
            $eqs[$eqsArr[$i]->cid] = $eqsArr[$i];
            $ids[] = $eqsArr[$i]->cid;
        }
        $cellsArr = Cell::whereIn("id",$ids)->where("is_lock",'!=',true)->get();
        for ($i = 0; $i < count($cellsArr); $i++){
            $cellsArr[$i]['equ'] = $eqs[$cellsArr[$i]->id];
        }
        return JsonResource::collection($cellsArr);
    }

    public function notWorking($bid){
        return $this->sendResponse($this->notWorkingItems($bid) , 'Cells retrieved successfully.');
    }

    private function freeItems($bid,$withOwnedInfo){
        $cellsArr = Cell::where("block",$bid)->where("is_lock",'!=',true)->get();
        $ids   = array();
        $cells = array();
        for ($i = 0; $i < count($cellsArr); $i++){
            $cells[$cellsArr[$i]->id] = $cellsArr[$i];
            $ids[] = $cellsArr[$i]->id;
        }
        $equipments = DB::table('equipment')->whereIn('cid', $ids)->where('uid',0)->get()->toArray();
        $equOwned = $withOwnedInfo ? Equipment::whereIn('equipment.ocid', $ids)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->select("etypes.tname","etypes.tmark","etypes.tmodel", "etypes.is_cons", "equipment.*")->get() : null;
        foreach ($equipments as $equ){
            unset($cells[$equ->cid]);
        }
        if ($equOwned != null){
            foreach ($equOwned as $equ){
                if (array_key_exists($equ->ocid,$cells)){
                    $cells[$equ->ocid]->equ = $equ;
                    $cells[$equ->ocid]->men = Man::whereId($equ->uid)->first();
                }
            }
        }
        return JsonResource::collection($cells);
    }

    public function free($bid)
    {
        return $this->sendResponse($this->freeItems($bid,false), 'Cells retrieved successfully.');
    }

    public function freeOwned($bid)
    {
        return $this->sendResponse($this->freeItems($bid,true), 'Cells retrieved successfully.');
    }

    private function busyItems($bid){
        $eqs = DB::table('equipment')->where('equipment.cid', '>', '0')
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->select("etypes.tname","etypes.csid","etypes.img","etypes.tmark","etypes.tmodel","etypes.is_cons","equipment.*")->get();
        if (count($eqs) == 0){
            return array();
        }
        $idsCell = array();
        $idsMen  = array();
        $eqsArr  = array();
        for ($i = 0; $i < count($eqs); $i++){
            $idsCell[] = $eqs[$i]->cid;
            $eqsArr[$eqs[$i]->cid] = $eqs[$i];
            if ($eqs[$i]->uid > 0){
                $idsMen[] = $eqs[$i]->uid;
            }
        }
        $mensArr = count($idsMen) == 0 ? array() : DB::table('men')->whereIn('men.id', $idsMen)
            ->leftJoin("roles", "roles.id", "=", "men.rid")
            ->select("roles.etypes","roles.is_admin","roles.rname","men.*")->get();
        $mens = array();
        for ($i = 0; $i < count($mensArr); $i++){
            $mens[$mensArr[$i]->id] = $mensArr[$i];
        }
        $cells = JsonResource::collection(Cell::where("block",$bid)->where("is_lock",'!=',true)->whereIn('id', $idsCell)->get());
        $cellsBusy = array();
        for ($i = 0; $i < count($cells); $i++){
            $equ = $eqsArr[$cells[$i]['id']];
            $cells[$i]['equ'] = $equ;
            if ($equ->uid > 0 && array_key_exists($equ->uid,$mens)) {
                $cells[$i]['men'] = $mens[$equ->uid];
            }else{
                $cells[$i]['men'] = null;
            }
            if ($cells[$i]['men'] == null){
                $cellsBusy[] = $cells[$i];
            }
        }
        return $cellsBusy;
    }

    public function busy($bid)
    {
        return $this->sendResponse($this->busyItems($bid), 'Cells retrieved successfully.');
    }

    public function sizes()
    {
        return $this->sendResponse(CellSize::all(), 'cell sizes retrieved successfully.');
    }

    public function update(Request $request){
        Cell::whereId($request->id)->update($request->all());
        return $this->sendResponse(true, 'cell update successfully.');
    }

    public function updateCellColors(Request $request){
        foreach ($request->data as $cid => $color){
            CellColor::firstOrCreate(['cid' => intval($cid)],['col' => $color])->update(['col' => $color]);
        }
        return $this->sendResponse(true, 'cell colors update successfully.');
    }

    public function setState(Request $request){
        Cell::whereId($request->id)->update(array('dst' => $request->dst));
        return $this->sendResponse(true, 'cell update successfully.');
    }

    public function setLock(Request $request){
        Cell::whereId($request->id)->update(array('is_lock' => $request->is_lock));
        return $this->sendResponse(true, 'cell update successfully.');
    }

    public function cellEdit($cid){
        return abort(404);
    }

    public function cellEditAction(Request $request){
        Cell::whereId($request->id)->update(array('csid'    => $request->input("csid") != null ? $request->input("csid") : 0,
                                                  'des'     => $request->input("des"),
                                                  'dst'     => $request->dst != null ? $request->dst : 0,
                                                  'is_lock' => $request->input("is_lock")));
        return "true";
    }
}
