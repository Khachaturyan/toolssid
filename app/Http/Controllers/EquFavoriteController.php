<?php

namespace App\Http\Controllers;

use App\Models\CustomData;
use App\Models\EquFavorite;
use App\Models\Equipment;
use App\SapEwmDm\SapEwmDmHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;

// функционал для временно сданного оборудования

class EquFavoriteController extends Controller
{
    public static function checkFavoriteAsyncRequest(){
        $url = "http://127.0.0.1/api/check-favorite";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function checkFavorite(){
        if (request()->ip() == "127.0.0.1"){
            $maxRetTimeSet = CustomData::where('label', 'max-ret-time')->first();
            $maxRetTime = $maxRetTimeSet != null ? $maxRetTimeSet->data : 0;
            if ($maxRetTime > 0){
                $items = EquFavorite::where('equ_favorites.updated_at','<', Carbon::now()->addSeconds(-$maxRetTime))
                    ->leftJoin("men", "men.id","=", "equ_favorites.uid")
                    ->leftJoin("equipment", "equipment.id","=", "equ_favorites.eid")
                    ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                    ->leftJoin("cells", "cells.id","=", "equipment.cid")
                    ->leftJoin("blocks", "blocks.id","=", "cells.block")
                    ->select("equipment.cid","equipment.tid","equipment.label","equipment.ser_num","equipment.inv_num",
                        "equipment.ocid","equipment.estat_des","equipment.estat","etypes.tname","etypes.tmark",
                        "etypes.tmodel","equ_favorites.*", "blocks.st_id","men.mlabel","men.str_ext_login")
                    ->get();
                foreach ($items as $item){
                    $equ = Equipment::whereId($item->eid)->first();
                    if ($equ == null) continue;
                    $estat_old = $equ->estat;
                    $estat_des_old = ApiEquipmentController::getStatDescription($estat_old);
                    $estat_color_old = ApiEquipmentController::getStatColor($estat_old);
                    $item->delete();
                    $estat = ApiEquipmentController::getEstatFromEquipment($equ);
                    $estat_color = ApiEquipmentController::getStatColor($estat);
                    $estat_des = ApiEquipmentController::getStatDescription($estat);
                    if (SapEwmDmHelper::syncEnable()){
                        SapEwmDmHelper::sendMessage($item->st_id,$item->str_ext_login,
                            false,$estat_des,$equ->label,true);
                    }
                    $equ->update(['estat' => $estat, 'estat_des' => $estat_des, 'estat_old' => $estat_old]);
                    $cell = ApiCellController::staticCellById($item->cid);
                    $str = "Система изменила статус устройства по истечении времени хранения " .
                        "<a href='/events/" . $item->id . "'>" . $item->tname . "</a>" .
                        " блок: " . $cell->name . " <a href='/events-cell/" . $cell->id . "'>" . "ячейка: " . $cell->block_pos . "</a>" . " сост. : Рабочее. метка: " .
                        "<a href='/events/" . $item->id . "'>" . $item->label . "</a>" . " серийный номер: " .
                        "<a href='/events-ser/" . $item->ser_num . "'>" . $item->ser_num . "</a>" .
                        " инв. номер: " . "<a href='/events-inv/" . $item->inv_num . "'>" . $item->inv_num . "</a>";
                    ApiLogController::addLogText($str,$cell->block, $equ->uid,$equ->tid,$equ->id,$equ->cid,0,$estat_des,
                        $estat_color,$estat_des_old,$estat_color_old);
                }
            }
        }
        return "ok.";
    }
}
