<?php

namespace App\Http\Controllers;


use App\Mail\MailSandler;
use App\Models\Act;
use App\Models\ActItem;
use App\Models\AlertMail;
use App\Models\AndComm;
use App\Models\Block;
use App\Models\BreakingState;
use App\Models\Cell;
use App\Models\CellBusy;
use App\Models\CellSize;
use App\Models\CloudVps;
use App\Models\CustomData;
use App\Models\EquExpired;
use App\Models\EquFavorite;
use App\Models\Equipment;
use App\Models\EquMenRigidLigament;
use App\Models\EquRate;
use App\Models\EquStatus;
use App\Models\Etype;
use App\Models\FailureType;
use App\Models\FbToken;
use App\Models\Feedback;
use App\Models\Log;
use App\Models\LogTakeItem;
use App\Models\Man;
use App\Models\MenBlockUse;
use App\Models\MenCellLink;
use App\Models\MenLock;
use App\Models\OrionSendItem;
use App\Models\Product;
use App\Models\Role;
use App\Models\Setting;
use App\Models\Stage;
use App\Models\StatState;
use App\Models\TerminalMessage;
use App\Models\TypeLoad;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

class ProjectSettingsController extends Controller
{
    //------------------------------------------------------------------------------------------------------------------
    public static string $MAIL_ESTATE_RETURN_NOT_WORKING_EQU = "mernwe"; // сотрудник вернул неработающее оборудование
    public static string $MAIL_ESTATE_UNLIM_USE_EQU          =  "meuue"; // превышен лимит использования оборудования
    public static string $MAIL_ESTATE_CURRENT_EQU_STATE      =  "meces"; // ежедневный отчёт о состоянии оборудования
    public static string $MAIL_ESTATE_SET_PARCELABLE_BOX     =  "mespb"; // Оповещение о закладке посылки в бокс
    public static string $MAIL_ESTATE_EQU_IN_HAND            =  "meeih"; // Оповещение о всех устройствах на руках

    public static function getAlertMailEstateDefault(){
        return self::$MAIL_ESTATE_RETURN_NOT_WORKING_EQU . ',' .
                    self::$MAIL_ESTATE_UNLIM_USE_EQU . ',' .
                        self::$MAIL_ESTATE_CURRENT_EQU_STATE;
    }

    public static function getMailEstateDescription($estate){
        switch($estate)
        {
            case self::$MAIL_ESTATE_RETURN_NOT_WORKING_EQU:
                return  __('str.settings_mail_estate_mernwe');
            case self::$MAIL_ESTATE_UNLIM_USE_EQU:
                return  __('str.settings_mail_estate_meuue');
            case self::$MAIL_ESTATE_CURRENT_EQU_STATE:
                return  __('str.settings_mail_estate_meces');
            case self::$MAIL_ESTATE_SET_PARCELABLE_BOX:
                return  __('str.settings_mail_estate_mespb');
            case self::$MAIL_ESTATE_EQU_IN_HAND:
                return  __('str.settings_mail_estate_meeih');
            default:
                return null;
        }
    }

    public function getAlertMails(){
        return json_encode(AlertMail::all());
    }

    public function updAlertEstate(Request $request){
        $item = AlertMail::where('mail',$request->mail)->first();
        if ($item == null){
            $item = new AlertMail();
            $item->mail = $request->mail;
            $item->estate = '';
            $item->save();
        }
        $checked = 'true' === $request->checked;
        if ($checked){
            if (!str_contains($item->estate, $request->estate)) {
                $item->estate .= strlen($item->estate) > 0 ? ',' . $request->estate : $request->estate;
            }
        }else{
            if (str_contains($item->estate, $item->estate)) {
                $item->estate = str_replace($request->estate,'', $item->estate);
            }
        }
        $estate = '';
        $arr = explode(',',$item->estate);
        foreach ($arr as $str){
            if (strlen($str) > 0){
                $estate .= strlen($estate) > 0 ? ',' . $str : $str;
            }
        }
        $item->estate = $estate;
        $item->update();
        return 'true';
    }
    //------------------------------------------------------------------------------------------------------------------

    private static $timeOffset = null;

    public static function getTimeOffset() : int{
        if (self::$timeOffset == null){
            self::$timeOffset = CustomData::firstOrCreate(['label' => 'time-offset'],['data' => 10800])->data;
        }
        return self::$timeOffset;
    }

    public static function toLocalTime($strDateTime) : string {
        return Carbon::createFromTimestamp(strtotime($strDateTime) + self::getTimeOffset())->toDateTimeString();
    }

    public static function toGmtTime($strDateTime) : string {
        return Carbon::createFromTimestamp(strtotime($strDateTime) - self::getTimeOffset())->toDateTimeString();
    }

    public $offsets = array(    "-43200" => "(GMT -12:00) Eniwetok, Kwajalein",
                                "-39600" => "(GMT -11:00) Midway Island, Samoa",
                                "-36000" => "(GMT -10:00) Hawaii",
                                "-34200" => "(GMT -9:30) Taiohae",
                                "-32400" => "(GMT -9:00) Alaska",
                                "-28800" => "(GMT -8:00) Pacific Time (US &amp; Canada)",
                                "-25200" => "(GMT -7:00) Mountain Time (US &amp; Canada)",
                                "-21600" => "(GMT -6:00) Central Time (US &amp; Canada), Mexico City",
                                "-18000" => "(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima",
                                "-16200" => "(GMT -4:30) Caracas",
                                "-14400" => "(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz",
                                "-12600" => "(GMT -3:30) Newfoundland",
                                "-10800" => "(GMT -3:00) Brazil, Buenos Aires, Georgetown",
                                 "-7200" => "(GMT -2:00) Mid-Atlantic",
                                 "-3600" => "(GMT -1:00) Azores, Cape Verde Islands",
                                     "0" => "(GMT) Western Europe Time, London, Lisbon, Casablanca",
                                  "3600" => "(GMT +1:00) Brussels, Copenhagen, Madrid, Paris",
                                  "7200" => "(GMT +2:00) Kaliningrad, South Africa",
                                 "10800" => "(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg",
                                 "12600" => "(GMT +3:30) Tehran",
                                 "14400" => "(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi",
                                 "16200" => "(GMT +4:30) Kabul",
                                 "18000" => "(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent",
                                 "19800" => "(GMT +5:30) Bombay, Calcutta, Madras, New Delhi",
                                 "20700" => "(GMT +5:45) Kathmandu, Pokhara",
                                 "21600" => "(GMT +6:00) Almaty, Dhaka, Colombo",
                                 "23400" => "(GMT +6:30) Yangon, Mandalay",
                                 "25200" => "(GMT +7:00) Bangkok, Hanoi, Jakarta",
                                 "28800" => "(GMT +8:00) Beijing, Perth, Singapore, Hong Kong",
                                 "31500" => "(GMT +8:45) Eucla",
                                 "32400" => "(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk",
                                 "34200" => "(GMT +9:30) Adelaide, Darwin",
                                 "36000" => "(GMT +10:00) Eastern Australia, Guam, Vladivostok",
                                 "37800" => "(GMT +10:30) Lord Howe Island",
                                 "39600" => "(GMT +11:00) Magadan, Solomon Islands, New Caledonia",
                                 "41400" => "(GMT +11:30) Norfolk Island",
                                 "43200" => "(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka",
                                 "45900" => "(GMT +12:45) Chatham Islands",
                                 "46800" => "(GMT +13:00) Apia, Nukualofa",
                                 "50400" => "(GMT +14:00) Line Islands, Tokelau"
    );

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function getLastNewIdLabel($uid) : string{
        return 'last_news_id_' . $uid;
    }

    public static function getLastManualIdLabel($uid) : string{
        return 'last_manual_id_' . $uid;
    }

    public function clearBase(Request $request){
        $token = self::getSettings('tim_token',"")->data;
        AlertMail::truncate();
        AndComm::truncate();
        Block::truncate();
        BreakingState::truncate();
        Cell::truncate();
        CellSize::truncate();
        CloudVps::truncate();
        CustomData::truncate();
        Equipment::truncate();
        EquExpired::truncate();
        EquFavorite::truncate();
        EquMenRigidLigament::truncate();
        EquStatus::truncate();
        Etype::truncate();
        FailureType::truncate();
        FbToken::truncate();
        Feedback::truncate();
        Log::truncate();
        Man::truncate();
        OrionSendItem::truncate();
        Product::truncate();
        Role::where('perm_index',0)->delete();
        Setting::truncate();
        Stage::truncate();
        StatState::truncate();
        TerminalMessage::truncate();
        TypeLoad::truncate();
        User::truncate();
        EquRate::truncate();
        MenBlockUse::truncate();
        LogTakeItem::truncate();
        CellBusy::truncate();
        MenLock::truncate();
        ActItem::truncate();
        Act::truncate();
        MenCellLink::truncate();
        self::getSettings('tim_token',"")->update(['data' => $token]);
        return "true";
    }

    public function updRnMaxId(Request $request){
        CustomData::firstOrCreate(['label' => self::getLastNewIdLabel($request->uid)],['data' => 0])->update(['data' => $request->id]);
        return "true";
    }

    public function updManualMaxId(Request $request){
        CustomData::firstOrCreate(['label' => self::getLastManualIdLabel($request->uid)],['data' => 0])->update(['data' => $request->id]);
        return "true";
    }

    private function getEquExpired($expiredOffset){
        return CustomData::firstOrCreate(['label' => 'equ-expired'],['data' => $expiredOffset]);
    }

    private function getMinOwnTime($minOwnTime){
        return CustomData::firstOrCreate(['label' => 'min-own-time'],['data' => $minOwnTime]);
    }

    private function getMaxRetTime($maxRetTime){
        return CustomData::firstOrCreate(['label' => 'max-ret-time'],['data' => $maxRetTime]);
    }

    public static function getUiLang($lang = "ru"){
        return CustomData::firstOrCreate(['label' => 'ui-lang'],['data' => $lang]);
    }

    public static function getSettings($label,$defValue){
        return CustomData::firstOrCreate(['label' => $label],['data' => $defValue]);
    }

    public function desSwitch(){
        $cd = CustomData::firstOrCreate(['label' => 'design-test'],['data' => 'false']);
        if ($cd->data == 'true'){
            $cd->data = 'false';
        }else{
            $cd->data = 'true';
        }
        $cd->update();
        return redirect()->route("project-settings2");
    }

    public function index()
    {
        return view('project-settings', [
            'offsets'          => $this->offsets,
            'timeOffset'       => CustomData::firstOrCreate(['label' => 'time-offset'],['data' => 10800])->data,
            'min_own_time'     => $this->getMinOwnTime(0)->data,
            'equ_expired'      => $this->getEquExpired(0)->data / 3600,
            'max_ret_time'     => $this->getMaxRetTime(0)->data,
            'ui_lang'          => self::getUiLang()->data,
            'ret_any_block'    => self::getSettings('ret_any_block',0)->data,
            'debug_mode'       => CustomData::firstOrCreate(['label' => 'debug_mode'     ],['data' => 0 ])->data,
            'srv_adr'          => CustomData::firstOrCreate(['label' => 'srv_adr'        ],['data' => $_SERVER['SERVER_ADDR']])->data,
            'org_name'         => CustomData::firstOrCreate(['label' => 'org_name'       ],['data' => ""])->data,
            'org_yr_name'      => CustomData::firstOrCreate(['label' => 'org_yr_name'    ],['data' => ""])->data,
            'org_yr_adr'       => CustomData::firstOrCreate(['label' => 'org_yr_adr'     ],['data' => ""])->data,
            'org_yr_inn'       => CustomData::firstOrCreate(['label' => 'org_yr_inn'     ],['data' => ""])->data,
            'org_yr_kpp'       => CustomData::firstOrCreate(['label' => 'org_yr_kpp'     ],['data' => ""])->data,
            'sync_tim_en'      => CustomData::firstOrCreate(['label' => 'sync_tim_en'    ],['data' => 1 ])->data
        ]);
    }

    public function saveSoti(Request $request){
        self::getSettings('soti_host','')->update(['data' => $request->soti_host]);
        self::getSettings('soti_client_id','')->update(['data' => $request->soti_client_id]);
        self::getSettings('soti_client_sec','')->update(['data' => $request->soti_client_secret]);
        self::getSettings('soti_api_user','')->update(['data' => $request->soti_api_username]);
        self::getSettings('soti_api_pass','')->update(['data' => $request->soti_api_password]);
        return redirect()->route("sync");
    }

    public function sapEwmAction(Request $request){
        CustomData::firstOrCreate(['label' => 'sap_ewm_en'],['data' => 0])
            ->update(['data' => $request->sap_ewm_en != null ? $request->sap_ewm_en : 0]);
        CustomData::firstOrCreate(['label' => 'sap_ewm_url'],['data' => ''])
            ->update(['data' => $request->sap_ewm_url != null ? $request->sap_ewm_url : '']);
        CustomData::firstOrCreate(['label' => 'sap_ewm_log'],['data' => ''])
            ->update(['data' => $request->sap_ewm_log != null ? $request->sap_ewm_log : '']);
        CustomData::firstOrCreate(['label' => 'sap_ewm_pas'],['data' => ''])
            ->update(['data' => $request->sap_ewm_pas != null ? $request->sap_ewm_pas : '']);
        CustomData::firstOrCreate(['label' => 'sap_ewm_en_log'],['data' => 0])
            ->update(['data' => $request->sap_ewm_en_log != null ? $request->sap_ewm_en_log : 0]);
        return redirect()->route("sync");
    }

    public function sigurAction(Request $request){
        CustomData::firstOrCreate(['label' => 'sigur_sync_en'],['data' => 0])
            ->update(['data' => $request->sigur_sync_en != null ? $request->sigur_sync_en : 0]);
        CustomData::firstOrCreate(['label' => 'sigur_log_en'],['data' => 0])
            ->update(['data' => $request->sigur_log_en != null ? $request->sigur_log_en : 0]);
        CustomData::firstOrCreate(['label' => 'sigur_mode'],['data' => 0])
            ->update(['data' => $request->sigur_mode != null ? $request->sigur_mode : 0]);
        return redirect()->route("sync");
    }

    public function sigurAction2(Request $request){
        $sigur_sync_en = $request->sigur_sync_en != null ? $request->sigur_sync_en : 0;
        if ($sigur_sync_en == 0){
            ApiSigurController::staticStopServer();
        }else{
            ApiSigurController::staticStartServer();
        }
        CustomData::firstOrCreate(['label' => 'sigur_sync_en2'],['data' => 0])
            ->update(['data' => $sigur_sync_en]);
        CustomData::firstOrCreate(['label' => 'sigur_log_en2'],['data' => 0])
            ->update(['data' => $request->sigur_log_en != null ? $request->sigur_log_en : 0]);
        CustomData::firstOrCreate(['label' => 'sigur_sync_employees'],['data' => 0])
            ->update(['data' => $request->sigur_sync_employees != null ? $request->sigur_sync_employees : 0]);
        CustomData::firstOrCreate(['label' => 'sigur_jwt_token'],['data' => ''])
            ->update(['data' => $request->sigur_jwt_token != null ? $request->sigur_jwt_token : '']);
        CustomData::firstOrCreate(['label' => 'sigur_host'],['data' => ''])
            ->update(['data' => $request->sigur_host != null ? $request->sigur_host : '']);
        CustomData::firstOrCreate(['label' => 'sigur_api_pas'],['data' => ''])
            ->update(['data' => $request->sigur_api_pas != null ? $request->sigur_api_pas : '']);
        CustomData::firstOrCreate(['label' => 'sigur_sys_int'],['data' => ''])
            ->update(['data' => $request->sigur_sys_int != null ? $request->sigur_sys_int : '']);
        return redirect()->route("sync");
    }

    public function saveAction(Request $request)
    {
        $str = '';
        $newTo = $request->time_offset != null ? intval($request->time_offset) : 0;
        $objTimeOffset = CustomData::firstOrCreate(['label' => 'time-offset'],['data' => $newTo]);
        if ($objTimeOffset->data != $newTo) {
            $str .= 'изменил часовой пояс';
        }
        $objTimeOffset->update(['data' => $newTo]);

        $minOwnTime = $request->min_own_time != null ? $request->min_own_time : 0;
        $objMinOwnTime = $this->getMinOwnTime($minOwnTime);
        if ($objMinOwnTime->data != $minOwnTime) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил минимальное время владения';
        }
        $objMinOwnTime->update(['data' => $minOwnTime]);

        $expiredOffset = $request->equ_expired != null ? $request->equ_expired : 0;
        $objEquExpired = $this->getEquExpired($expiredOffset);
        if ($objEquExpired->data != $expiredOffset * 3600) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил лимит использования';
        }
        $objEquExpired->update(['data' => $expiredOffset * 3600]);

        $maxRetTime = $request->max_ret_time != null ? $request->max_ret_time : 0;
        $objMaxRetTime = $this->getMaxRetTime($maxRetTime);
        if ($objMaxRetTime->data != $maxRetTime) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил время хранения';
        }
        $objMaxRetTime->update(['data' => $maxRetTime]);

        $lang = $request->ui_lang != null ? $request->ui_lang : "auto";
        $objUiLang = self::getUiLang($lang);
        if ($objUiLang->data != $lang) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил язык системы';
        }
        $objUiLang->update(['data' => $lang]);

        $data = $request->ret_any_block != null ? $request->ret_any_block : 0;
        $objRetAnyBlock = self::getSettings('ret_any_block',0);
        if ($objRetAnyBlock->data != $data) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил настройку возврат в любой блок';
        }
        $objRetAnyBlock->update(['data' => $data]);

        $data = $request->debug_mode != null ? 1 : 0;
        $objDebugMode = CustomData::firstOrCreate(['label' => 'debug_mode'],['data' => 0]);
        if ($objDebugMode->data != $data) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил режим дебага';
        }
        $objDebugMode->update(['data' => $data]);

        $data = $request->srv_adr != null ? $request->srv_adr : "";
        $objSrvAdr = CustomData::firstOrCreate(['label' => 'srv_adr'],['data' => $_SERVER['SERVER_ADDR']]);
        if ($objSrvAdr->data != $data) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил адрес сервера';
        }
        $objSrvAdr->update(['data' => $data]);

        $data = $request->org_name != null ? $request->org_name : "";
        $objOrgName = CustomData::firstOrCreate(['label' => 'org_name'],['data' => '']);
        if ($objOrgName->data != $data) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил имя компании';
        }
        $objOrgName->update(['data' => $data]);

        $data = $request->org_yr_name != null ? $request->org_yr_name : "";
        $objOrgYrName = CustomData::firstOrCreate(['label' => 'org_yr_name'],['data' => '']);
        if ($objOrgYrName->data != $data) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил юр. имя компании';
        }
        $objOrgYrName->update(['data' => $data]);

        $data = $request->org_yr_adr != null ? $request->org_yr_adr : "";
        $objOrgYrAdr = CustomData::firstOrCreate(['label' => 'org_yr_adr'],['data' => '']);
        if ($objOrgYrAdr->data != $data) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил юр. адрес компании';
        }
        $objOrgYrAdr->update(['data' => $data]);

        $data = $request->org_yr_inn != null ? $request->org_yr_inn : "";
        $objOrgYrInn = CustomData::firstOrCreate(['label' => 'org_yr_inn'],['data' => '']);
        if ($objOrgYrInn->data != $data) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил ИНН компании';
        }
        $objOrgYrInn->update(['data' => $data]);

        $data = $request->org_yr_kpp != null ? $request->org_yr_kpp : "";
        $objOrgYrKpp = CustomData::firstOrCreate(['label' => 'org_yr_kpp'],['data' => '']);
        if ($objOrgYrKpp->data != $data) {
            if (strlen($str) > 0) $str .= ', ';
            $str .= 'изменил КПП компании';
        }
        $objOrgYrKpp->update(['data' => $data]);

        if (strlen($str) > 0){
            ApiLogController::addLogText('Обновлены настройки, пользователь: '
                . '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a> ' . $str);
        }

        return redirect()->route("project-settings2");
    }

    public function addNewMail(Request $request) : string{
        if ($request->mail == null){
            return "no mail";
        }
        $mail = AlertMail::where('mail',$request->mail)->first();
        if ($mail != null){
            return "mail " . $request->mail . " already exist";
        }
        $mail = new AlertMail();
        $mail->mail = $request->mail;
        $mail->save();
        return "true";
    }

    public function removeMail(Request $request) : string{
        if ($request->id == null){
            return "no id";
        }
        AlertMail::whereId($request->id)->delete();
        return "true";
    }

    public function setSyncTimEn(Request $request){
        $v = $request->sync_tim_en != null ? $request->sync_tim_en : 0;
        CustomData::firstOrCreate(['label' => 'sync_tim_en'],['data' => 1 ])->update(['data' => $v]);
        return "true";
    }

    public function showSync(){
        return view('sync',[
            'soti_host'            => CustomData::firstOrCreate(['label' => 'soti_host'            ],['data' => ''         ])->data,
            'soti_client_id'       => CustomData::firstOrCreate(['label' => 'soti_client_id'       ],['data' => ''         ])->data,
            'soti_client_secret'   => CustomData::firstOrCreate(['label' => 'soti_client_sec'      ],['data' => ''         ])->data,
            'soti_api_username'    => CustomData::firstOrCreate(['label' => 'soti_api_user'        ],['data' => ''         ])->data,
            'soti_api_password'    => CustomData::firstOrCreate(['label' => 'soti_api_pass'        ],['data' => ''         ])->data,
            'parsec_sync_time'     => CustomData::firstOrCreate(['label' => 'prs_sync_time'        ],['data' => 0          ])->data,
            'prs_dom'              => CustomData::firstOrCreate(['label' => 'prs_dom'              ],['data' => '127.0.0.1'])->data,
            'prs_host'             => CustomData::firstOrCreate(['label' => 'prs_host'             ],['data' => ''         ])->data,
            'prs_port'             => CustomData::firstOrCreate(['label' => 'prs_port'             ],['data' => '10101'    ])->data,
            'prs_opr'              => CustomData::firstOrCreate(['label' => 'prs_opr'              ],['data' => ''         ])->data,
            'prs_pas'              => CustomData::firstOrCreate(['label' => 'prs_pas'              ],['data' => ''         ])->data,
            'prs_hex'              => CustomData::firstOrCreate(['label' => 'prs_hex'              ],['data' => ''         ])->data,
            'ori_host'             => CustomData::firstOrCreate(['label' => 'orion_ip'             ],['data' => ''         ])->data,
            'ori_port'             => CustomData::firstOrCreate(['label' => 'orion_port'           ],['data' => '8090'     ])->data,
            'ori_card_lock'        => CustomData::firstOrCreate(['label' => 'ori_card_lock'        ],['data' => 0          ])->data,
            'sigur_sync_en'        => CustomData::firstOrCreate(['label' => 'sigur_sync_en'        ],['data' => 0          ])->data,
            'sigur_log_en'         => CustomData::firstOrCreate(['label' => 'sigur_log_en'         ],['data' => 0          ])->data,
            'sigur_mode'           => CustomData::firstOrCreate(['label' => 'sigur_mode'           ],['data' => 0          ])->data,
            'sigur_sync_en2'       => CustomData::firstOrCreate(['label' => 'sigur_sync_en2'       ],['data' => 0          ])->data,
            'sigur_sync_employees' => CustomData::firstOrCreate(['label' => 'sigur_sync_employees' ],['data' => 0          ])->data,
            'sigur_log_en2'        => CustomData::firstOrCreate(['label' => 'sigur_log_en2'        ],['data' => 0          ])->data,
            'sigur_jwt_token'      => CustomData::firstOrCreate(['label' => 'sigur_jwt_token'      ],['data' => ''         ])->data,
            'sigur_host'           => CustomData::firstOrCreate(['label' => 'sigur_host'           ],['data' => ''         ])->data,
            'sigur_api_pas'        => CustomData::firstOrCreate(['label' => 'sigur_api_pas'        ],['data' => ''         ])->data,
            'sigur_sys_int'        => CustomData::firstOrCreate(['label' => 'sigur_sys_int'        ],['data' => ''         ])->data,
            'sap_ewm_en'           => CustomData::firstOrCreate(['label' => 'sap_ewm_en'           ],['data' => 0          ])->data,
            'sap_ewm_url'          => CustomData::firstOrCreate(['label' => 'sap_ewm_url'          ],['data' => ''         ])->data,
            'sap_ewm_log'          => CustomData::firstOrCreate(['label' => 'sap_ewm_log'          ],['data' => ''         ])->data,
            'sap_ewm_pas'          => CustomData::firstOrCreate(['label' => 'sap_ewm_pas'          ],['data' => ''         ])->data,
            'sap_ewm_en_log'       => CustomData::firstOrCreate(['label' => 'sap_ewm_en_log'       ],['data' => 0          ])->data,
            'seven_seals_dsn'      => CustomData::firstOrCreate(['label' => 'seven_seals_dsn'      ],['data' => ''         ])->data,
            'seven_seals_user'     => CustomData::firstOrCreate(['label' => 'seven_seals_user'     ],['data' => ''         ])->data,
            'seven_seals_pass'     => CustomData::firstOrCreate(['label' => 'seven_seals_pass'     ],['data' => ''         ])->data,
            'seven_seals_sync_en'  => CustomData::firstOrCreate(['label' => 'seven_seals_sync_en'  ],['data' => 0          ])->data
        ]);
    }

    public function sendMail(Request $request){
        MailSandler::send2($request->adr,$request->sbj,$request->bod);
        return 'true';
    }

//----------------------------------------------------------------------------------------------------------------------

    public function saveMailAction(Request $request){
        CustomData::firstOrCreate(['label' => 'set_mail_port'  ],['data' => 0])->update(['data' => $request->mail_port]);
        CustomData::firstOrCreate(['label' => 'set_mail_host'  ],['data' => 0])->update(['data' => $request->mail_host]);
        CustomData::firstOrCreate(['label' => 'set_mail_user'  ],['data' => 0])->update(['data' => $request->mail_user]);
        CustomData::firstOrCreate(['label' => 'set_mail_pass'  ],['data' => 0])->update(['data' => $request->mail_pass]);
        ApiLogController::addLogText('Обновлены настройки почты, пользователь: '
            . '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a> ');
        return redirect()->route("mail-settings");
    }

    public function getAnyData($page,$uid){
        return json_encode(CustomData::where('label', 'like', $page . '.' . $uid . '.' . '%')->get());
    }

    public function setAnyData(Request $request){
        CustomData::firstOrCreate(['label' => $request->label],['data' => ''])->update(['data' => '' . $request->data]);
        return "true";
    }

    public function mailSettings(){
        return view('mail-settings',[
            'day_equ_report'          =>
                CustomData::firstOrCreate(['label' => 'day_equ_report' ]         ,['data' => 0])->data,
            'day_equ_report_equ_work' =>
                CustomData::firstOrCreate(['label' => 'day_equ_report_equ_work' ],['data' => 0])->data,
            'day_rep_time'            =>
                CustomData::firstOrCreate(['label' => 'day_rep_time'   ]         ,['data' => 0])->data,
            'day_rep_time_equ_work'            =>
                CustomData::firstOrCreate(['label' => 'day_rep_time_equ_work'   ] ,['data' => 0])->data,
            'set_mail_custom'         =>
                CustomData::firstOrCreate(['label' => 'set_mail_custom']         ,['data' => 0])->data,
            'set_mail_port'           =>
                CustomData::firstOrCreate(['label' => 'set_mail_port'  ]         ,['data' => MailSandler::$PORT])->data,
            'set_mail_host'           =>
                CustomData::firstOrCreate(['label' => 'set_mail_host'  ]         ,['data' => MailSandler::$HOST])->data,
            'set_mail_user'           =>
                CustomData::firstOrCreate(['label' => 'set_mail_user'  ]         ,['data' => MailSandler::$USER])->data,
            'set_mail_pass'           =>
                CustomData::firstOrCreate(['label' => 'set_mail_pass'  ]         ,['data' => MailSandler::$PASS])->data,
        ]);
    }

    public static function getAutoUpdateBool() : bool{
        return !file_exists(storage_path("e.key"));
    }

    public function getAutoupdate() : string{
        return self::getAutoUpdateBool() ? 'true' : 'false';
    }

    public function setAutoupdate($st){
        $st = 'true' == $st;
        $path = storage_path("e.key");
        if ($st){
            if (file_exists($path)) unlink($path);
        }else{
            if (!file_exists($path)) file_put_contents($path, "");
        }
        return file_exists($path) ? 'false' : 'true';
    }

    public function packDcuProject(): string
    {
        if  (CustomData::firstOrCreate(['label' => 'org_name'],['data' => ""])->data !== 'Тестовый сервер Атеуко'){
            return "Упаковка проекта возможна только из тестового сервера";
        }
        ini_set('max_execution_time', 60); // 1 Day Timeout
        if (!file_exists('bac')) {
            mkdir('bac', 0777, true);
        }
        $fileName = '_file_back_' . time() . '.zip';
        $rootPath = realpath(getcwd() . '/../..');
        $zip = new ZipArchive();
        $zip->open('bac/' . $fileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY);
        foreach ($files as $name => $file) {
            if (!$file->isDir()) {
                $nm = basename($name);
                if (!str_starts_with($nm, "_file_back_") &&
                    $nm != ".env" && $nm != ".env" && $nm != "access.dat") {
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);
                    if (!str_starts_with($relativePath,'.git') &&
                        !str_starts_with($relativePath,'doc') &&
                        !str_starts_with($relativePath,'.idea') &&
                        !str_starts_with($relativePath,'dcu/storage') &&
                        !str_starts_with($relativePath,'JSigurDcu')){
                        $zip->addFile($filePath, $relativePath);
                    }
                }
            }else{
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);
                if ($relativePath === 'dcu/storage/app' ||
                    $relativePath === 'dcu/storage/framework' ||
                    $relativePath === 'dcu/storage/logs'){
                    $zip->addEmptyDir($relativePath);
                }
            }
        }
        $zip->close();
        $filePath = $rootPath . '/dcu/public/bac/' . $fileName;
        $resUpload = TimServerController::uploadFileIntoTimServer($filePath, $fileName);
        unlink($filePath);
        return $resUpload;
    }

}
