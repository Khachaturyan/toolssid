<?php

namespace App\Http\Controllers;

use App\DataTables\BlockDataTable;
use App\DataTables\TypeLoadDataTable;
use App\Models\Block;
use App\Models\CellSize;
use App\Models\EquExpired;
use App\Models\Equipment;
use App\Models\Etype;
use App\Models\Log;
use App\Models\Stage;
use App\Models\TypeLoad;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;

class TypeLoadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function bTypeToText($btype) : string{
        switch ($btype){
            case 1: return __('str.block_type_products');
            default: return __('str.block_type_tools');
        }
    }

    public function blockData($archive)
    {
        return Datatables::of(Block::query()->where('blocks.is_arc',$archive)
            ->leftJoin("stages", "blocks.st_id","=", "stages.id")
            ->select("blocks.*","stages.name as sname","stages.adr"))
            ->addColumn('action', function ($row) {
                return '<img src="/img/create-outline.svg" width="20px" height="20px">';
            })
            ->addColumn('btype2', function ($row) {
                return $this->bTypeToText($row->btype);
            })
            ->addColumn('action_size', function ($row) {
                return '<img src="/img/grid-four-up.svg" width="20px" height="20px">';
            })
            ->rawColumns(['action','action_size'])
            ->make(true);
    }

    private static function addTypeLoadItem($time,$sid){
        $types = Etype::where('hide',false)->where('is_cons',false)->get();
        foreach ($types as $type){
            $item = new TypeLoad();
            $item->sid = $sid;
            $item->bid = 0;
            $item->tid = $type->id;
            $item->count_box = $sid == 0 ?
                Equipment::where('tid',$type->id)->where('archive',false)
                    ->where('cid','>',0)->count() :
                Equipment::where('tid',$type->id)->where('archive',false)
                    ->where('mark_sid',$sid)->where('cid','>',0)->count();
            $item->count_men = $sid == 0 ? Equipment::where('equipment.tid',$type->id)
                ->where('equipment.archive',false)
                ->where('equipment.uid','>',0)
                ->leftJoin("men", "men.id","=", "equipment.uid")
                ->leftJoin("roles", "roles.id","=", "men.rid")
                ->where('roles.is_admin',false)
                ->where('roles.perm_index','!=',RoleController::ADMIN_INDEX)
                ->count()
                :
                Equipment::where('equipment.tid',$type->id)
                    ->where('equipment.archive',false)
                    ->where('mark_sid',$sid)
                    ->where('equipment.uid','>',0)
                    ->leftJoin("men", "men.id","=", "equipment.uid")
                    ->leftJoin("roles", "roles.id","=", "men.rid")
                    ->where('roles.is_admin',false)
                    ->where('roles.perm_index','!=',RoleController::ADMIN_INDEX)
                    ->count();
            $item->count_all = $sid == 0 ?
                Equipment::where('tid',$type->id)->where('archive',false)->count() :
                Equipment::where('tid',$type->id)->where('mark_sid',$sid)->where('archive',false)->count();
            $item->created_at = $time;
            $item->save();
        }
    }

    public static function update(){
        $t = Carbon::now()->timestamp;
        $stages = Stage::where('is_arc',false)->get();
        self::addTypeLoadItem($t,0);
        foreach ($stages as $stage){
            self::addTypeLoadItem($t,$stage->id);
        }
    }

    public function main()
    {
        return $this->mainStage(0);
    }

    public function mainStage($st_id)
    {
        if (!auth()->user()->enableAuthWeb()){
            return redirect()->route("stages");
        }
        $stages = Stage::where('is_arc',false)->get();
        if ($st_id != 0){
            $idInStages = false;
            foreach ($stages as $stage){
                if ($stage->id == $st_id){
                    $idInStages = true;
                    break;
                }
            }
            if (!$idInStages){
                return redirect()->route("main");
            }
        }
        return view('main',[
            'stages' => $stages,
            'current_stage_id' => $st_id,
            'cellSizes' => CellSize::all(),
            'stat_items' => ApiEquipmentController::getStatusArray($st_id)
        ]);
    }

    public function getUnlimitUseItems($sid){
        $builder = EquExpired::query()
            ->leftJoin("equipment", "equipment.id","=", "equ_expireds.eid")
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin("men", "men.id","=", "equ_expireds.mid")
            ->leftJoin("cells", "cells.id","=", "equ_expireds.cid")
            ->leftJoin("blocks", "blocks.id","=", "cells.block")
            ->leftJoin("stages", "stages.id","=", "blocks.st_id")
            ->where("men.fired","!=",1)
            ->where("stages.is_arc","=",false) // почему с этим не работает так и не понял....
            ->where("equipment.archive",false);
        if ($sid > 0){
            $builder = $builder->where("blocks.st_id",$sid);
        }
        $builder = $builder->select("men.mname","men.msurname","men.mpatronymic","etypes.tname","etypes.tmark","etypes.tmodel",
            "equipment.label","equipment.inv_num","equipment.ser_num","cells.block_pos",
            "blocks.name as bname","stages.name as sname","equ_expireds.*");
        //die($builder->toSql());
        //file_put_contents('file.txt', $builder->toSql());
        return Datatables::of($builder)
            ->addColumn('fio', function ($row) {
                return $row->msurname . ' ' . $row->mname . ' ' . $row->mpatronymic;
            })
            ->addColumn('time_last', function ($row) {
                $SEC_IN_DAY  = 86400;
                $SEC_IN_HOUR = 3600;
                $SEC_IN_MIN  =   60;
                $elapsedTime = time() - strtotime($row->time); //+ ProjectSettingsController::getTimeOffset();
                $elapsedDay  = intval($elapsedTime / $SEC_IN_DAY);
                $elapsedHour = intval(($elapsedTime % $SEC_IN_DAY) / $SEC_IN_HOUR);
                $elapsedMin  = intval(($elapsedTime % $SEC_IN_HOUR) / $SEC_IN_MIN);
                if ($elapsedDay > 0){
                    return ProjectSettingsController::toLocalTime($row->time) . '<br>' . $elapsedDay . "д";
                }
                $elapsedMin = "" . $elapsedMin;
                if (strlen($elapsedMin) == 1) $elapsedMin = "0" . $elapsedMin;
                return ProjectSettingsController::toLocalTime($row->time) . '<br>' . $elapsedHour . "ч " . $elapsedMin . "м";
            })
            ->rawColumns(['time_last'])
            ->make(true);
    }

    public function getNotWorkingItems($sid){
        $builder = Equipment::query()
            ->where('equipment.archive','=',false)
            ->where('equipment.is_work','=',false);
        if ($sid > 0){
            $builder = $builder->where('equipment.mark_sid',$sid);
        }
        $builder = $builder->leftJoin("breaking_states", "breaking_states.eid","=", "equipment.id")
            ->leftJoin("men", "men.id","=", "breaking_states.mid")
            ->leftJoin("failure_types", "failure_types.id","=", "equipment.ftid")
            ->leftJoin("roles", "roles.id","=", "men.rid")
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->select("roles.rname","roles.is_admin","men.mlabel","men.mname","men.msurname","men.mpatronymic","men.post",
                "etypes.tname","etypes.tmark","etypes.tmodel", "equipment.*","failure_types.name as ft_name");
        return Datatables::of($builder)
            ->addColumn('fio', function ($row) {
                //return json_encode($row);
                return $row->msurname . ' ' . $row->mname . ' ' . $row->mpatronymic;
            })
            ->addColumn('time',
                function($data){
                    $RETURN_ITEM_NOT_WORKING_LTP = 8;
                    $l = Log::where('eid', $data->id)
                        ->where('ltp',$RETURN_ITEM_NOT_WORKING_LTP)
                        ->orderBy('created_at', 'desc')
                        ->first();
                    if ($l == null) {
                        return "";
                    }
                    $parts = explode(".",ProjectSettingsController::toLocalTime($l->created_at));
                    return str_replace(array('T'), " ", $parts[0]);
                })
            ->make(true);
    }

    public function show(TypeLoadDataTable $typeLoadDataTable)
    {
        $types = Etype::where('hide', 0)->get();
        foreach ($types as $type){
            $type->count_men = Equipment::where('equipment.tid',$type->id)
                ->where('equipment.archive',false)
                ->where('equipment.uid','>',0)
                ->leftJoin("men", "men.id","=", "equipment.uid")
                ->leftJoin("roles", "roles.id","=", "men.rid")
                ->where('roles.is_admin',false)
                ->count();
        }
        return $typeLoadDataTable->render('type-load', [
            'types'=> $types,
            'stages' => Stage::where('is_arc',false)->get(),
            'blocks' => Block::where('is_arc',false)->get(),
            'cellSizes' => CellSize::all()
        ]);
    }

    private function getSecondsFromTimeFrameIndex($timeframe) : int {
        switch (intval($timeframe)){
            case 2: return 3600;
            case 3: return 86400;
            case 4: return 604800;
            case 6: return 7200;
            case 7: return 1209600;
            default: return 0;
        }
    }

    private function getLimitFromTimeFrame($timeframe){
        switch (intval($timeframe)){
            case  2: return 12;
            case  3: return 24;
            case  4: return 7;
            case  6: return 24;
            case  7: return 14;
            default: return 30;
        }
    }

    private function canAddAsTimeLine($timeframeSec, $checkTime, $lastTime){
        return $lastTime == null || $lastTime - $checkTime > $timeframeSec;
        //return true;
    }

    public function listItems($tf,$sid){
        $limit = $this->getLimitFromTimeFrame($tf);
        $timeframeSec = $this->getSecondsFromTimeFrameIndex($tf);// промежуток в секундах между точками
        if ($timeframeSec <= 0){
            $timeframeSec = intval((TypeLoad::max('created_at') - TypeLoad::min('created_at')) / ($limit - 1));
        }else{
            $timeframeSec /= $limit - 1;
        }
        $items = TypeLoad::where('sid',$sid)
            ->where('created_at','>=',time() - $timeframeSec * ($limit + 1))
            ->select('created_at')
            ->distinct()
            ->orderBy('created_at', 'desc')
            ->get();
        //die('' .$timeframeSec);
        $arr = array();
        $lastTime = null;
        for($i = 0; $i < count($items); $i++){
            $item = $items[$i];
            if ($this->canAddAsTimeLine($timeframeSec,$item->created_at,$lastTime)){
                $arr[] = $item->created_at;
                $lastTime = intval($item->created_at);
                if (count($arr) >= $limit){
                    break;
                }
            }
        }
        $items = TypeLoad::whereIn('type_loads.created_at',$arr)
            ->where('type_loads.count_all','>',0)
            ->where('type_loads.sid',$sid)
            ->where('etypes.hide',false)
            ->where('etypes.is_cons',false)
            ->leftJoin("etypes", "type_loads.tid", "=", "etypes.id",)
            ->select("type_loads.*","etypes.tname","etypes.tmark","etypes.tmodel")
            ->get();
        $localTimeOffset = ProjectSettingsController::getTimeOffset();
        for($i = 0; $i < count($items); $i++){
            $items[$i]->created_at += $localTimeOffset;
        }
        return json_encode($items);
    }
}
