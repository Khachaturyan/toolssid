<?php

namespace App\Http\Controllers;


use App\DataTables\StatsDataTable;
use App\Models\Man;

class StatsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function stats(StatsDataTable $dataTable, $mid)
    {
        $dataTable->mid = $mid;
        return $dataTable->render('stats',['men' => Man::whereId($mid)->first()]);
    }

    public function statsAll(StatsDataTable $dataTable)
    {
        $dataTable->mid = 0;
        return $dataTable->render('stats',['men' => null]);
    }
}
