<?php

namespace App\Http\Controllers;

use App\Mail\MailSandler;
use App\Models\CustomData;
use App\Models\EquExpired;
use App\Models\Equipment;
use Carbon\Carbon;
use Illuminate\Http\Request;

// функционал для не вернувших оборудование вовремя

class EquExpiredController extends Controller
{

    public static function checkExpiredAsyncRequest(){
        $url = "http://127.0.0.1/api/check-expired";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function checkExpired(){
        if (request()->ip() == "127.0.0.1"){
            $timeOffsetSet = CustomData::where('label','equ-expired')->first();
            $timeOffset = $timeOffsetSet != null ? $timeOffsetSet->data : 0;
            if ($timeOffset > 0){
                $items = Equipment::where('equipment.ocid','>',0)
                    ->where('equipment.updated_at', '<', Carbon::now()->addSeconds(-$timeOffset))
                    ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                    ->leftJoin("men", "men.id","=", "equipment.uid")
                    ->leftJoin("cells", "cells.id","=", "equipment.cid")
                    ->leftJoin("blocks", "blocks.id","=", "cells.block")
                    ->select("men.id as mid","men.mlabel","men.mname","men.msurname","etypes.tname","etypes.tmark","etypes.tmodel",
                        "equipment.*","cells.block_pos","blocks.name as bname")->get();
                $expiredIds = array();
                foreach ($items as $item){
                    if ($item->mid == null) continue;
                    $exp = EquExpired::where('eid', $item->id)->first();
                    if ($exp == null){
                        $exp = EquExpired::create(['state' => 1, 'des' => '', 'time' => $item->updated_at,
                            'cid' => $item->ocid, 'eid' => $item->id, 'mid' => $item->mid]);
                        $exp->save();
                        $str = $item->mname . " " . $item->msurname . " не вернул вовремя " . $item->tname . " " .
                            $item->tmark . " " . $item->tmodel . " который получил " . ProjectSettingsController::toLocalTime($item->updated_at);
                        ApiLogController::addLogText($str);
                        MailSandler::sendToAll("Превышен лимит использования оборудования",$str,
                            ProjectSettingsController::$MAIL_ESTATE_UNLIM_USE_EQU);
                    }
                    $expiredIds[] = $exp->id;
                }
                EquExpired::whereNotIn('id', $expiredIds)->delete();
            }else{
                EquExpired::where('id','>',0)->delete();
            }
        }
        return "ok.";
    }
}
