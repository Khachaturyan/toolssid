<?php

namespace App\Http\Controllers;

use App\DataTables\CellSizeDataTable;
use App\Models\CellSize;
use Illuminate\Http\Request;

class CellSizeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(CellSizeDataTable $dataTable){
        return $dataTable->render('cell-sizes',["table_header" => __('str.cell_sizes_header')]);
    }

    public function create(Request $request){
        $cSize = $request->id != null ? CellSize::whereId($request->id)->first() : new CellSize();
        $cSize->name = $request->name;
        $cSize->des = $request->des != null ? $request->des : "";
        if ($request->id != null) $cSize->update(); else $cSize->save();
        return redirect()->route("cell-sizes");
    }
}
