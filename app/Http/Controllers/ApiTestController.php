<?php

namespace App\Http\Controllers;

use App\Models\Man;
use Illuminate\Http\Request;

class ApiTestController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function apiTest(Request $req) : string{
        try{
            $c = new ApiManController();
            $c->deleteByLabel('000test000');
            $res = $c->manAction(new Request(array('rid' => 1,'mname' => 'Тест', 'pin' => '000','fired' => '0',
                'post' => 'Президент','str_ext_id' => '','can_dia' => true,
                'msurname' => 'Тест', 'mpatronymic' => 'Тест', 'mlabel' => '000test000', 'str_ext_login' => '')));
            if ("true" !== $res){
                return $res;
            }
            $c->deleteByLabel('000test000');
            return $res;
        }
        catch(\Exception $e){
            die($e->getMessage());
        }
    }
}
