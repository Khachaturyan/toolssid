<?php

namespace App\Http\Controllers;

use App\Models\CustomData;
use App\SevenSeals\SevenSealsClient;
use Illuminate\Http\Request;



class AutostartController extends Controller
{
    public function fbTest(){
        return SevenSealsClient::testConnection();
    }

    public function onStart(){
        if ('127.0.0.1' == request()->ip()){
            $sigurSyncEnable = intval(CustomData::firstOrCreate(['label' => 'sigur_sync_en2' ],['data' => 0])->data) == 1;
            if ($sigurSyncEnable){
                ApiSigurController::staticStartServer();
            }
            ApiLogController::addLogText("система была благополучно перезапущена");
        }else{
            ApiLogController::addLogText("система была перезапущена " . request()->ip());
        }
    }
}
