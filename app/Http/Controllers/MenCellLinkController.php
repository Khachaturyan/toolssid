<?php

namespace App\Http\Controllers;

use App\Models\Cell;
use App\Models\Man;
use App\Models\MenCellLink;
use Illuminate\Http\Request;

class MenCellLinkController extends Controller
{

    public function AddLink($mid,$cid){
        $item = MenCellLink::where('mid',$mid)->where('cid',$cid)->first();
        if ($item == null){
            (new MenCellLink(['mid' => $mid, 'cid' => $cid]))->save();
        }
        $men = Man::whereId($mid)->first();
        $cell = Cell::where('cells.id',$cid)
            ->leftJoin("blocks", "blocks.id","=", "cells.block")
            ->leftJoin("stages", "blocks.st_id","=", "stages.id")
            ->select("blocks.name","stages.name as sname","cells.*")
            ->first();
        if ($men == null || $cell == null){
            return "false";
        }
        ApiLogController::addLogText('Пользователь: '
            . '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>'
            . ' создал связку сотрудника id=' . $mid . ' ' . $men->mname . ' ' . $men->msurname . ' и ячейки id='
            . $cid . ', Объект: ' . $cell->sname . ', Блок: ' . $cell->name . ', Номер: ' . $cell->block_pos);
        return "true";
    }

    public function RemoveLink($mid,$cid){
        MenCellLink::where('mid',$mid)->where('cid',$cid)->delete();
        $men = Man::whereId($mid)->first();
        $cell = Cell::where('cells.id',$cid)
            ->leftJoin("blocks", "blocks.id","=", "cells.block")
            ->leftJoin("stages", "blocks.st_id","=", "stages.id")
            ->select("blocks.name","stages.name as sname","cells.*")
            ->first();
        if ($men == null || $cell == null){
            return "false";
        }
        ApiLogController::addLogText('Пользователь: '
            . '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>'
            . ' удалил связку сотрудника id=' . $mid . ' ' . $men->mname . ' ' . $men->msurname . ' и ячейки id='
            . $cid . ', Объект: ' . $cell->sname . ', Блок: ' . $cell->name . ', Номер: ' . $cell->block_pos);
        return "true";
    }

}
