<?php

namespace App\Http\Controllers;

use App\DataTables\EquipmentDataTable;
use App\Models\Block;
use App\Models\EquFavorite;
use App\Models\Equipment;
use App\Models\EquMenRigidLigament;
use App\Models\EquStatus;
use App\Models\Etype;
use App\Models\Man;
use App\Models\Product;
use App\Models\Stage;
use App\Soti\SotiClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Yajra\DataTables\DataTables;

class EquController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function isTypeHasEqu($tid){
        return Equipment::where('tid',$tid)->where('archive',false)->count() > 0 ? "true" : "false";
    }

    public function equipmentCreate(){
        $etypes = DB::table('etypes')->where('is_cons','=',false)->get();
        return view('equipment-create',["etypes" => $etypes]);
    }

    public function equipmentCreateCons(){
        $etypes = DB::table('etypes')->where('is_cons','=',true)->get();
        return view('equipment-create-cons',["etypes" => $etypes]);
    }

    public function addEquipmentCount($id){
        $etype = DB::table('etypes')->where('etypes.id', "=", $id)->first();
        if ($etype == null){
            return abort(404);
        }
        return view('set-equipment-count',["etype" => $etype]);
    }

    private function equRender(EquipmentDataTable $dataTable, $header){
        $etypes = DB::table('etypes')->where('is_cons','=',false)->where('hide',false)->get();
        return $dataTable->render('equipment',[
            "table_header" => $header,
            "etypes"       => $etypes,
            "mens"         => Man::all(),
            "stages"       => Stage::where('is_arc',false)->get(),
            "blocks"       => Block::where('is_arc',false)->get(),
            "equ_state"    => EquStatus::all()
        ]);
    }

    public function equipment(EquipmentDataTable $dataTable)
    {
        $dataTable->notArchiveItems = true;
        return $this->equRender($dataTable, __('str.equ_main_header'));
    }

    public function equipmentArchive(EquipmentDataTable $dataTable)
    {
        $dataTable->notArchiveItems = false;
        return $this->equRender($dataTable,__('str.equ_archive_main_header'));
    }

    public function equipmentNotWorking(EquipmentDataTable $dataTable)
    {
        $dataTable->notWorkingItems = true;
        return $this->equRender($dataTable, __('str.equ_not_working_main_header'));
    }
/*
    public function equEstatUpdate($id,$st){
        $equ = $st > 0 ? EquStatus::whereId($st)->first() : null;
        $descr = $equ == null ? ApiEquipmentController::getStatDescription($st) : $equ->descr;
        $equ = Equipment::whereId($id)->first();
        if ($equ != null){
            $estat_des_old = $equ->estat_des;
            $equ->estat_old = $equ->estat;
            $equ->estat = $st;
            $equ->estat_des = $descr;
            $equ->update();
            if ($equ->estat_old != $equ->estat && $equ->estat == -7){
                EquMenRigidLigament::where('eid',$equ->id)->delete();
            }
            ApiLogController::addLogText('Обновлён статус устройства, метка: <a href="/events/' . $equ->id . '">' .
                $equ->label . '</a> id=' .
                $equ->id
                . ', Пользователь: ' .
                '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>',ApiLogController::CHANGE_EQU_STATUS_FAKE_BID_ID,
                auth()->user()->mid(),0,$equ->id,0,0,$equ->estat_des,"",$estat_des_old,"");
        }
        return "true";
    }
*/
    private function equipmentValidate($id,$label,$ser_num,$adr){
        $labelIsUnique = Equipment::where('id', '!=', $id)->where('label',$label)->get()->count() == 0;
        if (!$labelIsUnique){
            $validator = Validator::make([],['unique' => 'required'],['Метка должна быть уникальной']);
            if ($validator->fails()) {
                return redirect($adr)
                    ->withErrors($validator)
                    ->withInput();
            }
        }
        if (strlen($ser_num) > 0){
            $serNumIsUnique = Equipment::where('id', '!=',$id)->where('ser_num',$ser_num)->get()->count() == 0;
            if (!$serNumIsUnique){
                $validator = Validator::make([],['unique' => 'required'],['Серийник должен быть уникальным']);
                if ($validator->fails()) {
                    return redirect($adr)
                        ->withErrors($validator)
                        ->withInput();
                }
            }
        }
        return null;
    }

    public function equipmentAddAction(Request $request){
        $request->validate([
            "count" => "required|min:1|max:255"
        ]);
        for ($i = 0; $i < $request->input("count"); $i++){
            $equipment = new Equipment();
            $equipment->tid = $request->input("id");
            $equipment->uid = 0;
            $equipment->cid = 0;
            $equipment->ocid = 0;
            $equipment->label       =    '';
            $equipment->description =    '';
            $equipment->is_work     =  true;
            $equipment->archive     = false;
            $equipment->ser_num     =    "";
            $equipment->save();
        }
        return redirect()->route("etypes");
    }

    public function clearFavorites(Request $request){
        EquFavorite::where('eid',$request->id)->delete();
        return "true";
    }

    public function equipmentCreateAction(Request $request){
        $equipment = new Equipment();
        $equipment->tid = $request->input("tid");
        $equipment->uid = 0;
        $equipment->cid = 0;
        $equipment->ocid = 0;
        $equipment->label   = $request->input("label") != null ? $request->input("label") : "";
        $equipment->ser_num = $request->input("ser_num") != null ? $request->input("ser_num") : "";
        $equipment->description =    '';
        $equipment->is_work     =  true;
        $equipment->archive     = false;
        $validateRes = $this->equipmentValidate($equipment->id,$equipment->label,$equipment->ser_num,'/equipment-create');
        if ($validateRes != null){
            return $validateRes;
        }
        $equipment->save();
        return redirect()->route("equipment");
    }

    public function equipmentCreateActionCons(Request $request){
        $request->validate([
            "count" => "required|min:1|max:255"
        ]);
        for ($i = 0; $i < $request->input("count"); $i++){
            $equipment = new Equipment();
            $equipment->tid = $request->input("tid");
            $equipment->uid = 0;
            $equipment->cid = 0;
            $equipment->ocid = 0;
            $equipment->label       =    '';
            $equipment->description =    '';
            $equipment->is_work     =  true;
            $equipment->archive     = false;
            $equipment->ser_num     =    '';
            $equipment->save();
        }
        return redirect()->route("equipment");
    }

    public function getRdpLink(Request $request) : string{
        $client = new SotiClient();
        return '' . $client->getRdpLinkFromMac($request->mac);
    }

    public function equEject($id){
        $equ = Equipment::where('id',$id)->first();
        if ($equ == null) {
            return "equ with id - $id not found";
        }
        EquMenRigidLigament::where('eid',$id)->delete();
        EquFavorite::where('eid',$id)->delete();
        $equ->cid = 0;
        $equ->uid = 0;
        $equ->ocid = 0;
        $equ->estat_old = $equ->estat;
        $equ->estat = ApiEquipmentController::getEstatFromEquipment($equ);
        $equ->estat_des = ApiEquipmentController::getStatDescription($equ->estat);
        $equ->ftid = ApiEquipmentController::getFailureStateIdFromEstat($equ->estat,$equ->ftid);
        $equ->update();
        if ($equ->estat_old != $equ->estat && $equ->estat == -7){
            EquMenRigidLigament::where('eid',$equ->id)->delete();
        }
        ApiLogController::addLogText('Пользователь: ' . '<a href="/events-find/' . auth()->user()->email . '">' .
            auth()->user()->email . '</a> перевёл устройство  id: ' .  $equ->id . ' в статус: ' . $equ->estat_des,ApiLogController::CHANGE_EQU_STATUS_FAKE_BID_ID,
            auth()->user()->mid(),0,$equ->id,0,0,$equ->estat_des,"",
            ApiEquipmentController::getStatDescription($equ->estat_old),"");
        return "true";
    }

    public function equRigitData()
    {
        $query = Equipment::query()->where('archive',false)->orderBy('equipment.updated_at', 'desc')
            ->select("equipment.*");
        return Datatables::of($query)
            ->addColumn('fio', function ($row) {
                $mens = EquMenRigidLigament::where('equ_men_rigid_ligaments.eid',$row->id)
                    ->leftJoin("men", "men.id","=", "equ_men_rigid_ligaments.uid")
                    ->select("men.*")->get();
                $strRes = "";
                foreach ($mens as $men){
                    if (strlen($strRes) > 0){
                        $strRes .= "<br>";
                    }
                    $strRes .= $men->mlabel . " " . $men->mname . " " . $men->msurname . " - " . $men->post;
                }
                $row->fio = $strRes;
                return $strRes;
            })
            ->addColumn('add_rigit', function ($row) {
                return '<img src="/img/add_circle_outline_black_24dp.svg" width="20px" height="20px">';
            })
            ->addColumn('remove_rigit', function ($row) {
                return strlen($row->fio) == 0 ? '' : '<img src="/img/create-outline.svg" width="20px" height="20px">';
            })
            ->rawColumns(['fio','post','remove_rigit','add_rigit'])
            ->make(true);
    }

    public function takeApartEqu($id){
        $equ = Equipment::where('equipment.id',$id)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->select("etypes.tname","etypes.tmark","etypes.tmodel", "etypes.is_comp", "equipment.*")
            ->first();
        Equipment::where('comp_id',$id)->update(['comp_id' => 0]);
        $equ->delete();
        ApiLogController::addLogText('Пользователь: ' . '<a href="/events-find/' . auth()->user()->email . '">' .
            auth()->user()->email . '</a> дезинтегрировал комплект  id: ' .  $equ->id . ' ' . $equ->tname . ' ' . $equ->tmark . ' ' . $equ->tmodel,0,
            auth()->user()->mid(), $equ->tid, $equ->id,0,0,"","",
            $equ->estat_des,"");
        return 'true';
    }

    public function getKitItems($kid){
        $equArr = Equipment::where('equipment.comp_id',$kid)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->select("etypes.tname","etypes.tmark","etypes.tmodel", "etypes.is_comp", "equipment.*")
            ->get();
        return json_encode($equArr);
    }

    public function getKitItemsDes($kid){
        $arr = explode(",",$kid);
        $ids = array();
        foreach ($arr as $item){
            if (strlen($item) > 0) $ids[] = intval($item);
        }
        $items = Etype::query()->whereIn('id',$ids)->select("tname","tmark","tmodel")->get();
        $res = "";
        foreach ($items as $item){
            if (strlen($res) > 0) $res .= ",";
            $res .= $item->tname . ' ' . $item->tmark . ' ' . $item->tmodel;
        }
        return $res;
    }
}
