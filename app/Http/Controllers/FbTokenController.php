<?php

namespace App\Http\Controllers;

use App\Models\FbToken;
use Illuminate\Http\Request;

class FbTokenController extends BaseController
{

    public function update(Request $request){
        $data = $request->all();
        $fbToken = FbToken::where('aid',$data["aid"])->first();
        if ($fbToken == null){
            $fbToken = new FbToken();
            $fbToken->aid = $data["aid"];
            $fbToken->fbs_token = $data["fbs_token"];
            $fbToken->save();
        }else{
            $fbToken->update($data);
        }
        return $this->sendResponse(true, 'fb token update successfully.');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public static function sendNotification($device_token, $title, $message, $id)
    {
        $SERVER_API_KEY = env('FCM_KEY');
        $dataArr = [
            "mid" => $id,
            "body" => $message,
            "title" => $title,
            "type" => "basic"
        ];
        $data = [
            "to" => $device_token, // for single device id
            "data" => $dataArr
        ];
        $dataString = json_encode($data);
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

}
