<?php

namespace App\Http\Controllers;

use App\Imports\EquImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class EquImportExcelController extends Controller
{
    public function import(Request $request)
    {
        $equImport = new EquImport();
        $equImport->tid = $request->tid;
        Excel::import($equImport, $request->file('file'));
        return back();
    }
}
