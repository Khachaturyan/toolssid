<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Cell;
use App\Models\CellSize;
use App\Models\Equipment;
use App\Models\Etype;
use App\Models\Stage;
use Illuminate\Http\Request;

class BlockController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function blocks()
    {
        $types = Etype::where('hide', false)->get();
        foreach ($types as $type){
            $type->count_men = Equipment::where('equipment.tid',$type->id)
                ->where('equipment.archive',false)
                ->where('equipment.uid','>',0)
                ->leftJoin("men", "men.id","=", "equipment.uid")
                ->leftJoin("roles", "roles.id","=", "men.rid")
                ->where('roles.is_admin',false)
                ->count();
        }
        return view('blocks',[
            'types'=> $types,
            'stages' => Stage::where('is_arc',false)->get(),
            'blocks' => Block::where('is_arc',false)->get(),
            'cellSizes' => CellSize::all(),
        ]);
    }

    public function blockAction(Request $request)
    {
        $arcStr = "";
        $isNewBlock = $request->input("id") == null;
        if ($isNewBlock) {
            $block = new Block();
        }else{
            $block = Block::whereId($request->input("id"))->first();
            if ($block == null){
                return "Не найден блок с id - " . $request->input("id");
            }
            if ($request->input("is_arc") && !$block->isEmpty()){
                return "Только пустой блок можно отправить в архив";
            }
            if (($block->btype != $request->input("btype")) && !$block->isEmpty()){
                return "Тип блока можно изменить только у пустого бокса";
            }
        }
        $block->name = $request->input("name");
        $block->is_arc = $request->input("is_arc");
        $block->st_id = $request->input("st_id");
        $block->description = $request->input("description") != null ? $request->input("description") : "";
        $block->cell_count = $request->input("cell_count");
        $block->btype = $request->input("btype");
        $block->pin = $request->input("pin") != null ? $request->input("pin") : "99998";
        $block->work_rest = $request->input("work_rest") != null ? $request->input("work_rest") : "";
        $block->encode_alg = $request->input("encode_alg") != null ? $request->input("encode_alg") : 0;
        $block->show_win_sel_sizes = 1 == $request->input("show_win_sel_sizes");
        $block->time_ret = 1 == $request->input("time_ret");
        $block->orion_lab_encode = 1 == $request->input("orion_lab_encode");
        $block->cut_label_6b = $isNewBlock ? 0 : $block->cut_label_6b;
        $block->ret_any_cell = 1 == $request->input("ret_any_cell");
        if ($isNewBlock){
            try{
                $addRes = $block->save();
            }
            catch(\Exception $e){
                die($e->getMessage());
            }
            if ($addRes){
                for ($i = 0; $i < $block->cell_count; $i++){
                    $cell = new Cell();
                    $cell->is_arc    = $block->is_arc;
                    $cell->block     = $block->id;
                    $cell->block_pos = $i + 1;
                    $cell->csid      = 0; // размер ячейки по умолчанию
                    $cell->save();
                }
            }
        }else{
            $block->update();
            Cell::where('block',$block->id)->update(['is_arc' => $block->is_arc]);
        }
        ApiLogController::addLogText('Обновлен Блок: id=' .
            $block->id
            . ' (' . $block->name . ')' . ', Пользователь: '
            . '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>' . $arcStr);
        return "true";
    }

    public function blockEquipment()
    {
        $types = Etype::where('hide', 0)->get();
        foreach ($types as $type){
            $type->count_men = Equipment::where('equipment.tid',$type->id)
                ->where('equipment.archive',false)
                ->where('equipment.uid','>',0)
                ->leftJoin("men", "men.id","=", "equipment.uid")
                ->leftJoin("roles", "roles.id","=", "men.rid")
                ->where('roles.is_admin',false)
                ->count();
        }
        $blocks = Block::all();
        return view('block-equipment', [
            'blocks' => $blocks,
            'types'=> $types
        ]);
    }

}
