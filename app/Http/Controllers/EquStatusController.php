<?php

namespace App\Http\Controllers;

use App\Models\Equipment;
use App\Models\EquStatus;
use App\Models\Log;
use App\Models\Man;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class EquStatusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(){
        return view('equ-states',['stat_items' => ApiEquipmentController::getStatusDefault()]);
    }

    public function itemsData(){
        return Datatables::of(EquStatus::all())
            ->addColumn('delete', function ($row) {
                return '<img src="/img/delete_black_24dp.svg" width="20px" height="20px">';
            })
            ->addColumn('action', function ($row) {
                return '<img src="/img/create-outline.svg" width="20px" height="20px">';
            })
            ->addColumn('color2', function ($row) {
                return '<input type="color" value="' . $row->color . '" style="width:100%;" disabled>';
            })
            ->addColumn('count', function ($row) {
                return Equipment::where('estat',$row->id)->count();
            })
            ->rawColumns(['action','delete','color2'])
            ->make(true);
    }

    public function itemEdit(Request $request){
        $newItem = $request->input("id") == null;
        if ($newItem){
            $item = new EquStatus();
        }else{
            $item = EquStatus::whereId($request->input("id"))->first();
        }
        $item->color = $request->input("color");
        $item->descr = $request->input("descr");
        $item->des2 = $request->input("des2") != null ? $request->input("des2") : "";
        if ($newItem){
            $item->save();
            ApiLogController::addLogText('Создан статус: id=' . $item->id . ' (' . $item->descr .
                ')' . ', Пользователь: ' .
                '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>');
        }else{
            $item->update();
            ApiLogController::addLogText('Изменён статус: id=' . $item->id . ' (' . $item->descr .
                ')' . ', Пользователь: ' .
                '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>');
        }
        return redirect()->route("equ-states");
    }

    public function itemDelete($id){
        $item = EquStatus::whereId($id)->first();
        $item->delete();
        ApiLogController::addLogText('Удалён статус: id=' . $item->id . ' (' . $item->descr .
            ')' . ', Пользователь: ' .
            '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>');
        return "true";
    }
}
