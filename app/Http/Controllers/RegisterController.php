<?php

namespace App\Http\Controllers;

use App\Models\Man;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class RegisterController extends BaseController
{

    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;
            return $this->sendResponse($success, 'User login successfully.');
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }

    public function checkLogin(Request $request){
        return User::where('email',$request->email)->count() == 0 ? "true" : "false";
    }

    public function sign(Request $request)
    {
        $men = Man::whereId($request->mid)->first();
        if ($men == null){
            return "men not found";
        }
        $user = User::create([
            'name' => $request->nam,
            'email' => $request->log,
            'password' => Hash::make($request->pas),
        ]);
        $men->web_uid = $user->id;
        $men->update();
        return "true";
    }

    public function linkLogin(Request $request){
        $user = User::where('email',$request->log)->first();
        if ($user == null){
            return "user not found";
        }
        $men = Man::whereId($request->mid)->first();
        if ($men == null){
            return "men not found";
        }
        $men->web_uid = $user->id;
        $men->update();
        return "true";
    }

}
