<?php

namespace App\Http\Controllers;

use App\Models\Cell;
use App\Models\EquFavorite;
use App\Models\EquMenRigidLigament;
use App\Models\EquStatus;
use App\Models\Etype;
use App\Models\Equipment;
use App\Models\Man;
use App\Models\OrionSendItem;
use App\SapEwmDm\SapEwmDmHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use DB;
use JetBrains\PhpStorm\Pure;

class ApiEquipmentController extends BaseController
{

    private static $estatArray = array(
        ['id' => -6],
        ['id' => -2],
        ['id' => -7],
        ['id' => -4],
        ['id' => -5],
        ['id' => -3],
        ['id' => -8]
    );


    public static function getStatusArray($st_id){
        $resDataPack = array();
        $resDataPack[''] = self::getStatusArrayData(self::$estatArray,true, $st_id, 0);
        $types = Etype::where('hide',false)->get();
        foreach ($types as $etype){
            $resDataPack[$etype->tname . ' ' . $etype->tmark . ' ' . $etype->tmodel] =
                self::getStatusArrayData(self::$estatArray,true, $st_id, $etype->id);
        }
        //die(json_encode($resDataPack));
        return $resDataPack;
    }

    public static function getStatusDefault(){
        $arr = array();
        for ($i = -8; $i < -1; $i++){
            $arr[] = array('id' => $i);
        }
        return self::getStatusArrayData($arr,false,0, 0);
    }

    private static function getStatusArrayData(array $res, bool $fillFromBase, $st_id, $tid): array
    {
        for ($i = 0; $i < count($res); $i++){
            $res[$i]['color'] = self::getStatColor($res[$i]['id']);
            $res[$i]['descr'] = self::getStatDescription($res[$i]['id']);
            $res[$i]['count'] = self::getStatCount($res[$i]['id'],$st_id,$tid);
            $res[$i]['desfu'] = self::getStatDesFull($res[$i]['id']);
        }
        if ($fillFromBase){
            $items = EquStatus::all();
            foreach ($items as $item){
                $builder = Equipment::where('archive',false)->where('estat',$item->id);
                if ($st_id > 0){
                    $builder = $builder->where('mark_sid',$st_id);
                }
                if ($tid > 0){
                    $builder = $builder->where('tid',$tid);
                }
                $res[] = array('id' => $item->id, 'color' => $item->color,
                    'descr' => $item->descr, 'count' => $builder->count());
            }
        }
        return $res;
    }

    public static $NOT_WORKING_INDEX = -2;
    public static $IS_FREE_INDEX     = -3;
    public static $IN_BOX_INDEX      = -4;
    public static $IN_HAND_INDEX     = -5;
    public static $REPAIR_INDEX      = -7;

    public static function getStatCount($estat, $st_id, $tid){
        if ($estat == -8){
            $builder = EquFavorite::query()
                            ->leftJoin('equipment','equ_favorites.eid','=','equipment.id');
            if ($st_id > 0){
                $builder = $builder->where('equipment.mark_sid',$st_id);
            }
            if ($tid > 0){
                $builder = $builder->where('equipment.tid',$tid);
            }
            return $builder->count();
        }
        $archive_status_is_archive = $estat == -1;
        $builder = Equipment::where('archive',$archive_status_is_archive);
        if ($st_id > 0){
            $builder = $builder->where('mark_sid',$st_id);
        }
        if ($tid > 0){
            $builder = $builder->where('tid',$tid);
        }
        switch ($estat){
            case -6:
            case -1: return $builder->count();
            case -2: return $builder->where('estat',-2)->count();
            case -3: return $builder->where('estat',-3)->count();
            case -4: return $builder->where('estat',-4)->count();
            case -5: return $builder->where('estat',-5)->count();
            case -7: return $builder->where('estat',-7)->count();
            default: return 0;
        }
    }

    public static function getIsNotWorkingEstatIndexColor(): string
    {
        return self::getStatColor(self::$NOT_WORKING_INDEX);
    }

    public static function getStatColor($estat){
        switch ($estat){
            //case  0: return "#00000000";
            //case -1: return "#00000000"; // table_archive
            case -2: return "#EB2400"; // table_not_working
            case -3: return "#FFA154"; // table_is_free
            case -4: return "#1CBA38"; // table_on_box
            case -5: return "#3D2ED1"; // table_in_hand
            case -6: return "#525454"; // home_total
            case -7: return "#710C9C"; // table_in_renovation
            case -8: return "#EB7407"; // table_in_temp_return
            default:
                $item = EquStatus::whereId($estat)->first();
                return $item == null ? "#00000000" : $item->color;
        }
    }

    public static function getFailureStateIdFromEstat($estat,$ftid){
        return ($estat == -2 || $estat == -7) ? $ftid : 0;
    }

    public static function getEstatFromEquipment($equ){
        if (EquFavorite::where('eid',$equ->id)->count() > 0){
            return -8;
        }
        if ($equ->archive)     return -1;
        if (!$equ->is_work)    return -2;
        if ($equ->uid  == 0 &&
            $equ->cid  == 0 &&
            $equ->ocid == 0)   return -3;
        if ($equ->cid > 0)     return -4;
        if ($equ->uid > 0)     return -5;
        return 0;
    }

    public static function getStatDescription($estat){
        switch ($estat){
            //case  0: return __('str.table_unknown_status');
            case -1: return __('str.table_archive');
            case -2: return __('str.table_not_working');
            case -3: return __('str.table_is_free');
            case -4: return __('str.table_on_box');
            case -5: return __('str.table_in_hand');
            case -6: return __('str.table_total');
            case -7: return __('str.table_in_renovation');
            case -8: return __('str.table_in_temp_return');
            default: return __('str.table_unknown_status');
        }
    }

    private static function getStatDesFull($estat){
        switch ($estat){
            case -1: return __('str.table_archive_des');
            case -2: return __('str.table_not_working_des');
            case -3: return __('str.table_is_free_des');
            case -4: return __('str.table_on_box_des');
            case -5: return __('str.table_in_hand_des');
            case -6: return __('str.table_total_des');
            case -7: return __('str.table_in_renovation_des');
            case -8: return __('str.table_in_temp_return_des');
            default: return '';
        }
    }

    public function itemsConsumable(){
        return $this->sendResponse(DB::table('etypes')->where('hide',false)->where('is_cons',true)->get(), 'Equ retrieved successfully.');
    }

    public function itemsKits(){
        $items = Etype::where('hide',false)->where('is_comp',true)->get();
        $eTypeIds = array();
        foreach ($items as $item){
            if ($item->comp_data != null){
                $arr = explode(',',$item->comp_data);
                foreach ($arr as $s){
                    if (strlen($s) > 0){
                        $id = intval($s);
                        if (!in_array($id,$eTypeIds)){
                            $eTypeIds[] = $id;
                        }
                    }
                }
            }
        }
        $eTypeArr = array();
        $items2 = Etype::whereIn('id',$eTypeIds)->get();
        foreach ($items2 as $item){
            $eTypeArr[$item->id] = $item;
        }
        $items3 = array();
        for ($i = 0; $i < count($items); $i++){
            if ($items[$i]->comp_data == null || strlen($items[$i]->comp_data) == 0) continue;
            $subItems = array();
            $arr = explode(',', $items[$i]->comp_data);
            foreach ($arr as $id){
                if (strlen($id) > 0){
                    if (array_key_exists($id, $eTypeArr)){
                        $subItems[] = $eTypeArr[$id];
                    }
                }
            }
            if (count($subItems) > 0) {
                $items[$i]->kit_items = $subItems;
                $items3[] = $items[$i];
            }
        }
        return $this->sendResponse($items3, 'equ kit load successfully');
    }

    private function updateTemporaryStatus($isTemporaryReturn,$uid,$eid){
        $equFavorite = EquFavorite::where('uid',$uid)->where('eid',$eid)->first();
        if ($isTemporaryReturn){ // возврат временный
            if ($equFavorite == null){
                EquFavorite::create([
                    'uid' => $uid,
                    'eid' => $eid
                ])->save();
            }else{
                $equFavorite->update(['updated_at' => Carbon::now()]);
            }
        }else{ // возврат не временный
            if ($equFavorite != null){
                $equFavorite->delete();
            }
        }
    }

    private function equUpdate($data){
        if ($data["description"] == null) $data["description"] = "";
        if ($data["label"]       == null) $data["label"]       = "";
        $isTemporaryReturn = 'true' === $data["isTemporaryReturn"];
        unset($data["isTemporaryReturn"]);
        $old_uid = $data["old_uid"];
        unset($data["old_uid"]);
        $isEnableOrionLock = 'true' === $data["isEnableOrionLock"];
        unset($data["isEnableOrionLock"]);
        if (array_key_exists('comp_equ_ft',$data)){
            $comp_equ_ft = $data["comp_equ_ft"];
            unset($data["comp_equ_ft"]);
            foreach ($comp_equ_ft as $key => $value){
                Equipment::whereId(intval($key))->update(['is_work' => false, 'ftid' => intval($value)]);
            }
        }
        $equ = Equipment::where('id',$data["id"])->first();
        $cidBeforeUpdate = $equ->cid;
        $oldWorkState = $equ->is_work;
        $equ->update($data);
        $newWorkState = $equ->is_work;
        $uid = $equ->uid > 0 ? $equ->uid : $old_uid;
        //--------------------------------------------------------------------------------------------------------------
        if ($newWorkState == false && $oldWorkState != $newWorkState){
            BreakingStateController::addNewBreakingStateItem($equ,$uid);
        }
        //--------------------------------------------------------------------------------------------------------------
        $men = $uid > 0 ? Man::where("men.id", $uid)
            ->leftJoin("roles", "roles.id", "=", "men.rid")
            ->select("roles.is_admin","roles.perm_index","men.*")
            ->first() : null;
        $etype = Etype::where('id',$equ->tid)->first();
        //--------------------------------------------------------------------------------------------------------------
        if ($etype->is_cons && $equ->uid > 0){ // расходник удаляется из базы если его берёт сотрудник
            $equ->delete();
            ApiLogController::addLogText('расходник id=' . $equ->id . ' удалён');
        }
        if (!$etype->is_cons){
            if ($equ->uid > 0){ // сотрудник забрал устройство
                EquFavorite::where('uid',$equ->uid)->where('eid',$equ->id)->delete();
                if ($etype->is_comp){
                    $equSubItems = Equipment::where('comp_id',$equ->id)->get();
                    foreach ($equSubItems as $e){
                        EquFavorite::where('uid',$equ->uid)->where('eid',$e->id)->delete();
                    }
                }
            }else{ // сотрудник разместил устройство
                $this->updateTemporaryStatus($isTemporaryReturn, $old_uid, $equ->id);
                if ($etype->is_comp){
                    $equSubItems = Equipment::where('comp_id',$equ->id)->get();
                    foreach ($equSubItems as $e){
                        $this->updateTemporaryStatus($isTemporaryReturn, $old_uid, $e->id);
                    }
                }
            }
        }
        //--------------------------------------------------------------------------------------------------------------
        $is_admin = $men != null && ($men->is_admin || $men->perm_index == RoleController::ADMIN_INDEX ||
                $men->perm_index == RoleController::IT_MEN_INDEX);
        $estat_des_old = $equ->estat_des;
        $estat_color_old = self::getStatColor($equ->estat);
        $estat_old = $equ->estat;
        //--------------------------------------------------------------------------------------------------------------
        if (!$etype->is_cons && $men != null && !$is_admin && $isEnableOrionLock){
            //--- блокировка ориона ------------------------------------------------------------------------------------
            $isBrigadier = $men->perm_index == RoleController::BOX_MANAGER_INDEX;
            $lockState = !$isBrigadier && Equipment::where('uid', $uid)->count() > 0;
            if (strlen($men->raw_label) > 0){
                ApiLogController::addLogText('check orion lock uid=' . $uid . ' state=' . $lockState);
                $orionSendItem = OrionSendItem::firstOrCreate(['raw_label' => $men->raw_label],['blocked' => 2, 'send_state' => 1, 'mlabel' => $men->mlabel]);
                $orionSendItem->update(['blocked' => $lockState, 'send_state' => 0, 'mlabel' => $men->mlabel]);
            }
            //----------------------------------------------------------------------------------------------------------
        }
        //--------------------------------------------------------------------------------------------------------------
        $is_work = $equ->is_work;
        if ($is_admin && !$is_work){
            $is_work = true;
            $estat = -7; // если админ изымает оборудование и оно не рабочее то состояние задаётся как в ремонте
            EquMenRigidLigament::where('eid',$equ->id)->delete(); // и все связки с сотрудниками удаляются
        } else if (!$etype->is_cons && $old_uid > 0 && $isTemporaryReturn){
            $estat = -8;
        } else {
            $estat = self::getEstatFromEquipment($equ);
        }
        $estat_des = self::getStatDescription($estat);
        $equ->update(['estat' => $estat, 'estat_des' => $estat_des,
            'ftid' => self::getFailureStateIdFromEstat($estat,$equ->ftid),
            'estat_old' => $estat_old, 'is_work' => $is_work]);
        $estat_color = self::getStatColor($estat);
        //--- интеграция -----------------------------------------------------------------------------------------------
        if (SapEwmDmHelper::syncEnable() && $men != null && $cidBeforeUpdate != $equ->cid){
            $cid = $equ->cid > 0 ? $equ->cid : $cidBeforeUpdate;
            $cell = Cell::where('cells.id',$cid)
                ->leftJoin("blocks", "blocks.id","=", "cells.block")
                ->select("blocks.st_id","cells.*")
                ->first();
            SapEwmDmHelper::sendMessage($cell->st_id,$men->str_ext_login,$equ->cid == 0,
                $estat_des,$equ->label,true);
        }
        //--------------------------------------------------------------------------------------------------------------
        if ($etype->is_comp){
            $kitItems = Equipment::where('comp_id',$equ->id)->get();
            foreach ($kitItems as $kitItem){
                $kitItem->estat = $equ->estat;
                $kitItem->estat_des = $equ->estat_des;
                $kitItem->estat_old = $equ->estat_old;
                $kitItem->uid = $equ->uid;
                $kitItem->cid = $equ->cid;
                $kitItem->ocid = $equ->ocid;
                $kitItem->is_work = $equ->is_work;
                if ($equ->is_work){
                    $kitItem->ftid = 0;
                }
                $kitItem->update();
            }
        }
        //--------------------------------------------------------------------------------------------------------------
        return array('estat_des' => $estat_des,'estat_color' => $estat_color,
                'estat_des_old' => $estat_des_old,'estat_color_old' => $estat_color_old);
    }

    public function update(Request $request){
        $res = $this->equUpdate($request->all());
        return $this->sendResponse(true, 'equipment update successfully.',$res);
    }

    public function import(Request $request){
        $eArr = $request->items;
        foreach ($eArr as $eItem){
            $equ = Equipment::where('inv_num',$eItem['inv_num'])->first();
            $isNewItem = $equ == null;
            if ($isNewItem){
                $equ = new Equipment();
                $equ->uid       = 0;
                $equ->cid       = 0;
                $equ->tid       = $eItem['tid'];
                $equ->ocid      = 0;
                $equ->inv_num   = $eItem['inv_num'];
                $equ->estat     = -3;
                $equ->estat_old = 0;
                $equ->estat_des = self::getStatDescription(-3);
            }
            $equ->label = $eItem['label'];
            $equ->ser_num = $eItem['ser_num'];
            $equ->is_work = $eItem['is_work'];
            $equ->mac = $eItem['mac'];
            if ($isNewItem){
                $equ->save();
            }else{
                $equ->update();
            }
        }
        return $this->sendResponse(true, 'equipment import successfully.');
    }

    public function kitCreate(Request $request){
        $table_on_box_estat = -4;
        $kitEqu = $this->createEquItem($request->tid,$table_on_box_estat,$request->cid);
        $kitEqu->save();
        Equipment::whereIn('id',$request->items)
            ->update(['estat' => $kitEqu->estat, 'estat_des' => $kitEqu->estat_des,
                'uid' => $kitEqu->uid, 'cid' => $kitEqu->cid, 'comp_id' => $kitEqu->id]);
        $resStat = array('estat_des' => self::getStatDescription($table_on_box_estat),
            'estat_color' => self::getStatColor($table_on_box_estat),
            'estat_des_old' => '','estat_color_old' => '');
        return $this->sendResponse($kitEqu->id, 'equipment kit create successfully.',$resStat);
    }

    private function createEquItem($tid, $estat = -3, $cid = 0, $comp_id = 0,
                                  $label = '', $ser_num = '', $inv_num = '', $mac = '') : Equipment{
        $equ = new Equipment();
        $equ->tid         = $tid;
        $equ->uid         = 0;
        $equ->cid         = $cid;
        $equ->ocid        = 0;
        $equ->label       = $label;
        $equ->comp_id     = $comp_id;
        $equ->description = '';
        $equ->is_work     = true;
        $equ->archive     = false;
        $equ->ser_num     = $ser_num;
        $equ->inv_num     = $inv_num;
        $equ->mark_sid    = 0;
        $equ->mark_bid    = 0;
        $equ->ftid        = 0;
        $equ->mac         = $mac;
        $equ->estat       = $estat;
        $equ->estat_old   = 0;
        $equ->estat_des   = self::getStatDescription($estat);
        return $equ;
    }

    public function create(Request $request){
        $count = $request->count != null ? $request->count : 1;
        $data = $request->all();
        $defEstat    = -4;
        $defOldEstat = -3;
        $estat_des = self::getStatDescription($defEstat);
        $estat_color = self::getStatColor($defEstat);
        $estat_des_old = self::getStatDescription($defOldEstat);
        $estat_color_old = self::getStatColor($defOldEstat);
        for($i = 0;$i < $count; $i++){
            $this->createEquItem($data["tid"],$defEstat,$data["cid"])->save();
        }
        return $this->sendResponse(true, 'equipment create successfully.',
            array('estat_des' => $estat_des,'estat_color' => $estat_color,
            'estat_des_old' => $estat_des_old,'estat_color_old' => $estat_color_old));
    }

    public function createFromWeb(Request $request){
        $equipment = new Equipment();
        $equipment->tid  = $request->input("tid");
        $equipment->uid  = 0;
        $equipment->cid  = 0;
        $equipment->ocid        =     0;
        $equipment->description =    '';
        $equipment->is_work     =  true;
        $equipment->archive     = false;
        $equipment->estat       =    -3;
        $equipment->estat_old   =     0;
        $equipment->estat_des   = self::getStatDescription(-3);
        $equipment->mark_sid    = $request->input("mark_sid") != null ? $request->input("mark_sid") : 0;
        $equipment->mark_bid    = $request->input("mark_bid") != null ? $request->input("mark_bid") : 0;
        $equipment->mac         = $request->input("mac")     != null ? $request->input("mac"    ) : '';
        $equipment->inv_num     = $request->input("inv_num") != null ? $request->input("inv_num") : '';
        $equipment->label       = $request->input("label")   != null ? $request->input("label"  ) : '';
        $equipment->ser_num     = $request->input("ser_num") != null ? $request->input("ser_num") : '';
        if (strlen($equipment->label) > 0){
            $labelIsUnique = Equipment::where('label',$equipment->label)->first() == null;
            if (!$labelIsUnique){
                return "Метка должна быть пустой либо уникальной";
            }
        }
        if (strlen($equipment->ser_num) > 0){
            $serIsUnique = Equipment::where('ser_num',$equipment->ser_num)->first() == null;
            if (!$serIsUnique){
                return "Серийник должен быть пустым либо уникальным";
            }
        }
        $equipment->save();
        ApiLogController::addLogText('Создано устройство метка: <a href="/events/' . $equipment->id . '">' .
            $equipment->label . '</a> id=' .
            $equipment->id
            . ', Пользователь: ' .
            '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>',0,
            auth()->user()->mid(),0,$equipment->id,0,0,$equipment->estat_des,"",$equipment->estat_des,"");
        return 'true';
    }

    public function editFromWeb(Request $request){
        $equipment = Equipment::where('id', $request->input("id"))->first();
        if ($equipment == null){
            return "Не найдено оборудование с таким ИД";
        }
        $arcStr = "";
        $equipment->mac         = $request->input("mac")         != null ? $request->input("mac"    ) : "";
        $equipment->label       = $request->input("label")       != null ? $request->input("label"  ) : "";
        $equipment->ser_num     = $request->input("ser_num")     != null ? $request->input("ser_num") : "";
        $equipment->inv_num     = $request->input("inv_num")     != null ? $request->input("inv_num") : "";
        $equipment->description = $request->input("description") != null ? $request->input("description") : "";
        $equipment->is_work     = $request->input("is_work");
        if ($request->input("archive") && !$equipment->archive){
            $arcStr = " устройство отправлено в архив (списано)";
        }
        $equipment->archive     = $request->input("archive") != null ? $request->input("archive") : false;
        if ($request->tid != null){
            $equipment->tid = $request->tid;
        }
        if ($equipment->archive){
            EquMenRigidLigament::where('eid',$equipment->id)->delete();
            $equipment->label = "";
        }
        if (strlen($equipment->label) > 0){
            $labelIsUnique = Equipment::where('label',$equipment->label)->where('id','!=',$request->input("id"))->first() == null;
            if (!$labelIsUnique){
                return "Метка должна быть пустой либо уникальной";
            }
        }
        if (strlen($equipment->ser_num) > 0){
            $serIsUnique = Equipment::where('ser_num',$equipment->ser_num)->where('id','!=',$request->input("id"))->first() == null;
            if (!$serIsUnique){
                return "Серийник должен быть пустым либо уникальным";
            }
        }
        $equipment->ftid = self::getFailureStateIdFromEstat($equipment->estat,$equipment->ftid);
        $equipment->mark_sid    = $request->input("mark_sid") != null ? $request->input("mark_sid") : 0;
        $equipment->mark_bid    = $request->input("mark_bid") != null ? $request->input("mark_bid") : 0;
        //$equipment->estat_old   = $equipment->estat;
        //$equipment->estat       = self::getEstatFromEquipment($equipment);
        //$equipment->estat_des   = self::getStatDescription($equipment->estat);
        $st = $request->input("estat");
        if ($st != null && $equipment->estat != $st){
            $equStatus = $st > 0 ? EquStatus::whereId($st)->first() : null;
            $descr = $equStatus == null ? ApiEquipmentController::getStatDescription($st) : $equStatus->descr;
            $estat_des_old = $equipment->estat_des;
            $equipment->estat_old = $equipment->estat;
            $equipment->estat = $st;
            $equipment->estat_des = $descr;
            if ($equipment->estat == -7){
                EquMenRigidLigament::where('eid',$equipment->id)->delete();
            }
            ApiLogController::addLogText('Обновлён статус устройства, метка: <a href="/events/' . $equipment->id . '">' .
                $equipment->label . '</a> id=' .
                $equipment->id
                . ', Пользователь: ' .
                '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>',ApiLogController::CHANGE_EQU_STATUS_FAKE_BID_ID,
                auth()->user()->mid(),0,$equipment->id,0,0,$equipment->estat_des,"",$estat_des_old,"");
        }
        $equipment->update();
        ApiLogController::addLogText('Обновлено устройство метка: <a href="/events/' . $equipment->id . '">' .
            $equipment->label . '</a> id=' .
            $equipment->id
            . ', Пользователь: ' .
            '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>' . $arcStr,0,
            auth()->user()->mid(),0,$equipment->id,0,0,$equipment->estat_des,"",$equipment->estat_des,"");
        return 'true';
    }

    public function itemsByIds(Request $request){
        $equipment = DB::table('equipment')->where('archive',false)->whereIn('equipment.cid', $request->input("ids"))
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin('cell_sizes','etypes.csid','=','cell_sizes.id')
            ->select("cell_sizes.name as cs_name","cell_sizes.des as cs_des","etypes.tname","etypes.csid","etypes.img",
                        "etypes.tmark","etypes.tmodel","etypes.is_cons","equipment.*")->get();
        $array = $equipment->map(function($obj){
            return (array) $obj;
        })->toArray();
        return $this->sendResponse(JsonResource::collection($array), 'Cells retrieved successfully.');
    }

    public function itemByLabel($elabel){
        $equipment = DB::table('equipment')->where('archive',false)->where('equipment.label', $elabel)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin('cell_sizes','etypes.csid','=','cell_sizes.id')
            ->select("cell_sizes.name as cs_name","cell_sizes.des as cs_des","etypes.tname","etypes.csid",
                        "etypes.img","etypes.tmark","etypes.tmodel","etypes.is_cons","equipment.*")->get();
        $array = $equipment->map(function($obj){
            return (array) $obj;
        })->toArray();
        return $this->sendResponse(JsonResource::collection($array), 'Cells retrieved successfully.');
    }

    public function itemByUser($uid){
        $equipment = DB::table('equipment')->where('archive',false)->where('equipment.uid', $uid)
            ->leftJoin("etypes", "etypes.id", "=", "equipment.tid")
            ->leftJoin('cell_sizes','etypes.csid','=','cell_sizes.id')
            ->select("cell_sizes.name as cs_name","cell_sizes.des as cs_des","etypes.tname","etypes.csid","etypes.img",
                        "etypes.tmark","etypes.tmodel","etypes.is_cons","equipment.*")->get();
        $array = $equipment->map(function($obj){
            return (array) $obj;
        })->toArray();
        return $this->sendResponse(JsonResource::collection($array), 'Cells retrieved successfully.');
    }

    public function itemByUserIgnoreCons($uid){
        $equipment = DB::table('equipment')->where('equipment.archive',false)
            ->where(['equipment.uid' => $uid, 'etypes.is_cons' => false])
            ->leftJoin("etypes", "etypes.id", "=", "equipment.tid")
            ->leftJoin('cell_sizes','etypes.csid','=','cell_sizes.id')
            ->select("cell_sizes.name as cs_name","cell_sizes.des as cs_des","etypes.tname","etypes.csid","etypes.img",
                        "etypes.tmark","etypes.tmodel","etypes.is_cons","equipment.*")->get();
        $array = $equipment->map(function($obj){
            return (array) $obj;
        })->toArray();
        return $this->sendResponse(JsonResource::collection($array), 'Cells retrieved successfully.');
    }

}
