<?php

namespace App\Http\Controllers;

use App\DataTables\MenMessageDataTable;
use App\DataTables\TerminalMessagesDataTable;
use App\Models\Equipment;
use App\Models\FbToken;
use App\Models\Log;
use App\Models\Man;
use App\Models\TerminalMessage;
use Illuminate\Http\Request;

class TerminalMessageController extends BaseController
{
    public function index(TerminalMessagesDataTable $dataTable)
    {
        return $dataTable->render('terminal-messages');
    }

    public function tmMen(MenMessageDataTable $dataTable)
    {
        return $dataTable->render('men-message');
    }

    public function tmCreate(TerminalMessagesDataTable $dataTable, $tid){
        $terminal = Equipment::where('id', $tid)->first();
        if ($terminal == null){
            return abort(404);
        }
        $dataTable->aid = $tid;
        return $dataTable->render('tm-create',["tid" => $terminal->id, "name" => $terminal->name]);
    }

    public function tmActionSend(Request $request){
        $request->validate([
            "body" => "required|min:1|max:255",
        ]);
        $idMenArr = explode(",",$request->input("ids"));
        $arr = array();
        foreach ($idMenArr as $idStr){
            if (!in_array(intval($idStr),$arr)){
                $arr[] = intval($idStr);
            }
        }
        $equipments = Equipment::whereIn('uid',$arr)->where('bat',">=",0)->get();
        Log::create(['body' => 'count send equipment = ' . count($equipments) . ' ids=' . $request->input("ids")]);
        $title = $request->input("title") == null ? "" : $request->input("title");
        foreach ($equipments as $equ){
            $tm        = new TerminalMessage();
            $tm->aid   = $equ->id;
            $tm->title = $title;
            $tm->body  = $request->input("body");
            $tm->save();
            $fbToken = FbToken::where('aid',$tm->aid)->first();
            if ($fbToken != null){
                $sendRes = FbTokenController::sendNotification($fbToken->fbs_token,$tm->title,$tm->body,$tm->id);
                Log::create(['body' => 'sendRes = ' . $sendRes]);
            }else{
                Log::create(['body' => 'sendRes = token for ' . $tm->aid . ' not found']);
            }
        }
        return redirect()->route("tm-men");
    }

    public function tmAction(Request $request){
        $request->validate([
            "body" => "required|min:1|max:255",
        ]);
        $tm        = new TerminalMessage();
        $tm->aid   = $request->input("tid");
        $tm->title = $request->input("title") == null ? "" : $request->input("title");
        $tm->body  = $request->input("body");
        $tm->save();
        $fbToken = FbToken::where('aid',$tm->aid)->first();
        if ($fbToken != null){
            $sendRes = FbTokenController::sendNotification($fbToken->fbs_token,$tm->title,$tm->body,$tm->id);
            Log::create(['body' => 'sendRes = ' . $sendRes]);
        }else{
            Log::create(['body' => 'sendRes = token for ' . $tm->aid . ' not found']);
        }
        return redirect()->route("equipment");
    }

    public function tmMenActionAll(){
        $mens = Man::where("equipment.uid",">",0)
            ->where("equipment.bat",">=",0)
            ->leftJoin("equipment", "men.id","=", "equipment.uid")
            ->select("men.*","equipment.label")->get();
        $ids = "";
        foreach ($mens as $men){
            if (strlen($ids) > 0) $ids .= ',';
            $ids .= $men->id;
        }
        if (strlen($ids) == 0){
            return redirect()->route("tm-men");
        }
        return view('group-message-create',['ids' => $ids]);
    }

    public function tmMenActionSel($ids){
        return view('group-message-create',['ids' => $ids]);
    }
}
