<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Models\CustomData;
use App\Models\Man;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class UserController extends Controller
{

    public function confirmUser($hash){
        $users = User::all();
        foreach ($users as $user){
            if ($user->compareSecretHasFromLoginAndThisUser($hash) === true){
                $user->is_confirm = true;
                $user->email_verified_at = Carbon::now();
                $user->update();
            }
        }
        return redirect()->route("home");
    }

    public function checkUserAuth(Request $request){
        if ($request->email == null){
            return "неправильный запрос";
        }
        $user = User::where('email',$request->email)->first();
        if ($user == null){
            return "пользователь не найден";
        }
        if ($user->id > 5 && !$user->is_confirm){
            return "пользователь ещё не подтвердил почту";
        }
        //if (!$user->enableAuthWeb()){
        //    return "Присвоенная роль не имеет доступа к личному кабинету, обратитесь к администратору.";
        //}
        return "true";
    }

    public function index(UserDataTable $dataTable)
    {
        $this->middleware('auth');
        return $dataTable->render('users');
    }

    public function remove($id)
    {
        $this->middleware('auth');
        Man::where('web_uid',$id)->update(['web_uid' => 0]);
        User::whereId($id)->delete();
        ApiLogController::addLogText('удаление сотрудника по id ' . $id);
        return redirect()->route("project-settings2");
    }

    public function changeRegLockMode(){
        $this->middleware('auth');
        $cd = CustomData::where('label','reg')->first();
        if ($cd != null){
            if ($cd['data'] == 'true'){
                $cd['data'] = 'false';
            }else{
                $cd['data'] = 'true';
            }
            $cd->update();
        }else{
            $cd = new CustomData();
            $cd->label = "reg";
            $cd->data = "false";
            $cd->save();
        }
        return redirect()->route("project-settings2");
    }

    public function thema(){
        $user = Auth::user();
        if($user->thema){
            $user->thema = false;
        }
        else{
            $user->thema = true;
        }
        $user->save();
        return redirect()->back();
    }
}
