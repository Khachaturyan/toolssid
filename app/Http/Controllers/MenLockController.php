<?php

namespace App\Http\Controllers;

use App\Imports\MenImport;
use App\Models\EquFavorite;
use App\Models\Equipment;
use App\Models\Man;
use App\Models\MenLock;
use Illuminate\Http\Request;

class MenLockController extends Controller
{

    public static function getLockStatus($uid) : bool {
        $items = Equipment::where('uid',$uid)->where('archive',false)->get();
        $count = 0;
        foreach ($items as $item){
            if (EquFavorite::where('eid',$item->id)->where('uid',$uid)->first() == null){
                $count++;
                break;
            }
        }
        return $count > 0;
    }

    public static function updateLockStatusAsyncRequest(){
        $url = "http://127.0.0.1/api/update-lock-status";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function updateLockStatus(){
        if (request()->ip() == "127.0.0.1"){
            MenLock::query()
                ->leftJoin("men", "men.id","=", "men_locks.mid")
                ->leftJoin("roles", "roles.id","=", "men.rid")
                ->where("men.fired",1)
                ->delete();
            $manArr = Man::where('men.fired','!=',1)
                ->leftJoin("roles", "roles.id","=", "men.rid")
                ->select("men.*","roles.perm_index")
                ->get();
            foreach ($manArr as $men){
                $menLockItem = MenLock::firstOrCreate(['mid' => $men->id],['label' => '' , 'lock' => false]);
                $menLockItem->label = MenImport::staticArmtekReverseConvertCard($men->mlabel);
                $dl = RoleController::disableLock($men->perm_index);
                if ($dl){
                    $menLockItem->lock = false;
                }else{
                    $menLockItem->lock = self::getLockStatus($men->id);
                }
                $menLockItem->update();
            }
        }
        return "ok.";
    }
}
