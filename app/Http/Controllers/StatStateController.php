<?php

namespace App\Http\Controllers;

use App\Models\Equipment;
use App\Models\Stage;
use App\Models\StatState;
use Carbon\Carbon;

class StatStateController extends Controller
{
    public static function checkStatStateAsyncRequest(){
        $url = "http://127.0.0.1/api/check-stat-state";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function checkStatState()
    {
        if (request()->ip() == "127.0.0.1") {
            $statStateUpdateTime = intval(ProjectSettingsController::getSettings('ss_update_time',0)->data);
            $LOAD_TYPES_PERIOD_UPDATE = 600;
            if ($statStateUpdateTime + $LOAD_TYPES_PERIOD_UPDATE < Carbon::now()->timestamp){
                self::update();
                ProjectSettingsController::getSettings('ss_update_time',0)->update(['data' => Carbon::now()->timestamp]);
            }
        }
        return "ok.";
    }

    public static function update(){
        $time = Carbon::now();
        $item              = new StatState();
        $item->sid         = 0;
        $item->bid         = 0;
        $item->full        = Equipment::where('archive',false)->count();
        $item->not_working = Equipment::where('archive',false)->where('estat',ApiEquipmentController::$NOT_WORKING_INDEX)->count();
        $item->free        = Equipment::where('archive',false)->where('estat',ApiEquipmentController::$IS_FREE_INDEX)->count();
        $item->in_hand     = Equipment::where('archive',false)->where('estat',ApiEquipmentController::$IN_HAND_INDEX)->count();
        $item->in_cell     = Equipment::where('archive',false)->where('estat',ApiEquipmentController::$IN_BOX_INDEX)->count();
        $item->created_at  = $time;
        $item->updated_at  = $time;
        $item->save();
        $stages = Stage::where('is_arc',false)->get();
        foreach ($stages as $stage){
            $item              = new StatState();
            $item->sid         = $stage->id;
            $item->bid         = 0;
            $item->full        = Equipment::where('archive',false)
                ->where('mark_sid',$stage->id)
                ->count();
            $item->not_working = Equipment::where('archive',false)
                ->where('mark_sid',$stage->id)
                ->where('estat',ApiEquipmentController::$NOT_WORKING_INDEX)
                ->count();
            $item->free        = Equipment::where('archive',false)->where('mark_sid',$stage->id)
                ->where('estat',ApiEquipmentController::$IS_FREE_INDEX)
                ->count();
            $item->in_hand     = Equipment::where('archive',false)
                ->where('mark_sid',$stage->id)
                ->where('estat',ApiEquipmentController::$IN_HAND_INDEX)
                ->count();
            $item->in_cell     = Equipment::where('archive',false)
                ->where('mark_sid',$stage->id)
                ->where('estat',ApiEquipmentController::$IN_BOX_INDEX)
                ->count();
            $item->created_at  = $time;
            $item->updated_at  = $time;
            $item->save();
        }
    }

    function itemsLoad($ds,$de,$sid){
        $ds = $ds == "0" ? null : $ds . "T00:00:00";
        $de = $de == "0" ? null : $de . "T23:59:59";
        $builder = StatState::query();
        if ($ds != null){
            $builder = $builder->where('stat_states.created_at','>=',$ds);
        }
        if ($de != null){
            $builder = $builder->where('stat_states.created_at','<=',$de);
        }
        $builder = $builder->where('stat_states.sid',$sid);
        $items = $builder->get();
        return json_encode($items);
    }
}
