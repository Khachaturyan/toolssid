<?php

namespace App\Http\Controllers;

use App\Imports\MenImport;
use App\Models\AlertMailPersonal;
use App\Models\BreakingState;
use App\Models\CustomData;
use App\Models\Equipment;
use App\Models\AlertMail;
use App\Models\LogTakeItem;
use App\Models\Man;
use App\Models\MenCellLink;
use App\Orion\OrionClient;
use DB;
use Illuminate\Http\Request;
use App\Parsec\ParsecSoapEmulateClient;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;

class ApiManController extends BaseController
{
    //------------------------------------------------------------------------------------------------------------------
    public static function strIdsToArray($ids) : array{
        $arr = array();
        $arr2 = explode(',',$ids);
        foreach ($arr2 as $strId){
            if (strlen($strId) > 0){
                $arr[] = intval($strId);
            }
        }
        return $arr;
    }
    public static function idsArrayToStr($arr) : string{
        return implode(',', $arr);
    }
    //------------------------------------------------------------------------------------------------------------------


    public static function updateParsec() : string{
        $client = new ParsecSoapEmulateClient(
            'http://' . CustomData::firstOrCreate(['label' => 'prs_host'],['data' => ''])->data . ':' .
            CustomData::firstOrCreate(['label' => 'prs_port'],['data' => ''])->data . '/IntegrationService/IntegrationService.asmx?WSDL',
            CustomData::firstOrCreate(['label' => 'prs_dom'],['data' => ''])->data,
            CustomData::firstOrCreate(['label' => 'prs_opr'],['data' => ''])->data,
            CustomData::firstOrCreate(['label' => 'prs_pas'],['data' => ''])->data);
        if (!$client->isConnected()) {
            return "false - " . $client->lastResponse;
        }
        $units = $client->GetOrgUnitsHierarhyWithPersons();
        $client->close();
        if ($units == null){
            return 'false - ' . $client->lastResponse;
        }
        $menImport = new MenImport();
        $mid = array();
        foreach ($units as $men){
            echo json_encode($men) . "<br>";
            $fm = is_string($men['FIRST_NAME' ]) ? $men['FIRST_NAME' ] : "";
            $lm = is_string($men['LAST_NAME'  ]) ? $men['LAST_NAME'  ] : "";
            $mm = is_string($men['MIDDLE_NAME']) ? $men['MIDDLE_NAME'] : "";
            $isHex = "1" == CustomData::firstOrCreate(['label' => 'prs_hex' ],['data' => ''])->data;
            $code = $isHex ? $menImport->parsecConvertCardNumber($men['code']) : $men['code'];
            $newMen = $menImport->parseFormat('сотрудник',$men['ID'],$fm,$lm,$mm,$code,$men['code'],0,'');
            if ($newMen != null){
                $newMen->save();
                $updatingMenId = $newMen->id;
            }else{
                $updatingMenId = $menImport->updatingMenId;
            }
            $mid[] = $updatingMenId;
        }
        ApiLogController::addLogText('Обновление парсека, количество юнитов:' . count($units));
        $menImport->moveToFiredMid($mid,true);
        ProjectSettingsController::getSettings('prs_update_time',0)->update(['data' => Carbon::now()->timestamp]);
        return "";
    }

    private static function checkLab($label): bool
    {
        $count = Man::where("mlabel",$label)->count();
        return $count > 0 ? false : Equipment::where("label",$label)->count() == 0;
    }

    public static function staticGenFreeLabel() : string{
        $res = null;
        while(!$res || !self::checkLab($res)){
            $res = str_pad(random_int(1,999999), 6, '0', STR_PAD_LEFT);
        }
        return $res;
    }

    public function genLabel(): string
    {
        return self::staticGenFreeLabel();
    }

    public function sevenSealsImport(Request $request){
        CustomData::firstOrCreate(['label' => 'seven_seals_dsn'],['data' => ''])
            ->update(['data' =>  $request->seven_seals_dsn != null ? $request->seven_seals_dsn : '']);
        CustomData::firstOrCreate(['label' => 'seven_seals_user'],['data' => ''])
            ->update(['data' => $request->seven_seals_user != null ? $request->seven_seals_user : '']);
        CustomData::firstOrCreate(['label' => 'seven_seals_pass'],['data' => ''])
            ->update(['data' =>  $request->seven_seals_pass != null ? $request->seven_seals_pass : '']);
        CustomData::firstOrCreate(['label' => 'seven_seals_sync_en'],['data' => 0])
            ->update(['data' =>  $request->seven_seals_sync_en != null ? $request->seven_seals_sync_en : 0]);
        return redirect()->route("sync");
    }

    public function orionImport(Request $request){
        CustomData::firstOrCreate(['label' => 'ori_card_lock'],['data' => 0])->update(['data' =>  $request->ori_card_lock != null ? $request->ori_card_lock : 0]);
        CustomData::firstOrCreate(['label' => 'orion_ip'],['data' => ''])->update(['data' => $request->ori_host != null ? $request->ori_host : '']);
        CustomData::firstOrCreate(['label' => 'orion_port'],['data' => ''])->update(['data' =>  $request->ori_port != null ? $request->ori_port : '']);
        CustomData::firstOrCreate(['label' => 'ori_sync_time'],['data' => ''])->update(['data' =>  $request->orion_sync_time != null ? $request->orion_sync_time : 0]);
        AndCommController::sendUpdateComAll();
        $str = OrionClient::toImportLogResult($request->ori_host,$request->ori_port);
        ApiLogController::addLogText($str);
        return redirect()->route("mans");
    }

    public function parsecImport(Request $request){
        $prs_dom = $request->prs_dom != null ? $request->prs_dom : "";
        CustomData::firstOrCreate(['label' => 'prs_dom' ],['data' => ''])->update(['data' => $prs_dom             ]);
        CustomData::firstOrCreate(['label' => 'prs_host'],['data' => ''])->update(['data' => $request->prs_host   ]);
        CustomData::firstOrCreate(['label' => 'prs_port'],['data' => ''])->update(['data' => $request->prs_port   ]);
        CustomData::firstOrCreate(['label' => 'prs_opr' ],['data' => ''])->update(['data' => $request->prs_opr    ]);
        CustomData::firstOrCreate(['label' => 'prs_pas' ],['data' => ''])->update(['data' => $request->prs_pas    ]);
        CustomData::firstOrCreate(['label' => 'prs_hex' ],['data' => ''])->update(['data' => "1" == $request->hex ]);
        CustomData::firstOrCreate(['label' => 'prs_sync_time' ],['data' => 0])
            ->update(['data' => $request->parsec_sync_time != null ? $request->parsec_sync_time : 0 ]);
        $parsecSyncInProgress = ProjectSettingsController::getSettings('prs_progress',false);
        if (!$parsecSyncInProgress->data){
            $parsecSyncInProgress->update(['data' => true]);
            $updateRes = self::updateParsec();
            $str = "Обновление данных Parsec прошло " . (strlen($updateRes) == 0 ? 'успешно' : 'с ошибкой: ' . $updateRes);
            ApiLogController::addLogText($str);
            $parsecSyncInProgress->update(['data' => false]);
        }else{
            return 'Обновление данных парсека уже в процессе и не может прерываться';
        }
        return 'true';
    }

    public function show($mlabel)
    {
        $man = DB::table('men')->where('men.mlabel', "=", $mlabel)
            ->leftJoin("roles", "roles.id", "=", "men.rid")
            ->select("roles.etypes","roles.is_admin","roles.rname","men.*")->first();
        return $this->sendResponse($man, 'Man retrieved successfully.');
    }

    public function menById($id)
    {
        $man = DB::table('men')->where("men.id",$id)
            ->leftJoin("roles", "roles.id", "=", "men.rid")
            ->select("roles.etypes","roles.is_admin","roles.rname","men.*")->first();
        return $this->sendResponse($man, 'Man retrieved successfully.');
    }

    public function menByPin($pin){
        $man = DB::table('men')->where('pin',$pin)
            ->leftJoin("roles", "roles.id", "=", "men.rid")
            ->select("roles.etypes","roles.is_admin","roles.rname","men.*")->first();
        return $this->sendResponse($man, 'Man retrieved successfully.');
    }

    public function revertState(Request $request){
        $men = Man::whereId($request->id)->first();
        if ($men == null){
            abort(404);
        }
        $men->can_dia = $men->can_dia == 1 ? 0 : 1;
        $men->update();
        return "true";
    }

    public function manAction(Request $request){
        $res = HomeController::createUpdateMen($request->id,$request->rid,$request->pin,$request->mname,$request->msurname,
            $request->mpatronymic,$request->fired,$request->mlabel, $request->post, $request->str_ext_id,
            $request->can_dia, $request->str_ext_login,null);
        if ($res[0] === -1){
            return "update unknown id men";
        } else if ($res[0] === -2){
            return "duplicate label";
        } else {
            return is_array($res) ? "true" : "false";
        }
    }

    public function deleteByLabel($label){
        ApiLogController::addLogText('удаление сотрудников по метке ' . $label);
        return Man::where('mlabel',$label)->delete();
    }

    public function menWithEquResp(){
        return $this->sendResponse($this->menWithEqu(), 'Man retrieved successfully.');
    }

    public function menWithEqu(){
        $equ = Equipment::where('uid','>',0)->get();
        $arr0 = array();
        $arr1 = array();
        $arr2 = array();
        foreach ($equ as $e) {
            $arr0[] = $e->id;
            $arr1[] = $e->uid;
            $arr2[$e->id] = $e;
        }
        $mens = Man::whereIn('id',$arr1)->get();
        $arr3 = array();
        for($i = 0; $i < count($mens); $i++){
            $arr3[$mens[$i]->id] = $mens[$i];
        }
        foreach ($arr0 as $id){
            $arr3[$arr2[$id]->uid]['equ'] = $arr2[$id];
        }
        return $mens;
    }

    public function menDataForEqu($eid){
        $rawSubQuery = DB::raw("CASE
                                    WHEN (SELECT count(*) FROM equ_men_rigid_ligaments WHERE eid='$eid' AND uid=men.id) > 0 THEN 1
                                    ELSE 0
                                END AS is_checked");
        $query = Man::query()
            ->where("men.fired",'!=', 1)
            ->select('men.*',$rawSubQuery)
            ->orderByDesc('is_checked')
            ->orderBy('men.msurname');
        //ApiLogController::addLogText($query->toSql());
        return Datatables::of($query)
            ->editColumn('is_checked', function ($row) use ($eid){
                return $row->is_checked ? '<img src="/img/done_green_24dp.svg" style="width : 24px; height: 24px; padding: 6px"/>' : "";
            })
            ->rawColumns(['is_checked'])
            ->make(true);
    }

    public function menDataForCell($cid){
        $query = Man::query()
            ->where("men.fired",'!=', 1)
            ->orderByDesc(MenCellLink::selectRaw('count(*)')->whereColumn('mid', 'men.id')->where('cid',$cid))
            ->orderBy('men.msurname')
            ->select('men.*');
        return Datatables::of($query)
            ->addColumn('is_checked', function ($row) use ($cid){
                $isChecked = MenCellLink::where('mid',$row->id)->where('cid',$cid)->first() != null;
                return $isChecked ? '<img src="/img/done_green_24dp.svg" style="width : 24px; height: 24px; padding: 6px"/>' : "";
            })
            ->rawColumns(['is_checked'])
            ->make(true);
    }

    public function menForAlertPerson(){
        $query = Man::query()
            ->leftJoin("users", "users.id","=", "men.web_uid")
            ->where("men.fired",'!=', 1);
        $query = $query->orderByDesc(AlertMailPersonal::selectRaw('count(*)')->whereColumn('uid', 'men.web_uid'))
            ->select('men.*','users.email','users.id as uid');
        return Datatables::of($query)
            ->addColumn('time_des', function ($row){
                $items = AlertMailPersonal::where('uid',$row->uid)->orderBy('time')->get();
                $str = '';
                foreach ($items as $item){
                    if (strlen($str) > 0) $str .= '<br>';
                    $str .= AlertMailPersonalController::intTimeToStr($item->time);
                }
                return $str;
            })
            ->rawColumns(['time_des'])
            ->make(true);
    }

    public function menForAlertEstate($estate){
        $arr = AlertMail::where('alert_mails.estate','like', '%' . $estate . '%')
            ->leftJoin('users', 'users.email','=', 'alert_mails.mail')
            ->leftJoin('men', 'users.id','=', 'men.web_uid')
            ->pluck('men.id')->toArray();
        $ids = self::idsArrayToStr($arr);
        $query = Man::query()
            ->leftJoin("users", "users.id","=", "men.web_uid")
            ->where("men.fired",'!=', 1);
        if (strlen($ids) > 0){
            $query = $query->orderByRaw("FIELD(men.id, $ids) DESC");
        }
        $query = $query->orderBy('men.msurname')
            ->select('men.*','users.email');
        return Datatables::of($query)
            ->addColumn('is_checked', function ($row) use ($arr){
                //if (true) return $row->id . ' ' . (in_array($row->id,$arr) ? 'true' : 'false') . ' ' . self::idsArrayToStr($arr);
                $isChecked =  in_array($row->id,$arr);
                return $isChecked ? '<img src="/img/done_green_24dp.svg" style="width : 24px; height: 24px; padding: 6px"/>' : "";
            })
            ->rawColumns(['is_checked'])
            ->make(true);
    }

    public function hardworkingMenData($periodIndex,$sid){
        $SEC_IN_DAY = 86400;
        switch ($periodIndex){
            case 3:
                $periodSec = $SEC_IN_DAY;
                break;
            case 4:
                $periodSec = $SEC_IN_DAY * 7;
                break;
            case 5:
            default:
                $periodSec = $SEC_IN_DAY * 30;
                break;
        }
        $time = Carbon::now()->addSeconds(-$periodSec);
        //--------------------------------------------------------------------------------------------------------------
        $logQueryBuilder = LogTakeItem::where('log_take_items.created_at','>',$time);
        if ($sid > 0){
            $logQueryBuilder = $logQueryBuilder->leftJoin("equipment", "equipment.id","=", "log_take_items.eid")
                ->where("equipment.mark_sid",$sid);
        }
        $ids = $logQueryBuilder->select('mid')
            ->distinct()
            ->pluck('mid');
        $str1 = $sid > 0 ? " left join equipment on equipment.id = log_take_items.eid " : "";
        $str2 = $sid > 0 ? " and equipment.mark_sid = $sid " : "";
        $countQuery = DB::raw("(SELECT count(*) FROM log_take_items $str1 WHERE log_take_items.mid=men.id and log_take_items.created_at > '$time' $str2) as cnt");
        $query = Man::whereIn('id',$ids)->select('men.*',$countQuery)->orderBy('cnt', 'desc');
        return Datatables::of($query)
            ->editColumn('msurname', function ($row) {
                return $row->msurname . " " . $row->mname . " " . $row->mpatronymic;
            })
            ->make(true);
    }

    public function breakingMenData($periodIndex,$sid){
        $SEC_IN_DAY = 86400;
        $periodSec = $periodIndex == 4 ? $SEC_IN_DAY * 7 : $SEC_IN_DAY * 30;
        $time = Carbon::now()->addSeconds(-$periodSec);
        $builder = BreakingState::where('breaking_states.created_at','>',$time);
        if ($sid > 0){
            $builder = $builder->leftJoin("equipment", "equipment.id","=", "breaking_states.eid")->where("equipment.mark_sid",$sid);
        }
        $ids = $builder->select('mid')
            ->distinct()
            ->pluck('mid');
        $str1 = $sid > 0 ? " left join equipment on equipment.id = breaking_states.eid " : "";
        $str2 = $sid > 0 ? " and equipment.mark_sid = $sid " : "";
        $countQuery = DB::raw("(SELECT count(*) FROM breaking_states $str1 WHERE mid=men.id and breaking_states.created_at > '$time' $str2) as cnt");
        $query = Man::whereIn('id',$ids)->select('men.*',$countQuery)->orderBy('cnt', 'desc');
        return Datatables::of($query)
            ->editColumn('msurname', function ($row) {
                return $row->msurname . " " . $row->mname . " " . $row->mpatronymic;
            })
            ->make(true);
    }

    public function menSelect($sel_data){

        if (strlen($sel_data) > 1){
            $sel_data = substr($sel_data, 1, -1);
            $rawSubQuery = DB::raw("CASE
                                        WHEN id IN ($sel_data) THEN 1
                                        ELSE 0
                                    END AS is_checked");
        }else{
            $rawSubQuery = DB::raw("'0' AS is_checked");
        }
        $query = Man::query()
            ->where("men.fired",'!=', 1)
            ->select('men.*',$rawSubQuery)
            ->orderByDesc('is_checked')
            ->orderBy('men.msurname');
        return Datatables::of($query)
            ->editColumn('is_checked', function ($row) use ($sel_data){
                return $row->is_checked === 1 ?
                    '<img src="/img/done_green_24dp.svg" style="width : 24px; height: 24px; padding: 6px"/>' : "";
            })
            ->rawColumns(['is_checked'])
            ->make(true);
    }
}
