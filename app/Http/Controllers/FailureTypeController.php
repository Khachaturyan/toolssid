<?php

namespace App\Http\Controllers;

use App\DataTables\FailureTypeDataTable;
use App\Models\Block;
use App\Models\Cell;
use App\Models\FailureType;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FailureTypeController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(FailureTypeDataTable $dataTable)
    {
        $dataTable->showArchive = false;
        return $dataTable->render('failure-type');
    }

    public function showArchive(FailureTypeDataTable $dataTable)
    {
        $dataTable->showArchive = true;
        return $dataTable->render('failure-type');
    }

    public function action(Request $request){
        if (FailureType::where('name',$request->input("name"))->count() == 0){
            if ($request->input("id") == null){
                $item = new FailureType();
                $item->name = $request->input("name");
                $item->save();
                ApiLogController::addLogText('Создан тип поломки "' . $item->name . '" , создал: ' .
                    '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . ' ' .
                    auth()->user()->name . '</a>');
            }else{
                $item = FailureType::whereId($request->input("id"))->first();
                if ($item == null){
                    return abort(404);
                }
                $oldName = $item->name;
                $item->name = $request->input("name");
                $item->update();
                ApiLogController::addLogText('Изменено имя типа поломки с "' . $oldName . '" на "' . $item->name . '" , обновил: ' .
                    '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . ' ' .
                    auth()->user()->name . '</a>');
            }
        }
        return redirect()->route("failure-type");
    }

    public function changeArcState($id){
        $item = FailureType::whereId($id)->first();
        if ($item != null){
            $item->is_archive = $item->is_archive == 0 ? 1 : 0;
            $item->update();
            ApiLogController::addLogText('Изменен статус поломки "' . $item->name . '" с "' . (!$item->is_archive ? 'в архиве' : 'в работе') . '" на "' . ($item->is_archive ? 'в архиве' : 'в работе') . '", обновил: ' .
                '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . ' ' .
                auth()->user()->name . '</a>');
        }
        return "true";
    }

    public function list()
    {
        return $this->sendResponse(FailureType::where('is_archive',0)->get(), 'FailureType retrieved successfully.');
    }
}
