<?php

namespace App\Http\Controllers;


use App\DataTables\TerminalsDataTable;
use App\Models\Equipment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use App\Models\Log;
use App\Models\TerminalMessage;

class TerminalController extends BaseController
{

    public function checkAuth()
    {
        return $this->sendResponse(true, 'Auth check successfully.');
    }

    public function qr($tid)
    {
        $token = Auth::user()->createToken('MyApp')->accessToken;
        return view('qr',['data' => $tid . '$$' . $token]);
    }

    public function state(Request $request){
        $data = $request->all();
        $logItem = new Log();
        $logItem->body = $data["body"];
        $logItem->save();
        Equipment::whereId($data["tid"])->update(array('bat' => $data["bat"]));
        $messages = TerminalMessage::where('id', '>', $data["mid"])->where('aid',$data["tid"])->get();
        return $this->sendResponse(JsonResource::collection($messages), 'Messages retrieved successfully.');
    }
}
