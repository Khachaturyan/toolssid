<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends BaseController
{
    function delete(Request $request){
        Product::whereId($request->id)->delete();
        return $this->sendResponse(true, 'Product deleted successfully.');
    }

    function create(Request $request){
        $product = new Product();
        $product->uid = $request->uid;
        $product->cid = $request->cid;
        $product->save();
        return $this->sendResponse(true, 'Product created successfully.');
    }
}
