<?php


namespace App\Http\Controllers;



use App\Models\CustomData;

class FeedbackController extends Controller
{

    public const STATE_ON_OPEN     = -1;
    public const STATE_ON_WORK     = -2;
    public const STATE_ON_ANALISE  = -3;
    public const STATE_ON_HOLD     = -4;
    public const STATE_ON_COMPLETE = -5;

    private static function getColorScheme() : array {
        return array(
            self::getStateDes(self::STATE_ON_OPEN)     => self::getColor(self::STATE_ON_OPEN),    // Открыто - серый
            self::getStateDes(self::STATE_ON_WORK)     => self::getColor(self::STATE_ON_WORK),    // В работе - оранжевый
            self::getStateDes(self::STATE_ON_ANALISE)  => self::getColor(self::STATE_ON_ANALISE), // На анализе - оранжевый
            self::getStateDes(self::STATE_ON_HOLD)     => self::getColor(self::STATE_ON_HOLD),    // Отложено - красный
            self::getStateDes(self::STATE_ON_COMPLETE) => self::getColor(self::STATE_ON_COMPLETE) // Выполнено - зеленый
        );
    }

    private static function getColor($state) : string{
        switch ($state){
            case self::STATE_ON_WORK:     return '#FF6A00FF';
            case self::STATE_ON_ANALISE:  return '#FFB27FFF';
            case self::STATE_ON_HOLD:     return '#FF0000FF';
            case self::STATE_ON_COMPLETE: return '#00FF21FF';
            default:                      return '#A0A0A0FF';
        }
    }

    private static function getStateDes($state) : string{
        switch ($state){
            case self::STATE_ON_WORK:     return __('str.feedback_state_on_work');
            case self::STATE_ON_ANALISE:  return __('str.feedback_state_on_analise');
            case self::STATE_ON_HOLD:     return __('str.feedback_state_on_hold');
            case self::STATE_ON_COMPLETE: return __('str.feedback_state_on_complete');
            default:                      return __('str.feedback_state_on_open');
        }
    }


    public function feedbacks()
    {
        return view('feedback-list',[
            'org_name' => CustomData::firstOrCreate(['label' => 'org_name'],['data' => ""])->data,
            'color_scheme' => self::getColorScheme()]);
    }
}
