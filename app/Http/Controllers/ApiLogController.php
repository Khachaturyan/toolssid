<?php

namespace App\Http\Controllers;


use App\Exports\LogsExport;
use App\Models\Block;
use App\Models\Cell;
use App\Models\Log;
use App\Models\LogTakeItem;
use App\Models\Stage;
use Illuminate\Http\Request;
use Config;
use Carbon\Carbon;
use File;
use App\Mail\MailSandler;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class ApiLogController extends BaseController
{
    //--- для того что бы логи отображались в основном списке, вводятся фейковые ИД блоков -----------------------------
    const CHANGE_MEN_DATA_FAKE_BID_ID   = -1;
    const CHANGE_EQU_STATUS_FAKE_BID_ID = -2;

    public function onTick() : string{
        //--------------------------------------------------------------------------------------------------------------
        EquExpiredController::checkExpiredAsyncRequest();
        //--------------------------------------------------------------------------------------------------------------
        EquFavoriteController::checkFavoriteAsyncRequest();
        //--------------------------------------------------------------------------------------------------------------
        ParsecController::checkParsecAsyncRequest();
        //--------------------------------------------------------------------------------------------------------------
        OrionController::checkOrionAsyncRequest();
        //--- пересчёт периода загрузки типов --------------------------------------------------------------------------
        ApiTypeLoadController::checkTypeLoadAsyncRequest();
        //--- пересчёт состояний оборудования --------------------------------------------------------------------------
        StatStateController::checkStatStateAsyncRequest();
        //--------------------------------------------------------------------------------------------------------------
        ApiBreakingStateController::checkBreakingStatesAsyncRequest();
        //--- отправка отчёта по оборудованию раз в день для Макса -----------------------------------------------------
        ReportController::checkForOneDayReportAsyncRequest();
        ReportController::checkForOneDayReportEquWorkAsyncRequest();
        //--- TIM SERVER -----------------------------------------------------------------------------------------------
        TimServerController::syncTimSrvAsyncRequest();
        TimServerController::checkApkAsyncRequest();
        //--- update men lock statuses ---------------------------------------------------------------------------------
        MenLockController::updateLockStatusAsyncRequest();
        //--- update sigur server --------------------------------------------------------------------------------------
        ApiSigurController::checkSigurAsyncRequest();
        //--------------------------------------------------------------------------------------------------------------
        LogTakeItemController::clearItemsAsyncRequest();
        //--------------------------------------------------------------------------------------------------------------
        AlertMailPersonalController::checkAlertMailPersonalAsyncRequest();
        return "ok.";
    }

    public function getMaxVersion(){
        $files = File::files(public_path() . "/files");
        $maxVer = 0;
        foreach ($files as $f){
            $parts = explode("/",$f);
            $fileName = $parts[count($parts) - 1];
            $parts = explode("\\",$fileName);
            $fileName = $parts[count($parts) - 1];
            if (str_starts_with($fileName,'dcu_')){
                $arr = explode('_',$fileName);
                if (count($arr) > 1){
                    $fileName = $arr[1];
                    $arr = explode('.',$fileName);
                    $ver = intval($arr[count($arr) - 1]);
                    if ($ver > $maxVer) $maxVer = $ver;
                }
            }
        }
        return $maxVer;
    }

    public static function addLogText($text,$bid = 0,$mid = 0, $tid = 0, $eid = 0, $cid = 0, $ltp = 0,
                                      $estat_des = "", $estat_color = "", $estat_des_old = "", $estat_color_old = ""){
        $log = new Log();
        $log->bid = $bid;
        $log->mid = $mid;
        $log->tid = $tid;
        $log->eid = $eid;
        $log->cid = $cid;
        $log->ltp = $ltp;
        $log->estat_des = $estat_des;
        $log->estat_color = $estat_color;
        $log->estat_des_old = $estat_des_old;
        $log->estat_color_old = $estat_color_old;
        $log->body = $text;
        $log->save();
        return $log->id;
    }

    public function addLog(Request $request){
        $data = $request->all();
        if ($data['estat_des'      ] == null) $data['estat_des'      ] = "";
        if ($data['estat_color'    ] == null) $data['estat_color'    ] = "";
        if ($data['estat_des_old'  ] == null) $data['estat_des_old'  ] = "";
        if ($data['estat_color_old'] == null) $data['estat_color_old'] = "";
        Log::create($data);
        $TAKE_ITEM_WORKER_LTP = 6;
        if ($data['ltp'] == $TAKE_ITEM_WORKER_LTP && $data['eid'] > 0){
            LogTakeItem::create(['mid' => $data['mid'],'eid' => $data['eid'],'cid' => $data['cid']])->save();
        }
        $RETURN_ITEM_NOT_WORKING_LTP = 8;
        if ($data['ltp'] == $RETURN_ITEM_NOT_WORKING_LTP){
            ActController::createActFileForMailReport($data['eid']);
            MailSandler::sendToAll('возврат неисправного оборудования',$data['body'],
                                                ProjectSettingsController::$MAIL_ESTATE_RETURN_NOT_WORKING_EQU,
                '../storage/app/public/files/act.xlsx');
        }
        return $this->sendResponse(true, 'log add successfully.');
    }

    private function changeData(array $source) : array{
        for ($i = 0; $i < count($source); $i++){
            $source[$i]['created_at'] = ProjectSettingsController::toLocalTime($source[$i]['created_at']);
            $source[$i]['updated_at'] = ProjectSettingsController::toLocalTime($source[$i]['updated_at']);
        }
        return $source;
    }

    public function logByMan($id)
    {
        return $this->sendResponse($this->changeData(Log::where('mid',$id)->get()->toArray()), 'log load successfully.');
    }

    public function logByCell($id)
    {
        return $this->sendResponse($this->changeData(Log::where('cid',$id)->get()->toArray()), 'log load successfully.');
    }

    public function logByBlock($id)
    {
        return $this->sendResponse($this->changeData(Log::where('bid',$id)->get()->toArray()), 'log load successfully.');
    }

    public function getPing() : string{
        return $this->sendResponse(true, 'ping get successfully.');
    }

    public static function convertOldFilesToNewXlsxFormat(){
        $path = Storage::path('public/baclogs');
        if (is_dir($path)){
            $files = File::files($path);
            foreach ($files as $f){
                $fileName = basename($f);
                $arr2 = explode(".", $fileName);
                if ('txt' === $arr2[1]){
                    $link = $path . '/' . $fileName;
                    $items = json_decode(file_get_contents($link), true);
                    $itemsFiltered = array();
                    foreach ($items as $item){
                        if (array_key_exists('body',$item) && 'sigur update data =' !== $item['body']){
                            $itemsFiltered[] = $item;
                        }
                    }
                    if (count($itemsFiltered) > 0){
                        $snameArr     = array();
                        $bnameArr     = array();
                        $block_posArr = array();
                        for ($i = 0; $i < count($itemsFiltered); $i++){
                            if (!array_key_exists('sname',$itemsFiltered[$i])){
                                if ($itemsFiltered[$i]['bid'] > 0){
                                    if (!array_key_exists($itemsFiltered[$i]['bid'],$snameArr)){
                                        $b = Block::whereId($itemsFiltered[$i]['bid'])->first();
                                        $s = $b != null ? Stage::whereId($b->st_id)->first() : null;
                                        $snameArr[$itemsFiltered[$i]['bid']] = $s != null ? $s->name : "";
                                    }
                                    $itemsFiltered[$i]['sname'] = $snameArr[$itemsFiltered[$i]['bid']];
                                } else {
                                    $itemsFiltered[$i]['sname'] = "";
                                }
                            }
                            if (!array_key_exists('bname',$itemsFiltered[$i])){
                                if ($itemsFiltered[$i]['bid'] > 0){
                                    if (!array_key_exists($itemsFiltered[$i]['bid'],$bnameArr)){
                                        $b = Block::whereId($itemsFiltered[$i]['bid'])->first();
                                        $bnameArr[$itemsFiltered[$i]['bid']] = $b != null ? $b->name : "";
                                    }
                                    $itemsFiltered[$i]['bname'] = $bnameArr[$itemsFiltered[$i]['bid']];
                                } else {
                                    $itemsFiltered[$i]['bname'] = "";
                                }
                            }
                            if (!array_key_exists('block_pos',$itemsFiltered[$i])){
                                if ($itemsFiltered[$i]['cid'] > 0){
                                    if (!array_key_exists($itemsFiltered[$i]['cid'],$block_posArr)){
                                        $c = Cell::whereId($itemsFiltered[$i]['cid'])->first();
                                        $block_posArr[$itemsFiltered[$i]['cid']] = $c != null ? $c->block_pos : "";
                                    }
                                    $itemsFiltered[$i]['block_pos'] = $block_posArr[$itemsFiltered[$i]['cid']];
                                } else {
                                    $itemsFiltered[$i]['block_pos'] = "";
                                }
                            }
                        }
                        $tmpName = str_replace("logs", "", $arr2[0]);
                        $arr = explode('-', $tmpName);
                        $counter = count($arr) > 1 ? $arr[1] : '0';
                        self::createLogFileFromArray($itemsFiltered,$counter,$arr[0]);
                    }
                }
            }
        }
    }

    public static function createLogFileFromArray($items, $counter, $timeBackup){
        $strMin = "";
        $min = 0;
        $strMax = "";
        $max = 0;
        foreach ($items as $item){
            if ($min == 0 || strtotime($item['created_at']) < $min) {
                $min = strtotime($item['created_at']);
                $strMin = $item['created_at'];
            }
            if (strtotime($item['created_at']) > $max){
                $max = strtotime($item['created_at']);
                $strMax = $item['created_at'];
            }
        }
        $strMin = str_replace(array(' ',':','T'), "-",
            explode(".",ProjectSettingsController::toLocalTime($strMin))[0]);
        $strMax = str_replace(array(' ',':','T'), "-",
            explode(".",ProjectSettingsController::toLocalTime($strMax))[0]);
        $path =  'public/baclogs/logs_' . $timeBackup . '_' . $strMin . '_' . $strMax . '_' . $counter . '.xlsx';
        Excel::store(new LogsExport($items),$path);
    }

    public function clearLogs($dat){
        ini_set('max_execution_time', 600);
        $counter = 1;
        $timeBackup = time();
        Log::where('logs.created_at','<',$dat)
            ->leftJoin("cells",  "cells.id",  "=", "logs.cid")
            ->leftJoin("blocks", "blocks.id", "=", "logs.bid")
            ->leftJoin("stages", "stages.id", "=", "blocks.st_id")
            ->select("logs.*","cells.block_pos","blocks.name as bname","stages.name as sname")
            ->chunk(5000, function($items) use (&$counter, &$timeBackup){
                self::createLogFileFromArray($items,$counter,$timeBackup);
                $counter++;
            });
        Log::where('created_at','<',$dat)->delete();
        return "true";
    }

    private static function convertStorageTime($str){
        $arr = explode("-", $str);
        if (count($arr) < 6) return "";
        return $arr[0] . '-' . $arr[1] . '-' . $arr[2] . ' ' . $arr[3] . ':' . $arr[4] . ':' . $arr[5];
    }

    public function showArchiveLogs(){
        $path = Storage::path('public/baclogs');
        $arr = array();
        if (is_dir($path)) {
            $files = File::files($path);
            $count = 1;
            foreach ($files as $f) {
                $fileName = basename($f);
                $arr2 = explode(".", $fileName);
                if ('xlsx' === $arr2[1]) {
                    $arr3 = explode("_", $fileName);
                    $item = array();
                    $item['url'] = '/storage/baclogs/' . $fileName;
                    $item['create'] = ProjectSettingsController::toLocalTime(
                        count($arr3)  > 1 ? Carbon::createFromTimestamp(intval($arr3[1])) : "");
                    $item['min'] = self::convertStorageTime(count($arr3) > 2 ? $arr3[2] : '');
                    $item['max'] = self::convertStorageTime( count($arr3) > 3 ? $arr3[3] : '');
                    $item['num'] = $count;
                    $arr[] = $item;
                    $count++;
                }
            }
        }
        return view('archive-logs',['items' => $arr]);
    }
}
