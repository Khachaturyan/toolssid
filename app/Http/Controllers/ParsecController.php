<?php

namespace App\Http\Controllers;


class ParsecController extends Controller
{
    public static function checkParsecAsyncRequest(){
        $url = "http://127.0.0.1/api/check-parsec";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function checkParsec(){
        if (request()->ip() == "127.0.0.1"){
            $parsecSyncTime = ProjectSettingsController::getSettings('prs_sync_time',0)->data;
            if ($parsecSyncTime > 0){
                $prsUpdateTime = intval(ProjectSettingsController::getSettings('prs_update_time',0)->data);
                $parsecSyncInProgress = ProjectSettingsController::getSettings('prs_progress',false);
                $enableParsecConnect = ($prsUpdateTime + $parsecSyncTime < Carbon::now()->timestamp) && !$parsecSyncInProgress->data;
                $SEC_IN_DAY = 86400;
                $parsecUpdateError = $parsecSyncInProgress->data && $prsUpdateTime + $parsecSyncTime + $SEC_IN_DAY < Carbon::now()->timestamp;
                if ($enableParsecConnect || $parsecUpdateError){
                    $parsecSyncInProgress->update(['data' => true]);
                    $updateRes = ApiManController::updateParsec();
                    $str = "Обновление данных Parsec прошло " . (strlen($updateRes) == 0 ? 'успешно' : 'с ошибкой: ' . $updateRes);
                    ApiLogController::addLogText($str);
                    $parsecSyncInProgress->update(['data' => false]);
                    return $str . '<br>';
                }else{
                    return "enableParsecConnect - false<br>parsecSyncInProgress = " . $parsecSyncInProgress . "<br>prs_update_time = " . $prsUpdateTime . "<br>";
                }
            }else{
                return "parsecSyncTime < 0<br>";
            }
        }
        return "ok.";
    }
}
