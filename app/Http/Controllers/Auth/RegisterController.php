<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiManController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Mail\MailSandler;
use App\Models\CustomData;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'mname' => ['required', 'string', 'max:255'],
            'msurname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    public function registerNew(Request $request){
        //$this->sendSetupRegMail('x-invest@mail.ru');
        $isLockReg = CustomData::where('label','reg')->where('data','false')->first() != null;
        if ($isLockReg || $this->validator($request->all())->fails()){
            return 'false';
        }
        $this->create($request->all());
        return 'true';
    }

    public static function sendConfirmMail($user){
        self::sendSetupRegMail($user->email,$user->getSecretHasFromLogin());
    }

    private static function sendSetupRegMail($email,$secret = ''){
        //echo $_SERVER['HTTP_HOST'];
        $url = CustomData::firstOrCreate(['label' => 'srv_adr'],['data' => $_SERVER['SERVER_ADDR']])->data;
        if (!str_starts_with($url,'http')){
            $url = 'http://' . $url;
        }
        if (!str_ends_with($url,'/')){
            $url = $url . '/';
        }
        $confirmUrl = $url . "confirm/$secret";
        $link = '<a href="' . $confirmUrl . '">ссылке</a>';
        $body = 'Благодарим за использование интеллектуальной системы управления оборудованием TOOLSiD.<br><br>' .
            "Вы получили это письмо, потому что зарегистрировались на сайте: $url<br><br>" .
            "Просим Вас перейти по $link и активировать Ваш аккаунт.<br><br>" .
            'С наилучшими пожеланиями, команда Ateuco';
        MailSandler::send2($email,'Активация пользователя ' . $email, $body);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $mpatronymic = $data['mpatronymic'] != null ? $data['mpatronymic'] : '';
        $name = $data['msurname'] . ' ' . $data['mname'] . ' ' . $mpatronymic;
        $user = User::create([
            'name'       => $name,
            'email'      => $data['email'],
            'is_confirm' => false,
            'password'   => Hash::make($data['password']),
        ]);
        $rid = $user->isFirstOfFourNewUsers() ? RoleController::getAdminId() : RoleController::getRoleStorekeeperId();
        $arr = HomeController::createUpdateMen(null,$rid,'',
            $data['mname'],$data['msurname'],$mpatronymic,0,ApiManController::staticGenFreeLabel(),$data['post'],
            '',false,'',$user->id);
        if ($arr[0] < 0){
            die($arr[1]);
        }
        self::sendSetupRegMail($data['email'],$user->getSecretHasFromLogin());
        //$this->sendSetupRegMail('x-invest@mail.ru',$user->getSecretHasFromLogin());
        return $user;
    }
}
