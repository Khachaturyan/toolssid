<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Cell;
use App\Models\CustomData;
use App\Models\Equipment;
use App\Models\EquRate;
use App\Models\Log;
use App\Models\LogTakeItem;
use Carbon\Carbon;
use Config;
use File;
use Illuminate\Support\Facades\Storage;

class TimServerController extends Controller
{

    public static function uploadFileIntoTimServer($filePath, $fileName){
        $target_url = 'http://90.156.203.30/api/upload-dcu';
        //$target_url = 'http://127.0.0.1:8080/api/upload-dcu'; // Write your URL here
        $cFile = curl_file_create($filePath);
        $post = array('file'=> $cFile,'name' => $fileName); // Parameter to be sent
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $target_url);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result=curl_exec($ch);
        curl_close ($ch);
        return $result;
    }

    private static function loadString($url,$header = array()){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        $sOutput = curl_exec($ch);
        curl_close($ch);
        return $sOutput;
    }

    private static function loadFile($url, $path)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        $file = fopen($path, "w+");
        fputs($file, $data);
        fclose($file);
        return true;
    }

    public static function syncTimSrvAsyncRequest(){
        $url = "http://127.0.0.1/api/sync-tim";
        $cmd = "curl -i -X GET $url    --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function syncTimSrv(){
        if (request()->ip() == "127.0.0.1"){
            $syncEnable = CustomData::firstOrCreate(['label' => 'sync_tim_en'],['data' => 1 ])->data == 1;
            if ($syncEnable){
                $SEC_IN_DAY = 86400;
                $equItems = EquRate::query()
                    ->leftJoin('equipment','equipment.id','=', 'equ_rates.eid')
                    ->leftJoin('etypes', 'etypes.id','=', 'equipment.tid')
                    ->where('equipment.cid','>',0)
                    ->leftJoin('cells', 'cells.id','=', 'equipment.cid')
                    ->select('equipment.*','cells.block as bid','etypes.tname','etypes.tmark','etypes.tmodel')->get();
                $equArr = array();
                foreach ($equItems as $equ){
                    if (!array_key_exists($equ->bid,$equArr)){
                        $equArr[$equ->bid] = array();
                    }
                    $equArr[$equ->bid][] = $equ;
                }
                $boxes = Block::where('blocks.is_arc',0)
                    ->where('blocks.last_update','>',time() - 3600)
                    ->leftJoin("stages", "blocks.st_id","=", "stages.id")
                    ->select("blocks.*","stages.name as sname","stages.adr")
                    ->get();
                for ($i = 0; $i < count($boxes); $i++){
                    $busyCount = Equipment::where('equipment.cid','>',0)
                        ->leftJoin("cells", "cells.id","=", "equipment.cid")
                        ->where("cells.block",$boxes[$i]['id'])
                        ->count();
                    $boxes[$i]["busy"] = $busyCount;
                    $boxes[$i]["lock"] = Cell::where('block',$boxes[$i]['id'])->where('is_lock',true)->count();
                    $boxes[$i]["free"] = $boxes[$i]["cell_count"] - $busyCount;
                    $boxes[$i]["take"] = LogTakeItem::where('log_take_items.created_at','>',Carbon::now()->addSeconds(-$SEC_IN_DAY))
                        ->leftJoin("cells", "cells.id","=", "log_take_items.cid")
                        ->where('cells.block',$boxes[$i]['id'])->count();
                    $boxes[$i]["cols"] = Cell::where('cells.block',$boxes[$i]['id'])
                        ->leftJoin("cell_colors", "cells.id","=", "cell_colors.cid")
                        ->select("cell_colors.col","cells.block_pos")
                        ->get();
                    if (array_key_exists($boxes[$i]['id'],$equArr)){
                        $boxes[$i]["nous"] = count($equArr[$boxes[$i]['id']]);
                    }else{
                        $boxes[$i]["nous"] = 0;
                    }
                }
                //die(json_encode($boxes));
                $MIN_10 = 600;
                $tokenData = ProjectSettingsController::getSettings('tim_token',"");
                if (strlen($tokenData->data) == 0){
                    $tokenData->data = md5(rand(0, 1000000) . time());
                    $tokenData->update();
                }
                $data = [
                    "token" => $tokenData->data,
                    "client" => ProjectSettingsController::getSettings('org_name',"")->data,
                    "ver" => Config::get('app.web_ver'),
                    "boxes" => $boxes,
                    "cash_logs" => Log::where('logs.updated_at','>',Carbon::now()->addSeconds(-$MIN_10))
                        ->where('logs.body', 'LIKE', "%[ca]%")
                        ->leftJoin("blocks", "blocks.id","=", "logs.bid")
                        ->leftJoin("stages", "stages.id","=", "blocks.st_id")
                        ->leftJoin("cells", "cells.id","=", "logs.cid")
                        ->select( "logs.*","cells.block_pos","blocks.name as bname","stages.name as sname")
                        ->get(),
                    "update_state" => ProjectSettingsController::getAutoUpdateBool(),
                    "debug_state"  => ProjectSettingsController::getSettings('debug_mode',0)->data
                ];
                //echo json_encode($data);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"http://90.156.203.30/api/on-client");
                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                $response = curl_exec($ch);
                curl_close($ch);
                if ($response == null){
                    $response = "connect tim server is null";
                }
            }else{
                $response = "connect tim server disabled";
            }
        }else{
            $response = "abort - request not local";
        }
        return "connect tim server with response: " . $response;
    }

    private function getApkFilesMaxVersion() : int{
        $path = Storage::path('public/apk');
        $maxVer = 0;
        if (is_dir($path)){
            $files = File::files($path);
            foreach ($files as $f){
                $fileName = basename($f);
                $arr = explode('/',$fileName);
                $arr = explode('_',$arr[count($arr) - 1]);
                if (count($arr) > 2){
                    $arr = explode('.',$arr[1]);
                    if (count($arr) >= 4){
                        $ver = $arr[3];
                        if ($maxVer < $ver) $maxVer = $ver;
                    }
                }
            }
        }
        return $maxVer;
    }

    public static function checkApkAsyncRequest(){
        $url = "http://127.0.0.1/api/check-apk";
        $cmd = "curl -i -X GET $url    --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function checkApk(){
        $url = "http://90.156.203.30/api/apk-find/" . $this->getApkFilesMaxVersion();
        $res = self::loadString($url);
        if ($res != null && str_ends_with($res,".apk")){
            $url = "http://90.156.203.30/files/$res";
            $dir = Storage::path('public/apk');
            File::ensureDirectoryExists($dir);
            File::cleanDirectory($dir);
            $result = self::loadFile($url,"$dir/$res");
        }else{
            $result = false;
        }
        return $result;
    }

    public function getFile($version){
        $files = File::files(Storage::path('public/apk'));
        $fName = "";
        $maxVer = intval($version);
        foreach ($files as $f){
            $parts = explode("/",$f);
            $fileName = $parts[count($parts) - 1];
            $parts = explode("\\",$fileName);
            $fileName = $parts[count($parts) - 1];
            if (str_starts_with($fileName,'dcu_')){
                $arr = explode('_',$fileName);
                if (count($arr) > 1){
                    $fileName = $arr[1];
                    $arr = explode('.',$fileName);
                    $ver = intval($arr[count($arr) - 1]);
                    if ($ver > $maxVer){
                        $fName = $parts[count($parts) - 1];
                    }
                }
            }
        }
        return $fName;
    }
}
