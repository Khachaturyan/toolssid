<?php

namespace App\Http\Controllers;

use App\Imports\MenHeaderReader;
use App\Imports\MenImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Man;
use App\Imports\XmlMenImport;
use Carbon\Carbon;

class MenImportExcelController extends Controller
{
    /**
     *
     */

    //php artisan make:import UsersImport --model=User

    public function export()
    {
        return "no export function";//Excel::download(new UsersExport, 'users.xlsx');
    }

    public function importExportView()
    {
        return "no import view";
    }

    public function import(Request $request)
    {
        $ext = strtolower($request->file('file')->getClientOriginalExtension());
        if ($ext == 'xml'){
            XmlMenImport::importAliexpressXmlItems(simplexml_load_file($request->file('file')->getPathName()));
        }else{
            $mh = new MenHeaderReader();
            if ($ext == 'ods'){
                Excel::import($mh,$request->file('file'),null,\Maatwebsite\Excel\Excel::ODS);
            }else{
                Excel::import($mh,$request->file('file'));
            }
            if (!$mh->headerIsInit){
                return 'неизвестный формат файла';
            }
            $mImport = new MenImport();
            $mImport->labelEncodeAlg = $request->lab_encode != null ? $request->lab_encode : 0;
            $mImport->isDnsHeaderFormat = $mh->isDnsFormat;
            $mImport->isFmFormat = $mh->isFmFormat;
            $mImport->startRowIndex = $mh->startRowIndex + 1;
            if ($ext == 'ods'){
                Excel::import($mImport,$request->file('file'),null,\Maatwebsite\Excel\Excel::ODS);
            }else{
                Excel::import($mImport,$request->file('file'));
            }
            $moveToFired = $request->remove_absent != null;
            if ($moveToFired) $mImport->moveToFiredMid($mImport->midArray,false);
        }
        return back();
    }
}
