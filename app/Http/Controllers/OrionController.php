<?php

namespace App\Http\Controllers;

use App\Orion\OrionClient;
use Illuminate\Http\Request;

class OrionController extends Controller
{
    public static function checkOrionAsyncRequest(){
        $url = "http://127.0.0.1/api/check-orion";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public function checkOrion(){
        if (request()->ip() == "127.0.0.1"){
            $orionSyncTime = ProjectSettingsController::getSettings('ori_sync_time',0)->data;
            if ($orionSyncTime > 0){
                $oriUpdateTime = intval(ProjectSettingsController::getSettings('ori_update_time',0)->data);
                $orionSyncInProgress = ProjectSettingsController::getSettings('ori_progress',false);
                if (($oriUpdateTime + $orionSyncTime < Carbon::now()->timestamp) && !$orionSyncInProgress->data){
                    $orionSyncInProgress->update(['data' => true]);
                    $str = OrionClient::toImportLogResult();
                    ApiLogController::addLogText($str);
                    $orionSyncInProgress->update(['data' => false]);
                    ProjectSettingsController::getSettings('ori_sync_time',0)->update(['data' => Carbon::now()->timestamp]);
                }
            }
            OrionClient::uploadOrionSendItems();
        }
        return "ok.";
    }
}
