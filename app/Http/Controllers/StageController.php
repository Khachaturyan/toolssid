<?php

namespace App\Http\Controllers;

use App\DataTables\StageDataTable;
use App\Models\Block;
use App\Models\Cell;
use App\Models\Stage;
use Illuminate\Http\Request;

class StageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(StageDataTable $dataTable)
    {
        $dataTable->showArcOnly = false;
        return $dataTable->render('stages',["table_header" => __('str.stage_main_header')]);
    }

    public function showArc(StageDataTable $dataTable)
    {
        $dataTable->showArcOnly = true;
        return $dataTable->render('stages',["table_header" => __('str.stage_archive_main_header')]);
    }

    public function stageAction(Request $request){
        $arcStr = "";
        if ($request->input("id") == null){
            $stage = new Stage();
            $stage->name = $request->input("name");
            $stage->adr = $request->input("adr");
            $stage->is_arc = 0; //$request->input("is_arc");
            $stage->save();
        }else{
            $stage = Stage::whereId($request->input("id"))->first();
            if ($stage == null){
                abort(404);
            }
            if ($request->is_arc != $stage->is_arc){
                $blocks = Block::where('st_id',$stage->id)->get();
                for ($i = 0; $i < count($blocks); $i++){
                    $blocks[$i]->is_arc = $request->is_arc;
                    $blocks[$i]->update();
                    Cell::where('block',$blocks[$i]->id)->update(['is_arc' => $request->is_arc]);
                }
            }
            $stage->name   = $request->name;
            $stage->adr    = $request->adr;
            if ($request->input("is_arc") && !$stage->is_arc){
                $arcStr = " объект отправлен в архив";
            }
            $stage->is_arc = $request->is_arc;
            $stage->update();
        }
        ApiLogController::addLogText('Обновлен объект: id=' .
            $stage->id
            . ' (' . $stage->name . ')' . ', Пользователь: '
            . '<a href="/events-find/' . auth()->user()->email . '">' . auth()->user()->email . '</a>' . $arcStr);
        return redirect()->route("stages");
    }
}
