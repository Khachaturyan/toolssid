<?php

namespace App\Http\Controllers;

use App\Exports\LogsExport;
use App\Exports\MenExport;
use App\Exports\StatExport;
use App\Exports\StatEquExport;
use App\Exports\StatEquExport2;
use App\Exports\StatEquExport3;
use App\Exports\StatEquExport4;
use App\Exports\StatNotWorkingCurrentExport;
use App\Mail\MailSandler;
use App\Models\BreakingState;
use App\Models\EquExpired;
use App\Models\EquFavorite;
use App\Models\Equipment;
use App\Models\EquMenRigidLigament;
use App\Models\EquStatus;
use App\Models\Log;
use App\Models\Man;
use App\Models\Stage;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{

    public static function checkForOneDayReportAsyncRequest(){
        $url = "http://127.0.0.1/api/check-one-day-report";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    public static function checkForOneDayReportEquWorkAsyncRequest(){
        $url = "http://127.0.0.1/api/check-one-day-report-equ-work";
        $cmd = "curl -i -X GET $url --insecure  > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    //--- отправка отчёта по оборудованию которое на руках раз в день ---------------------------------------------------------
    public function checkOneDayReportEquWork(): string
    {
        if (request()->ip() == "127.0.0.1"){
            $enableSendDayReportEquWork = intval(ProjectSettingsController::getSettings('day_equ_report_equ_work',0)->data) == 1;
            if ($enableSendDayReportEquWork){
                $day_rep_time_equ_work = "" . ProjectSettingsController::getSettings('day_rep_time_equ_work',0)->data;
                if ($day_rep_time_equ_work !== "0"){
                    $completeSettings = ProjectSettingsController::getSettings('day_rep_complete_equ_work',0);
                    $nowTimeSec = (Carbon::now()->timestamp + ProjectSettingsController::getTimeOffset())  % 86400;
                    $day_rep_time_equ_work = intval(explode(":",$day_rep_time_equ_work)[0]) * 3600 + intval(explode(":",$day_rep_time_equ_work)[1]) * 60;
                    if (intval($completeSettings->data) == 0 && $nowTimeSec > $day_rep_time_equ_work){
                        $items = $this->equExcelMaxReportItems(0,true);
                        Excel::store(new StatEquExport4($items), '/public/files/device_return_time_report.xlsx');
                        $strDate = date("d.m.y");
                        MailSandler::sendToAll("Отчёт о возврате оборудования",
                            "Добрый день! Коллеги, во вложении отчёт по текущему состоянию ТСД СГП на " . $strDate,
                            ProjectSettingsController::$MAIL_ESTATE_EQU_IN_HAND,
                            '../storage/app/public/files/device_return_time_report.xlsx');
                        $completeSettings->update(['data' => 1]);
                    } else if (intval($completeSettings->data) == 1 && $nowTimeSec < $day_rep_time_equ_work){
                        $completeSettings->update(['data' => 0]);
                    }
                }
            }
        }
        return 'ok.';
    }

    //--- отправка отчёта по оборудованию раз в день для Макса ---------------------------------------------------------
    public function checkOneDayReport(){
        if (request()->ip() == "127.0.0.1"){
            $enableSendDayReport = intval(ProjectSettingsController::getSettings('day_equ_report',0)->data) == 1;
            if ($enableSendDayReport){
                $day_rep_time = "" . ProjectSettingsController::getSettings('day_rep_time',0)->data;
                if ($day_rep_time !== "0"){
                    $completeSettings = ProjectSettingsController::getSettings('day_rep_complete',0);
                    $nowTimeSec = (Carbon::now()->timestamp + ProjectSettingsController::getTimeOffset())  % 86400;
                    $day_rep_time = intval(explode(":",$day_rep_time)[0]) * 3600 + intval(explode(":",$day_rep_time)[1]) * 60;
                    if (intval($completeSettings->data) == 0 && $nowTimeSec > $day_rep_time){
                        $items = $this->equExcelMaxReportItems(0,false);
                        Excel::store(new StatEquExport2($items), '/public/files/device_report.xlsx');
                        $strDate = date("d.m.y");
                        MailSandler::sendToAll("Отчёт по текущему состоянию  ТСД на " . $strDate,
                            "Добрый день! Коллеги, во вложении отчёт по текущему состоянию ТСД СГП на " . $strDate,
                            ProjectSettingsController::$MAIL_ESTATE_CURRENT_EQU_STATE,
                            '../storage/app/public/files/device_report.xlsx');
                        $completeSettings->update(['data' => 1]);
                    } else if (intval($completeSettings->data) == 1 && $nowTimeSec < $day_rep_time){
                        $completeSettings->update(['data' => 0]);
                    }
                }
            }
        }
        return 'ok.';
    }

    private static function staticCreateReportItems($ds, $de, $stageId){
        $builder = BreakingState::query();
        if ($ds != "0"){
            $builder = $builder->where('breaking_states.created_at','>=',ProjectSettingsController::toLocalTime($ds));
        }
        if ($de != "0"){
            $builder = $builder->where('breaking_states.created_at','<=',ProjectSettingsController::toLocalTime($de));
        }
        if ($stageId != "0"){
            $builder = $builder->where('equipment.mark_sid',$stageId);
        }
        $items = $builder
            ->leftJoin("equipment", "equipment.id","=", "breaking_states.eid")
            ->leftJoin("men", "men.id","=", "breaking_states.mid")
            ->leftJoin("roles", "roles.id","=", "men.rid")
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin("failure_types", "failure_types.id","=", "breaking_states.ftid")
            ->leftJoin("cells", "cells.id","=", "breaking_states.cid")
            ->where('equipment.archive','=',false)
            ->select("roles.rname","roles.is_admin","men.mlabel","men.mname","men.msurname","men.mpatronymic",
                "men.post","men.fired",
                "etypes.tname","etypes.tmark","etypes.tmodel","cells.block_pos",
                "equipment.inv_num","equipment.mac","failure_types.name as ft_name","breaking_states.eid","breaking_states.created_at as bs_created_at")
            ->get();
        $TAKE_ITEM_WORKER        = 6;
        $RETURN_ITEM_WORKER      = 7;
        $RETURN_ITEM_NOT_WORKING = 8;
        for($i = 0; $i < count($items); $i++){
            //--- предыдущий владелец устройства -----------------------------------------------------------------------
            $men_ret_w = Log::where('eid',$items[$i]->eid)
                ->where('logs.ltp',$TAKE_ITEM_WORKER)
                ->where('logs.created_at','<=',$items[$i]->bs_created_at)
                ->orderBy('logs.created_at', 'desc')
                ->leftJoin("men", "men.id","=", "logs.mid")
                ->select("men.mname","men.msurname","men.mpatronymic","men.post","men.fired","logs.created_at")
                ->first();
            //--- находим время получения устройства -------------------------------------------------------------------
            $log_take = Log::where('eid',$items[$i]->eid)
                ->where('logs.ltp',$TAKE_ITEM_WORKER)
                ->where('logs.created_at','<=',$items[$i]->bs_created_at)
                ->leftJoin("blocks", "blocks.id","=", "logs.bid")
                ->orderBy('logs.created_at', 'desc')
                ->select("logs.*","blocks.name as bname")
                ->first();

            //--- находим время владения устройством -------------------------------------------------------------------
            $items[$i]->time_own = $log_take != null ? strtotime($items[$i]->bs_created_at) - strtotime($log_take->created_at) : 0;
            $items[$i]->time_own = sprintf('%02d:%02d:%02d', ($items[$i]->time_own / 3600),($items[$i]->time_own / 60 % 60), $items[$i]->time_own % 60);
            //----------------------------------------------------------------------------------------------------------
            $items[$i]->last_owner = $men_ret_w != null ? $men_ret_w->mname . ' ' . $men_ret_w->msurname . ' ' . $men_ret_w->mpatronymic : "";
            $items[$i]->bname = $log_take != null ? $log_take->bname : "";
            $created_at = $log_take != null ? ProjectSettingsController::toLocalTime($log_take->created_at) : "";
            $items[$i]->created_at = $created_at;
            $items[$i]->bs_created_at = ProjectSettingsController::toLocalTime($items[$i]->bs_created_at);
        }
        return $items;
    }

    public function createRepNotWorking($ds,$de,$stageId){
        $this->middleware('auth');
        $items = self::staticCreateReportItems($ds,$de,$stageId);
        return Excel::download(new StatNotWorkingCurrentExport($items), 'malfunction_report.xlsx');
    }

    public function reportXlsx($ds,$de,$mids,$eid)
    {
        $this->middleware('auth');
        return Excel::download(new StatExport($this->arrayCreate($ds,$de,$mids,$eid,false)), 'users.xlsx');
    }

    public function reportEquXlsx($ds,$de,$stageId)
    {
        $this->middleware('auth');
        $items = $this->arrayCreate($ds,$de,"0","0",true,$stageId)['items'];
        return Excel::download(new StatEquExport($items), 'used_device_report.xlsx');
    }

    public function reportCreate($ds,$de,$mids,$eid){
        $this->middleware('auth');
        return view("equ-men-rep",$this->arrayCreate($ds,$de,$mids,$eid,false));
    }

    private function arrayCreate($ds,$de,$mids,$eid, $hideEqu,$stageId = 0){
        $this->middleware('auth');
        ini_set('max_execution_time', 600);
        $urlstr = "$ds/$de/$mids/$eid";
        if ($mids != "0"){
            $arr = explode(',',$mids);
            $mids = array();
            foreach ($arr as $str) $mids[] = intval($str);
        }else{
            $mids = null;
        }
        $eid = $eid == "0" ? null : intval($eid);


        $dsPostfix = count(explode('T',$ds)) == 1 ? " 00:00:00" : "";
        $dePostfix = count(explode('T',$de)) == 1 ? " 23:59:59" : "";

        $ds = $ds == "0" ? null : $ds . $dsPostfix;
        $de = $de == "0" ? null : $de . $dePostfix;

        if ($ds != null) $ds = ProjectSettingsController::toGmtTime($ds);
        if ($de != null) $de = ProjectSettingsController::toGmtTime($de);

        $builder = $eid == null ? Log::where('logs.eid', ">", 0) : Log::where('logs.eid',$eid);
        if ($ds != null){
            $builder = $builder->where('logs.created_at','>=',$ds);
        }
        if ($de != null){
            $builder = $builder->where('logs.created_at','<=',$de);
        }
        if ($mids != null){
            $builder = $builder->whereIn('logs.mid',$mids);
        }
        if (intval($stageId) != 0){
            $builder = $builder->where('equipment.mark_sid',$stageId);
        }
        $mainArray = array(array(),array());
        $builder->leftJoin("etypes", "etypes.id","=", "logs.tid")
            ->leftJoin("men", "men.id","=", "logs.mid")
            ->leftJoin("equipment", "equipment.id","=", "logs.eid")
            ->leftJoin("blocks", "blocks.id","=", "logs.bid")
            ->orderBy('logs.id', 'asc')
            ->select("logs.id","logs.bid","logs.mid","logs.tid","logs.eid","logs.cid","logs.ltp",
                "men.mname","men.msurname","men.mpatronymic","men.post","men.fired","men.mlabel",
                "etypes.tname","etypes.tmark",
                "etypes.tmodel","equipment.label",
                "equipment.ser_num","equipment.mac","equipment.inv_num","logs.created_at","blocks.name as bname")
            ->chunk(250, function($results) use (&$mainArray){
                foreach($results as $result) {
                    if (!in_array($result->eid, $mainArray[0])){
                        $mainArray[0][] = $result->eid;
                    }
                    if ($result->ltp == 6){
                        $item = Log::where('eid',$result->eid)
                            ->where('id','>',$result->id)
                            ->orderBy('id', 'asc')
                            ->whereIn('ltp',array(5,8,7))
                            ->first();
                        $result->created_at = ProjectSettingsController::toLocalTime($result->created_at);
                        if ($item != null){
                            $result->ret = ProjectSettingsController::toLocalTime($item->created_at);
                            $result->ret_bname = $item->bname;
                        }
                        $mainArray[1][] = $result;
                    }
                }
            });
        if (!$hideEqu){
            $equ = Equipment::whereIn('equipment.id',$mainArray[0])->where('etypes.is_cons','=',false)
                ->where('equipment.archive','=',false)
                ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                ->leftJoin("men", "men.id","=", "equipment.uid")
                ->leftJoin("roles", "roles.id","=", "men.rid")
                ->leftJoin("cells", "cells.id","=", "equipment.cid")
                ->leftJoin("blocks", "blocks.id","=", "cells.block")
                ->leftJoin("failure_types", "failure_types.id","=", "equipment.ftid")
                ->select("roles.rname","roles.is_admin","men.mlabel","men.mname","men.msurname","men.post",
                    "etypes.tname","etypes.tmark","etypes.tmodel", "equipment.*","cells.block_pos",
                    "blocks.name as bname","failure_types.name as ft_name")->get();
        }else{
            $equ = null;
        }
        return ['items' => $mainArray[1], 'equ' => $equ, 'urlstr' => $urlstr];
    }

    public function reports()
    {
        $this->middleware('auth');
        $stages = Stage::where('is_arc',false)->get();
        return view('reports',['stages' => $stages]);
    }

//----------------------------------------------------------------------------------------------------------------------

    private static function place($data){
        if ($data->uid > 0){
            return $data->rname . " " . $data->post . " " . $data->mlabel . " " . $data->mname . " " . $data->msurname;
        }
        if ($data->cid == 0){
            return "не размещено";
        }
        return "Блок " . $data->bname . ", ячейка " . $data->block_pos;
    }

    private static function temporaryReturn($row) {
        $mens = EquFavorite::where('equ_favorites.eid',$row->id)
            ->leftJoin("men", "men.id","=", "equ_favorites.uid")
            ->select("men.*")->get();
        $str = "";
        foreach ($mens as $men){
            if (strlen($str) > 0){
                $str .= ", ";
            }
            $str .= $men->mlabel . " " . $men->mname . " " . $men->msurname;
        }
        return $str;
    }

    private static function rig_owner($row) {
        $mens = EquMenRigidLigament::where('equ_men_rigid_ligaments.eid',$row->id)
            ->leftJoin("men", "men.id","=", "equ_men_rigid_ligaments.uid")
            ->select("men.*")->get();
        $str = "";
        foreach ($mens as $men){
            if (strlen($str) > 0) $str .= ", ";
            $str .= $men->mlabel . " " . $men->mname . " " . $men->msurname;
        }
        return $str;
    }

    private function working_state($data){
        $str = $data->is_work ? __('str.table_working') : __('str.table_not_working');
        if (!$data->is_work && $data->ft_name != null){
            $str .= '-' . $data->ft_name;
        }
        return $str;
    }

    function estat_des($row) {
        if ($row->estat <= 0){
            return ApiEquipmentController::getStatDescription($row->estat);
        }
        $item = EquStatus::whereId($row->estat)->first();
        return $item == null ? "" : $item->descr;
    }

    public function equExcelMaxReport(){
        return json_encode($this->equExcelMaxReportItems(0,false));
    }

    public function equExcelReport3($stageId){
        $items = $this->equExcelMaxReportItems($stageId,false);
        for($i = 0; $i < count($items); $i++){
            $items[$i]->working_state = $this->working_state($items[$i]);
            $items[$i]->estat_des = $this->estat_des($items[$i]);
        }
        return Excel::download(new StatEquExport3($items), 'all_devices.xlsx');
    }

    public function equExcelMaxReportItems($stageId,$inHandEstateOnly){
        return self::equExcelMaxReportItemsStatic($stageId,$inHandEstateOnly);
    }

    public static function equExcelMaxReportItemsStatic($stageId,$inHandEstateOnly){
        $builder = Equipment::where('etypes.is_cons','=',false);
        if (intval($stageId) != 0){
            $builder = $builder->where('equipment.mark_sid',$stageId);
        }
        if ($inHandEstateOnly){
            $builder = $builder->where('equipment.estat',ApiEquipmentController::$IN_HAND_INDEX);
        }
        $items = $builder->where('equipment.archive','=',false)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin("men", "men.id","=", "equipment.uid")
            ->leftJoin("roles", "roles.id","=", "men.rid")
            ->leftJoin("cells", "cells.id","=", "equipment.cid")
            ->leftJoin("blocks", "blocks.id","=", "cells.block")
            ->leftJoin("failure_types", "failure_types.id","=", "equipment.ftid")
            ->select("roles.rname","roles.is_admin","men.mlabel","men.mname","men.msurname","men.post",
                "etypes.tname","etypes.tmark","etypes.tmodel", "equipment.*","cells.block_pos",
                "blocks.name as bname","failure_types.name as ft_name")
            ->get();
        for($i = 0; $i < count($items); $i++){
            $items[$i]->rig_owner = self::rig_owner($items[$i]);
            $items[$i]->owner = self::temporaryReturn($items[$i]);
            $items[$i]->ft_name = $items[$i]->is_work ? "" : $items[$i]->ft_name;
            $items[$i]->place = self::place($items[$i]);
            $items[$i]->is_expired = EquExpired::where('eid', $items[$i]->id)->first() != null;
            if ($items[$i]->ocid > 0) {
                $old_cell = ApiCellController::staticCellById($items[$i]->ocid);
                if ($old_cell != null){
                    $items[$i]->old_bname = $old_cell->name;
                    $items[$i]->old_block_pos = $old_cell->block_pos;
                }
            }
        }
        return $items;
    }

    public function reportMenXlsx(){
        $items = Man::where("fired",'!=',1)
            ->leftJoin("roles", "roles.id","=", "men.rid")
            ->leftJoin("users", "users.id","=", "men.web_uid")
            ->select("roles.rname","roles.is_admin","men.*","users.email")->get();
        //return json_encode($items);
        return Excel::download(new MenExport($items), 'actual_users.xlsx');
    }

    public function reportDayLogXlsx($date,$stageId){
        $SEC_IN_DAY = 86400;
        $time = strtotime($date) - ProjectSettingsController::getTimeOffset();
        $ds = date("Y-m-d H:i:s", $time);
        $de = date("Y-m-d H:i:s", $time + $SEC_IN_DAY);
        $builder = Log::query();
        if (intval($stageId) != 0){
            $builder = $builder->where('stages.id',$stageId);
        }
        $items = $builder->where('logs.created_at','>=',$ds)
            ->where('logs.created_at','<=',$de)
            ->leftJoin("blocks", "blocks.id","=", "logs.bid")
            ->leftJoin("stages", "stages.id","=", "blocks.st_id")
            ->leftJoin("cells", "cells.id","=", "logs.cid")
            ->select( "logs.*","cells.block_pos","blocks.name as bname","stages.name as sname")
            ->orderBy('logs.created_at', 'desc')
            ->get();
        return Excel::download(new LogsExport($items), 'today_logs.xlsx');
    }

}
