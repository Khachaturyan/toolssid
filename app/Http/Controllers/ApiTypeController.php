<?php

namespace App\Http\Controllers;


use App\Models\EquFavorite;
use App\Models\Equipment;
use App\Models\EquMenRigidLigament;
use App\Models\Product;
use DB;

class ApiTypeController extends BaseController
{
    public function check($label,$bid=0)
    {
        $labs = array($label);
        $orion_label = request()->header('orionlab');
        if ($orion_label != null){
            $labs[] = $orion_label;
        }
        $man = DB::table('men')
            ->whereIn('men.mlabel', $labs)
            //->leftJoin("men_block_uses", "men.id", "=", "men_block_uses.mid")
            ->leftJoin("roles", "roles.id", "=", "men.rid")
            ->select("roles.lim_items as role_lim_items","roles.etypes","roles.perm_index","roles.is_admin",
                            "roles.rname","roles.more_one_type","men.*")
            ->first();
        $equipment = DB::table('equipment')->where('equipment.label', $label)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin('cell_sizes','etypes.csid','=','cell_sizes.id')
            ->select("etypes.tname","etypes.csid","cell_sizes.name as cs_name","cell_sizes.des as cs_des",
                        "etypes.img","etypes.tmark","etypes.tmodel","etypes.is_cons", "equipment.*")->get()->first();
        if ($man != null){
            $equFavorite = EquFavorite::where('equ_favorites.uid', $man->id)
                ->leftJoin("equipment", "equipment.id","=", "equ_favorites.eid")
                ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                ->select("etypes.tname","etypes.img","etypes.tmark","etypes.tmodel","etypes.is_cons", "equipment.*")
                ->get();
            $man->equTimeRet = $equFavorite;
            //--- удалить в ближайшее время оставлено для совместимости ------------------------------------------------
            $arr = array();
            for ($i = 0; $i < count($equFavorite); $i++){
                $arr[] = $equFavorite[$i]->eid;
            }
            $man->fid = $arr;
            //----------------------------------------------------------------------------------------------------------
            $eids = EquMenRigidLigament::where('uid',$man->id)->select('eid')->get();
            $man->rigits = array();
            foreach ($eids as $eid){
                $equ = DB::table('equipment')->where('equipment.id',$eid->eid)
                    ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                    ->leftJoin('cell_sizes','etypes.csid','=','cell_sizes.id')
                    ->select("etypes.tname","etypes.csid","cell_sizes.name as cs_name","cell_sizes.des as cs_des",
                        "etypes.img","etypes.tmark","etypes.tmodel","etypes.is_cons", "equipment.*")->get()->first();
                if ($equ != null){
                    if ($equ->cid > 0) {
                        $equ->cell = ApiCellController::staticCellById($equ->cid);
                    }
                    if ($equ->ocid > 0) {
                        $equ->old_cell = ApiCellController::staticCellById($equ->ocid);
                    }
                    $man->rigits[] = $equ;
                }
            }
            $man->products = Product::where('uid', $man->id)->get();
            $man->mbu_bind = MenBlockUseController::isBind($man->id) ? 1 : 0;
        }
        if ($equipment != null){
            if ($equipment->cid > 0) {
                $equipment->cell = ApiCellController::staticCellById($equipment->cid);
            }
            if ($equipment->ocid > 0) {
                $equipment->old_cell = ApiCellController::staticCellById($equipment->ocid);
            }
            if ($equipment->comp_id > 0){
                $equipment->comp_items = Equipment::where('equipment.comp_id',$equipment->comp_id)
                    ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                    ->where('equipment.id','!=',$equipment->id)
                    ->select("etypes.tname","etypes.img","etypes.tmark","etypes.tmodel","etypes.is_cons", "equipment.*")
                    ->get();
                $equipment->comp_main = Equipment::where('equipment.id', $equipment->comp_id)
                    ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                    ->select("etypes.tname","etypes.img","etypes.tmark","etypes.tmodel","etypes.is_cons", "equipment.*")
                    ->first();
            }
        }
        $data = array();
        $data[] = $man;
        $data[] = $equipment;
        if ($man == null && $equipment == null){
            ApiLogController::addLogText("Отсканированна неизвестная метка: $label",$bid);
        }
        return $this->sendResponse($data, 'Type retrieved successfully.');
    }

    public function menEquInfo($id){
        $equipment = DB::table('equipment')->where('equipment.uid', $id)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin('cell_sizes','etypes.csid','=','cell_sizes.id')
            ->select("etypes.tname","etypes.csid","cell_sizes.name as cs_name","cell_sizes.des as cs_des",
                "etypes.img","etypes.tmark","etypes.tmodel","etypes.is_cons", "equipment.*")->get();
        for ($i = 0; $i < count($equipment); $i++){
            if ($equipment[$i]->cid > 0) {
                $equipment[$i]->cell = ApiCellController::staticCellById($equipment[$i]->cid);
            }
            if ($equipment[$i]->ocid > 0) {
                $equipment[$i]->old_cell = ApiCellController::staticCellById($equipment[$i]->ocid);
            }
        }
        return $this->sendResponse($equipment, 'Equ retrieved successfully.');
    }
}
