<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController as BaseController;

use App\Models\Block;
use App\Models\Log;
use Illuminate\Http\Resources\Json\JsonResource;

class ApiBlockController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $SEC_IN_DAY = 86400;
        $blocks = Block::where('blocks.is_arc',false)
            ->leftJoin("stages", "blocks.st_id","=", "stages.id")
            ->select("blocks.*","stages.name as sname","stages.adr")
            ->get();
        for($i = 0; $i < count($blocks); $i++){
            $log = Log::where('bid',$blocks[$i]->id)->orderBy('updated_at', 'desc')->first();
            $blocks[$i]->used = $log !=null && (time() - strtotime($log->updated_at)) < $SEC_IN_DAY;
        }
        return $this->sendResponse($blocks, 'Blocks retrieved successfully.');
    }

    public function find($id)
    {
        return $this->sendResponse(Block::whereId($id)->get(), 'Blocks retrieved successfully.');
    }
}
