<?php

namespace App\Http\Controllers;

use App\Models\AndComm;
use App\Models\Block;
use Illuminate\Http\Request;

class AndCommController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function sendUpdateComAll(){
        $i = new AndCommController();
        $i->addComUpdateSettingsAll();
    }

    public function addComUpdateSettingsAll(){
        $this->addComAll('cd-update');
    }

    public function addComAll($com){
        $this->addCom(null,$com);
    }

    public function addCom($bid,$com){
        if ($bid == null){
            $blocks = Block::all();
            foreach ($blocks as $block){
                $item = new AndComm();
                $item->bid = $block->id;
                $item->com = $com;
                $item->save();
            }
        }else{
            $item = new AndComm();
            $item->bid = $bid;
            $item->com = $com;
            $item->save();
        }
    }

    public function add(Request $request){
        $this->addCom($request->bid,$request->com);
        return "true";
    }

    public function get($bid){
        $items = AndComm::where('bid',$bid)->get()->toArray();
        AndComm::where('bid',$bid)->forceDelete();
        return $this->sendResponse($items, 'commands retrieved successfully.');
    }
}
