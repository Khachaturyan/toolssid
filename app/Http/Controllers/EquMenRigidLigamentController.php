<?php

namespace App\Http\Controllers;

use App\Models\Equipment;
use App\Models\EquMenRigidLigament;
use Yajra\DataTables\DataTables;

class EquMenRigidLigamentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function remove($eid){
        EquMenRigidLigament::where('eid',$eid)->delete();
        return redirect()->route("equipment");
    }

    public function remove2($eid,$uid){
        EquMenRigidLigament::where('eid',$eid)->where('uid',$uid)->delete();
        return "true";
    }

    public function add($eid,$uid){
        EquMenRigidLigament::firstOrCreate(['eid' => $eid,'uid' => $uid],[]);
        return redirect()->route("equipment");
    }

    public function add2($eid,$uid){
        EquMenRigidLigament::firstOrCreate(['eid' => $eid,'uid' => $uid],[]);
        return "true";
    }

    public function show()
    {
        return view('rigid-links');
    }

    public function getData($eid){
        $query = EquMenRigidLigament::query()->where('equ_men_rigid_ligaments.eid',$eid)
            ->leftJoin("men", "men.id","=", "equ_men_rigid_ligaments.uid")
            ->leftJoin("equipment", "equipment.id","=", "equ_men_rigid_ligaments.eid")
            ->select("men.*","equ_men_rigid_ligaments.eid","equ_men_rigid_ligaments.uid");
        return Datatables::of($query)
            ->addColumn('remove_rigit', function ($row) {
                return '<img src="/img/delete_black_24dp.svg" width="20px" height="20px">';
            })
            ->rawColumns(['remove_rigit'])
            ->make(true);
    }
}
