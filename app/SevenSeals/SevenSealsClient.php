<?php

namespace App\SevenSeals;

use App\Models\Man;
use PDO;

class SevenSealsClient
{
    private static function createPdo(){
        $dsn = 'firebird:dbname=office.sevenseals.ru/3050:c:\ACS\Base\Acs_ado.fdb;';
        $username = 'SYSDBA';
        $password = 'masterkey';
        $pdo = null;
        try {
            $pdo = new PDO($dsn, $username, $password);
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
        return $pdo;
    }

    private static function uploadData($pdo,$tableName){//
        $manItems = Man::query()->select('mname','msurname','mpatronymic','post','mlabel','fired')->get();
        $data = [];
        foreach ($manItems as $man){
            $data[] = array($man['mname'],$man['msurname'],$man['mpatronymic'],iconv('utf-8//IGNORE', 'windows-1251//IGNORE', $man['post']),$man['mlabel'],$man['fired']);
        }
        $stmt = $pdo->prepare("INSERT INTO " . $tableName . " (LASTNAME,FIRSTNAME,MIDLENAME,DOLJNOST,TABELNOMER,fired) VALUES (?,?,?,?,?,?)");
        try {
            $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);
            $pdo->beginTransaction();
            $pdo->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
            foreach ($data as $row)
            {
                $stmt->execute($row);
            }
            $pdo->commit();
        }catch (Exception $e){
            $pdo->rollback();
            echo "Exception сработала";
            throw $e;
        }
    }

    private static function uploadDataTotal($pdo){
        self::uploadData($pdo,'synchropersonaltotal');
    }

    private static function uploadDataChanges($pdo){
        self::uploadData($pdo,'synchropersonalchanges');
    }

    private static function readAllData($pdo){
        $stmt = $pdo->query('SELECT * FROM table');
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Вывод данных на экран
        foreach ($rows as $row) {
            echo $row['field1'] . ' ' . $row['field2'] . "\n";
        }
    }

    public static function testConnection() : string{
        $pdo = self::createPdo();
        if ($pdo == null) return "no firebird connect";
        self::uploadDataTotal($pdo);
        self::uploadDataChanges($pdo);
        // Отправка запроса на выборку данных

        return "ok.";
    }
}
