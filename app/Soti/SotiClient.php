<?php

namespace App\Soti;

use App\Http\Controllers\ProjectSettingsController;

class SotiClient
{

    private $token = null;

    private $soti_host          ;
    private $soti_client_id     ;
    private $soti_client_secret ;
    private $soti_api_username  ;
    private $soti_api_password  ;

    public function __construct()
    {
        $this->soti_host = ProjectSettingsController::getSettings('soti_host','')->data;
        $this->soti_client_id = ProjectSettingsController::getSettings('soti_client_id','')->data;
        $this->soti_client_secret = ProjectSettingsController::getSettings('soti_client_sec','')->data;
        $this->soti_api_username = ProjectSettingsController::getSettings('soti_api_user','')->data;
        $this->soti_api_password = ProjectSettingsController::getSettings('soti_api_pass','')->data;
        if ( strlen($this->soti_host)          > 0 &&
             strlen($this->soti_client_id)     > 0 &&
             strlen($this->soti_client_secret) > 0 &&
             strlen($this->soti_api_username)  > 0 &&
             strlen($this->soti_api_password)  > 0 ){
            $this->auth();
        }
    }

    public function isAuth(){
        return $this->token != null;
    }

    private function auth(){
        $url = 'https://' . $this->soti_host . '/MobiControl/api/token';
        $post = "grant_type=password&username=" . $this->soti_api_username . "&password=" . $this->soti_api_password;
        $header = 'Authorization: Basic ' . base64_encode($this->soti_client_id . ':' . $this->soti_client_secret);
        //$fp = fopen(dirname(__FILE__).'/errorlog.txt', 'w');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        //curl_setopt($ch, CURLOPT_STDERR, $fp);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array($header));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $sOutput = curl_exec($ch);
        curl_close($ch);
        if ($sOutput != null){
            $dat = json_decode($sOutput, true);
            $this->token = $dat['access_token'];
        }
    }

    private function manufacturersList(){
        $url = 'https://' . $this->soti_host .
            '/mobicontrol/api/agentsAndPlugins/android/agents/manufacturers?status=All&forceRefresh=false';
        return $this->getReq($url);
    }

    private function agentsList(){
        $url = 'https://' . $this->soti_host .
            '/mobicontrol/api/agentsAndPlugins/android/agents?manufacturer=All&status=All&forceRefresh=false';
        return $this->getReq($url);
    }

    private function deviceList(){
        $url = 'https://' . $this->soti_host . '/mobicontrol/api/devices';
        return $this->getReq($url);
    }

    public function getRdpLinkFromMac($mac){
        if (!$this->isAuth()){
            return 'wrong auth data';
        }
        $str = $this->deviceList();
        if ($str == null){
            return 'device list load error';
        }
        $arr = json_decode($str, true);
        $DeviceId = null;
        foreach ($arr as $item){
            if ($item['MACAddress'] != null){
                if (strcmp(mb_strtolower($item['MACAddress']),mb_strtolower($mac)) == 0){
                    $DeviceId = $item['DeviceId'];
                }
            }
        }
        return $DeviceId != null ? 'https://' . $this->soti_host . '/sotixsight/#/home/dashboard/remoteControl/' . $DeviceId : 'mac not found';
    }

    private function getReq($url){
        $header = 'Authorization: Bearer ' . $this->token;
        //$fp = fopen(dirname(__FILE__).'/errorlog.txt', 'w');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //curl_setopt($ch, CURLOPT_STDERR, $fp);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array($header));
        $sOutput = curl_exec($ch);
        curl_close($ch);
        //echo $sOutput;
        return $sOutput;
    }
}
