<?php

namespace App\DataTables;

use App\Http\Controllers\ApiCellController;
use App\Models\Etype;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class EtypeDataTable extends LocalDataTable
{
    public $hide = false;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action-edit-role', function ($row) {
                return '<a href="/add-role-item/' . $row->id . '" style="height: 20px"><img src="/img/credit-card.svg" width="20px" height="20px"></i></a>';
            })
            ->addColumn('action-add-count', function ($row) {
                return '<a href="/add-equipment-count/' . $row->id . '" style="height: 20px"><img src="/img/create-outline.svg" width="20px" height="20px"></a>';
            })
            ->addColumn('action-edit-etype', function ($row) {
                return '<img src="/img/create-outline.svg" width="20px" height="20px">';
            })
            ->editColumn('img',
                function($data){
                    return strlen($data->img) > 0 ? "<img src=\"/storage/" . $data->img . "\" width=\"20px\" height=\"20px\">" : __('str.table_no');
                })
            ->addColumn('is_comp2',
                function($data){
                    return $data->is_comp ? __('str.table_yes') : __('str.table_no');
                })
            ->addColumn('is_cons2',
                function($data){
                    return $data->is_cons ? __('str.table_yes') : __('str.table_no');
                })
            ->addColumn('cs_name2',
                function($data){
                    return $data->csid == 0 ? __('str.table_def_size') : $data->cs_name . " " . $data->cs_des;
                })
            ->rawColumns(['action-edit-role','action-add-count','action-edit-etype','img']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Etype $model)
    {
        return $model->newQuery()
            ->where("hide",$this->hide)
            ->leftJoin('cell_sizes','etypes.csid','=','cell_sizes.id')
            ->select("cell_sizes.name as cs_name","cell_sizes.des as cs_des","etypes.*");
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('block-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->scrollX(true)
                    ->orderBy(2);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('action-edit-role')->title(__('str.table_to_role'))->width(50)->className("text-center"),
            Column::make('action-edit-etype')->title(__('str.table_edit'))->width(50)->className("text-center"),
            Column::make('id')->title(__('str.table_num'))->width(64)->className("text-center"),
            Column::make('img')->title(__('str.table_picture'))->width(64)->className("text-center"),
            Column::make('cs_name2')->title(__('str.table_size'))->className("text-center")->name("cell_sizes.name"),
            Column::make('cs_des')->title("")->visible(false)->name("cell_sizes.des"),
            Column::make('tname')->title(__('str.table_type')),
            Column::make('tmark')->title(__('str.table_mark')),
            Column::make('tmodel')->title(__('str.table_model')),
            Column::make('is_cons2')->title(__('str.table_is_cons'))->width(50)->className("text-center"),
            Column::make('is_comp2')->title(__('str.table_is_comp'))->width(50)->className("text-center"),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Etype_' . date('YmdHis');
    }
}
