<?php

namespace App\DataTables;

use App\Http\Controllers\ProjectSettingsController;
use App\Models\Log;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class LogDataTable extends LocalDataTable
{

    public $ignoreWebLogs = true;
    public $bid = 0;
    public $mid = 0;
    public $eid = 0;
    public $cid = 0;
    public $ser = null;
    public $inv = null;

    private const CASH_PREFIX = "[ca]";

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()->eloquent($query)
            ->editColumn('created_at',
                function($data){
                    $parts = explode(".",ProjectSettingsController::toLocalTime($data->created_at));
                    return str_replace(array('T'), " ", $parts[0]);
                })
            ->addColumn('st_from', function ($row) {
                $str = $row->estat_des_old;
                if (strlen($str) == 0){
                    if (!str_contains($row->body,self::CASH_PREFIX)){
                        return "";
                    }
                    $str = __('str.logs_status_not_found');
                }
                $color = $row->estat_color_old;
                if (strlen($color) == 0){
                    $color = "#aaaaaa";
                }
                return "<button type=\"button\" style='padding: 8px; border: none;color:white;border-radius: 8px;" .
                            "width: 100%;height: 50px;background-color:" . $color . ";'>" . $str . "</button>";
            })
            ->addColumn('st_to', function ($row) {
                $str = $row->estat_des;
                if (strlen($str) == 0){
                    if (!str_contains($row->body,self::CASH_PREFIX)){
                        return "";
                    }
                    $str = __('str.logs_status_not_found');
                }
                $color = $row->estat_color;
                if (strlen($color) == 0){
                    $color = "#aaaaaa";
                }
                return "<button type=\"button\"  style='padding: 8px; border: none;color:white;border-radius: 8px;" .
                        "width: 100%;height: 50px;background-color:" . $color . ";'>" . $str . "</button>";
            })
            ->editColumn('body',
                function($data){
                    if ($data->ltp == 3){
                        $str = "<span style=\"color:#ff0000\">" . $data->body . "</span>";
                    }else{
                        $str = $data->body;
                    }
                    if (str_contains($str,self::CASH_PREFIX)){
                        $str = str_replace(self::CASH_PREFIX,'',$str);
                        $str .= ' ' . __('str.logs_cashed');
                    }
                    return $str;
                })
            ->rawColumns(['st_to','st_from','body']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Log $model)
    {
        // получение выбранного номера позиции ячейки в блоке
        $block_pos = $this->request()->get('block_pos');

        $builder = $model->newQuery();
        // ---------------------выборка записей по номеру ячейки из БД оператором where---------------------------------
        if (!empty($block_pos)) {
            $builder->where('cells.block_pos',$block_pos);
        }
        // -------------------------------------------------------------------------------------------------------------
        if ($this->bid > 0){
            $builder->where('logs.bid',$this->bid);
        }else if ($this->ignoreWebLogs) {
            $builder->where('logs.bid','!=',0);
        }
        if ($this->mid > 0){
            $builder->where('logs.mid',$this->mid);
        }
        if ($this->eid > 0){
            $builder->where('logs.eid',$this->eid);
        }
        if ($this->cid > 0){
            $builder->where('logs.cid',$this->cid);
        }
        if ($this->ser != null){
            $builder->where('logs.body', 'LIKE', "%{$this->ser}%");
        }
        if ($this->inv != null){
            $builder->where('logs.body', 'LIKE', "%{$this->inv}%");
        }
        return $builder
                ->leftJoin("blocks", "blocks.id","=", "logs.bid")
                ->leftJoin("stages", "stages.id","=", "blocks.st_id")
                ->leftJoin("cells", "cells.id","=", "logs.cid")
                ->select( "logs.*","cells.block_pos","blocks.name as bname","stages.name as sname");
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('log-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->initComplete("function () { onInitComplete(this) }")
                    ->dom('frtip')
                    ->scrollX(true)
                    ->orderBy(0)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title(__('str.table_num'))->width(80)->className("text-center"),
            Column::make('created_at')->title(__('str.table_created'))->width(160)->className("text-center"),
            Column::make('body')->title(__('str.table_description')),
            Column::make('st_from')->title(__('str.table_from_estat'))->className("text-center"),
            Column::make('st_to')->title(__('str.table_to_estat'))->className("text-center"),
            Column::make('sname')->title(__('str.table_object'))->name("stages.name"),
            Column::make('bname')->title(__('str.table_block'))->name("blocks.name"),
            Column::make('block_pos')->title(__('str.table_cell'))->name("cells.block_pos"),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Log_' . date('YmdHis');
    }
}
