<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;

class UserDataTable extends LocalDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                return '<a href="/user-remove/' . $row->id . '" style="height: 20px">
                        <img src="/img/delete_black_24dp.svg" width="20px" height="20px"></a>';
            })
            ->addColumn('send', function ($row) {
                return '<img src="/img/contact_mail_black_24dp.svg" width="20px" height="20px">';
            })
            ->editColumn('created_at',
                function($data){
                    $parts = explode(".",$data->created_at);
                    return str_replace(array('T'), " ", $parts[0]);
                })
            ->rawColumns(['action','send']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('project-settings-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->scrollX(true)
                    ->orderBy(2)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('action')->title(__('str.table_remove'))->width(64)->className("text-center"),
            Column::make('send')->title(__('str.table_mail_test'))->width(64)->className("text-center"),
            Column::make('id')->title(__('str.table_num'))->width(80)->className("text-center"),
            Column::make('name')->title(__('str.table_name')),
            Column::make('email')->title(__('str.table_mail')),
            Column::make('created_at')->title(__('str.table_reg')),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
