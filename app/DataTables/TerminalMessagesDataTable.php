<?php

namespace App\DataTables;


use App\Models\TerminalMessage;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class TerminalMessagesDataTable extends LocalDataTable
{

    public int $aid = 0;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    public function dataTable($query)
    {
        return datatables()->eloquent($query)->editColumn('created_at',
            function($data){
                $parts = explode(".",$data->created_at);
                return str_replace(array('T'), " ", $parts[0]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\TerminalGroup $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TerminalMessage $model)
    {
        if ($this->aid == 0){
            return $model->newQuery()
                ->leftJoin("equipment", "equipment.id","=", "terminal_messages.aid")
                ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                ->select("etypes.tname as name","terminal_messages.*");
        }else{
            return $model->newQuery()
                ->leftJoin("equipment", "equipment.id","=", "terminal_messages.aid")
                ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                ->select("etypes.tname as name","terminal_messages.*")->where("aid","=", $this->aid);
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('tg-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->scrollX(true)
                    ->orderBy(0);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title(__('str.table_num')),
            Column::make('created_at')->title(__('str.table_time')),
            Column::make('name')->title(__('str.table_name3')),
            Column::make('body')->title(__('str.table_message')),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'TerminalGroup_' . date('YmdHis');
    }
}
