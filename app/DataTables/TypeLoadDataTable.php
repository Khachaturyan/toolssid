<?php

namespace App\DataTables;

use App\Models\Block;
use App\Models\Equipment;
use App\Models\TypeLoad;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;

class TypeLoadDataTable extends LocalDataTable{

    public function dataTable($query)
    {
        return datatables()->eloquent($query)
            ->addColumn('tname2', function ($row) {
                return $row->tname . ' ' . $row->tmark . ' ' . $row->tmodel;
            })
            ->addColumn('in_work', function ($row) {
                return $row->count_men;
            })
            ->addColumn('count_full', function ($row) {
                return Equipment::where('tid',$row->tid)->where('archive',false)->count();
            })
            ->addColumn('not_working', function ($row) {
                return Equipment::where('tid',$row->tid)->where('archive',false)->where('is_work',0)->count();
            })
            ->addColumn('free_items', function ($row) {
                return Equipment::where('tid',$row->tid)->where('archive',false)->count() - $row->count_all;
            })
            ->addColumn('using', function ($row) {
                if ($row->count_all == 0){
                    return '0%';
                }
                return (intval((10000.0 * ($row->count_men) / $row->count_all)) / 100) . '%';
            })
            ->rawColumns(['tname2','in_work','using','free_items']);
    }

    public function query(TypeLoad $model)
    {
        $t = TypeLoad::max('created_at');
        $t = $t != null ? $t : Carbon::now();
        return $model->newQuery()
            ->where('type_loads.created_at',$t)
            ->leftJoin("etypes", "type_loads.tid", "=", "etypes.id",)
            ->select("type_loads.*","etypes.tname","etypes.tmark","etypes.tmodel");
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('type-load-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->orderBy(0)
                    ->scrollX(true)
                    //->parameters([
                    //    'drawCallback' => 'function() { alert("Table Draw Callback") }',
                    //])
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title(__('str.table_num'))->width(80)->className("text-center"),
            Column::make('tname2')->title(__('str.table_name'))->name("etypes.tname"),
            Column::make('count_full')->title(__('str.home_total'))->className("text-center"),
            Column::make('count_all')->title(__('str.table_in_work'))->className("text-center"),
            Column::make('count_box')->title(__('str.table_on_box'))->className("text-center"),
            Column::make('in_work')->title(__('str.table_in_hand'))->className("text-center"),
            Column::make('not_working')->title(__('str.table_not_working'))->className("text-center"),
            Column::make('free_items')->title(__('str.table_is_free'))->className("text-center"),
            Column::make('using')->title(__('str.home_k_using'))->className("text-center"),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Items_' . date('YmdHis');
    }
}
