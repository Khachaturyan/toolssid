<?php

namespace App\DataTables;

use App\Http\Controllers\RoleController;
use App\Models\Equipment;
use App\Models\Etype;
use App\Models\Role;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class RolesDataTable extends LocalDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    public $showArcOnly = false;

    private $etypesCash = array();

    public function dataTable($query)
    {
        return datatables()->eloquent($query)
            ->addColumn('action_edit', function ($row) {
                return '<img src="/img/create-outline.svg" width="20px" height="20px">';
            })
            //->addColumn('is_admin2',
            //    function($data){
            //        return $data->is_admin || $data->perm_index == RoleController::ADMIN_INDEX ? __('str.table_admin') : __('str.table_not_admin');
            //    })
            ->addColumn('etypes2',
            function($data){
                if ($data->is_admin) return __('str.table_any');
                if (strlen($data->etypes) == 0 || strcmp("#",$data->etypes) == 0) return __('str.table_nothing');
                $parts = explode("#",$data->etypes);
                $str = "";
                foreach ($parts as $part) if (strlen($part) > 0){
                    if (array_key_exists($part,$this->etypesCash)){
                        $etype = $this->etypesCash[$part];
                        if (strlen($str) > 0) $str .= "; ";
                        $str .= $etype->tname . " " . $etype->tmark . " " . $etype-> tmodel;
                    }else{
                        $etype = Etype::where('id', $part)->where('hide',0)->first();
                        if ($etype != null){
                            $this->etypesCash[$part] = $etype;
                            if (strlen($str) > 0) $str .= "; ";
                            $str .= $etype->tname . " " . $etype->tmark . " " . $etype->tmodel;
                        }
                    }
                }
                return mb_strlen($str) > 128 ? mb_substr($str,0,128) . "..." : $str;
            })
            ->rawColumns(['action_edit']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Role $model)
    {
        return $model->newQuery()->where('perm_index',0)->where('is_arc',$this->showArcOnly ? 1 : 0);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('roles-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->scrollX(true)
                    ->orderBy(0)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columnEdit = Column::make('action_edit')->title(__('str.equ_edit'))->width(80)->className("text-center");
        if (!auth()->user()->create_role()){
            $columnEdit->visible(false);
        }
        return [
            $columnEdit,
            Column::make('id')->title(__('str.table_num'))->width(80)->className("text-center"),
            Column::make('rname')->title(__('str.table_name'))->width(256),
            Column::make('etypes2')->title(__('str.table_units_enable')),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Roles_' . date('YmdHis');
    }
}
