<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use Request;
use Config;

class LocalDataTable extends DataTable
{
    public function __construct()
    {
        $lang = \App\Http\Controllers\ProjectSettingsController::getUiLang()->data;
        $lang = $lang === 'auto' ? Request::getPreferredLanguage(Config::get('app.available_locales')) : $lang;
        $lang = $lang != null ? $lang : 'en';
        \App::setLocale($lang);
    }
}
