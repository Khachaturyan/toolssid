<?php

namespace App\DataTables;


use App\Http\Controllers\ApiEquipmentController;
use App\Models\EquFavorite;
use App\Models\Equipment;
use App\Models\EquMenRigidLigament;
use App\Models\EquStatus;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;

class EquipmentDataTable extends LocalDataTable
{
    public $notArchiveItems = true;
    public $notWorkingItems = false;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    public static function createKitItemsScreen($row) : string{
        $equArr = Equipment::where('equipment.comp_id',$row->id)
            ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin("failure_types", "failure_types.id","=", "equipment.ftid")
            ->select("etypes.tname","etypes.tmark","etypes.tmodel", "etypes.is_comp",
                        "equipment.*", "failure_types.name as ft_name")
            ->get();
        $str =
            '<table style="width : 100%;">' .
            '<thead>' .
            '<tr>' .
            '<th>' . __('str.table_inv_num')      . '</th>' .
            '<th>' . __('str.table_ser')          . '</th>' .
            '<th>' . __('str.equ_mac')            . '</th>' .
            '<th>' . __('str.equipment2')         . '</th>' .
            '<th>' . __('str.table_label')        . '</th>' .
            '<th>' . __('str.table_failure_type') . '</th>' .
            '</tr>' .
            '</thead>' .
            '<tbody>';
        foreach ($equArr as $equ){
            $strRow  = "<tr>";
            $strRow .= "<td>" . $equ['inv_num'] . "</td>";
            $strRow .= "<td>" . $equ['ser_num'] . "</td>";
            $strRow .= "<td>" . $equ['mac']     . "</td>";
            $strRow .= "<td>" . $equ['tname'  ] . " " . $equ['tmark'] . " " . $equ['tmodel'] . "</td>";
            $strRow .= "<td>" . $equ['label'  ] . "</td>";
            $strRow .= "<td>" . $equ['ft_name'] . "</td>";
            $strRow .= "<tr>";
            $str .= $strRow;
        }
        $str .= '</tbody></table>';
        $linesCount = count($equArr) + 1; //количество строк + заголовок
        return'<br><div class="compscr" lines-count="' . $linesCount . '" style="width: 100%; position: absolute;z-index:1; background: #283046; margin-left: -20px">' .
            $str . '</div>';
    }

    public function dataTable($query)
    {
        return datatables()->eloquent($query)
            ->addColumn('menu',
                function($row){
                    $is_comp_li = $row->is_comp ? '<li><a class="dropdown-item" onclick="on_take_apart_click(\'' .
                        urlencode(json_encode($row)) . '\')">' .  __('str.equ_take_apart') . '</a></li>' : '';
                    $kitItemsTableStr = $row->is_comp ? self::createKitItemsScreen($row) : '';
                    $menu_width = $row->is_comp ? '100' : '84';
                    $editEquMenuItemStr = auth()->user()->enableEditEqu() ?
                        '<li><a class="dropdown-item" onclick="on_edit_click(\'' . urlencode(json_encode($row)) .
                        '\')">' .  __('str.table_edit') . '</a></li>' : "";
                    return $row->id . '<br><br><div class="dropdown">
                          <button style="border-radius : 24px;min-width: unset;width : 52px;" class="btn btn-secondary dropdown-toggle" type="button" id="ddmb' . $row->id . '"
                           data-bs-toggle="dropdown" aria-expanded="false">...</button>
                          <ul class="dropdown-menu" style="z-index:1001;border-radius : 16px;min-width: unset;width : ' . $menu_width . 'px;" aria-labelledby="ddmb' . $row->id . '">' .
                        '<li><a class="dropdown-item" onclick="onReportClick(\'' . urlencode(json_encode($row)) . '\')">' .  __('str.table_report') . '</a></li>' .
                        $editEquMenuItemStr .
                        '<li><a class="dropdown-item" onclick="onEquClick(\'' . urlencode(json_encode($row)) . '\')">' .  __('str.mens_bind') . '</a></li>' .
                        $is_comp_li .
                        '</ul>' .
                        '</div>' . $kitItemsTableStr;
                })
            ->editColumn('label2',
                function($row){
                    return '<a href="/events/' . $row->id . '" style="height: 20px">' . $row->label . '</a>';
                })
            ->editColumn('archive',
                function($data){
                    $str = $data->archive ? __('str.equ_is_archived') : __('str.equ_not_archived');
                    return $str;
                })
            ->editColumn('is_work2',
                function($data){
                    $str = $data->is_work ? __('str.table_working') : __('str.table_not_working');
                    if (!$data->is_work && $data->ft_name != null){
                        $str .= '-' . $data->ft_name;
                    }
                    return $str;
                })
            ->editColumn('mlabel2',
                function($data){
                    $textColor = substr(ApiEquipmentController::getStatColor($data->estat),0,7);
                    if ($data->uid > 0){
                        return '<a href="/events/0/' . $data->uid . '" style="height: 20px">' .
                            "<font color=\"$textColor\">" . $data->rname . " " . $data->post . " " . $data->mlabel . " " .
                            $data->mname . " " . $data->msurname  . "</font>" . '</a>';
                    }
                    if ($data->cid == 0){
                        return "<font color=\"$textColor\">не размещено</font>";
                    }
                    return "<font color=\"$textColor\">Блок " . $data->bname . ", ячейка " . $data->block_pos . "</font>";
                })
            ->editColumn('bat2',
                function($data){
                    if ($data->bat < 0){
                        return "нет";
                    }
                    $bat = $data->bat . "%";
                    if ($data->bat > 50){
                        return "<font color=\"#0b0\">" . $bat . "</font>";
                    }else{
                        return "<font color=\"#b00\">" . $bat . "</font>";
                    }
                })
            ->addColumn('act-qr', function ($row) {
                return '<a href="/qr/' . $row->id . '" style="height: 20px">
                                  <img src="/img/grid-four-up.svg" width="20px" height="20px"></a>';
            })
            ->addColumn('ft_name2', function ($row) {
                return $row->estat == -2 || $row->estat == -7 ? $row->ft_name : "";
            })
            ->addColumn('estat_des2', function ($row) {
                if ($row->estat <= 0){
                    return "<p style='color: " . ApiEquipmentController::getStatColor($row->estat) . ";'>" . ApiEquipmentController::getStatDescription($row->estat) . '</p>';
                }
                $item = EquStatus::whereId($row->estat)->first();
                return $item == null ? "" : "<p style='color: " . $item->color . ";'>" . $item->descr . '</p>';
            })
            ->addColumn('owner', function ($row) {
                $mens = EquFavorite::where('equ_favorites.eid',$row->id)
                    ->leftJoin("men", "men.id","=", "equ_favorites.uid")
                    ->select("men.*")->get();
                $str = "";
                foreach ($mens as $men){
                    if (strlen($str) > 0){
                        $str .= ", ";
                    }
                    $str .= '<a href="/events/0/' . $men->id . '" style="height: 20px">' .
                        $men->mlabel . " " . $men->mname . " " . $men->msurname . '</a>';
                }
                return $str;
            })
            ->addColumn('rig_owner', function ($row) {
                $mens = EquMenRigidLigament::where('equ_men_rigid_ligaments.eid',$row->id)
                    ->leftJoin("men", "men.id","=", "equ_men_rigid_ligaments.uid")
                    ->select("men.*")->get();
                $str = "";
                foreach ($mens as $men){
                    if (strlen($str) > 0){
                        $str .= ", ";
                    }
                    $str .= '<a href="/events/0/' . $men->id . '" style="height: 20px">' .
                        $men->mlabel . " " . $men->mname . " " . $men->msurname . '</a>';
                }
                return $str;
            })
            ->rawColumns([ 'menu', 'act-qr', 'bat2', 'mes', 'mlabel2', 'label2','owner','rig_owner', 'estat_des2']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Equipment $model)
    {
        $builder = $model->newQuery()
            ->where('etypes.is_cons','=',false)
            ->where('equipment.comp_id',0);
        if ($this->notWorkingItems){
            $builder = $builder
                ->where('equipment.archive','=',false)
                ->where('equipment.is_work','=',false);
        }else{
            $builder = $builder
                ->where('equipment.archive','=',$this->notArchiveItems == false);
        }

        return $builder->leftJoin("etypes", "etypes.id","=", "equipment.tid")
            ->leftJoin("men", "men.id","=", "equipment.uid")
            ->leftJoin("roles", "roles.id","=", "men.rid")
            ->leftJoin("cells", "cells.id","=", "equipment.cid")
            ->leftJoin("blocks", "blocks.id","=", "cells.block")
            ->leftJoin("failure_types", "failure_types.id","=", "equipment.ftid")
            ->leftJoin("stages", "stages.id","=", "equipment.mark_sid")
            ->leftJoin("blocks as blocks2", "blocks2.id","=", "equipment.mark_bid")
            ->select("roles.rname","roles.is_admin","men.mlabel","men.mname","men.msurname","men.post",
                "etypes.tname","etypes.tmark","etypes.tmodel", "etypes.is_comp", "equipment.*","cells.block_pos",
                "blocks.name as bname","blocks.id as bid","failure_types.name as ft_name","stages.name as sname",
                "blocks2.name as bname2");
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */

    public function html()
    {
        return $this->builder()
            ->setTableId('equ-table')
            ->columns($this->getColumns())
            //->fixedHeader(true)
            ->minifiedAjax()
            ->initComplete("function() { onInitComplete(this) }")
            ->createdRow("function(v) { onRowCreated(v) }" )
            ->dom('frtip')
            ->scrollX(true)
            //->scrollY(true)
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */

    protected function getColumns()
    {
        return [
            Column::make('menu')->title("")->width(64)->className("text-center"),
            Column::make('inv_num')->title(__('str.table_inv_num'))->name("equipment.inv_num")->className("text-center"),
            Column::make('ser_num')->title(__('str.table_ser'))->className("text-center"),
            Column::make('mac')->title(__('str.equ_mac'))->className("text-center"),
            Column::make('bname2')->title(__('str.table_block'))->name("blocks2.name"),
            Column::make('sname')->title(__('str.table_object'))->name("stages.name"),
            Column::make('archive')->title(__('str.equ_archiving'))->className("text-center"),
            Column::make('tname')->title(__('str.table_type'))->name("etypes.tname"),
            Column::make('tmodel')->title(__('str.table_model'))->name("etypes.tmodel"),
            Column::make('estat_des2')->title(__('str.table_status')),
            Column::make('label2')->title(__('str.table_label'))->name("equipment.label"),
            Column::make('mlabel2')->title(__('str.table_place'))->name("men.mlabel"),
            Column::make('owner')->title(__('str.table_owner')),
            Column::make('rig_owner')->title(__('str.table_linkin')),
            Column::make('ft_name2')->title(__('str.table_failure_type'))->className("text-center"),
            Column::make('description')->title(__('str.table_description'))->className("text-center"),
            Column::make('ft_name')->title(__('str.table_failure_type'))->name("failure_types.name")->visible(false),
            Column::make('estat_des')->title(__('str.table_status'))->name("equipment.estat_des")->visible(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Equipment_' . date('YmdHis');
    }
}
