<?php

namespace App\DataTables;

use App\Http\Controllers\MenLockController;
use App\Http\Controllers\ProjectSettingsController;
use App\Models\Equipment;
use App\Models\Man;
use App\Models\MenLock;
use App\Models\OrionSendItem;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ManDataTable extends LocalDataTable
{

    public $isFired = false;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                return '<img src="/img/create-outline.svg" width="20px" height="20px">';
            })
            ->addColumn('fired2', function ($row) {
                if ($row->fired == 0){
                    return __('str.mens_in_state');
                } else if ($row->fired == 2){
                    return __('str.mens_status_outsource');
                } else {
                    return __('str.mens_status_fired');
                }
            })
            ->addColumn('can_dia2', function ($row) {
                return $row->can_dia == 0 ? "Нет" : "Да";
            })
            ->editColumn('msurname2', function ($row) {
                return '<a href="/events-men/' . $row->id . '" style="height: 20px">' .
                        $row->msurname . " " . $row->mname . " " . $row->mpatronymic .
                        '</a>';
            })
            ->editColumn('ucr',
                function($row){
                    if ($row->ucr == null || $row->ucr == 0){
                        return "";
                    }
                    $parts = explode(".",ProjectSettingsController::toLocalTime($row->ucr));
                    return str_replace(array('T'), " ", $parts[0]);
                })
            ->editColumn('is_lock_orion', function($row){
                    if (strlen($row->raw_label) == 0){
                        $orionRes =  "-";
                    }else{
                        $item = OrionSendItem::where('raw_label',$row->raw_label)->first();
                        if ($item == null){
                            $orionRes =  __('str.table_no') . '/' . __('str.table_no');
                        }else{
                            if ($item->blocked == 1){ // заблокировано
                                if ($item->send_state == 1){
                                    $orionRes =  __('str.table_yes') . '/' . __('str.table_yes');
                                }else{
                                    $orionRes =  __('str.table_yes');
                                }
                            }else{ // разблокировано
                                if ($item->send_state == 1){
                                    $orionRes =  __('str.table_no') . '/' . __('str.table_no');
                                }else{
                                    $orionRes =  __('str.table_no');
                                }
                            }
                        }
                    }
                    $lockRes = !MenLockController::getLockStatus($row->id) ? __('str.table_no') : __('str.table_yes');
                    return $lockRes . '<br>' . $orionRes;
                })
            ->addColumn('equ', function ($row) {
                if ($row->is_admin == 1){
                    return "";
                }
                $equipments = Equipment::where('equipment.uid',$row->id)
                    ->leftJoin("etypes", "etypes.id","=", "equipment.tid")
                    ->select("etypes.tname","etypes.tmark","etypes.tmodel", "equipment.*")->get();
                $str = "";
                foreach ($equipments as $equ){
                    if (strlen($str) > 0) $str .= "; ";
                    $str .= '<a href="/events/' . $equ->id . '" style="height: 20px">' .  $equ->label . '</a>' .
                            " " . $equ->tname . " " . $equ->tmark . " " . $equ->tmodel;
                }
                return $str;
            })
            ->rawColumns(['action', 'msurname2','to-stat','equ','is_lock_orion']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Man $model)
    {
        if ($this->isFired){
            return $model->newQuery()
                ->where("fired",'=',1)
                ->leftJoin("roles", "roles.id","=", "men.rid")
                ->leftJoin("users", "users.id","=", "men.web_uid")
                ->select("roles.rname","roles.is_admin","men.*","users.email","users.created_at as ucr");
        }else{
            return $model->newQuery()
                ->where("fired",'!=',1)
                ->leftJoin("roles", "roles.id","=", "men.rid")
                ->leftJoin("users", "users.id","=", "men.web_uid")
                ->select("roles.rname","roles.is_admin","men.*","users.email","users.created_at as ucr");
        }
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('men-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->initComplete("function () { onInitComplete(this) }")
                    ->dom('frtip')
                    ->scrollX(true)
                    ->orderBy(1)
                    ->parameters([
                        'responsive' => true
                    ])
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('action')->title(__('str.table_edit'))->width(50)->className("text-center")->visible(auth()->user()->create_user()),
            //Column::make('to-stat')->title(__('str.table_state3'))->width(50)->className("text-center")->visible(auth()->user()->create_user()),
            Column::make('id')->title(__('str.table_num'))->width(80)->className("text-center"),
            Column::make('msurname2')->title(__('str.table_fio'))->name("men.msurname"),
            Column::make('fired2')->title(__('str.table_state2'))->width(50)->name("men.fired"),
            Column::make('mname')->title(__('str.table_name2'))->name("men.mname")->visible(false),
            Column::make('mpatronymic')->name("men.mpatronymic")->visible(false),
            Column::make('mlabel')->title(__('str.table_label')),
            Column::make('rname')->title(__('str.table_role'))->name("roles.rname"),
            Column::make('email')->title(__('str.table_feedback_email'))->name("users.email"),
            Column::make('post')->title(__('str.table_post'))->name("men.post"),
            Column::make('is_lock_orion')->title(__('str.table_orion_lock'))->width(50)->className("text-center")->name("men.raw_label"),
            Column::make('ucr')->title(__('str.table_user_create_date'))->name("users.created_at")->width(80)->className("text-center"),
            Column::make('equ')->title(__('str.equipment')),
            Column::make('can_dia2')->title(__('str.table_diagnostic'))->name("men.can_dia")->className("text-center"),
            Column::make('str_ext_login')->title(__('str.mens_login_in_export'))->name("men.str_ext_login")->className("text-center")
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Man_' . date('YmdHis');
    }
}
