<?php

namespace App\DataTables;

use App\Models\Equipment;
use App\Models\Man;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use DB;

class MenMessageDataTable extends LocalDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()->eloquent($query)
            ->addColumn('action', function ($row) {
                return "<label style='height:20px'><input type='checkbox' style='height:20px' onclick='handleClick(this);' value='" . $row->id . "' ></label>";
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Man $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Man $model)
    {
        return $model->newQuery()
            ->leftJoin("equipment", "men.id","=", "equipment.uid")
            ->leftJoin("roles", "men.rid","=", "roles.id")
            ->where("equipment.uid",">", 0)
            ->where("equipment.bat",">=",0)
            ->select("men.*","equipment.label","equipment.id as eid","roles.rname");
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('block-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->scrollX(true)
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('action')->title(__('str.table_select'))->width(50)->className("text-center"),
            Column::make('id')->title(__('str.table_num'))->width(50)->className("text-center"),
            Column::make('rname')->title(__('str.table_role'))->name("roles.rname"),
            Column::make('mname')->title(__('str.table_name2')),
            Column::make('msurname')->title(__('str.table_surname')),
            Column::make('mlabel')->title(__('str.table_label_men')),
            Column::make('label')->title(__('str.table_label_unit'))->name("equipment.label")
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ManMessage_' . date('YmdHis');
    }
}
