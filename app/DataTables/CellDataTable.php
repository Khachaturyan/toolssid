<?php

namespace App\DataTables;

use App\Http\Controllers\ApiCellController;
use App\Models\Block;
use App\Models\Cell;
use App\Models\MenCellLink;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use function PHPUnit\Framework\isNull;

class CellDataTable extends LocalDataTable
{

    public $bid = null;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $kitItemsTableStr = $row->is_comp ? EquipmentDataTable::createKitItemsScreen($row) : '';
                $menu_width = $row->is_comp ? '100' : '84';
                return '<div class="dropdown">
                          <button style="border-radius : 24px;min-width: unset;width : 52px;" class="btn btn-secondary dropdown-toggle" type="button" id="ddmb' . $row->id . '"
                           data-bs-toggle="dropdown" aria-expanded="false">...</button>
                          <ul class="dropdown-menu" style="border-radius : 16px;min-width: unset;width : ' . $menu_width . 'px;" aria-labelledby="ddmb' . $row->id . '">
                            <li><a class="dropdown-item" onclick="on_edit_click(\'' . urlencode(json_encode($row)) . '\')">' .  __('str.table_edit') . '</a></li>
                            <li><a class="dropdown-item" onclick="on_bind_click(\'' . urlencode(json_encode($row)) . '\')">' .  __('str.mens_bind') . '</a></li>
                            <li><a class="dropdown-item" onclick="on_unbind_click(\'' . urlencode(json_encode($row)) . '\')">' .  __('str.mens_unbind') . '</a></li>' .
                           '</ul></div>' . $kitItemsTableStr;
            })
            ->addColumn('tname2', function ($row) {
                return $row->pr_id ? __('str.products') : $row->tname;
            })
            ->addColumn('tmark2', function ($row) {
                return $row->tmark . ' ' . $row->tmodel;
            })
            ->addColumn('owner', function ($row) {
                $mitems = MenCellLink::where('men_cell_links.cid',$row->id)
                    ->leftJoin("men", "men.id", "=", "men_cell_links.mid")
                    ->where("men.fired","!=",1)
                    ->select("men.*")
                    ->get();
                $str = '';
                foreach ($mitems as $men){
                    if (strlen($str) > 0) $str .= "<br>";
                    $str .= $men->mname . " " . $men->msurname . " " . $men->mpatronymic;
                }
                return $str;
            })
            ->addColumn('dst2',
                function($data){
                    if ($data->is_lock){
                        return __('str.state_lock');
                    }
                    switch ($data->dst){
                        case 1:
                            return __('str.table_open');
                        case 2:
                            return __('str.table_close');
                        case 3:
                            return __('str.table_hack');
                        case 4:
                            return __('str.table_opening');
                        case 5:
                            return __('str.table_error');
                        default:
                            return __('str.table_unknown');
                    }
                })
            ->editColumn('cs_name2',
                function($data){
                    return $data->csid == 0 ? __('str.table_def_size') : $data->cs_name . " " . $data->cs_des;
                })
            /*
            ->editColumn('is_lock2',
                function($data){
                    if ($data->is_lock){
                        return __('str.table_yes');
                    }else{
                        return __('str.table_no');
                    }
                })
            */
            ->rawColumns(['owner','action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Cell $model)
    {
        // инициализация переменных учавствующих в запросе
        $tableAndField = 'cells.is_arc';
        $numberVal = 0;

        // получение выбранного номера позиции ячейки в блоке
        $block_pos = $this->request()->get('block_pos');
        if(!empty($block_pos)) {
            // изменение значений переменных для выполнения запроса
            $tableAndField = 'cells.block_pos';
            $numberVal = $block_pos;
        }

        $builder = $model->newQuery();
        if ($this->bid != null){
            $builder = $builder->where("cells.block",$this->bid);
        }
        $builder = $builder->where("$tableAndField", $numberVal)
            ->leftJoin("blocks", "blocks.id","=", "cells.block")
            ->leftJoin('equipment','equipment.cid','=','cells.id')
            ->leftJoin('etypes','equipment.tid','=','etypes.id')
            ->leftJoin('cell_sizes','cells.csid','=','cell_sizes.id')
            ->leftJoin('products','cells.id','=','products.cid')
            ->select("blocks.name","cell_sizes.name as cs_name","cell_sizes.des as cs_des","cells.*",
                "products.id as pr_id", "etypes.tname","etypes.tmark","etypes.tmodel","etypes.is_comp",
                "equipment.label","equipment.id as equ_id","equipment.inv_num");
        return $builder;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('cell-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->initComplete("function () { onInitComplete(this) }")
                    ->createdRow("function(v) { onRowCreated(v) }" )
                    ->dom('frtip')
                    ->scrollX(true)
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('action')->title(__('str.table_edit'))->width(50)->className("text-center"),
            Column::make('owner')->title(__('str.table_owner')),
            Column::make('tname2')->title(__('str.table_unit'))->name("etypes.tname"),
            Column::make('tmark2')->title(__('str.table_mark')),
            Column::make('tmark')->title(__('str.table_mark'))->visible(false)->name("etypes.tmark"),
            Column::make('tmodel')->title(__('str.table_model'))->visible(false)->name("etypes.tmodel"),
            Column::make('label')->title(__('str.table_label'))->name("equipment.label"),
            Column::make('inv_num')->title(__('str.table_inv_num'))->name("equipment.inv_num")->className("text-center"),
            Column::make('block_pos')->name("cells.block_pos")->title(__('str.table_block_num'))->width(64)->className("text-center"),
            Column::make('dst2')->title(__('str.table_dst'))->className("text-center")->name("cells.dst"),
            Column::make('name')->title(__('str.table_block_name'))->name("blocks.name"),
            Column::make('cs_name2')->title(__('str.table_size'))->className("text-center")->name("cell_sizes.name"),
            Column::make('cs_des')->title("")->visible(false)->name("cell_sizes.des"),
            //Column::make('is_lock2')->title(__('str.table_lock'))->name("cells.is_lock")->className("text-center"),
            Column::make('des')->title(__('str.table_description'))->name("cells.des"),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Cell_' . date('YmdHis');
    }
}
