<?php

namespace App\DataTables;

use App\Http\Controllers\ProjectSettingsController;
use App\Models\Log;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;

/**
 * @property mixed|null TAKE_ITEM_WORKER
 */
class StatsDataTable extends LocalDataTable
{
    private $TAKE_ITEM_WORKER        = 6;
    private $RETURN_ITEM_WORKER      = 7;
    private $RETURN_ITEM_NOT_WORKING = 8;

    public $mid = 0;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()->eloquent($query)
            ->addColumn('created_at2',
                function($data){
                    $parts = explode(".",ProjectSettingsController::toLocalTime($data->created_at));
                    $parts = explode(' ',str_replace(array('T'), " ", $parts[0]));
                    $parts = explode('-', $parts[0]);
                    return count($parts) > 2 ? $parts[2] . '.' . $parts[1] . '.' . $parts[0] : '';
                })
            ->addColumn('time_input',
                function($data){
                    $parts = explode(".",ProjectSettingsController::toLocalTime($data->created_at));
                    $parts = explode(' ',str_replace(array('T'), " ", $parts[0]));
                    return $parts[1];
                })
            ->addColumn('body2',
                function($data){
                    $parts = explode(' ', $data->body);
                    if (count($parts) < 3){
                        return "";
                    }else{
                        return $parts[1] . ' ' . $parts[2];
                    }

                })
            ->addColumn('own_time', function ($row) {
                $logRet = Log::whereIn('ltp',array($this->RETURN_ITEM_WORKER, $this->RETURN_ITEM_NOT_WORKING))
                    ->where('mid',$row->mid)
                    ->where('created_at','>',$row->created_at)
                    ->orderBy('id')
                    ->first();
                if ($logRet == null){
                    return __('str.table_on_hand');
                }
                $hourOwn = intval(((strtotime($logRet->created_at) - strtotime($row->created_at)) * 10 / 3600)) / 10;
                return "" . $hourOwn;
            })
            ->addColumn('time_complete', function ($row) {
                $logRet = Log::whereIn('ltp',array($this->RETURN_ITEM_WORKER, $this->RETURN_ITEM_NOT_WORKING))
                    ->where('mid',$row->mid)
                    ->where('created_at','>',$row->created_at)
                    ->orderBy('id')
                    ->first();
                if ($logRet == null){
                    return "";
                }
                $parts = explode(".",ProjectSettingsController::toLocalTime($logRet->created_at));
                $parts = explode(' ',str_replace(array('T'), " ", $parts[0]));
                return $parts[1];
            })
            ->addColumn('tsd_status', function ($row) {
                $logRet = Log::whereIn('ltp',array($this->RETURN_ITEM_WORKER, $this->RETURN_ITEM_NOT_WORKING))
                    ->where('mid',$row->mid)
                    ->where('created_at','>',$row->created_at)
                    ->orderBy('id')
                    ->first();
                if ($logRet == null){
                    return __('str.table_on_hand');
                }
                return $logRet->ltp == $this->RETURN_ITEM_WORKER ? __('str.table_tsd_state_work') : __('str.table_tsd_state_not_work');
            })
            ->rawColumns(['body','own_time','created_at','time_input','time_complete']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Log $model)
    {
        if ($this->mid == 0){
            return $model->newQuery()->where('ltp', $this->TAKE_ITEM_WORKER);
        }else{
            return $model->newQuery()->where('mid',$this->mid)->where('ltp', $this->TAKE_ITEM_WORKER);
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('stats-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->scrollX(true)
                    ->orderBy(0)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('body2')->title(__('str.table_men'))->name('body'),
            Column::make('created_at2')->title(__('str.table_date'))->width(160)->className("text-center"),
            Column::make('time_input')->title(__('str.table_time_output'))->width(160)->className("text-center"),
            Column::make('time_complete')->title(__('str.table_time_complete'))->width(160)->className("text-center"),
            Column::make('own_time')->title(__('str.table_using'))->width(256)->className("text-center"),
            Column::make('tsd_status')->title(__('str.table_tsd_status'))->width(160)->className("text-center"),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Items_' . date('YmdHis');
    }
}
