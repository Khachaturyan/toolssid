<?php

namespace App\DataTables;

use App\Models\FailureType;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;

class FailureTypeDataTable extends LocalDataTable{

    public $showArchive = false;

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $img = !$this->showArchive ? 'delete_gray_24dp.svg' : 'restore_gray_24dp.svg';
                return '<img src="/img/' . $img . '" width="20px" height="20px" onclick="onChangeArchiveStatus(' . $row->id . ')">';
            })
            ->rawColumns(['action']);
    }

    public function query(FailureType $model)
    {
        return $model->newQuery()->where('is_archive',$this->showArchive ? 1 : 0);
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('failure-type-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->scrollX(true)
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    protected function getColumns()
    {
        return [
            Column::make('action')->title($this->showArchive ? __('str.table_restore') : __('str.table_remove'))->width(50)->className("text-center"),
            Column::make('id')->title(__('str.table_num'))->width(80)->className("text-center"),
            Column::make('name')->title(__('str.table_name')),
        ];
    }

    protected function filename()
    {
        return 'Items_' . date('YmdHis');
    }
}
