<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class MenHeaderReader implements ToCollection
{
    public $startRowIndex = 0;
    public $headerIsInit  = false;
    public $isDnsFormat   = false;
    public $isFmFormat    = false;

    public function collection(Collection $rows)
    {
        $counter = 0;
        foreach ($rows as $row){
            if ($row[0] == "№ п/п"  || $row[0] == "№"     || $row[0] == "EXT_ID" || $row[0] == "Смена" ||
                $row[0] == "Дата"   || $row[0] == "Табел."|| $row[0] == "NAME"   || $row[0] == "Метка" ||
                $row[0] == "Группа" || $row[0] == "Имя"   || $row[0] == "ФИО"    || $row[0] == "Name"  ||
                $row[0] == "Табельный номер"  || $row[0] == "Фамилия" || $row[0] == "Тип записи" || $row[0] == "ID в базе экспорта"){
                $this->startRowIndex = $counter;
                $this->headerIsInit = true;
                $this->isDnsFormat = $row[0] === "Тип записи";
                $this->isFmFormat = $row[0] === "№";
                //header('Content-Type: text/html; charset=utf-8');
                //die($row[0]);
                break;
            }
            $counter++;
        }
    }
}
