<?php

namespace App\Imports;

use App\Exports\MenExport;
use App\Http\Controllers\RoleController;
use App\Models\Man;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MenImport implements ToModel, WithHeadingRow
{
    public $startRowIndex = 0;
    public $storeKeeperId = 0; // ид кладовщика

    public $isDnsHeaderFormat   = false;
    public $isFmFormat          = false;
    public $lastExtId           =  null;
    public $updatingMenId       =  null;

    public $midArray            = array();

    public int $labelEncodeAlg  = 0;

    function __construct() {
        $this->storeKeeperId = RoleController::getRoleStorekeeperId();
    }

    public function moveToFired($timeOffset = 900){ // 15 min
        Man::where('updated_at', '<', Carbon::now()->addSeconds(-$timeOffset))->where('str_ext_id','!=','')->where('fired',0)->update(['fired' => 1, 'mlabel' => '']);
    }

    public function moveToFiredMid(array $mid, bool $ignoreEmptyExtId){
        if ($ignoreEmptyExtId){
            Man::whereNotIn('id', $mid)->where('str_ext_id','!=','')->update(['fired' => 1, 'mlabel' => '']);
        }else{
            Man::whereNotIn('id', $mid)->update(['fired' => 1, 'mlabel' => '']);
        }
    }

    private function labelEncode($label){
        switch ($this->labelEncodeAlg){
            case  1: return $this->protecLabelEncode($label);
            case  2: return $this->easyHexDecEncode($label);
            case  3: return $this->ozonHexDecEncode($label);
            case  4: return $this->karcherHexDecEncode($label);
            case  5: return $this->lentaHexDecEncode($label);
            case  6: return $this->sberLogisticHexDecEncode($label);
            default: return $label;
        }
    }

    private function protecLabelEncode($label): string
    {
        $hex = hexdec($label) >> 1;
        $hex = "" . ($hex | (1 << 25));
        while (strlen($hex) < 10) $hex = "0" . $hex;
        return $hex;
    }

    private function easyHexDecEncode($label) : string
    {
        $res = hexdec($label);
        while (strlen($res) < 10) $res = "0" . $res;
        return $res;
    }

    private function ozonHexDecEncode($label) : string
    {
        $valLabel = $label;
        $arrayPartsLabel = explode('/', $valLabel);
        // echo '<pre>';
        //     print_r($arrayPartsLabel);
        // echo '</pre>';
        $arrayPartsLabel[0] = dechex((float) $arrayPartsLabel[0]);
        $arrayPartsLabel[1] = dechex((float) $arrayPartsLabel[1]);
        $valLabel = strval(implode('', $arrayPartsLabel));
        // echo '<pre>';
        //     print_r($valLabel);
        // echo '</pre>';
        //die;
        // if (strlen($valLabel) < 8 || preg_match('/[^A-F0-9]/', $valLabel)) {
        //     return "";
        // }
        $res = "" . hexdec(substr($valLabel, -6));
        while (strlen($res) < 10){
            $res = "0" . $res;
        }
        return $res;
    }

    private function karcherHexDecEncode($label) : string
    {
        if (strlen($label) < 8 || preg_match('/[^A-F0-9]/', $label)) {
            return "";
        }
        $res = "" . hexdec(substr(substr($label,0,-2), -6));
        while (strlen($res) < 10){
            $res = "0" . $res;
        }
        return $res;
    }

    private function lentaHexDecEncode($label) : string
    {
        $the_character = '.';
        if (strpos($label, $the_character) === false) {
            $res = "" . hexdec($label);
            while (strlen($res) < 10) $res = "0" . $res;
            return $res;
        } else {
            $arr = explode($the_character, $label);
            if (count($arr) != 2){
                return "";
            }
            $hex0 = dechex(intval($arr[0]));
            $hex1 = dechex(intval($arr[1]));
            while (strlen($hex1) < 4) $hex1 = "0" . $hex1;
            $hex = $hex0 . $hex1;
            $res = "" . hexdec($hex);
            while (strlen($res) < 10) $res = "0" . $res;
            return $res;
        }
    }

    private function sberLogisticHexDecEncode($label) : string
    {
        $arrSimCyrillic = ['Ф','И','С','В','У','А'];
        $arrSimLatin = ['A','B','C','D','E','F'];

        $input = $label;
        $input = str_replace($arrSimCyrillic, $arrSimLatin, $input);
        if (strlen($input) < 8 || preg_match('/[^A-F0-9]/', $input)) {
            return "";
        }
        $res = base_convert($input, 16, 10) & 0x3FFFFFF;
        while (strlen($res) < 10) {
            $res = "0" . $res;
        }
        return $res;
    }

    /**
     * @return int
     */
    public function headingRow(): int
    {
        return $this->startRowIndex;
    }

    private function isDetskiyMir(array $row){
        return array_key_exists('pp',               $row) &&
               array_key_exists('metka',            $row) &&
               array_key_exists('familiya',         $row) &&
               array_key_exists('imya',             $row) &&
               array_key_exists('otcestvo',         $row) &&
               array_key_exists('dolznost',         $row) &&
               array_key_exists('rol',              $row) &&
               array_key_exists('brigadir',         $row) &&
               array_key_exists('status_sotrudnika',$row);
    }

    private function isFmGroup(array $row){
        return array_key_exists('gruppa',       $row) &&
               array_key_exists('podrazd',      $row) &&
               array_key_exists('fio',          $row) && //$row['fio'      ] != null &&
               array_key_exists('dolznost',     $row) &&
               array_key_exists('data',         $row) &&
               array_key_exists('vremya',       $row) &&
               array_key_exists('klyuc',        $row) && //$row['klyuc'    ] != null &&
               array_key_exists('obnovlenie',   $row) &&
               array_key_exists('vremzona',     $row) &&
               array_key_exists('organizaciya', $row) &&
               array_key_exists('id',           $row) /*&& $row['id'       ] != null*/;
    }

    private function isLeruaMerlen(array $row){
        return array_key_exists('familiya', $row) &&
               array_key_exists('imya',     $row) &&
               array_key_exists('otcestvo', $row) &&
               array_key_exists('gruppa',   $row) &&
               array_key_exists('firma',    $row) &&
               array_key_exists('otdel',    $row) &&
               array_key_exists('dolznost', $row) &&
               array_key_exists('karty',    $row) &&
               array_key_exists('rol',      $row);
    }

    private function isPercoFormat(array $row){
        return array_key_exists('pp',          $row) && $row['pp']          != null &&
                             array_key_exists('tab',         $row) && $row['tab']         != null &&
                             array_key_exists('dolznost',    $row) && $row['dolznost']    != null &&
                             array_key_exists('familiya',    $row) && $row['familiya']    != null &&
                             array_key_exists('imya',        $row) && $row['imya']        != null &&
                             array_key_exists('otcestvo',    $row) &&
                             array_key_exists('nomer_karty', $row) && $row['nomer_karty'] != null &&
                             strlen(trim($row['dolznost'])) > 0;
    }

    private function isAlidiFormat(array $row){
        return array_key_exists('id_v_baze_eksporta',         $row) && $row['id_v_baze_eksporta'        ] != null &&
            array_key_exists('metka',      $row) && $row['metka'     ] != null &&
            array_key_exists('familiya', $row) && $row['familiya'] != null &&
            array_key_exists('otcestvo', $row) && $row['otcestvo'] != null &&
            array_key_exists('dolznost', $row) && $row['dolznost'] != null &&
            array_key_exists('imya',         $row) && $row['imya']         != null;
    }

    private function isAliexpressFormat(array $row){
        return array_key_exists('name',         $row) && $row['name'        ] != null &&
                              array_key_exists('firstname',    $row) &&
                              array_key_exists('secondname',   $row) &&
                              array_key_exists('tableno',      $row) && $row['tableno'     ] != null &&
                              array_key_exists('fullcardcode', $row) && $row['fullcardcode'] != null &&
                              array_key_exists('post',         $row) && $row['post']         != null;
    }

    private function isLamodaFormat(array $row){
        return array_key_exists('smena',                  $row) && $row['smena']                  != null &&
                              array_key_exists('tabelnyi_nomer',         $row) &&
                              array_key_exists('fio',                    $row) && $row['fio']                    != null &&
                              array_key_exists('grafik_smennosti',       $row) && $row['grafik_smennosti']       != null &&
                              array_key_exists('dolznost',               $row) && $row['dolznost']               != null &&
                              array_key_exists('ucastok_1s',             $row) && $row['ucastok_1s']             != null &&
                              array_key_exists('data_uvolneniya',        $row) &&
                              array_key_exists('login',                  $row) && $row['login']                  != null &&
                              array_key_exists('id_propuska',            $row) && $row['id_propuska']            != null &&
                              array_key_exists('data_priyoma_na_rabotu', $row) && $row['data_priyoma_na_rabotu'] != null;
    }

    private function isRusClimatFormat(array $row): bool
    {
        return array_key_exists('podrazdelenie',  $row) &&
                                 array_key_exists('otcestvo',       $row) &&
                                 array_key_exists('imya',           $row) &&
                                 array_key_exists('dolznost',       $row) &&
                                 array_key_exists('data',           $row) && $row['data'      ]  != null &&
                                 array_key_exists('familiya',       $row) && $row['familiya'  ]  != null &&
                                 array_key_exists('tab_nomer',      $row) && $row['tab_nomer' ]  != null &&
                                 array_key_exists('kod_karty',      $row) && $row['kod_karty' ]  != null;
    }

    private function isHouseHoldFormat(array $row): bool
    {
        return array_key_exists('tabel',       $row) && $row['tabel'       ]  != null &&
                                 array_key_exists('fio',         $row) && $row['fio'         ]  != null &&
                                 array_key_exists('dolznost',    $row) && $row['dolznost'    ]  != null &&
                                 array_key_exists('nomer_karty', $row) && $row['nomer_karty' ]  != null;
    }

    private function isCitilinkFormat(array $row): bool
    {
        return array_key_exists('tabelnyi_nomer', $row) && $row['tabelnyi_nomer' ]  != null &&
               array_key_exists('familiya',       $row) && $row['familiya'       ]  != null &&
               array_key_exists('imya',           $row) && $row['imya'           ]  != null &&
               array_key_exists('otdel',          $row) && $row['otdel'          ]  != null &&
               array_key_exists('kod_propuska',   $row) && $row['kod_propuska'   ]  != null &&
               array_key_exists('dolznost',       $row) && $row['dolznost'       ]  != null;
    }

    private function isGreenfieldFormat(array $row): bool
    {
        return array_key_exists('familiya',      $row) && $row['familiya'     ]  != null &&
               array_key_exists('imya',          $row) && $row['imya'         ]  != null &&
               array_key_exists('otcestvo',      $row) &&
               array_key_exists('dolznost',      $row) && $row['dolznost'     ]  != null &&
               array_key_exists('id',            $row) && $row['id'           ]  != null &&
               array_key_exists('nomera_kart',   $row) && $row['nomera_kart'  ]  != null;
    }

    //private function isSDEKFormat(array $row): bool
    //{
    //    return array_key_exists('familiya',            $row) && $row['familiya'     ]  != null &&
    //           array_key_exists('imya',                $row) &&
    //           array_key_exists('otcestvo',            $row) &&
    //           array_key_exists('dolznost',            $row) && $row['dolznost'     ]  != null &&
    //           array_key_exists('podrazdelenie',       $row) && $row['podrazdelenie']  != null &&
    //           array_key_exists('kod_semeistva_karty', $row) &&
    //           array_key_exists('nomer_karty',         $row) &&
    //           array_key_exists('identifikator',       $row);
    //}

    private function isRusAgroMasloFormat(array $row): bool
    {
        return
            array_key_exists('pp',                  $row) &&
            array_key_exists('tab',                 $row) &&
            array_key_exists('sotrudnik',           $row) &&
            array_key_exists('dolznost',            $row) &&
            array_key_exists('grafik_raboty',       $row) &&
            array_key_exists('priem_na_rabotu',     $row) &&
            array_key_exists('naruseniya',          $row) &&
            array_key_exists('familiya',            $row) &&
            array_key_exists('imya',                $row) &&
            array_key_exists('otcestvo',            $row) &&
            array_key_exists('familiya_io',         $row) &&
            array_key_exists('kod_semeistva_karty', $row) &&
            array_key_exists('nomer_karty',         $row) &&
            array_key_exists('identifikator',       $row);
    }

    private function isDnsFormat(array $row): bool
    {
        return array_key_exists('tip_zapisi',          $row) && $row['tip_zapisi'    ]  != null &&
               array_key_exists('imya',                $row) && $row['imya'          ]  != null &&
               array_key_exists('otdel',               $row) && $row['otdel'         ]  != null &&
               array_key_exists('dolznostmodel',       $row) &&
               array_key_exists('tab',                 $row) && $row['tab'           ]  != null &&
               array_key_exists('rezim',               $row) && $row['rezim'         ]  != null &&
               array_key_exists('nomer_propuska',      $row) && $row['nomer_propuska']  != null &&
               array_key_exists('vremya_vydaci',       $row) && $row['vremya_vydaci' ]  != null &&
               array_key_exists('srok_deistviya',      $row) && $row['srok_deistviya']  != null &&
               array_key_exists('nomer_telefona',      $row);
    }

    private function isArmtekFormat(array $row): bool
    {
        return array_key_exists('imya',                $row) && $row['imya'          ]  != null &&
               array_key_exists('otdel',               $row) && $row['otdel'         ]  != null &&
               array_key_exists('dolznostmodel',       $row) &&
               array_key_exists('tab',                 $row) && $row['tab'           ]  != null &&
               array_key_exists('rezim',               $row) && $row['rezim'         ]  != null &&
               array_key_exists('nomer_propuska',      $row) && $row['nomer_propuska']  != null &&
               array_key_exists('vremya_vydaci',       $row) && $row['vremya_vydaci' ]  != null &&
               array_key_exists('srok_deistviya',      $row) && $row['srok_deistviya']  != null &&
               array_key_exists('nomer_telefona',      $row) &&
               array_key_exists('operator',            $row);
    }

    private function isAshanFormat(array $row) : bool {
        return array_key_exists('fio',                $row) && $row['fio'            ]  != null &&
            array_key_exists('tabel',                 $row) && $row['tabel'          ]  != null &&
            array_key_exists('skud',                  $row) && $row['skud'           ]  != null;
    }

    private function isMVideoFormat(array $row) : bool {
        return array_key_exists('post',     $row) &&
            array_key_exists('id_propuska', $row) && $row['id_propuska' ]  != null &&
            array_key_exists('name',        $row) && $row['name'        ]  != null &&
            array_key_exists('midname',     $row) &&
            array_key_exists('lastname',    $row) && $row['lastname'    ]  != null;
    }

/*
    private function isDnsFormatLite(array $row): bool
    {
        return array_key_exists('nomer_propuska',      $row) && $row['nomer_propuska']  != null;
    }
*/
    private function isCanonFormat(array $row){
        return array_key_exists('ext_id',          $row) &&
               array_key_exists('ext_login',       $row) &&
               array_key_exists('post',            $row) &&
               array_key_exists('lastname',        $row) &&
               array_key_exists('firstname',       $row) &&
               array_key_exists('middlename',      $row) &&
               array_key_exists('hid',             $row);
    }
    //------------------------------------------------------------------------------------------------------------------
    private function toValidDnsDecimalLabel($rawLabel) : string {
        $arr = explode("\n",$rawLabel);
        $res = "";
        foreach ($arr as $arrItem){
            if(strpos($arrItem, ',') !== false){
                $res = $arrItem;
                break;
            }
        }
        return $res;
    }
    private function updateDnsPropusk($str_ext_id, $label){
        $men = Man::where('str_ext_id',$str_ext_id)->first();
        if ($men != null){
            $men->raw_label .= "\n" . $label;
            $men->mlabel = $this->toValidDnsDecimalLabel($men->raw_label);
            $men->update();
        }
    }
    //------------------------------------------------------------------------------------------------------------------
    public function parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,$raw_label,$fired,$str_ext_login){
        if (($str_ext_id == null || strlen($str_ext_id) == 0) && ($mlabel == null || strlen($mlabel) == 0)) die("нет айдишника и метки...(((");
        $men = $str_ext_id != null ? Man::where('str_ext_id',$str_ext_id)->first() : Man::where('mlabel',$mlabel)->first();
        if ($fired == 1) $mlabel = "";
        if ($men != null){
            $this->updatingMenId = $men->id;
            $men->msurname       = $msurname;
            $men->mname          = $mname;
            $men->mpatronymic    = $mpatronymic;
            $men->mlabel         = $mlabel;
            $men->str_ext_id     = $str_ext_id;
            $men->fired          = $fired;
            $men->post           = $post;
            $men->raw_label      = $raw_label;
            $men->str_ext_login  = $str_ext_login;
            $men->update();
            $this->midArray[] = $men->id;
            return null;
        }else{
            $men = new Man([
                'str_ext_id'    => $str_ext_id,
                'str_ext_login' => $str_ext_login,
                'msurname'      => $msurname,
                'mname'         => $mname,
                'mpatronymic'   => $mpatronymic,
                'mlabel'        => $mlabel,
                'fired'         => $fired,
                'rid'           => $this->storeKeeperId,
                'pin'           => '',
                'en_items'      => '',
                'post'          => $post,
                'can_dia'       => 0,
                'raw_label'     => $raw_label,
            ]);
            $this->midArray[] = $men->id;
            return $men;
        }
    }

    private function percoConvertCardNumber($semKarty,$numKarty){
        $colAJ = dechex(intval($semKarty));
        $colAK = dechex(intval($numKarty));
        $colAl = strlen($colAK) < 4 ? '0' . $colAK : $colAK;
        $colAM = strlen($colAl) < 4 ? '0' . $colAl : $colAl;
        $colAN = strlen($colAM) < 4 ? '0' . $colAM : $colAM;
        $colAO = $colAJ . $colAN;
        $colAP = hexdec($colAO);
        $colAQ = str_pad($colAP, 10, "0", STR_PAD_LEFT);
        return $colAQ;
    }

    public function laModaConvertCardNumber($numKarty){
        if (strlen($numKarty) < 8 || preg_match('/[^A-F0-9]/', $numKarty)) {
            return "";
        }
        $res = "" . hexdec(substr(substr($numKarty,0,-2), -6));
        while (strlen($res) < 10){
            $res = "0" . $res;
        }
        return $res;
    }

    public function mVideoConvertCardNumber($numKarty){
        if (strlen($numKarty) < 8 || preg_match('/[^A-F0-9]/', $numKarty)) {
            return "";
        }
        $res = "" . hexdec(substr(substr($numKarty,0,-2), -6));
        while (strlen($res) < 10){
            $res = "0" . $res;
        }
        return $res;
    }

    public function orionHackConvert($numKarty){
        return substr(substr($numKarty,0,-2), -6);
    }

    public function aliExpressConvertCardNumber($numKarty) : string{
        if (strlen($numKarty) < 8 || preg_match('/[^A-F0-9]/', $numKarty)) {
            return "";
        }
        $res = "" . hexdec(substr($numKarty, -8));
        while (strlen($res) < 10){
            $res = "0" . $res;
        }
        return $res;
    }

    public function sdekCardNumber($numKarty) : string{
        if (strlen($numKarty) < 1 || preg_match('/[^A-F0-9]/', $numKarty)) {
            return "";
        }
        $res = "" . hexdec(substr($numKarty, -8));
        while (strlen($res) < 10){
            $res = "0" . $res;
        }
        return $res;
    }

    public function leruaMerlenCardNumber(array $cardArray) : string{
        $hex0 = dechex(intval($cardArray[0]));
        $hex1 = dechex(intval($cardArray[1]));
        while (strlen($hex1) < 4){
            $hex1 = "0" . $hex1;
        }
        $res = "" . hexdec( $hex0 . $hex1);
        while (strlen($res) < 10){
            $res = "0" . $res;
        }
        return $res;
    }

    public function parsecConvertCardNumber($numKarty){
        if (strlen($numKarty) < 8 || preg_match('/[^A-F0-9]/', $numKarty)) {
            return "";
        }
        $res = "" . hexdec($numKarty);
        while (strlen($res) < 10){
            $res = "0" . $res;
        }
        return $res;
    }

    public function fmGroupConvertCardNumber($numKarty){
        if (strlen($numKarty) == 0 || preg_match('/[^A-F0-9]/', $numKarty)) {
            return "";
        }
        $res = "" . hexdec($numKarty);
        while (strlen($res) < 10){
            $res = "0" . $res;
        }
        return $res;
    }

    public function greenfieldConvertCardNumber($numKarty) : string{
        if (strlen($numKarty) < 1 || preg_match('/[^A-F0-9]/', $numKarty)) {
            return "";
        }
        $res = "" . $numKarty;
        while (strlen($res) < 10){
            $res = "0" . $res;
        }
        return $res;
    }

    public static function staticArmtekReverseConvertCard($label){
        return dechex(intval($label));
    }

    public function armtekConvertCardNumber($numKarty) : string{
        $arr = explode(",","" . $numKarty);
        if (count($arr) != 2){
            return "";
        }
        $hex0 = dechex(intval($arr[0]));
        $hex1 = dechex(intval($arr[1]));
        while (strlen($hex1) < 4) $hex1 = "0" . $hex1;
        //if ($outInHex){
        //    return $hex0 . $hex1;
        //}
        while (strlen($hex0) < 4) $hex0 = "0" . $hex0;
        $hex = $hex0 . $hex1;
        $res = "" . hexdec($hex);
        while (strlen($res) < 10) $res = "0" . $res;
        return $res;
    }

    public function ashanConvertCardNumber($cardNum) : string{
        return dechex($cardNum);
    }

    public function model(array $row)
    {
        //if ($this->isDnsHeaderFormat){
        //    die("dns");
        //    if ($this->isDnsFormat($row)){
        //        $post        = $row['dolznostmodel' ] != null ? $row['dolznostmodel'] : 'сотрудник';
        //        $fioArr      = explode(" ", trim($row['imya']));
        //        $msurname    = $fioArr[0];
        //        $mname       = count($fioArr) > 1 ? $fioArr[1] : "";
        //        $mpatronymic = count($fioArr) > 2 ? $fioArr[2] : "";
        //        $str_ext_id  = $row['tab'];
        //        $mlabel      = $row['nomer_propuska'];
        //        $this->lastExtId = $str_ext_id;
        //        return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,$row['nomer_propuska'],0);
        //    } else if ($this->isDnsFormatLite($row) && $this->lastExtId != null){
        //        $this->updateDnsPropusk($this->lastExtId,$row['nomer_propuska']);
        //        return null;
        //    } else {
        //        $this->lastExtId = null;
        //        die(json_encode($row));
        //        return null;
        //    }
        //}
        if ($this->isFmFormat){
            //die("fm");
            $row = array_values($row);
            $str_ext_id  = $row[0];
            $mlabel      = $row[1];
            $msurname    = $row[2];
            $mname       = $row[3] != null ? $row[3] : "";
            $mpatronymic = $row[4] != null ? $row[4] : "";
            $post        = $row[5] != null ? $row[5] : 'сотрудник';
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,$mlabel,0,'');
        }
        if ($this->isPercoFormat($row)){
            //die("perco");
            $post = trim($row['dolznost']);
            $str_ext_id = $row['tab'];
            $mname = $row['imya'];
            $msurname = $row['familiya'];
            $mpatronymic = $row['otcestvo'] != null ? $row['otcestvo'] : '';
            $semKarty = $row['kod_semeistva_karty'] != null ? $row['kod_semeistva_karty'] : 0;
            $numKarty = $row['nomer_karty'];
            $mlabel = $this->percoConvertCardNumber($semKarty,$numKarty);
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
        }
        if ($this->isCanonFormat($row)){
            //die("canon" . ($row['ext_id'] == null));
            if ($row['lastname'] == null || $row['firstname'] == null || $row['hid'] == null){
                return null;
            }
            $fired = 0;
            if (array_key_exists('status',$row)){
                if ($row['status'] == MenExport::getFiredDescription(0)) $fired = 0;
                if ($row['status'] == MenExport::getFiredDescription(1)) $fired = 1;
                if ($row['status'] == MenExport::getFiredDescription(2)) $fired = 2;
            }
            $mlabel = $this->labelEncode($row['hid']);
            $str_ext_id = $row['ext_id'] != null ? $row['ext_id'] : "";
            $post = $row['post' ] != null ? $row['post'] : 'сотрудник';
            $mname = $row['firstname' ];
            $msurname = $row['lastname'];
            $mpatronymic = $row['middlename'] != null ? $row['middlename'] : '';
            $str_ext_login = $row['ext_login'] != null ? $row['ext_login'] : '';
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,$row['hid'],$fired,$str_ext_login);
        }
        if ($this->isLamodaFormat($row)){
            //die("lamoda");
            $post        = trim($row['dolznost']);
            $fioArr      = explode(" ", trim($row['fio']));
            $msurname    = $fioArr[0];
            $mname       = count($fioArr) > 1 ? $fioArr[1] : "";
            $mpatronymic = count($fioArr) > 2 ? $fioArr[2] : "";
            $str_ext_id  = $row['login'];
            $mlabel      = $this->laModaConvertCardNumber($row['id_propuska']);
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
        }
        if ($this->isRusClimatFormat($row)){
            //die("rus-climat");
            $post        = $row['dolznost' ] != null ? $row['dolznost'] : 'сотрудник';
            $mname       = $row['imya'     ] != null ? $row['imya'    ] : '';
            $mpatronymic = $row['otcestvo' ] != null ? $row['otcestvo'] : '';
            $msurname    = $row['familiya' ];
            $str_ext_id  = $row['tab_nomer'];
            $mlabel      = $this->laModaConvertCardNumber($row['kod_karty']);
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
        }
        if ($this->isHouseHoldFormat($row)){
            //die("house hold");
            $post        = $row['dolznost'];
            $fioArr      = explode(" ", trim($row['fio']));
            $msurname    = $fioArr[0];
            $mname       = count($fioArr) > 1 ? $fioArr[1] : "";
            $mpatronymic = count($fioArr) > 2 ? $fioArr[2] : "";
            $str_ext_id  = $row['nomer_karty'];
            $mlabel      = $this->laModaConvertCardNumber($row['nomer_karty']);
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
        }
        if ($this->isAliexpressFormat($row)){
            //die("aliexpress");
            $post        = $row['post'       ] != null ? $row['post'      ] : 'сотрудник';
            $mname       = $row['name'       ] != null ? $row['name'      ] : '';
            $mpatronymic = $row['secondname' ] != null ? $row['secondname'] : '';
            $msurname    = $row['firstname'  ] != null ? $row['firstname' ] : '';
            $str_ext_id  = $row['tableno'    ];
            $mlabel      = $this->aliExpressConvertCardNumber($row['fullcardcode']);
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
        }
        if ($this->isCitilinkFormat($row)){
            //die("citilink");
            $post        = $row['dolznost'   ];
            $arr         = explode(' ',$row['imya']);
            $mname       = $arr[0];
            $mpatronymic = count($arr) > 1 ? $arr[1] : "-";
            $msurname    = $row['familiya'   ];
            $str_ext_id  = $row['tabelnyi_nomer'];
            $mlabel      = $this->parsecConvertCardNumber($row['kod_propuska']);
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
        }
        if ($this->isGreenfieldFormat($row)){
            //die("greenfield");
            $post        = $row['dolznost'      ];
            $mname       = $row['imya'          ];
            $mpatronymic = $row['otcestvo'      ] != null ?  $row['otcestvo'] : "";
            $msurname    = $row['familiya'      ];
            $str_ext_id  = $row['id'            ];
            $mlabel      = $row['nomera_kart'   ];
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
        }
        //if ($this->isSDEKFormat($row)){
        //    //die("sdek");
        //    $post        = $row['dolznost'      ];
        //    $mname       = $row['imya'          ] != null ? $row['imya'    ] : "";
        //    $mpatronymic = $row['otcestvo'      ] != null ? $row['otcestvo'] : "";
        //    $msurname    = $row['familiya'      ];
        //    $str_ext_id  = md5($row['familiya'] . $row['imya']);
        //    $nomer_karty   = $row['nomer_karty']   != null ? $row['nomer_karty']   : "";
        //    $mlabel        = $this->sdekCardNumber( $nomer_karty );
        //    return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
        //}
        if ($this->isAlidiFormat($row)){
            //die("alidi");
            $post        = $row['dolznost'          ];
            $mname       = $row['imya'              ] != null ? $row['imya'    ] : "";
            $mpatronymic = $row['otcestvo'          ] != null ? $row['otcestvo'] : "";
            $msurname    = $row['familiya'          ];
            $str_ext_id  = $row['id_v_baze_eksporta'];
            $mlabel      = $row['metka'      ];
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
        }
        if ($this->isFmGroup($row)){
            //die("fmgroup");
            if ($row['fio'] != null && $row['klyuc'] != null && $row['id'] != null){
                $fioArr      = explode(" ", trim($row['fio']));
                $msurname    = $fioArr[0];
                $mname       = count($fioArr) > 1 ? $fioArr[1] : "";
                $mpatronymic = count($fioArr) > 2 ? $fioArr[2] : "";
                $str_ext_id  = $row['id'];
                $post        = $row['dolznost'] != null ? $row['dolznost'] : 'сотрудник';
                $mlabel      = $this->fmGroupConvertCardNumber($row['klyuc']);
                return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
            }else{
                return null;
            }
        }
        if ($this->isDetskiyMir($row)){
            //die("detskiy mir");
            if ($row['pp'] != null && $row['metka'] != null && $row['familiya'] != null && $row['imya'] != null){
                $str_ext_id  = md5($row['familiya'] . $row['imya']);
                $post        = $row['dolznost'] != null ? $row['dolznost'] : 'сотрудник';
                $mlabel      = $row['metka'   ];
                $msurname    = $row['familiya'];
                $mname       = $row['imya'    ];
                $mpatronymic = $row['otcestvo'] != null ? $row['otcestvo'] : "";
                return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
            }else{
                die(json_encode($row));
            }
        }
        if ($this->isArmtekFormat($row)){
            //die("armtek");
            $str_ext_id  = $row['tab'];
            $post        = $row['dolznostmodel'] != null ? $row['dolznostmodel'] : 'сотрудник';
            $mlabel      = $this->armtekConvertCardNumber($row['nomer_propuska']);
            $fioArr      = explode(" ", trim($row['imya']));
            $msurname    = $fioArr[0];
            $mname       = count($fioArr) > 1 ? $fioArr[1] : "";
            $mpatronymic = count($fioArr) > 2 ? $fioArr[2] : "";
            $rawLabel    = $row['nomer_propuska']; //$this->armtekConvertCardNumber($row['nomer_propuska'],true);
            //die($rawLabel);
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,$rawLabel,0,'');
        }
        //if ($this->isAshanFormat($row)){
        //    //die("ashan");
        //    $str_ext_id  = $row['tabel'];
        //    $post        = 'сотрудник';
        //    $mlabel      = $row['skud'];
        //    $fioArr      = explode(" ", trim($row['fio']));
        //    $msurname    = $fioArr[0];
        //    $mname       = count($fioArr) > 1 ? $fioArr[1] : "";
        //    $mpatronymic = count($fioArr) > 2 ? $fioArr[2] : "";
        //    return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,'',0,'');
        //}
        if ($this->isRusAgroMasloFormat($row)){
            //die("rus agro maslo " . json_encode($row));
            if ($row['pp'] != null && $row['dolznost'] != null && $row['familiya'] != null && $row['imya'] != null &&
                $row['kod_semeistva_karty'] != null && $row['nomer_karty'] != null){
                $post        = $row['dolznost'      ];
                $mname       = $row['imya'          ];
                $mpatronymic = $row['otcestvo'      ] != null ? $row['otcestvo'] : "";
                $msurname    = $row['familiya'      ];
                $str_ext_id  = md5($row['familiya'] . $row['imya']);
                $mlabel      = $this->percoConvertCardNumber($row['kod_semeistva_karty'], $row['nomer_karty']);
                return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,
                    $row['kod_semeistva_karty'] . ' ' . $row['nomer_karty'],0,'');
            }else{
                die("rus agro maslo " . json_encode($row));
            }
        }
        if ($this->isLeruaMerlen($row)){
            //die("lerua merlen");
            if ($row['familiya'] != null && $row['imya'] != null && $row['karty'] != null){
                $labArr = explode(' ', $row['karty']);
                if (count($labArr) != 2){
                    die("lerua merlen - неправильный формат карты: " . $row['karty']);
                }
                $msurname    = $row['familiya'];
                $mname       = $row['imya'];
                $mpatronymic = $row['otcestvo'] != null ? $row['otcestvo'] : '';
                $str_ext_id  = md5($msurname . $mname . $mpatronymic);
                $post        = $row['dolznost'] != null ? $row['dolznost'] : 'сотрудник';
                $mlabel      = $this->leruaMerlenCardNumber($labArr);
                return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,$row['karty'],0,'');
            }else{
                die("lerua merlen " . json_encode($row));
            }
        }
        if ($this->isMVideoFormat($row)) {
            //die("mvideo " . json_encode($row));
            $msurname    = $row['lastname'];
            $mname       = $row['name'];
            $mpatronymic = $row['midname'] != null ? $row['midname'] : '';
            $str_ext_id  = md5($msurname . $mname . $mpatronymic);
            $post        = $row['post'] != null ? $row['post'] : 'сотрудник';
            $mlabel      = $this->mVideoConvertCardNumber($row['id_propuska']);
            return $this->parseFormat($post,$str_ext_id,$mname,$msurname,$mpatronymic,$mlabel,$row['id_propuska'],0,'');
        }
        //die("Ошибка чтения строки файла, обратитесь к разработчику:" . json_encode($row));
        return null;
    }
}
