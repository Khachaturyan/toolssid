<?php

namespace App\Imports;

use App\Http\Controllers\ApiEquipmentController;
use App\Models\Equipment;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EquImport implements ToModel, WithHeadingRow
{
    public $tid = 0;

    /**
     * @return int
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //die(json_encode($row));
        if (!array_key_exists("pp",             $row) ||
            !array_key_exists("inv",            $row) || $row['inv'           ] == null ||
            !array_key_exists("seriinyi_nomer", $row) || $row['seriinyi_nomer'] == null ||
            !array_key_exists("metka",          $row) || $row['metka'         ] == null ||
            !array_key_exists("mac",            $row)){
            //echo("не все поля присутствуют, либо заполнены " . json_encode($row));
            return null; // {"pp":null,"inv":null,"metka":null,"seriinyi_nomer":null,"mac":null,"kommentarii":null}
        }
        //die(json_encode($row));
        $equ = Equipment::where('inv_num',$row['inv'])->first();
        $des = $row['kommentarii'] != null ? $row['kommentarii'] : "";
        $mac = $row['mac'] != null ? $row['mac'] : "";
        //die(json_encode($equ));
        if ($equ != null){
            $equ->tid = $this->tid;
            $equ->label = $row['metka'];
            $equ->inv_num = $row['inv'];
            $equ->ser_num = $row['seriinyi_nomer'];
            $equ->mac = $mac;
            $equ->description = $des;
            $equ->update();
            return null;
        }else{
            $res = new Equipment([
                'uid' => 0,
                'cid' => 0,
                'ocid' => 0,
                'estat' => -3,
                'estat_old' => 0,
                'estat_des' => ApiEquipmentController::getStatDescription(-3),
                'label' => $row['metka'],
                'description' => $des,
                'tid' => $this->tid,
                'ftid' => 0,
                'archive' => false,
                'inv_num' => $row['inv'],
                'is_work' => true,
                'ser_num' => $row['seriinyi_nomer'],
                'mark_sid' => 0,
                'mark_bid' => 0,
                'mac' => $mac
            ]);
            //die(json_encode($res));
            return $res;
        }
    }
}
