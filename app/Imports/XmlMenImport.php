<?php

namespace App\Imports;

class XmlMenImport
{
    public static function importAliexpressXmlItems($xml){
        $menImport = new MenImport();
        foreach($xml->children() as $item){
            $label = $menImport->laModaConvertCardNumber($item->FULLCARDCODE);
            $men = $menImport->parseFormat($item->POST,$item->TABLENO,$item->NAME,$item->FIRSTNAME,$item->SECONDNAME,$label,$item->FULLCARDCODE,0,'');
            if ($men != null){
                $men->save();
            }
        }
    }
}
