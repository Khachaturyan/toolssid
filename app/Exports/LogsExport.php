<?php

namespace App\Exports;

use App\Http\Controllers\ProjectSettingsController;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class LogsExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize, WithColumnWidths
{
    private $data = null;

    public function __construct($arr){
        $this->data = $this->convertArrayForExcel($arr);
    }

    public function headings(): array
    {
        return [
            __('str.table_created'),
            __('str.table_description'),
            __('str.table_from_estat'),
            __('str.table_to_estat'),
            __('str.table_object'),
            __('str.table_block'),
            __('str.table_cell')
        ];
    }

    public function collection()
    {
        return new Collection($this->data);
    }

    public function styles(Worksheet $sheet)
    {
        return [
            "A1:G" . (count($this->data) + 1)  => [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '000000'],
                    ],
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                ]
            ],
            1    => [
                'font' => ['bold' => true],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ]
            ],
        ];
    }

    public function columnWidths(): array
    {
        return [
            'B' => 96,
        ];
    }

    private function convertArrayForExcel($arr) : array{
        $res = array();
        foreach ($arr as $item){
            $res[] = array(ProjectSettingsController::toLocalTime($item['created_at']),
                                strip_tags($item['body']),$item['estat_des_old'],$item['estat_des'],$item['sname'],
                $item['bname'],$item['block_pos']);
        }
        return $res;
    }
}
