<?php

namespace App\Exports;

use App\Http\Controllers\ProjectSettingsController;
use App\Models\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ActOutputExport implements FromCollection, WithStyles, ShouldAutoSize, WithEvents, WithColumnFormatting
{
    private $data    = null;
    private $actNum  = null;
    private $itemsCount = 0;
    private $date    = null;
    private $yr_name = null;
    private $yr_adr  = null;
    private $yr_inn  = null;
    private $yr_kpp  = null;

    public function __construct($arr,$_actNum,$_date,$_yr_name,$_yr_adr,$_yr_inn,$_yr_kpp){
        $this->yr_name = $_yr_name;
        $this->yr_adr  = $_yr_adr;
        $this->yr_inn  = $_yr_inn;
        $this->yr_kpp  = $_yr_kpp;
        $this->date = explode(" ",$_date)[0];
        $this->itemsCount = count($arr);
        $this->actNum = $_actNum;
        $this->data = $this->convertArrayForExcel($arr);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getColumnDimension('A')->setAutoSize(false);
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(8.5);
                $event->sheet->getDelegate()->getColumnDimension('B')->setAutoSize(false);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(35);
                $event->sheet->getDelegate()->getRowDimension(6)->setRowHeight(42);
                $event->sheet->getDelegate()->getRowDimension($this->itemsCount + 16)->setRowHeight(42);
            }
        ];
    }

    public function collection()
    {
        return new Collection($this->data);
    }

    private function litera($i){
        return chr(ord('A') + $i);
    }

    public function columnFormats(): array
    {
        $res = array();
        $res["C"] = NumberFormat::FORMAT_TEXT;
        $res["D"] = NumberFormat::FORMAT_TEXT;
        return $res;
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->mergeCells('A2:C2');
        $sheet->mergeCells('A9:A10');
        $sheet->mergeCells('E9:E10');
        $sheet->mergeCells('F9:F10');
        $sheet->mergeCells('B9:D9');
        $sheet->mergeCells('A6:F6');
        $sheet->mergeCells('B8:D8');
        $sheet->mergeCells('A' . ($this->itemsCount + 13) . ':F' . ($this->itemsCount + 13));

        $sheet->mergeCells('A' . ($this->itemsCount + 15) . ':D' . ($this->itemsCount + 15));
        $sheet->mergeCells('E' . ($this->itemsCount + 15) . ':F' . ($this->itemsCount + 15));

        $sheet->mergeCells('A' . ($this->itemsCount + 16) . ':D' . ($this->itemsCount + 16));
        $sheet->mergeCells('E' . ($this->itemsCount + 16) . ':F' . ($this->itemsCount + 16));

        $sheet->mergeCells('A' . ($this->itemsCount + 17) . ':D' . ($this->itemsCount + 17));
        $sheet->mergeCells('E' . ($this->itemsCount + 17) . ':F' . ($this->itemsCount + 17));

        $sheet->mergeCells('A' . ($this->itemsCount + 18) . ':D' . ($this->itemsCount + 18));

        $sheet->mergeCells('A' . ($this->itemsCount + 19) . ':D' . ($this->itemsCount + 19));

        $sheet->mergeCells('A' . ($this->itemsCount + 22) . ':D' . ($this->itemsCount + 22));
        $sheet->mergeCells('E' . ($this->itemsCount + 22) . ':F' . ($this->itemsCount + 22));
        $res = array();
        $res["A1:F" . ($this->itemsCount + 21)]  = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ]];
        $res[4] = [
            'font' => ['bold' => true],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]];
        $res[$this->itemsCount + 13] = [
            'font' => ['bold' => true],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]];
        $res['A2'] = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]];

        $res['A6'] = [
            'alignment' => [
                'wrapText' => true,
            ]];
        $res["A9:F" . ($this->itemsCount + 10)]  = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ]];
        $res['D2'] = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ]];
        return $res;
    }

    private function convertArrayForExcel($arr) : array{
        $res = array();
        $res[] = array("");
        $res[] = array("Акт приема-передачи оборудования №:","","",$this->actNum);
        $res[] = array("Дата:",$this->date);
        $res[] = array("");
        $res[] = array("");
        $res[] = array('Мы, нижеподписавшиеся, от имени Исполнителя - _______________________________________ с одной стороны, и от имени Заказчика - _________________________ ' . $this->yr_name . ' с другой стороны, составили настоящий Акт о том, что Заказчик передал, а Исполнитель принял нижеуказанное оборудование на ремонт:');
        $res[] = array("");
        $res[] = array("");
        $res[] = array("№ п/п","Оборудование","","","Заявленная неисправность","Комплектность");
        $res[] = array("","Марка","S/N","Инвентарный номер");
        $counter = 1;
        foreach ($arr as $item){
            $res[] = array($counter, $item['tname'] . ' ' . $item['tmark'],  ' ' . $item['ser_num'],  ' ' . $item['inv_num'], $item['ft_name'],$item['compl']);
            $counter++;
        }
        $res[] = array("");
        $res[] = array("");
        $res[] = array("Адреса, реквизиты, печати и подписи сторон.");
        $res[] = array("");
        $res[] = array("Наименование сервисного центра","","","",$this->yr_name,"");
        $res[] = array("Юридический адрес:","","","","Юридический адрес: " . $this->yr_adr,"");
        $res[] = array("ИНН/КПП:","","","","ИНН/КПП: " . $this->yr_inn . "/" . $this->yr_kpp,"");
        $res[] = array("Расчетный счет: ","","","");
        $res[] = array("Корр. счет: ","","","");

        $res[] = array("");
        $res[] = array("");
        $res[] = array("Исполнитель:_________________________________________","","","","Заказчик:_________________________________________","");
        return $res;
    }
}
