<?php

namespace App\Exports;

use App\Models\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class StatExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    private $data = null;

    public function __construct($arr){
        $this->data = $this->convertArrayForExcel($arr['items']);
    }

    public function headings(): array
    {
        return [
            __('str.et_equ_type'),
            __('str.et_model'),
            __('str.table_inv_num'),
            __('str.table_surname'),
            __('str.mens_name'),
            __('str.mens_patronymic'),
            __('str.mens_post'),
            __('str.mens_men_status'),
            __('str.table_date_output'),
            __('str.table_block_output'),
            __('str.table_date_return'),
            __('str.table_block_return'),
            __('str.table_time_use'),
        ];
    }

    public function collection()
    {
        return new Collection($this->data);
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
            //'C'  => [ 'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,],
        ];
    }

    private function getFiredDescription($fired){
        switch($fired){
            case 0: return __('str.mens_status_in_state');
            case 1: return __('str.mens_status_fired');
            default: return __('str.mens_status_outsource');
        }
    }

    private function getTimeUse($time1,$time2) : string{
        if (strlen('' . $time1) == 0 || strlen('' . $time2) == 0) return "";
        $str = Carbon::createFromTimestamp(strtotime($time2) - strtotime($time1))->toDateTimeString();
        $str = explode("-",$str)[2];
        $arr = explode(" ",$str);
        $dayCount = intval($arr[0]) - 1;
        $dayCount = $dayCount == 0 ? "" : $dayCount . "d";
        return $dayCount . " " . $arr[1];
    }

    private function formatDate($t){
        if (strlen('' . $t) == 0) return "";
        $arr1 = explode(' ', $t);
        $arr2 = explode('-', $arr1[0]);
        $arr3 = explode(':', $arr1[1]);
        return $arr2[2] . "." . $arr2[1] . "." . $arr2[0] . " " . $arr3[0] . ":" . $arr3[1];
    }

    private function convertArrayForExcel($arr) : array{
        $res = array();
        foreach ($arr as $item){
            $res[] = array($item['tname'],$item['tmodel'],$item['inv_num'],$item['mname'],$item['msurname'],
                $item['mpatronymic'],$item['post'],$this->getFiredDescription($item['fired']),$this->formatDate($item['created_at']),
                $item['bname'],$this->formatDate($item['ret']),$item['ret_bname'],$this->getTimeUse($item['created_at'],$item['ret']));
        }
        return $res;
    }
}
