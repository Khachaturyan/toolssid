<?php

namespace App\Exports;


use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MenExport implements FromCollection, WithStyles, ShouldAutoSize, WithEvents
{
    private $data = null;

    public function __construct($arr){
        $this->data = $this->convertArrayForExcel($arr);
    }

    public static function getFiredDescription($fired) : string {
        if ($fired == 0) return 'В штате';
        if ($fired == 2) return 'Аутсорс';
        return 'Уволен';
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $FIRED_COLUMN = 'J';
                $event->sheet->getDelegate()->getColumnDimension($FIRED_COLUMN)->setAutoSize(false);
                $event->sheet->getDelegate()->getColumnDimension($FIRED_COLUMN)->setWidth(16);
                $row_count = count($this->data) + 1;
                $options = [
                    self::getFiredDescription(0),
                    self::getFiredDescription(1),
                    self::getFiredDescription(2),
                ];

                $validation = $event->sheet->getCell($FIRED_COLUMN . "3")->getDataValidation();
                $validation->setType(DataValidation::TYPE_LIST );
                $validation->setErrorStyle(DataValidation::STYLE_INFORMATION );
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setFormula1(sprintf('"%s"',implode(',',$options)));

                for ($i = 3; $i <= $row_count; $i++) {
                    $event->sheet->getCell($FIRED_COLUMN . "{$i}")->setDataValidation(clone $validation);
                }
            }
        ];
    }

    public function headings(): array
    {
        return [
            "EXT_ID",
            "EXT_LOGIN",
            "LASTNAME",
            "FIRSTNAME",
            "MIDDLENAME",
            "HID",
            "POST",
            "ROLE",
            "EMAIL",
            "STATUS",
            "Описание"
        ];
    }

    public function collection()
    {
        return new Collection($this->data);
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('K3:K12');
        $res = array();
        $res[1] = [
            'font' => ['bold' => true],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]];
        $res["K3"] = [
            'font' => ['bold' => true],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true
            ]];
        $res[2] = ['font' => ['bold' => true]];
        $res["A2:J" . (count($this->data))]  = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ]];
        return $res;
    }

    private function convertArrayForExcel($arr) : array{
        $res = array();
        $res[] = array("Список сотрудников.");
        $res[] = $this->headings();
        for($i = 0; $i < count($arr); $i++){
            $item = array($arr[$i]['str_ext_id'],$arr[$i]['str_ext_login'],$arr[$i]['msurname'],$arr[$i]['mname'],$arr[$i]['mpatronymic'],
                $arr[$i]['mlabel'],$arr[$i]['post'],$arr[$i]['rname'],$arr[$i]['email'],self::getFiredDescription($arr[$i]['fired']));
            if ($i == 0){
                $item[] = "EXT_ID - уникальный ID в базе СКУД\nEXT_LOGIN - Логин в сторонних системах\nLASTNAME - Фамилия\nFIRSTNAME - Имя\nMIDDLENAME - Отчество\nHID - Метка\nPOST - Должность\nROLE - Роль\nEMAIL - связанная почта";
            }
            $res[] = $item;
        }
        return $res;
    }
}
