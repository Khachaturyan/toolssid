<?php

namespace App\Exports;

use App\Models\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class StatNotWorkingCurrentExport implements FromCollection, WithHeadings, WithStyles, ShouldAutoSize
{
    private $data = null;

    public function __construct($arr){
        $this->data = $this->convertArrayForExcel($arr);
    }

    public function headings(): array
    {
        return [
            __('str.rep_xls_header_type'),
            __('str.rep_xls_header_model'),
            __('str.rep_xls_header_inv_num'),
            __('str.table_mac'),
            __('str.rep_xls_header_fam'),
            __('str.rep_xls_header_name'),
            __('str.rep_xls_header_fname'),
            __('str.rep_xls_header_label'),
            __('str.rep_xls_header_status'),
            //__('str.rep_xls_header_take_data'),
            __('str.table_date_return'),
            __('str.rep_xls_header_obj'),
            __('str.rep_xls_header_use_time'),
            __('str.rep_xls_header_not_working_type'),
            __('str.rep_xls_header_fio_last'),
        ];
    }

    public function collection()
    {
        return new Collection($this->data);
    }

    public function styles(Worksheet $sheet)
    {
        $res = array();
        $res[1] = ['font' => ['bold' => true]];
        $res["A1:N" . (count($this->data) + 1)]  = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ]];
        return $res;
    }

    private function getFiredDescription($fired){
        switch($fired){
            case 0: return __('str.mens_status_in_state');
            case 1: return __('str.mens_status_fired');
            default: return __('str.mens_status_outsource');
        }
    }

    private function convertArrayForExcel($arr) : array{
        $res = array();
        foreach ($arr as $item){
            $res[] = array($item['tname'],$item['tmodel'],' ' . $item['inv_num'], " " . $item['mac'], $item['msurname'],$item['mname'],
                $item['mpatronymic'],$item['mlabel'],$this->getFiredDescription($item['fired']),$item['bs_created_at'],
                $item['bname'],$item['time_own'],$item['ft_name'],$item['last_owner']);
        }
        return $res;
    }
}
