<?php

namespace App\Exports;

use App\Http\Controllers\ProjectSettingsController;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class StatEquExport3 implements FromCollection, WithHeadings, WithColumnFormatting, WithStyles, ShouldAutoSize
{
    private $data = null;

    public function __construct($arr){
        $this->data = $this->convertArrayForExcel($arr);
    }

    public function headings(): array
    {
        return [
            __('str.et_equ_type'),
            __('str.et_model'),
            __('str.table_inv_num'),
            __('str.rep_ser_num'),
            __('str.rep_xls_header_label2'),
            __('str.state'),
            __('str.table_state2'),
            __('str.rep_xls_header_change_status'),
            __('str.table_place'),
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,
            'H' => NumberFormat::FORMAT_TEXT,
            'I' => NumberFormat::FORMAT_TEXT,
            'J' => NumberFormat::FORMAT_TEXT,
            'K' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function collection()
    {
        return new Collection($this->data);
    }

    public function styles(Worksheet $sheet)
    {
        $res = array();
        $res[1] = ['font' => ['bold' => true]];
        $res["A1:I" . (count($this->data) + 1)]  = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ]];
        return $res;
    }

    private function convertArrayForExcel($arr) : array{
        $res = array();
        foreach ($arr as $item){
            $res[] = array($item['tname'],$item['tmodel'],' ' . $item['inv_num'],' ' . $item['ser_num'],
                ' ' . $item['label'], $item->working_state, $item->estat_des, ProjectSettingsController::toLocalTime($item->updated_at), $item->place);
        }
        return $res;
    }
}
