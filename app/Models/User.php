<?php

namespace App\Models;

use App\Http\Controllers\RoleController;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_confirm',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getSecretHasFromLogin() : string {
        return md5($this->email . 'secret code 777');
    }

    public function compareSecretHasFromLoginAndThisUser(string $code) : bool{
        return $this->getSecretHasFromLogin() === $code;
    }

    public function enableAuthWeb() : bool {
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->show_pages;
    }

    public function isFirstOfFourNewUsers() : bool{
        return User::max('id') <= 4;
    }

    public function needShowAlertAboutNewSystemMessages() : bool{
        return $this->create_role();
    }

    public function mid() {
        $men = Man::where('web_uid',$this->id)->first();
        return $men == null ? 0 : $men->id;
    }

    public function rid() {
        $men = Man::where('web_uid',$this->id)->first();
        return $men == null ? 0 : $men->rid;
    }

    public function enableEditEqu() : bool {
        return $this->enableUpdateWeb();
    }

    public function enableUpdateWeb() : bool {
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && ($men->perm_index == RoleController::ADMIN_INDEX ||
                $men->perm_index == RoleController::IT_MEN);
    }

    public function create_user() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->create_user;
    }

    public function create_role() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->create_role;
    }

    public function create_unit_type() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->create_unit_type;
    }

    public function change_status() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->change_status;
    }

    public function change_object() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->change_object;
    }

    public function change_box() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->change_box;
    }

    public function change_cell() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->change_cell;
    }

    public function show_logs() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->show_logs;
    }

    public function rigit_men() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->rigit_men;
    }

    public function create_failure_types() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->create_failure_types;
    }

    public function export_report() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->export_report;
    }

    public function access_settings() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && $men->access_settings;
    }

    public function create_ticket() : bool{
        $men = Man::where('web_uid',$this->id)->leftJoin("roles", "roles.id","=", "men.rid")->select("roles.*")->first();
        return $men != null && ($men->create_ticket || $men->perm_index == RoleController::ADMIN_INDEX ||
                $men->perm_index == RoleController::IT_MEN || $men->perm_index == RoleController::SECURE_INDEX ||
                $men->perm_index == RoleController::BOX_MANAGER_INDEX);
    }

}
