<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function items()
    {
        return $this->belongsTo(Etype::class, 'tid',  'id');
    }

    public function tip($id)
    {
        $item = Etype::where('id', $id)->first();
        return $item;
    }

    public function work($tid, $block, $work)
    {
        $cells = Cell::where('block', $block)->get();
        $arr = [];
        foreach ($cells as $item){
            $arr[] = $item->id;
        }
        $items = Equipment::whereIn('cid', $arr)->where('tid', $tid)->where('is_work', $work)->get();

        return $items;
    }

    public function workNew($tid)
    {
        $items = TypeLoad::where('tid', $tid)->first();
        if($items){
            return $items->count_men;
        }
        else{
            return 0;
        }

    }
}
