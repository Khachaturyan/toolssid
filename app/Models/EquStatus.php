<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// кастомный статус

class EquStatus extends Model
{
    use HasFactory;
    protected $guarded = [];
}
