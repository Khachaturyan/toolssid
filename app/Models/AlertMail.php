<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlertMail extends Model
{
    use HasFactory;
    protected $guarded = [];
}
