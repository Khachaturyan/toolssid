<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Etype extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function work($tid, $work)
    {
        $items = Equipment::where('tid', $tid)->where('archive', false)->where('is_work', $work)->get();
        return $items;
    }

    public function workNew($id)
    {
        $items = TypeLoad::where('tid', $id)->first();
        if($items){
            return $items->count_men;
        }
        else{
            return 0;
        }
    }
}
