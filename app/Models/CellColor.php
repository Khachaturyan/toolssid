<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

//--- эта модель для контроля текущего цвета ячейки

class CellColor extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;
}
