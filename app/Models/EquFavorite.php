<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// переменная хранения временного возврата оборудования

class EquFavorite extends Model
{
    use HasFactory;
    protected $guarded = [];
}
