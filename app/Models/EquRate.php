<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


// если частота использования устройства ниже среднесуточного, оно попадает в таблицу equ_rates

class EquRate extends Model
{
    use HasFactory;
    protected $guarded = [];
}
