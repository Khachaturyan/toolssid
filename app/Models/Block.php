<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function isEmpty() : bool{
        $cells = Cell::where('block',$this->id)->get();
        $cids = array();
        foreach ($cells as $cell){
            $cids[] = $cell->id;
        }
        if (Equipment::whereIn('cid',$cids)->count() > 0) return false;
        return Equipment::whereIn('ocid',$cids)->count() == 0 && Product::whereIn('cid',$cids)->count() == 0;
    }

    public function items($id)
    {
        $cells = Cell::where('block', $id)->get();
        $arr = [];
        foreach ($cells as $item){
            $arr[] = $item->id;
        }
        $items = Equipment::whereIn('cid', $arr)->get();
        return $items;
    }

    public function itemsWarning($id)
    {
        $cells = Cell::where('block', $id)->get();
        $arr = [];
        foreach ($cells as $item){
            $arr[] = $item->id;
        }
        $items = Equipment::whereIn('cid', $arr)->where('is_work', 0)->get();
        return $items;
    }
}
