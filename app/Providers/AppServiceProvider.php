<?php

namespace App\Providers;

use App\Http\Controllers\ProjectSettingsController;
use App\Models\CustomData;
use App\Models\Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ini_set('memory_limit', '256M');
        Schema::defaultStringLength(191);

        view()->composer('*', function($view)
        {
            $user = Auth::user();
            if($user){
                $thema = $user->thema;
                $debug_mode = CustomData::firstOrCreate(['label' => 'debug_mode'],['data' => 0])->data == 1;
                $last_news_id = CustomData::firstOrCreate(['label' =>
                    ProjectSettingsController::getLastNewIdLabel($user->id)],['data' => 0])->data;
                $last_manual_id = CustomData::firstOrCreate(['label' =>
                    ProjectSettingsController::getLastManualIdLabel($user->id)],['data' => 0])->data;
                $view->with([
                    'thema' => $thema,
                    'header_company_name' => CustomData::firstOrCreate(['label' => 'org_name'],['data' => ''])->data,
                    'debug_mode_var'      => $debug_mode,
                    'last_news_id'        => $last_news_id,
                    'last_manual_id'      => $last_manual_id
                ]);
            }
            else{
                $view->with([
                    'thema' => false,
                    'debug_mode_var' => false
                ]);
            }
        });
    }
}
