<?php

namespace App\SapEwmDm;

use App\Http\Controllers\ApiLogController;
use App\Http\Controllers\ProjectSettingsController;
use App\Models\CustomData;
use Carbon\Carbon;

class SapEwmDmHelper
{
    public static function syncEnable() : bool{
        return CustomData::firstOrCreate(['label' => 'sap_ewm_en'],['data' => 0])->data == 1;
    }

    private static function sendSystemPostReq($url, $post_array, $headers=null, $check_ssl=true) {
        if(!is_null($headers)){
            $head = '';
            foreach($headers as $item){
                $head .= " -H '". $item."' ";
            }
        }
        $cmd = "curl -X POST " . $head;
        $cmd.= " -d '" . json_encode($post_array) . "' " . $url;
        if(!$check_ssl){
            $cmd.= "  --insecure";
        }
        $cmd .= " > /dev/null 2>&1 &";
        exec($cmd, $output, $exit);
        return $exit == 0;
    }

    private static function sendCurlPostReq($url, $post_array, $headers=null, $check_ssl=true){
        $MIN_10 = 45;
        //echo json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $MIN_10);
        curl_setopt($ch, CURLOPT_TIMEOUT, $MIN_10); //timeout in seconds
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $check_ssl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_array));
        $response = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);
        if ($curl_errno > 0) {
            return "cURL Error ($curl_errno): $curl_error\n";
        } else {
            return "connect sap ewm server with response: " . $response;
        }
    }

    public static function sendMessage($sid,$user,bool $isTakeItem,$estat_des,$label,bool $sendSystemCurlMode){
        $url = CustomData::firstOrCreate(['label' => 'sap_ewm_url'],['data' => ''])->data;
        $login = CustomData::firstOrCreate(['label' => 'sap_ewm_log'],['data' => ''])->data;
        $password = CustomData::firstOrCreate(['label' => 'sap_ewm_pas'],['data' => ''])->data;
        if (CustomData::firstOrCreate(['label' => 'sap_ewm_en_log'],['data' => 0])->data != 0){
            ApiLogController::addLogText('Sap Ewm: sid=' . $sid . ' user=' . $user . ' take_item=' . $isTakeItem);
        }
        //echo "step 1";
        if (strlen($url) == 0 || strlen($login) == 0 || strlen($password) == 0){
            return null;
        }
        $timeStr =
            Carbon::createFromTimestamp(time() + ProjectSettingsController::getTimeOffset())->toDateTimeString();
        $data = [
            "LGNUM"  => $sid,
            "UNAME"  => $user,
            "LOGON"  => $isTakeItem ? '1' : '0',
            "CHGTST" => str_replace(array(' ', 'T', '-', ':'), '',  $timeStr),
            "STATUS" => $estat_des,
            "DEVICE" => $label
        ];
        //echo json_encode($data);
        $headers = [
            'Authorization: Basic ' . base64_encode($login . ':' . $password)
        ];
        return $sendSystemCurlMode ? self::sendSystemPostReq($url,$data,$headers,false) :
                                            self::sendCurlPostReq($url,$data,$headers,false);
    }
}
