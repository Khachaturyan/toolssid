<?php

namespace App\Parsec;

use App\Http\Controllers\ApiLogController;

class ParsecSoapEmulateClient
{
    private $url;
    private $sessionId;
    public  $lastResponse;

    public function __construct($_url,$domain,$operator,$password)
    {
        $this->url = $_url;
        $session = $this->req('<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope"
            xmlns:ns1="http://parsec.ru/Parsec3IntergationService"><env:Body>
            <ns1:OpenSession><ns1:domain>' . $domain . '</ns1:domain>
            <ns1:userName>' . $operator . '</ns1:userName>
            <ns1:password>' . $password . '</ns1:password>
            </ns1:OpenSession></env:Body>
        </env:Envelope>','http://parsec.ru/Parsec3IntergationService/OpenSession');
        $this->sessionId = property_exists($session,'OpenSessionResponse') && property_exists($session->OpenSessionResponse,'OpenSessionResult') &&
        property_exists($session->OpenSessionResponse->OpenSessionResult,'Value') ? $session->OpenSessionResponse->OpenSessionResult->Value->SessionID : null;
    }

    public function close(){
        $this->req('<?xml version="1.0" encoding="utf-8"?>
            <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
              <soap12:Body>
                <CloseSession xmlns="http://parsec.ru/Parsec3IntergationService">
                  <sessionID>' . $this->sessionId . '</sessionID>
                </CloseSession>
              </soap12:Body>
            </soap12:Envelope>');
    }

    private function isAssoc(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    private function GetPersonIdentifiers($uid){
        $xml = $this->req('<?xml version="1.0" encoding="utf-8"?>
                <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                  <soap12:Body>
                    <GetPersonIdentifiers xmlns="http://parsec.ru/Parsec3IntergationService">
                      <sessionID>' . $this->sessionId . '</sessionID>
                      <personID>' . $uid . '</personID>
                    </GetPersonIdentifiers>
                  </soap12:Body>
                </soap12:Envelope>');
        $items = property_exists($xml,'GetPersonIdentifiersResponse') &&
                    property_exists($xml->GetPersonIdentifiersResponse,'GetPersonIdentifiersResult')  ?
                        $xml->GetPersonIdentifiersResponse->GetPersonIdentifiersResult->children() : null;
        $items = $items != null ? json_decode(json_encode($items),true)['Identifier'] : null;
        if ($items == null || !is_array($items)){
            return null;
        }
        if ($this->isAssoc($items)){
            return array_key_exists('CODE',$items) ? $items['CODE'] : null;
        }
        $res = null;
        foreach ($items as $item){
            if (array_key_exists('CODE',$item)){
                $res = $item['CODE'];
                break;
            }
        }
        return $res;
    }

    public function GetOrgUnitsHierarhyWithPersons() : ?array
    {
        $xml = $this->req('<?xml version="1.0" encoding="utf-8"?>
                <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                    xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                  <soap12:Body>
                    <GetOrgUnitsHierarhyWithPersons xmlns="http://parsec.ru/Parsec3IntergationService">
                      <sessionID>' . $this->sessionId . '</sessionID>
                    </GetOrgUnitsHierarhyWithPersons>
                  </soap12:Body>
                </soap12:Envelope>');
        $items = property_exists($xml,'GetOrgUnitsHierarhyWithPersonsResponse') &&
        property_exists($xml->GetOrgUnitsHierarhyWithPersonsResponse,'GetOrgUnitsHierarhyWithPersonsResult')  ?
            $xml->GetOrgUnitsHierarhyWithPersonsResponse->GetOrgUnitsHierarhyWithPersonsResult->children() : null;
        $items = $items != null ? json_decode(json_encode($items),true)['BaseObject'] : null;
        if ($items == null){
            return null;
        }
        if ($this->isAssoc($items)){
            $items = array($items);
        }
        //$logStr = "импортировано из парсека " . count($items) . " элементов<br>\n";
        $res = array();
        foreach ($items as $item){
            if (array_key_exists('FIRST_NAME', $item) && array_key_exists('LAST_NAME', $item)){
                $item['code'] = $this->GetPersonIdentifiers($item['ID']);
                //$logStr .= json_encode($item) . "<br>\n";
                if ($item['code'] != null) $res[] = $item;
            }
        }
        //ApiLogController::addLogText($logStr);
        return $res;
    }

    public function isConnected() : bool {
        return $this->sessionId != null;
    }

    private function req($xml,$action = ''){
        $header = 'Content-Type: application/soap+xml; charset=utf-8;';
        if (strlen($action) > 0) $header .= ' action="' . $action . '"';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 400); //timeout in seconds
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array($header));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $sOutput = curl_exec($ch);
        curl_close($ch);
        if ($sOutput == null){
            die('resp = null; req = ' . $this->url . " xml = " . $xml);
        }
        $this->lastResponse = $sOutput;
        $xml = simplexml_load_string(str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $sOutput));
        if (property_exists($xml,'Body')){
            return $xml->Body;
        }else{
            die($sOutput);
        }
    }
}
