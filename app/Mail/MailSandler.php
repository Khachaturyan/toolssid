<?php
namespace App\Mail;

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function


use App\Models\AlertMail;
use App\Models\CustomData;

class MailSandler
{
    public static $PORT = 465;
    public static $HOST = 'smtp.mail.ru';
    public static $USER = 'xinvestoriginal@mail.ru';
    public static $PASS = 'sQru30Wkzkzdq9uzZzS3';

    public static function sendToAll($sbj, $bod, $event, $filePath = null){
        $mails = AlertMail::where('estate', 'like', '%' . $event . '%')->get();
        $res = "";
        foreach ($mails as $mail){
            $sendRes = self::send2($mail->mail,$sbj,$bod,$filePath);
            $res = strlen($res) > 0 ? $res . "<br>" . $sendRes : $sendRes;
        }
        return $res;
    }

    public static function send2($adr, $sbj, $bod, $filePath = null){
        $isAutoSettings = CustomData::firstOrCreate(['label' => 'set_mail_custom'],['data' => 0])->data == 0;
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPDebug = false;
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = $isAutoSettings ? self::$HOST : CustomData::firstOrCreate(['label' => 'set_mail_host'],['data' => self::$HOST])->data;
        $mail->Port = $isAutoSettings ? self::$PORT : CustomData::firstOrCreate(['label' => 'set_mail_port'],['data' => self::$PORT])->data;
        $user = $isAutoSettings ? self::$USER : CustomData::firstOrCreate(['label' => 'set_mail_user'],['data' => self::$USER])->data;
        $mail->Username = $user;
        $mail->Password = $isAutoSettings ? self::$PASS : CustomData::firstOrCreate(['label' => 'set_mail_pass'  ],['data' => self::$PASS])->data;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->setFrom($user, 'tools-id');
        $mail->addAddress($adr);
        if ($filePath != null){
            $mail->addAttachment($filePath,basename($filePath));
        }
        $mail->Subject = $sbj;
        $mail->msgHTML($bod);
        $res = $mail->send();
        $mail->smtpClose();
        return $res;
    }
}
