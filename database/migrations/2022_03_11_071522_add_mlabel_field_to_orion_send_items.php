<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMlabelFieldToOrionSendItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orion_send_items', function (Blueprint $table) {
            $table->string('mlabel', 256)->after('raw_label')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orion_send_items', function (Blueprint $table) {
            //
        });
    }
}
