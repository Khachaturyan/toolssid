<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStrExtIdColToMen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('men', function (Blueprint $table) {
            $table->string('str_ext_id',64)->after('rid')->default("");
            $table->dropColumn(['ext_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('men', function (Blueprint $table) {
            //
        });
    }
}
