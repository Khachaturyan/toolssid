<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertMailPersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alert_mail_personals', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('uid')->default(0);
            $table->tinyInteger('state')->default(0);
            $table->integer('time');
            $table->string('estate',64);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alert_mail_personals');
    }
}
