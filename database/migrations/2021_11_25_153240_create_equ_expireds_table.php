<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquExpiredsTable extends Migration
{
    /**
     * Run the migrations.
     * для контроля уведомлений о превышении времени лимита обладания устройством
     * @return void
     */
    public function up()
    {
        Schema::create('equ_expireds', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cid'); // ячейка
            $table->unsignedBigInteger('eid'); // устройство
            $table->unsignedBigInteger('mid'); // сотрудник
            $table->tinyInteger('state');      // статус - зарезервировано для будущего
            $table->string('time',64);   // время взятия
            $table->text('des');               // описание сообщения
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equ_expireds');
    }
}
