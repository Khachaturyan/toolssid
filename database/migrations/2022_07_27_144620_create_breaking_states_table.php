<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBreakingStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    // скан состояния неработающего оборудования

    public function up()
    {
        Schema::create('breaking_states', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("eid");
            $table->bigInteger("mid");
            $table->bigInteger("cid");
            $table->bigInteger("ftid");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breaking_states');
    }
}
