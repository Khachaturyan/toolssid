<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsComplFieldInEtypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('etypes', function (Blueprint $table) {
            $table->boolean('is_comp')->after('is_cons')->default(false);
            $table->text('comp_data')->after('is_comp')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('etypes', function (Blueprint $table) {
            //
        });
    }
}
