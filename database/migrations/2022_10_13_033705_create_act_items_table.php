<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('act_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("aid");
            $table->unsignedBigInteger("eid");
            $table->tinyInteger("state")->default(0);
            $table->text("compl");
            $table->text("des");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('act_items');
    }
}
