<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetAdminRoleToArchive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $newAdminRole = \App\Models\Role::where('rname','!=','admin')->where('perm_index',\App\Http\Controllers\RoleController::ADMIN_INDEX)->first();
        if ($newAdminRole != null){
            $oldAdminRoles = \App\Models\Role::where('rname','admin')->get();
            foreach ($oldAdminRoles as $role){
                if ($role->is_admin){
                    \App\Models\Man::where('rid',$role->id)->update(['rid' => $newAdminRole->id]);
                    $role->delete();
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archive', function (Blueprint $table) {
            //
        });
    }
}
