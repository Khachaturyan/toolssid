<?php

use App\Models\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Http\Controllers\RoleController;

class AddPermIndexFieldInRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->integer("perm_index")->after("etypes")->default(0);
        });
        Role::create([
            'rname'      => RoleController::SCLAD_MEN,
            'is_admin'   => 0,
            'is_arc'     => 0,
            'lim_items'  => 1,
            'etypes'     => '',
            'perm_index' => RoleController::SCLAD_MEN_INDEX,
        ]);
        Role::create([
            'rname'      => RoleController::BOX_MANAGER,
            'is_admin'   => 0,
            'is_arc'     => 0,
            'lim_items'  => 1,
            'etypes'     => '',
            'perm_index' => RoleController::BOX_MANAGER_INDEX,
        ]);
        Role::create([
            'rname'      => RoleController::IT_MEN,
            'is_admin'   => 0,
            'is_arc'     => 0,
            'lim_items'  => 1,
            'etypes'     => '',
            'perm_index' => RoleController::IT_MEN_INDEX,
        ]);
        Role::create([
            'rname'      => RoleController::ADMIN,
            'is_admin'   => 0,
            'is_arc'     => 0,
            'lim_items'  => 1,
            'etypes'     => '',
            'perm_index' => RoleController::ADMIN_INDEX,
        ]);
        Role::create([
            'rname'      => RoleController::SECURE,
            'is_admin'   => 0,
            'is_arc'     => 0,
            'lim_items'  => 1,
            'etypes'     => '',
            'perm_index' => RoleController::SECURE_INDEX,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            //
        });
    }
}
