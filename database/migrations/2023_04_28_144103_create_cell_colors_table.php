<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCellColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    //--- это таблица для контроля текущего цвета ячейки ---------------------------------------------------------------

    public function up()
    {
        Schema::create('cell_colors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cid');
            $table->string('col',16);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cell_colors');
    }
}
