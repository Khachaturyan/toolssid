<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Role;
use App\Http\Controllers\RoleController;

class AddPermissionsFieldsInRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->boolean('show_pages')->after('perm_index')->default(true);
            $table->boolean('create_user_role')->after('show_pages')->default(true);
            $table->boolean('create_unit_type')->after('create_user_role')->default(true);
            $table->boolean('change_status')->after('create_unit_type')->default(true);
            $table->boolean('change_object')->after('change_status')->default(true);
            $table->boolean('change_box')->after('change_object')->default(true);
            $table->boolean('change_cell')->after('change_box')->default(true);
            $table->boolean('show_logs')->after('change_cell')->default(true);
            $table->boolean('rigit_men')->after('show_logs')->default(true);
            $table->boolean('create_failure_types')->after('rigit_men')->default(true);
            $table->boolean('export_report')->after('create_failure_types')->default(true);
            $table->boolean('access_settings')->after('export_report')->default(true);
            $table->boolean('create_ticket')->after('access_settings')->default(true);
        });
        Role::where('is_admin',true)->update(['perm_index' => RoleController::ADMIN_INDEX]);
        Role::where('perm_index',0)->update([
            'show_pages'           => false,
            'create_user_role'     => false,
            'create_unit_type'     => false,
            'change_status'        => false,
            'change_object'        => false,
            'change_box'           => false,
            'change_cell'          => false,
            'show_logs'            => false,
            'rigit_men'            => false,
            'create_failure_types' => false,
            'export_report'        => false,
            'access_settings'      => false,
            'create_ticket'        => false
        ]);
        Role::where('perm_index',RoleController::SCLAD_MEN_INDEX)->update([
            'show_pages'           => false,
            'create_user_role'     => false,
            'create_unit_type'     => false,
            'change_status'        => false,
            'change_object'        => false,
            'change_box'           => false,
            'change_cell'          => false,
            'show_logs'            => false,
            'rigit_men'            => false,
            'create_failure_types' => false,
            'export_report'        => false,
            'access_settings'      => false,
            'create_ticket'        => false
        ]);
        Role::where('perm_index',RoleController::BOX_MANAGER_INDEX)->update([
            'show_pages'           => true,
            'create_user_role'     => false,
            'create_unit_type'     => false,
            'change_status'        => false,
            'change_object'        => false,
            'change_box'           => false,
            'change_cell'          => false,
            'show_logs'            => false,
            'rigit_men'            => true,
            'create_failure_types' => false,
            'export_report'        => true,
            'access_settings'      => false,
            'create_ticket'        => false
        ]);
        Role::where('perm_index',RoleController::IT_MEN_INDEX)->update([
            'show_pages'           => true,
            'create_user_role'     => true,
            'create_unit_type'     => true,
            'change_status'        => true,
            'change_object'        => false,
            'change_box'           => false,
            'change_cell'          => true,
            'show_logs'            => true,
            'rigit_men'            => true,
            'create_failure_types' => true,
            'export_report'        => true,
            'access_settings'      => true,
            'create_ticket'        => true
        ]);
        Role::where('perm_index',RoleController::ADMIN_INDEX)->update([
            'show_pages'           => true,
            'create_user_role'     => true,
            'create_unit_type'     => true,
            'change_status'        => true,
            'change_object'        => true,
            'change_box'           => true,
            'change_cell'          => true,
            'show_logs'            => true,
            'rigit_men'            => true,
            'create_failure_types' => true,
            'export_report'        => true,
            'access_settings'      => true,
            'create_ticket'        => true
        ]);
        Role::where('perm_index',RoleController::SECURE_INDEX)->update([
            'show_pages'           => true,
            'create_user_role'     => false,
            'create_unit_type'     => false,
            'change_status'        => false,
            'change_object'        => false,
            'change_box'           => false,
            'change_cell'          => false,
            'show_logs'            => true,
            'rigit_men'            => false,
            'create_failure_types' => false,
            'export_report'        => true,
            'access_settings'      => false,
            'create_ticket'        => false
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            //
        });
    }
}
