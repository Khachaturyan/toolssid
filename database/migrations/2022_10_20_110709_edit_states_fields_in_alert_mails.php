<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditStatesFieldsInAlertMails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alert_mails', function (Blueprint $table) {
            $table->string('estate',1024)->after('mail')
                ->default(\App\Http\Controllers\ProjectSettingsController::getAlertMailEstateDefault());
            $table->dropColumn(['st1', 'st2', 'st3', 'st4', 'st5', 'st6', 'st7']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alert_mails', function (Blueprint $table) {
            //
        });
    }
}
