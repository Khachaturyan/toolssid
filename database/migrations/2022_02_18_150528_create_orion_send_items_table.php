<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrionSendItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orion_send_items', function (Blueprint $table) {
            $table->id();
            $table->string('raw_label', 256)->default('');
            $table->tinyInteger('blocked')->default(0);
            $table->tinyInteger('send_state')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orion_send_items');
    }
}
