<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alert_mails', function (Blueprint $table) {
            $table->id();
            $table->string("mail",256);
            $table->boolean("st1")->default(false);
            $table->boolean("st2")->default(false);
            $table->boolean("st3")->default(false);
            $table->boolean("st4")->default(false);
            $table->boolean("st5")->default(false);
            $table->boolean("st6")->default(false);
            $table->boolean("st7")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alert_mails');
    }
}
