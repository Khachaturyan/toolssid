<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * Таблица для связи сотрудника и предпочтительного оборудования
     */
    public function up()
    {
        Schema::create('equ_favorites', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('uid');
            $table->unsignedBigInteger('eid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equ_favorites');
    }
}
