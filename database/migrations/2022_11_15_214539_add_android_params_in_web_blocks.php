<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAndroidParamsInWebBlocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blocks', function (Blueprint $table) {
            $table->string('pin',16)->after('is_arc')->default('99998');
            $table->boolean('show_win_sel_sizes')->after('pin')->default(true);
            $table->boolean('time_ret')->after('show_win_sel_sizes')->default(true);
            $table->boolean('orion_lab_encode')->after('time_ret')->default(false);
            $table->boolean('cut_label_6b')->after('orion_lab_encode')->default(false);
            $table->boolean('ret_any_cell')->after('cut_label_6b')->default(true);
            $table->boolean('diag_mode')->after('ret_any_cell')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blocks', function (Blueprint $table) {
            //
        });
    }
}
