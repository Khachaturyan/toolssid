<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeEstateDesStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\Log::where('estat_des','Неисправное оборудование')->update(['estat_des' => 'Нерабочее']);
        \App\Models\Log::where('estat_des_old','Неисправное оборудование')->update(['estat_des_old' => 'Нерабочее']);
        \App\Models\Equipment::where('estat_des','Неисправное оборудование')->update(['estat_des' => 'Нерабочее']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
