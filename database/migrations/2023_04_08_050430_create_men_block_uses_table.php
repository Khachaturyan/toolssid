<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenBlockUsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    //таблица для проверки не работает ли сотрудник с другим блоком

    public function up()
    {
        Schema::create('men_block_uses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("mid");
            $table->unsignedBigInteger("bid");
            $table->tinyInteger('state')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('men_block_uses');
    }
}
