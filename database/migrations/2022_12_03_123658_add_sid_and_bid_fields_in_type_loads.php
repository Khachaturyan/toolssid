<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSidAndBidFieldsInTypeLoads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_loads', function (Blueprint $table) {
            $table->unsignedBigInteger('sid')->after('id')->default(0);
            $table->unsignedBigInteger('bid')->after('sid')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_loads', function (Blueprint $table) {
            //
        });
    }
}
