<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCloudVpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cloud_vps', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("taid"); // ид тарифа
            $table->bigInteger("seid"); // ид сервера в РЕГ.РУ
            $table->string("name",256);
            $table->string("ip",32);
            $table->string("ipv6",32);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cloud_vps');
    }
}
