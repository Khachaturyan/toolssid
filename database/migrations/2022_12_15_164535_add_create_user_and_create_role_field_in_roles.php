<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreateUserAndCreateRoleFieldInRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->boolean("create_user")->after("show_pages")->default(false);
            $table->boolean("create_role")->after("create_user")->default(false);
        });
        $items = \App\Models\Role::all();
        foreach ($items as $item){
            $item->update(['create_user' => $item->create_user_role, 'create_role' => $item->create_user_role]);
        }
        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn(['create_user_role']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            //
        });
    }
}
