<?php

use App\Models\LogTakeItem;
use App\Models\Log;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogTakeItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_take_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("mid");
            $table->unsignedBigInteger("eid");
            $table->timestamps();
        });
        $TAKE_ITEM_WORKER_LTP = 6;
        Log::where('ltp',$TAKE_ITEM_WORKER_LTP)->chunk(100, function($items){
            foreach ($items as $item){
                LogTakeItem::create([
                    'mid' => $item->mid,
                    'eid' => $item->eid,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at
                ])->save();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_take_items');
    }
}
