<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    // если частота использования устройства ниже среднесуточного, оно попадает в таблицу equ_rates

    public function up()
    {
        Schema::create('equ_rates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('eid')->default(0);
            $table->float('rate')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equ_rates');
    }
}
