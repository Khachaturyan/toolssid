<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBidMidTidEidToLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('logs', function (Blueprint $table) {
            $table->bigInteger( 'bid')->after( 'id')->default(0);
            $table->bigInteger( 'mid')->after('bid')->default(0);
            $table->bigInteger( 'tid')->after('mid')->default(0);
            $table->bigInteger( 'eid')->after('tid')->default(0);
            $table->bigInteger( 'cid')->after('eid')->default(0);
            $table->tinyInteger('ltp')->after('cid')->default(0); // тип лога
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function (Blueprint $table) {
            //
        });
    }
}
