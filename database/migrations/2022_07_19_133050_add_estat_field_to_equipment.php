<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEstatFieldToEquipment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipment', function (Blueprint $table) {
            $table->integer('estat')->after('inv_num');
            $table->string('estat_des',128)->after('estat');
        });
        $items = \App\Models\Equipment::all();
        foreach ($items as $equ){
            $estat = \App\Http\Controllers\ApiEquipmentController::getEstatFromEquipment($equ);
            $estat_des = \App\Http\Controllers\ApiEquipmentController::getStatDescription($estat);
            $equ->update(['estat' => $estat, 'estat_des' => $estat_des]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipment', function (Blueprint $table) {
            //
        });
    }
}
