@extends('layouts.app')

@section('third_party_stylesheets')
    <style>
        .shadow__card {
            background: #ffffff;
            background: linear-gradient(157deg, #ffffff 10%, #f2f2f2 100%);
            box-shadow: 6px 6px 10px -4px rgba(0, 0, 0, 0.40);
        }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-4">
            <div class="container-fluid">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-body">
                        <div class="text-center">
                            <button class="btn btn-success" id="prs-import" style="margin: 2px 2px 2px 16px;">{{ __('str.mens_import_parsec') }}</button>
                        </div>

                        <br><br>

                        <div id="prs-import-container" style="display: none;height: 480px;">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.mens_host') }}</span>
                                </div>
                                <input type="text" value="{{$prs_host}}" id="prs_host" name="prs_host" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.mens_port') }}</span>
                                </div>
                                <input type="number" value="{{$prs_port}}" id="prs_port" name="prs_port" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.mens_domen') }}</span>
                                </div>
                                <input type="text" value="{{$prs_dom}}" id="prs_dom" name="prs_dom" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" >
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.mens_operator') }}</span>
                                </div>
                                <input type="text" value="{{$prs_opr}}" id="prs_opr" name="prs_opr" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.mens_password') }}</span>
                                </div>
                                <input type="password" value="{{$prs_pas}}" id="prs_pas" name="prs_pas" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.mens_label_type') }}</span>
                                </div>
                                <select class="form-control" id="hex" name="hex" required>
                                    <option value=""  @if ($prs_hex != '0' && $prs_hex != '1') selected @endif disabled>{{ __('str.mens_label_type_select') }}</option>
                                    <option value="0" @if ($prs_hex == '0') selected @endif >{{ __('str.mens_label_type_10') }}</option>
                                    <option value="1" @if ($prs_hex == '1') selected @endif >{{ __('str.mens_label_type_hex') }}</option>
                                </select>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sync_parsec') }}</span>
                                </div>
                                <select class="form-control" id="parsec_sync_time" name="parsec_sync_time" required>
                                    <option value="0" selected>{{ __('str.settings_missing') }}</option>
                                    <option value="600">{{ __('str.settings_min10') }}</option>
                                    <option value="900">{{ __('str.settings_min15') }}</option>
                                    <option value="1800">{{ __('str.settings_min30') }}</option>
                                    <option value="3600">{{ __('str.settings_clock1') }}</option>
                                    <option value="7200">{{ __('str.settings_clock2') }}</option>
                                    <option value="14400">{{ __('str.settings_clock4') }}</option>
                                    <option value="43200">{{ __('str.settings_clock12') }}</option>
                                    <option value="86400">{{ __('str.settings_clock24') }}</option>
                                </select>
                            </div>
                            <button id="men-parsec-import" class="btn btn-success">{{ __('str.mens_import_parsec2') }}</button>
                            <br><br>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="container-fluid">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-body">
                        <div class="text-center">
                            <button class="btn btn-success" id="ori-import" style="margin: 2px 2px 2px 16px;">{{ __('str.mens_import_orion') }}</button>
                        </div>

                        <br><br>

                        <div id="ori-import-container" style="display: none;height: 480px;">
                            <form action="/men-ori-import" method="POST" enctype="multipart/form-data" style="width: 100%">
                                @csrf
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.mens_ip') }}</span>
                                    </div>
                                    <input type="text" value="{{$ori_host}}" id="ori_host" name="ori_host" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.mens_port') }}</span>
                                    </div>
                                    <input type="number" value="{{$ori_port}}" id="ori_port" name="ori_port" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sync_orion') }}</span>
                                    </div>
                                    <select class="form-control" id="orion_sync_time" name="orion_sync_time" required>
                                        <option value="0" selected>{{ __('str.settings_missing') }}</option>
                                        <option value="600">{{ __('str.settings_min10') }}</option>
                                        <option value="900">{{ __('str.settings_min15') }}</option>
                                        <option value="1800">{{ __('str.settings_min30') }}</option>
                                        <option value="3600">{{ __('str.settings_clock1') }}</option>
                                        <option value="7200">{{ __('str.settings_clock2') }}</option>
                                        <option value="14400">{{ __('str.settings_clock4') }}</option>
                                        <option value="43200">{{ __('str.settings_clock12') }}</option>
                                        <option value="86400">{{ __('str.settings_clock24') }}</option>
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.mens_lock_card_if_has_equ') }}</span>
                                    </div>
                                    <select class="form-control" id="ori_card_lock" name="ori_card_lock" required>
                                        <option value="0" @if ($ori_card_lock == '0') selected @endif >{{ __('str.mens_not_lock') }}</option>
                                        <option value="1" @if ($ori_card_lock != '0') selected @endif >{{ __('str.mens_lock') }}</option>
                                    </select>
                                </div>
                                <button class="btn btn-success">{{ __('str.mens_import_orion2') }}</button>
                            </form>
                            <br><br>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="container-fluid">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-body">
                        <div class="text-center">
                            <button class="btn btn-success" id="soti-import" style="margin: 2px 2px 2px 16px;">{{ __('str.settings_go_soti_page') }}</button>
                        </div>

                        <br><br>

                        <div id="soti-import-container" style="display: none;height: 480px;">
                            <form method="post" action="/soti-save-action">
                                @csrf

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.soti_settings_host') }}</span>
                                    </div>
                                    <input type="text" value="{{$soti_host}}" id="soti_host" name="soti_host" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.soti_settings_client_id') }}</span>
                                    </div>
                                    <input type="text" value="{{$soti_client_id}}" id="soti_client_id" name="soti_client_id" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.soti_settings_client_secret') }}</span>
                                    </div>
                                    <input type="text" value="{{$soti_client_secret}}" id="soti_client_secret" name="soti_client_secret" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.soti_settings_api_username') }}</span>
                                    </div>
                                    <input type="text" value="{{$soti_api_username}}" id="soti_api_username" name="soti_api_username" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.soti_settings_api_password') }}</span>
                                    </div>
                                    <input type="text" value="{{$soti_api_password}}" id="soti_api_password" name="soti_api_password" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>

                                <br>
                                <button type="submit" class="btn btn-success" style="width: 200px">{{ __('str.soti_settings_save') }}</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="container-fluid">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-body">
                        <div class="text-center">
                            <button class="btn btn-success" id="sigur-import" style="margin: 2px 2px 2px 16px;">{{ __('str.settings_sync_sigur') }}</button>
                        </div>
                        <br><br>
                        <div id="sigur-import-container" style="display: none;height: 480px;">
                            <form action="/settings-sigur-act" method="POST" enctype="multipart/form-data" style="width: 100%">
                            @csrf
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sync_sigur_switch') }}</span>
                                    </div>
                                    <select class="form-control" id="sigur_sync_en" name="sigur_sync_en" required>
                                        <option value="0" @if ($sigur_sync_en == '0') selected @endif >{{ __('str.settings_off') }}</option>
                                        <option value="1" @if ($sigur_sync_en != '0') selected @endif >{{ __('str.settings_on') }}</option>
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_log_sigur_switch') }}</span>
                                    </div>
                                    <select class="form-control" id="sigur_log_en" name="sigur_log_en" required>
                                        <option value="0" @if ($sigur_log_en == '0') selected @endif >{{ __('str.settings_off') }}</option>
                                        <option value="1" @if ($sigur_log_en != '0') selected @endif >{{ __('str.settings_on') }}</option>
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sigur_delegation_mode') }}</span>
                                    </div>
                                    <select class="form-control" id="sigur_mode" name="sigur_mode" required>
                                        <option value="0" @if ($sigur_mode == '0') selected @endif >{{ __('str.settings_sigur_mode_check') }}</option>
                                        <option value="1" @if ($sigur_mode == '1') selected @endif >{{ __('str.settings_sigur_mode_all') }}</option>
                                        <option value="2" @if ($sigur_mode == '2') selected @endif >{{ __('str.settings_sigur_mode_none') }}</option>
                                    </select>
                                </div>
                                <br><br>
                                <button class="btn btn-success">{{ __('str.save') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="container-fluid">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-body">
                        <div class="text-center">
                            <button class="btn btn-success" id="sap-ewm-import" style="margin: 2px 2px 2px 16px;">{{ __('str.settings_sync_sap_ewm') }}</button>
                        </div>
                        <br><br>
                        <div id="sap-ewm-container" style="display: none;height: 480px;">
                            <form action="/settings-sap-ewm-act" method="POST" enctype="multipart/form-data" style="width: 100%">
                                @csrf
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sync_sap_ewm_switch') }}</span>
                                    </div>
                                    <select class="form-control" id="sap_ewm_en" name="sap_ewm_en" required>
                                        <option value="0" @if ($sap_ewm_en == '0') selected @endif >{{ __('str.settings_off') }}</option>
                                        <option value="1" @if ($sap_ewm_en != '0') selected @endif >{{ __('str.settings_on') }}</option>
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sync_sap_ewm_url') }}</span>
                                    </div>
                                    <input type="text" value="{{$sap_ewm_url}}" id="sap_ewm_url" name="sap_ewm_url" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" >
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-info" style="width: 100%"
                                                onclick='$("#sap_ewm_url").val("http://sappoq.detmir-group.ru:50000/RESTAdapter/TSD/Logon")'
                                        >{{ __('str.settings_sync_sap_ewm_test_url') }}</button>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="btn btn-info" style="width: 100%"
                                                onclick='$("#sap_ewm_url").val("http://pi-haproxy.detmir-group.ru:84/RESTAdapter/TSD/Logon")'
                                        >{{ __('str.settings_sync_sap_ewm_work_url') }}</button>
                                    </div>
                                </div>
                                <br>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="sap_ewm_log" style="width: 200px">{{ __('str.settings_sync_sap_ewm_log') }}</span>
                                    </div>
                                    <input type="text" value="{{$sap_ewm_log}}" id="sap_ewm_log" name="sap_ewm_log" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" >
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sync_sap_ewm_pas') }}</span>
                                    </div>
                                    <input type="text" value="{{$sap_ewm_pas}}" id="sap_ewm_pas" name="sap_ewm_pas" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" >
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sync_sap_ewm_switch_log') }}</span>
                                    </div>
                                    <select class="form-control" id="sap_ewm_en_log" name="sap_ewm_en_log" required>
                                        <option value="0" @if ($sap_ewm_en_log == '0') selected @endif >{{ __('str.settings_off') }}</option>
                                        <option value="1" @if ($sap_ewm_en_log != '0') selected @endif >{{ __('str.settings_on') }}</option>
                                    </select>
                                </div>
                                <br><br>
                                <button class="btn btn-success">{{ __('str.save') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="container-fluid">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-body">
                        <div class="text-center">
                            <button class="btn btn-success" id="sigur-import2" style="margin: 2px 2px 2px 16px;">{{ __('str.settings_sync_sigur2') }}</button>
                        </div>
                        <br><br>
                        <div id="sigur-import-container2" style="display: none;height: 600px;">
                            <form action="/settings-sigur-act2" method="POST" enctype="multipart/form-data" style="width: 100%">
                                @csrf
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sync_sigur_switch') }}</span>
                                    </div>
                                    <select class="form-control" id="sigur_sync_en2" name="sigur_sync_en" required>
                                        <option value="0" @if ($sigur_sync_en2 == '0') selected @endif >{{ __('str.settings_off') }}</option>
                                        <option value="1" @if ($sigur_sync_en2 != '0') selected @endif >{{ __('str.settings_on') }}</option>
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sync_sigur_employees') }}</span>
                                    </div>
                                    <select class="form-control" id="sigur_sync_employees" name="sigur_sync_employees" required>
                                        <option value="0" @if ($sigur_sync_employees == '0') selected @endif >{{ __('str.settings_off') }}</option>
                                        <option value="1" @if ($sigur_sync_employees != '0') selected @endif >{{ __('str.settings_on') }}</option>
                                    </select>
                                </div>
                                <div class="input-group mb-3" style="display: none">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_log_sigur_switch') }}</span>
                                    </div>
                                    <select class="form-control" id="sigur_log_en2" name="sigur_log_en" required>
                                        <option value="0" @if ($sigur_log_en2 == '0') selected @endif >{{ __('str.settings_off') }}</option>
                                        <option value="1" @if ($sigur_log_en2 != '0') selected @endif >{{ __('str.settings_on') }}</option>
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sync_sigur_host') }}</span>
                                    </div>
                                    <input type="text" value="{{$sigur_host}}" id="sigur_host" name="sigur_host" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_sync_sigur_jwt_token') }}</span>
                                    </div>
                                    <input type="text" value="{{$sigur_jwt_token}}" id="sigur_jwt_token" name="sigur_jwt_token" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">SYSTEM INTEGRATION</span>
                                    </div>
                                    <input type="text" value="{{$sigur_sys_int}}" id="sigur_sys_int" name="sigur_sys_int" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">SIGUR PASSWORD</span>
                                    </div>
                                    <input type="text" value="{{$sigur_api_pas}}" id="sigur_api_pas" name="sigur_api_pas" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <button type="button" class="btn btn-info" style="width: 100%" onclick='checkJSigur()'>{{ __('str.settings_sigur_check_jserver') }}</button>
                                    </div>
                                    <div class="col-lg-4">
                                        <button type="button" class="btn btn-info" style="width: 100%" onclick='startJSigur()'>{{ __('str.settings_sigur_on_jserver') }}</button>
                                    </div>
                                    <div class="col-lg-4">
                                        <button type="button" class="btn btn-info" style="width: 100%" onclick='stopJSigur()'>{{ __('str.settings_sigur_off_jserver') }}</button>
                                    </div>
                                </div><br>
                                <button type="button" class="btn btn-info" style="width: 100%" onclick='getSigurLogin()'>тест получения токена</button><br><br>
                                <button type="button" class="btn btn-info" style="width: 100%" onclick='emplTestSigur()'>тест выгрузки сотрудников</button>
                                <br><br>
                                <button class="btn btn-success">{{ __('str.save') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="container-fluid">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-body">
                        <div class="text-center">
                            <button class="btn btn-success"
                                    id="sigur-import"
                                    onclick="let st=$('#seven-seals-import-container').css('display') == 'block';$('#seven-seals-import-container').css('display', st ? 'none' : 'block');"
                                    style="margin: 2px 2px 2px 16px;">
                                {{ __('str.settings_sync_seven_seals') }}
                            </button>
                        </div>
                        <br><br>
                        <div id="seven-seals-import-container" style="display: none;height: 480px;">
                            <form action="/seven-seals-import" method="POST" enctype="multipart/form-data" style="width: 100%">
                                @csrf
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text"
                                              id="inputGroup-sizing-default"
                                              style="width: 200px">
                                            {{ __('str.settings_sync_seven_seals_switch') }}
                                        </span>
                                    </div>
                                    <select
                                        class="form-control"
                                        id="seven_seals_sync_en"
                                        name="seven_seals_sync_en" required>
                                        <option value="0" @if (intval($seven_seals_sync_en) === 0) selected @endif >{{ __('str.settings_off') }}</option>
                                        <option value="1" @if (intval($seven_seals_sync_en) !== 0) selected @endif >{{ __('str.settings_on') }}</option>
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text"
                                              id="inputGroup-sizing-default"
                                              style="width: 200px">{{ __('str.settings_sync_seven_seals_dsn') }}</span>
                                    </div>
                                    <input type="text"
                                           value="{{$seven_seals_dsn}}"
                                           id="seven_seals_dsn"
                                           name="seven_seals_dsn"
                                           class="form-control"
                                           aria-label="Default"
                                           aria-describedby="inputGroup-sizing-default">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text"
                                              id="inputGroup-sizing-default"
                                              style="width: 200px">{{ __('str.settings_sync_seven_seals_user') }}
                                        </span>
                                    </div>
                                    <input type="text"
                                           value="{{$seven_seals_user}}"
                                           id="seven_seals_user"
                                           name="seven_seals_user"
                                           class="form-control"
                                           aria-label="Default"
                                           aria-describedby="inputGroup-sizing-default">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text"
                                              id="inputGroup-sizing-default"
                                              style="width: 200px">{{ __('str.settings_sync_seven_seals_pass') }}
                                        </span>
                                    </div>
                                    <input type="text"
                                           value="{{$seven_seals_pass}}"
                                           id="seven_seals_pass"
                                           name="seven_seals_pass"
                                           class="form-control"
                                           aria-label="Default"
                                           aria-describedby="inputGroup-sizing-default">
                                </div>
                                <br><br>
                                <button class="btn btn-success">{{ __('str.mens_import_seven_seals') }}</button>
                            </form>
                            <br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script>

        let loadInProgress = false;

        function getSigurLogin(){
            $.get( "/sigur-get-token", function(data, status) {
                if (status === 'success'){
                    if (data.length > 0){
                        alert("Токен получен, сохраните его.");
                        $("#sigur_jwt_token").val(data);
                    }else{
                        alert("Токен не получен.");
                    }
                }
            });
        }

        function checkJSigur(){
            $.get( "/jsigur-check", function(data, status) {
                if (status === 'success'){
                    if ('true' === data){
                        alert('{{ __('str.settings_sigur_on_jserver_status') }}');
                    }else if ('false' === data){
                        alert('{{ __('str.settings_sigur_off_jserver_status') }}');
                    }else{
                        alert("Error!!! " + data);
                        console.log(data);
                    }
                }
            });
        }

        function startJSigur(){
            $.get( "/jsigur-on", function(data, status) {
                if (status === 'success'){
                    if ('true' === data){
                        alert('{{ __('str.settings_sigur_on_jserver_status') }}');
                    }else{
                        alert(data);
                        console.log(data);
                    }
                }
            });
        }

        function stopJSigur(){
            $.get( "/jsigur-off", function(data, status) {
                if (status === 'success'){
                    if ('true' === data){
                        alert('{{ __('str.settings_sigur_off_jserver_status') }}');
                    }else{
                        alert(data);
                        console.log(data);
                    }
                }
            });
        }

        function emplTestSigur(){
            $.get( "/sigur-empl-test", function(data, status) {
                if (status === 'success'){
                    alert(data);
                    console.log(data);
                }
            });
        }

        function uploadParsecData(){
            if (loadInProgress){
                alert('Обновление сотрудников из PARSEC уже идёт');
                return;
            }
            alert('Началось обновление сотрудников из PARSEC');
            loadInProgress = true;
            let formData = {
                _token: "{{csrf_token()}}",
                prs_host: $("#prs_host").val(),
                prs_port: $("#prs_port").val(),
                prs_dom: $("#prs_dom").val(),
                prs_opr: $("#prs_opr").val(),
                prs_pas: $("#prs_pas").val(),
                parsec_sync_time: $("#parsec_sync_time").val(),
                hex: $("#hex").val(),
            };
            $.ajax({
                type: "POST",
                url: "/men-prs-import",
                data: formData,
                timeout: 60000,
            }).done(function (data) {
                loadInProgress = false;
                //console.log(">>>");
                if (data.endsWith("true")){
                    alert("Обновление сотрудников выполнено успешно");
                }else{
                    alert("Ошибка обновления: " + data);
                    console.log(data);
                }
                document.location.reload();
            });
        }

        $(document).ready(function(){
            $('#prs-import').click(function () {
                $('#prs-import-container').slideToggle(500);
            });
            $('#ori-import').click(function () {
                $('#ori-import-container').slideToggle(500);
            });
            $('#soti-import').click(function () {
                $('#soti-import-container').slideToggle(500);
            });
            $('#sigur-import').click(function () {
                $('#sigur-import-container').slideToggle(500);
            });
            $('#sigur-import2').click(function () {
                $('#sigur-import-container2').slideToggle(500);
            });
            $('#parsec_sync_time').val("{{$parsec_sync_time}}");
            $("#men-parsec-import").click(uploadParsecData);
            $('#sap-ewm-import').click(function () {
                $('#sap-ewm-container').slideToggle(500);
            });
            // Тени и градиент
            let coverCardEvt = $(".card__modificator");
            if (!($("body").hasClass("dark-layout"))) {
                coverCardEvt.mouseover(function() {
                    $(this).addClass("shadow__card");
                });
                coverCardEvt.mouseout(function() {
                    $(this).removeClass("shadow__card");
                });
            }
        });
    </script>
@endpush
