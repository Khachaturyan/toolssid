<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TOOLSiD - Авторизация</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Alegreya+SC:ital,wght@0,400;1,700&family=IBM+Plex+Serif:ital,wght@1,500&family=Manrope:wght@400;600&family=Montserrat:wght@700&family=Roboto:ital,wght@0,400;1,400;1,700&family=Vollkorn+SC:wght@900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/style-login.css">
</head>
<body>
    <div class="container">
        <div class="login__container">
            <div class="left__side">
                <div class="login__layer-1"></div>
                <div class="login__layer-2"></div>
                <div class="login__layer"></div>
                <div class="left__content">
                    <div class="logo__content">
                        <div class="logo__top">
                            <img src="/assets/img/logo.svg" alt="">
                        </div>
                        <p class="logo__title">Добро пожаловать</p>
                        <p class="logo__title">в TOOLSiD !</p>
                    </div>
                    <div class="logo__bottom">Чтобы поддерживать связь с нами, пожалуйста, войдите в систему, используя свою личную информацию.</div>
                    <!-- <div class="logo__but"> -->
                    <!-- <div class="but__title"> -->
                    @if (\App\Models\CustomData::where('label','reg')->where('data','false')->first() == null)
                        <a href="{{ route('register') }}" class="logo__but but__title-dark btn-react">Регистрация</a>
                    @endif
                    <!-- </div> -->
                    <!-- </div> -->
                </div>
            </div>
            <div class="right__side">
                <div class="right__content">
                    <div class="form__container">
                        <div class="form__content">
                            <div class="form__create-account">
                                <div class="form__header">
                                    <div class="form__header-top">Вход</div>
                                    <div class="form__header-bottom form__header-bottom-log">Войдите в аккаунт</div>
                                    <form method="post" action="{{ url('/login') }}" id="auth-form">
                                        @csrf

                                        <div class="form__body">
                                            <div class="input__box">
                                                <div class="input__box-icon input__box-icon_email">
                                                    <input  type="email"
                                                            name="email"
                                                            id="email"
                                                            value="{{ old('email') }}"
                                                            placeholder="Email"
                                                            class="input__box-field @error('email') @enderror">
                                                </div>
                                                @error('email')
                                                <div class="error__under-box">
                                                    <img style="padding-right:8px;" src="/assets/img/close-circle.svg" alt="error">
                                                    {{ $message }}
                                                </div>
                                                @enderror
                                            </div>
                                            <div class="input__box">
                                                <div class="input__box-icon input__box-icon-psw">
                                                    <input  type="password"
                                                            name="password"
                                                            id="password__input"
                                                            placeholder="Password"
                                                            class="input__box-field @error('password') is-invalid @enderror"
                                                    >
                                                    <div class="input__box-icon-psw-img"></div>
                                                </div>
                                                @error('password')
                                                <div class="error__under-box">
                                                    <img style="padding-right:8px;" src="/assets/img/close-circle.svg" alt="error">
                                                    {{ $message }}
                                                </div>
                                                @enderror
                                            </div>
                                        </div>
                                        <br>
                                        <span style="color:red;font-size: large;" id="error-alert"></span>
                                        <div class="form__footer">
                                            <div class="btn-container">
                                                <button type="button" class="form__but but__title but__title-white" onclick="authButtonOnClick()">
                                                    <span class="text">Авторизоваться</span>
                                                    <div class="icon-container">
                                                        <div class="icon icon--left">
                                                            <svg>
                                                                <use xlink:href="#arrow-right"></use>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </button>
                                            </div>
                                            <svg style="display: none;">
                                                <symbol id="arrow-right" viewBox="0 0 20 10">
                                                    <path d="M14.84 0l-1.08 1.06 3.3 3.2H0v1.49h17.05l-3.3 3.2L14.84 10 20 5l-5.16-5z"></path>
                                                </symbol>
                                            </svg>
                                            <div class="form__footer-title">
                                                <a href="{{ route('password.request') }}"><p>Забыли логин или пароль? Восстановить доступ </p></a>
                                            </div>
                                        </div>
                                    </form> <!-- /.form -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>

    <script type="text/javascript">

        function authButtonOnClick(){
            let email = $('#email').val();
            let post_data = {};
            post_data['_token'  ] = "{{csrf_token()}}";
            post_data['email'   ] = email;

            $.post( "/check-user-auth", post_data, function(data, status) {
                if (status === 'success'){
                    //console.log(data);
                    if ('true' === data){
                        $("#auth-form").submit();
                    } else {
                        showAlert(data);
                    }
                }
            });
        }

        function showAlert(txt){
            $('#error-alert').html(txt);
        }

        $('body').on('click', '.input__box-icon-psw-img', function(){
            if ($('#password__input').attr('type') == 'password'){
                $(this).addClass('view');
                $('#password__input').attr('type', 'text');
            } else {
                $(this).removeClass('view');
                $('#password__input').attr('type', 'password');
            }
            return false;
        });

    </script>

</body>
</html>
