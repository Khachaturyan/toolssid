<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TOOLSiD - Регистрация</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Alegreya+SC:ital,wght@0,400;1,700&family=IBM+Plex+Serif:ital,wght@1,500&family=Manrope:wght@400;600&family=Montserrat:wght@700&family=Roboto:ital,wght@0,400;1,400;1,700&family=Vollkorn+SC:wght@900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/bootstrap-5.1.3-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/style-register.css">
</head>

<body>

<!-- Modal alert data -->
<div class="modal fade" id="modal-alert" tabindex="-1" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" style="border-radius:20px;overflow: hidden;">
            <div class="modal-header" style="justify-content:center;border-top-right-radius:20px;border-top-left-radius:20px;background-color:#ccc;">
                <h4 class="modal-title" style="font-weight:500;">Подтверждение email</h4>
            </div>
            <div class="modal-body" style="font-size:20px;margin:0;text-align:center;font-weight:500;background-color:#ccc;">
                <p id="headerToastInfo" style="text-align: center"></p>
                <p id="headerToastInfo1" style="text-align: center"></p>
            </div>
            <div class="modal-footer" style="justify-content:center;background-color:#ccc;">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal" style="border-radius:8px;">
                    <h4 style="font-size:20px;margin:0">Хорошо!</h4>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="container__box">
    <div class="login__container">
        <div class="left__side">
            <div class="login__layer-1"></div>
            <div class="login__layer-2"></div>
            <div class="login__layer"></div>
            <div class="left__content">
                <div class="logo__content">
                    <div class="logo__top">
                        <img src="/assets/img/logo.svg" alt="">
                    </div>
                    <p class="logo__title">Добро пожаловать</p>
                    <p class="logo__title">в TOOLSiD !</p>
                </div>
                <div class="logo__bottom">Чтобы поддерживать связь с нами, пожалуйста, войдите в систему, используя свою личную информацию.</div>
                <a href="{{ route('login') }}" class="logo__but but__title-dark btn-react">Вход</a>
            </div>
        </div>
        <div class="right__side">
            <div class="right__content">
                <div class="form__container">
                    <div class="form__content">
                        <div class="form__create-account">
                            <div class="form__header">
                                <div class="form__header-top">Регистрация</div>
                                <div class="form__header-bottom">Зарегистрируйте свой аккаунт</div>

                                <form method="post" action="{{ route('register') }}">
                                    @csrf
                                    <div class="form__body">
                                        <div class="input__box">
                                            <input type="text"
                                                   placeholder="{{ __('str.mens_surname') }}"
                                                   id="msurname"
                                                   name="msurname"
                                                   class="input__box-field" required>
                                        </div>

                                        <div class="input__box">
                                            <input type="text"
                                                   placeholder="{{ __('str.mens_name') }}"
                                                   id="mname"
                                                   name="mname"
                                                   class="input__box-field" required>
                                        </div>

                                        <div class="input__box">
                                            <input type="text"
                                                   placeholder="{{ __('str.mens_patronymic') }}"
                                                   id="mpatronymic"
                                                   name="mpatronymic"
                                                   class="input__box-field">
                                        </div>

                                        <div class="input__box">
                                            <input type="text"
                                                   placeholder="{{ __('str.mens_post') }}"
                                                   id="post"
                                                   name="post"
                                                   class="input__box-field">
                                        </div>

                                        <div class="input__box">
                                            <div class="input__box-icon input__box-icon_email">
                                                <input
                                                    type="email"
                                                    name="email"
                                                    id="email"
                                                    value="{{ old('email') }}"
                                                    class="input__box-field @error('email') @enderror"
                                                    placeholder="Email"
                                                >

                                                @error('email')
                                                <div class="error__under-box">
                                                    <img style="padding-right:8px;" src="/assets/img/close-circle.svg" alt="error">
                                                    {{ $message }}
                                                </div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="input__box">
                                            <div class="input__box-icon input__box-icon-psw">
                                                <input
                                                    type="password"
                                                    name="password"
                                                    id="password"
                                                    class="input__box-field @error('password') @enderror"
                                                    placeholder="Password" pattern=".{8,12}" required title="8 to 12 characters"
                                                >
                                                <div class="input__box-icon-psw-img"></div>

                                                @error('password')
                                                <div class="error__under-box">
                                                    <img style="padding-right:8px;" src="/assets/img/close-circle.svg" alt="error">
                                                    {{ $message }}
                                                </div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="input__box">
                                            <div class="input__box-icon input__box-icon-psw">
                                                <input
                                                    type="password"
                                                    name="password_confirmation"
                                                    id="password_confirmation"
                                                    class="input__box-field"
                                                    placeholder="Retype password"
                                                >
                                                <div class="input__box-icon-psw-img1"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="icheck-primary">
                                        <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                                        <label for="agreeTerms">
                                            Я принимаю <a href="#">соглашение</a>
                                        </label>
                                    </div>

                                    <div class="form__footer">
                                        <div class="btn-container">
                                            <button type="button" class="form__but but__title but__title-white" onclick="onSendFormData()">
                                                <span class="text">Зарегистрироваться</span>
                                                <div class="icon-container">
                                                    <div class="icon icon--left">
                                                        <svg>
                                                            <use xlink:href="#arrow-right"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </button>
                                        </div>
                                        <svg style="display: none;">
                                            <symbol id="arrow-right" viewBox="0 0 20 10">
                                                <path d="M14.84 0l-1.08 1.06 3.3 3.2H0v1.49h17.05l-3.3 3.2L14.84 10 20 5l-5.16-5z"></path>
                                            </symbol>
                                        </svg>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ mix('js/app.js') }}" ></script>

<script type="text/javascript">

    function showAlert(email){
        let str = "Мы отправили письмо с подверждением на " + email;
        let str1 = "Пройдите по ссылке в письме";
        $('#headerToastInfo').html(str);
        $('#headerToastInfo1').html(str1);
        $("#modal-alert").modal("show");
    }

    function onSendFormData(){

        let licenseIsConfirm = $('#agreeTerms').prop('checked');
        if (!licenseIsConfirm){
            alert('Прочитайте и примите лицензионное соглашение');
            return;
        }

        let msurname = $('#msurname').val();
        let mname = $('#mname').val();
        let mpatronymic = $('#mpatronymic').val();
        let _post = $('#post').val();
        let email = $('#email').val();
        let password = $('#password').val();
        let password_confirmation = $('#password_confirmation').val();

        if (msurname.length === 0 || mname.length === 0 ||
            mpatronymic.length === 0 || _post.length === 0 || email.length === 0){
            alert('Заполните все поля');
            return;
        }
        if (password.length < 8){
            alert('Пароль слишком короткий');
            return;
        }
        if (password != password_confirmation){
            alert('Логин и пароль не совпадают');
            return;
        }

        let post_data = {};
        post_data['_token'               ] = "{{csrf_token()}}";
        post_data['msurname'             ] = msurname;
        post_data['mname'                ] = mname;
        post_data['mpatronymic'          ] = mpatronymic;
        post_data['post'                 ] = _post;
        post_data['email'                ] = email;
        post_data['password'             ] = password;
        post_data['password_confirmation'] = password_confirmation;

        $.post( "/register-new", post_data, function(data, status) {
            if (status === 'success'){
                console.log(data);
                if ('true' === data){
                    showAlert(email);
                } else if ('false' === data) {
                    alert('Не удалось завершить регистрацию, пользователь с такой почтой уже существует');
                } else {
                    console.log(data);
                }
            }
        });
    }

    // Показать скрыть вводимый пароль
    $('body').on('click', '.input__box-icon-psw-img', function(){
        if ($('#password').attr('type') == 'password'){
            $(this).addClass('view');
            $('#password').attr('type', 'text');
        } else {
            $(this).removeClass('view');
            $('#password').attr('type', 'password');
        }
        return false;
    });
    $('body').on('click', '.input__box-icon-psw-img1', function(){
        if ($('#password_confirmation').attr('type') == 'password'){
            $(this).addClass('view');
            $('#password_confirmation').attr('type', 'text');
        } else {
            $(this).removeClass('view');
            $('#password_confirmation').attr('type', 'password');
        }
        return false;
    });

    //$(document).ready(function(){
        //console.log('data');
        //showAlert('email');
    //})

</script>
</body>
</html>
