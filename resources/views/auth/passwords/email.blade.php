<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }}</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
          integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
          crossorigin="anonymous"/> -->

    <!-- <link href="{{ mix('css/app.css') }}" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css2?family=Alegreya+SC:ital,wght@0,400;1,700&family=IBM+Plex+Serif:ital,wght@1,500&family=Manrope:wght@400;600&family=Montserrat:wght@700&family=Roboto:ital,wght@0,400;1,400;1,700&family=Vollkorn+SC:wght@900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/style-reset.css">

</head>
<body>
<div class="container">
    <div class="login__container">
        <div class="left__side">
            <div class="login__layer-1"></div>
            <div class="login__layer-2"></div>
            <div class="login__layer"></div>
            <div class="left__content">
                <div class="logo__content">
                    <div class="logo__top"><a href="{{ url('/') }}">
                            <img src="/assets/img/logo.svg" alt=""></a>
                    </div>
                </div>
                <p class="mt-1 mb-1">
                    <a href="{{ route("login") }}" class="logo__but but__title-dark btn-react">{{ __('str.auth') }}</a>
                </p>
                @if (\App\Models\CustomData::where('label','reg')->where('data','false')->first() == null)
                    <p class="mb-0">
                        <a href="{{ route("register") }}" class="logo__but but__title-dark btn-react">{{ __('str.reg') }}</a>
                    </p>
                @endif
            </div>
        </div>
        <div class="right__side">
            <div class="right__content">
                <div class="form__container">
                    <div class="form__content">
                        <div class="form__create-account">
                            <div class="form__header">
                                <div class="form__header-top">Забыли пароль?</div>
                                <div class="form__header-bottom form__header-bottom-log">Здесь его можно легко восстановить</div>

                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <form action="/pass-email" method="post">
                                    @csrf

                                    <div class="form__body">
                                        <div class="input__box">
                                            <div class="input__box-icon input__box-icon_email">
                                                <input type="email"
                                                       name="email"
                                                       class="input__box-field{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                       placeholder="Email"
                                                >
                                            </div>
                                            @if ($errors->has('email'))
                                                <span class="error invalid-feedback">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <br>
                                    <span style="color:red;font-size: large;" id="error-alert"></span>
                                    <div class="form__footer">
                                        <div class="btn-container">
                                            <button type="submit" class="form__but but__title but__title-white">
                                                <span class="text">{{ __('str.send_restore_link') }}</span>
                                                <div class="icon-container">
                                                    <div class="icon icon--left">
                                                        <svg>
                                                            <use xlink:href="#arrow-right"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </button>
                                        </div>
                                        <svg style="display: none;">
                                            <symbol id="arrow-right" viewBox="0 0 20 10">
                                                <path d="M14.84 0l-1.08 1.06 3.3 3.2H0v1.49h17.05l-3.3 3.2L14.84 10 20 5l-5.16-5z"></path>
                                            </symbol>
                                        </svg>
                                    </div>
                                </form> <!-- /.form -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ mix('js/app.js') }}" defer></script>

</body>
</html>
