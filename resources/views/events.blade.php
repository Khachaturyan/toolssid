@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                <div class="row">
                    <div class="card-body">
                        <h4 class="modal-title">
                            {{ __('str.events_events') }}
                        </h4>
                        <br>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.events_block') }}</span>
                            </div>
                            <select class="form-control" name="bid" id="bid">
                                @foreach ($blocks as $block)
                                    <option value="{{ $block->id }}"
                                            @if ($block->id == $selectedBlockId)
                                            selected="selected"
                                        @endif
                                    >{{ $block->adr . " " . $block->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.events_mens') }}</span>
                            </div>
                            <select class="form-control" name="mid" id="mid">
                                @foreach ($mens as $men)
                                    <option value="{{ $men->id }}"
                                            @if ($men->id == $selectedMenId)
                                            selected="selected"
                                        @endif
                                    >{{ $men->mname . " " . $men->msurname }}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                        "data-page-length" => "100",
                        "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    {{$dataTable->scripts()}}

    <script src="{{ asset('js/chart3.js') }}"></script>

    <script>
        function goToUrl(){
            let bid = document.getElementById("bid").value;
            let mid = document.getElementById("mid").value;
            window.location='{{ url("/events") }}' + '/' + bid + '/' + mid;
        }
        window.onload = function(){
            document.getElementById("bid").addEventListener('change', function() {
                goToUrl();
            });
            document.getElementById("mid").addEventListener('change', function() {
                goToUrl();
            });
        }
    </script>


@endpush
