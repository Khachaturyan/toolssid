@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                <h1 class="text-black-50">{{ __('str.group_message_send') }}</h1>
                <form method="post" action="/tm-men-send-action">
                    @csrf
                    <input type="text" name="ids" value="{{$ids}}" style="visibility: hidden">
                    <input type="text" name="title" value="{{ __('str.group_message_msg') }}" style="visibility: hidden">
                    <div class="input-group">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" style="width: 200px; height: 100px">{{ __('str.group_message_text') }}</span>
                        </div>
                        <textarea name="body" class="form-control" aria-label="With textarea"></textarea>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary" style="width: 200px">{{ __('str.group_message_send2') }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection
