@extends('layouts.app')

@section('content')
    <div class="modal fade" id="modal-users" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.equ_select_men2') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h5 id="headerToastInfo" style="height: 16px; text-align: right"></h5>
                    <table class="table table-bordered table-striped" id="men-table" data-page-length="12"></table>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                <table class="table table-bordered table-striped" id="equ-table" data-page-length="100"></table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        var equ = null;
        var rigitTableIsInit = false;
        var menTableIsInit = false;

        function onEquClick(data){
            equ = data;
            menDataUpdate(data.id)
            $('#modal-users').modal('show');
        }

        function menDataUpdate(eid){
            if (menTableIsInit){
                $('#men-table').DataTable().ajax.url('/men-data/' + eid).load();
            }else{
                menTableIsInit = true;
                $('#men-table').DataTable({
                    processing: true,
                    serverSide: true,
                    dom: "frtip",
                    ajax: '/men-data/' + eid,
                    columns: [
                        { data: 'is_checked',   title:'{{ __('str.table_linkin2') }}', className: 'text-center'  },
                        { data: 'post', name: 'men.post', title:'{{ __('str.table_post') }}' },
                        { data: 'msurname', name: 'men.msurname', title:'{{ __('str.table_surname') }}' },
                        { data: 'mname', name: 'men.mname', title:'{{ __('str.table_name2') }}' },
                        { data: 'mpatronymic', name: 'men.mpatronymic', title:'{{ __('str.mens_patronymic') }}'  },
                    ]
                }).on('click', 'tbody td', function() {
                    let tr  = $(this).closest('tr');
                    let row = $('#men-table').DataTable().row(tr);
                    onMenClick(row.data());
                }).on('dblclick', 'tbody td', function() {
                });
            }
        }

        function onMenClick(data){
            if (equ != null){
                let isChecked = data.is_checked.length > 0;
                let url = isChecked ? "/me-rigid-remove2/" + equ.id + "/" + data.id :
                                        "/me-rigid-add2/" + equ.id + "/" + data.id;
                $.get( url, function(_data, status) {
                    if (status === 'success'){
                        if ('true' === _data){
                            $('#equ-table').DataTable().ajax.reload();
                            menDataUpdate(equ.id);
                            let toast = isChecked ? "{{ __('str.rigit_links_men_remove_ok') }}" : "{{ __('str.rigit_links_men_add_ok') }}";
                            toast = data.mname + " " + data.msurname + " " + toast;
                            showToast(toast);
                        }else{
                            console.log(_data);
                            alert(_data);
                        }
                    }
                });
            }
        }

        var intervalId = null;

        function showToast(txt){
            if (intervalId != null){
                clearInterval(intervalId);
            }
            $("#headerToastInfo").text(txt);
            intervalId = setTimeout(function (){
                $("#headerToastInfo").text("");
                intervalId = null;
            },3000);
        }

        $(document).ready(function(){
            $('#equ-table').DataTable({
                processing: true,
                serverSide: true,
                dom: "frtip",
                ajax: '/equ-rigt-data',
                columns: [
                    { data: 'add_rigit', title:'{{ __('str.set_equ_add') }}', className: 'text-center', width: '80px' },
                    //{ data: 'remove_rigit', title:'{{ __('str.table_edit') }}', className: 'text-center', width: '80px' },
                    { data: 'ser_num', name: 'equipment.ser_num', title:'{{ __('str.table_ser') }}' },
                    { data: 'label', name: 'equipment.label', title:'{{ __('str.table_label') }}' },
                    { data: 'inv_num', name: 'equipment.inv_num', title:'{{ __('str.table_inv_num') }}' },
                    { data: 'estat_des', name: 'equipment.estat_des', title:'{{ __('str.table_status') }}'  },
                    { data: 'fio', title:'{{ __('str.table_linkin') }}' },
                ]
            }).on('click', 'tbody td', function() {
                let tr  = $(this).closest('tr');
                let row = $('#equ-table').DataTable().row(tr);
                if (this.cellIndex == 0){
                    onEquClick(row.data());
                }
            }).on('dblclick', 'tbody td', function() {
            });
        })
    </script>
@endpush
