@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                <h1 class="text-black-50">{{ __('str.set_equ_add') }}  <font color="#000">{{$etype->tname}} {{$etype->tmark}} {{$etype->tmodel}}</font> {{ __('str.set_equ_in_base') }}.</h1>
                <br>
                <form method="post" action="/add-equipment-action">
                    @csrf
                    <input type="number" name="id" value="{{$etype->id}}" style="visibility: hidden">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.set_equ_count') }}</span>
                        </div>
                        <input type="number" name="count" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary" style="width: 200px">{{ __('str.set_equ_create') }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection
