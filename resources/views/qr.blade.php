@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                <h1 class="text-black-50">{{ __('str.qr_code_need_code') }}</h1>
                <br>
                <br>
                <div style="margin: 64px; width: 100%" align="center">
                    {!! QrCode::size(512)->generate($data); !!}
                </div>
            </div>
        </div>
    </div>
@endsection

