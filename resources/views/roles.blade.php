@extends('layouts.app')

@section('third_party_stylesheets')
    <link href="{{ asset('/css/tabulator.css') }}" rel="stylesheet">
    <style>
        /* Стили для таблицы */
        .table thead th.th__set {
            font-size: 10px;
            text-align: center;
        }
        .tr__set {
            text-align: center;
        }
        .td-img__set {
            width : 24px;
            height: 24px;
            padding: 6px
        }
    </style>
@endsection

@section('nav-bar-container')

@endsection

@section('content')
    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">

            <div class="modal fade" id="modal-screen" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
                <div class="modal-dialog modal-lg" role="document" style="max-width: 1500px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modalLabelHeader" style="margin-top: 0px;">{{ __('str.mens_role') }}</h4>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close" onclick="onCloseModalScreen()">
                                <span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="/role-action">
                                @csrf
                                <input type="hidden" id="id" name="id" value="">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.roles_name') }}</span>
                                            </div>
                                            <input type="text" id="rname" name="rname" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 320px">{{ __('str.table_show_page') }}</span>
                                            </div>
                                            <select class="form-control" id="show_pages" name="show_pages" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.roles_admin_state') }}</span>
                                            </div>
                                            <select class="form-control" id="is_admin" name="is_admin" required>
                                                <option value="" selected disabled>{{ __('str.roles_select_status') }}</option>
                                                <option value="1">{{ __('str.et_yes') }}</option>
                                                <option value="0">{{ __('str.et_no') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.table_user_create') }}</span>
                                            </div>
                                            <select class="form-control" id="create_user" name="create_user" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 320px">{{ __('str.table_unit_type_create') }}</span>
                                            </div>
                                            <select class="form-control" id="create_unit_type" name="create_unit_type" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.roles_count_limit') }}</span>
                                            </div>
                                            <input type="number" min="1" id="lim_items" name="lim_items" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.table_create_change_status') }}</span>
                                        </div>
                                        <select class="form-control" id="change_status" name="change_status" required>
                                            <option value="0">{{ __('str.roles_disable') }}</option>
                                            <option value="1">{{ __('str.roles_enable') }}</option>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 320px">{{ __('str.table_create_change_blocks') }}</span>
                                        </div>
                                        <select class="form-control" id="change_box" name="change_box" required>
                                            <option value="0">{{ __('str.roles_disable') }}</option>
                                            <option value="1">{{ __('str.roles_enable') }}</option>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.roles_take_more_one_type_equ') }}</span>
                                            </div>
                                            <select class="form-control" id="more_one_type" name="more_one_type" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.table_create_change_objects') }}</span>
                                        </div>
                                        <select class="form-control" id="change_object" name="change_object" required>
                                            <option value="0">{{ __('str.roles_disable') }}</option>
                                            <option value="1">{{ __('str.roles_enable') }}</option>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 320px">{{ __('str.table_create_change_cells') }}</span>
                                            </div>
                                            <select class="form-control" id="change_cell" name="change_cell" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.roles_in_archive') }}</span>
                                            </div>
                                            <select class="form-control" id="is_arc" name="is_arc" required>
                                                <option value="" selected disabled>{{ __('str.roles_select_status') }}</option>
                                                <option value="0">{{ __('str.roles_not_archive') }}</option>
                                                <option value="1">{{ __('str.roles_is_archive') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.table_show_logs') }}</span>
                                            </div>
                                            <select class="form-control" id="show_logs" name="show_logs" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 320px">{{ __('str.table_link_user_unit') }}</span>
                                            </div>
                                            <select class="form-control" id="rigit_men" name="rigit_men" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.table_role_create') }}</span>
                                            </div>
                                            <select class="form-control" id="create_role" name="create_role" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.table_create_breaking_type') }}</span>
                                            </div>
                                            <select class="form-control" id="create_failure_types" name="create_failure_types" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 320px">{{ __('str.table_create_xlsx_reports') }}</span>
                                            </div>
                                            <select class="form-control" id="export_report" name="export_report" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.table_settings_access') }}</span>
                                            </div>
                                            <select class="form-control" id="access_settings" name="access_settings" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 320px">{{ __('str.table_submit_requests') }}</span>
                                            </div>
                                            <select class="form-control" id="create_ticket" name="create_ticket" required>
                                                <option value="0">{{ __('str.roles_disable') }}</option>
                                                <option value="1">{{ __('str.roles_enable') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 mt-4">
                                        <button type="submit" class="btn btn-success" style="width: 100%;">{{ __('str.roles_save') }}</button>
                                    </div>
                                </div>
                                <hr>
                                <br>
                                <div id="etype-table" style="margin-top: 16px"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="card__title-bg">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.roles_standart') }}</h4>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" style="font-size: 10px">
                        <thead>
                        <tr>
                            <th  class="th__set">{{ __('str.table_role') }}</th>
                            <th  class="th__set">{{ __('str.table_show_page') }}</th>
                            <th  class="th__set">{{ __('str.table_user_create') }}</th>
                            <th  class="th__set">{{ __('str.table_role_create') }}</th>
                            <th  class="th__set">{{ __('str.table_unit_type_create') }}</th>
                            <th  class="th__set">{{ __('str.table_create_change_status') }}</th>
                            <th  class="th__set">{{ __('str.table_create_change_blocks') }}</th>
                            <th  class="th__set">{{ __('str.table_create_change_objects') }}</th>
                            <th  class="th__set">{{ __('str.table_create_change_cells') }}</th>
                            <th  class="th__set">{{ __('str.table_show_logs') }}</th>
                            <th  class="th__set">{{ __('str.table_link_user_unit') }}</th>
                            <th  class="th__set">{{ __('str.table_create_breaking_type') }}</th>
                            <th  class="th__set">{{ __('str.table_create_xlsx_reports') }}</th>
                            <th  class="th__set">{{ __('str.table_settings_access') }}</th>
                            <th  class="th__set">{{ __('str.table_submit_requests') }}</th>
                            <th  class="th__set">{{ __('str.table_update_web') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="tr__set">
                            <td>{{ __('str.table_role_scladmen') }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="tr__set">
                            <td>{{ __('str.table_role_box_manager') }}</td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td></td>
                        </tr>
                        <tr class="tr__set">
                            <td>{{ __('str.table_role_itmen') }}</td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td></td>
                            <td></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                        </tr>
                        <tr class="tr__set">
                            <td>{{ __('str.table_role_admin') }}</td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                        </tr>
                        <tr class="tr__set">
                            <td>{{ __('str.table_role_secure') }}</td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td></td>
                            <td></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td></td>
                            <td><img class="td-img__set" src="/img/done_green_24dp.svg"/></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <br><br><br>
                <div id="b-container" class="row w-100 text-center">
                    <div class="col-md-6 text-start pl-1">
                        @if (Auth::user()->create_role())
                            <button class="btn btn-success" id="add-new" style="margin-right: 12px;" onclick="showModal()">+</button>
                        @endif
                        @if (Route::currentRouteName() == 'roles')
                            <button class="btn btn-success" onclick="window.location.href='/roles-arc'">{{ __('str.roles_show_archived') }}</button>
                        @else
                            <button class="btn btn-success" onclick="window.location.href='/roles'">{{ __('str.roles_show_current') }}</button>
                        @endif
                    </div>
                    <div class="col-md-6 text-start pt-3">
                        <h4 id="modalLabelHeader">{{ __('str.roles_users') }}</h4>
                    </div>
                </div>
                {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                    "data-page-length" => "100",
                    "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    {{$dataTable->scripts()}}

    <script src="{{ asset('/js/tabulator.min.js') }}"></script>

    <script>

        let etypesIds = "";
        let needReload = false;

        function onCloseModalScreen(){
            if (needReload){
                window.location.reload();
            }
        }

        function onChangeTypeAttached(element,idRole,idEtype){
            let isChecked = element.checked;
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            post['rid'    ] = idRole;
            post['state'  ] = isChecked;
            post['eid'    ] = idEtype;
            $.post( "/role-attache-type", post, function(data, status) {
                if (status === 'success'){
                    if ("true" === data){
                        needReload = true;
                        console.log("change ok.");
                    }else{
                        console.log(data);
                    }
                }
            });
        }

        function equIsAttached(cell){
            let v = etypesIds.indexOf('#' + cell.getData().id + '#') >= 0 ? 'checked' : '';
            return '<input type="checkbox" value="1" ' + v + ' onchange = "onChangeTypeAttached(this,' + $('#id').val() + ',' + cell.getData().id + ')"/>';
        }

        let etypeTable = new Tabulator("#etype-table", {
            virtualDomHoz:true,
            layout:"fitColumns",
            groupStartOpen:false,
            groupBy:function(data){
                return data.tname.substr(0,1).toUpperCase();
            },
            columns:[
                {title:"",  hozAlign:"center", field:"id", formatter: equIsAttached, editor:"input", editable:false, width:120},
                {title:"{{ __('str.roles_save') }}",  hozAlign:"center", field:"tname", editor:"input", editable:true, width:120},
                {title:"{{ __('str.roles_mark') }}", hozAlign:"center",field:"tmark",   editor:"input", editable:false, width:120},
                {title:"{{ __('str.roles_model') }}", hozAlign:"center",field:"tmodel",   editor:"input", editable:false},
            ],
        });

        function fillModal(data){
            if (data == null){
                $('#id').val("");
                $('#rname').val("");
                $('#is_admin').val(0);
                $('#is_arc').val(0);
                $('#lim_items').val(1);
                etypesIds = "";
                $('#etype-table').css("display", "none");
                $('#show_pages').val("0");
                $('#create_user').val("0");
                $('#create_role').val("0");
                $('#create_unit_type').val("0");
                $('#change_status').val("0");
                $('#change_object').val("0");
                $('#change_box').val("0");
                $('#change_cell').val("0");
                $('#show_logs').val("0");
                $('#rigit_men').val("0");
                $('#create_failure_types').val("0");
                $('#export_report').val("0");
                $('#access_settings').val("0");
                $('#create_ticket').val("0");
                $('#more_one_type').val("1");
            }else{
                $('#id').val(data.id);
                $('#rname').val(data.rname);
                $('#is_admin').val(data.is_admin);
                $('#is_arc').val(data.is_arc);
                $('#lim_items').val(data.lim_items);
                etypesIds = data.etypes;
                $('#etype-table').css("display", "block");
                etypeTable.setData(@json($etypes));
                $('#show_pages').val(data.show_pages);
                $('#create_user').val(data.create_user);
                $('#create_role').val(data.create_role);
                $('#create_unit_type').val(data.create_unit_type);
                $('#change_status').val(data.change_status);
                $('#change_object').val(data.change_object);
                $('#change_box').val(data.change_box);
                $('#change_cell').val(data.change_cell);
                $('#show_logs').val(data.show_logs);
                $('#rigit_men').val(data.rigit_men);
                $('#create_failure_types').val(data.create_failure_types);
                $('#export_report').val(data.export_report);
                $('#access_settings').val(data.access_settings);
                $('#create_ticket').val(data.create_ticket);
                $('#more_one_type').val(data.more_one_type);
            }
        }

        function showModal(){
            fillModal(null);
            needReload = false;
            $('#modal-screen').modal('show');
        }

        $(document).ready(function(){
            let enableAddNewRole = $('#add-new').length > 0;
            let enableEditRole = enableAddNewRole;
            //console.log("enableEditRole=" + enableEditRole);
            //--- обработка кликов по таблице --------------------------------------------------------------------------
            $('#roles-table').on('click', 'tbody td', function() {
                let tr  = $(this).closest('tr');
                let row = $('#roles-table').DataTable().row(tr);
                if (this.cellIndex == 0 && enableEditRole){
                    fillModal(row.data());
                    needReload = false;
                    $('#modal-screen').modal('show');
                }
            });
            $("#b-container").prependTo("#roles-table_filter");
            $("#roles-table_filter").css('width','100%');
            //----------------------------------------------------------------------------------------------------------
        });
    </script>
@endpush
