@extends('layouts.app')

@section('third_party_stylesheets')
    <style>
        .tabs { width: 100%; padding: 0px; margin: 0 auto; }
        .tabs>input { display: none; }
        .tabs>div {
            display: none;
            padding: 12px;
            border: 1px solid #5a5a5a;

        }
        .tabs>label {
            display: inline-block;
            padding: 7px;
            margin: 0 -5px -1px 0;
            text-align: center;
            color: #666666;
            border: 1px solid #C0C0C0;
            background: #E0E0E0;
            cursor: pointer;
        }
        .tabs>input:checked + label {
            color: #000000;
            border: 1px solid #C0C0C0;
            border-bottom: 1px solid #FFFFFF;
            background: #FFFFFF;
        }
        #tab_1:checked ~ #txt_1,
        #tab_2:checked ~ #txt_2 { display: block; }

        /* ----Выбор отображаемых колонок в таблице блока---- */
        /* ключевые кадры */
        @keyframes rotate_shesterenka {
            0% {
                transform: scale(1.1);
            }
            25% {
                transform: rotate(60deg);
            }
            50% {
                transform: scale(1.1);
            }
            75% {
                transform: rotate(0deg);
            }
            100% {
                transform: scale(1);
            }
        }
        .btn-set-visible {
            width: 22px;
            height: 22px;
            border-radius: 50%;
            border:none;
            background-color: rgba(255, 255, 255, 0);
            background-image: url('/img/new-des_setting-1.svg');
            background-repeat: no-repeat;
            background-position: center;
            padding: 0;
            cursor: pointer;
            transition: transform 0.2s linear;
        }
        .btn-set-visible:hover {
            /* transform: translateY(-3px); */
            animation-name: rotate_shesterenka;
            animation-duration: 1.5s;
            animation-timing-function: ease;
            animation-iteration-count: 1;
            animation-direction: normal;
            animation-play-state: running;
            animation-delay: 0s;
            animation-fill-mode: none;
        }
        .set-mb-pl {
            padding: 0 10px;
        }
    </style>
@endsection

@section('nav-bar-container')
    <div style="text-align:center">
        @if(auth()->user()->enableEditEqu())
            <button class="btn btn-success" id="add-new" style="margin: 2px 2px 2px 16px;" onclick="showCreateModal()">+</button>
        @endif
        <button class="btn btn-success" id="equipment-all" style="margin: 2px 2px 2px 16px;" onclick="window.location='equipment'">{{ __('str.equ_all') }}</button>
        <button class="btn btn-success" id="equipment-import" style="margin: 2px 2px 2px 16px;">{{ __('str.equ_import') }}</button>
        <button class="btn btn-success" id="equipment-archive" style="margin: 2px 2px 2px 16px;" onclick="window.location='equipment-archive'">{{ __('str.equ_archive') }}</button>
    </div>
@endsection

@section('content')

    <div class="modal fade" id="modal-users" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.equ_select_men2') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h5 id="headerToastInfo" style="height: 16px; text-align: right"></h5>
                    <table class="table table-bordered table-striped" id="men-table" data-page-length="12"></table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-screen" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader" style="margin-top: 0px;">{{ __('str.equ_add_unit') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_type') }}</span>
                        </div>
                        <select class="form-control" id="tid" name="tid" required>
                            <option value="" selected disabled>{{ __('str.equ_type_need') }}</option>
                            @foreach($etypes as $etype)
                                <option value="{{$etype->id}}">{{$etype->tname . " " . $etype->tmark . " " . $etype->tmodel . ($etype->is_comp ? " " . __('str.et_comp_txt') : "")}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_label') }}</span>
                        </div>
                        <input type="text" id="label" name="label" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_ser_num') }}</span>
                        </div>
                        <input type="text" id="ser_num" name="ser_num" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_inv_num') }}</span>
                        </div>
                        <input type="text" id="inv_num" name="inv_num" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_mac') }}</span>
                        </div>
                        <input type="text" id="mac" name="mac" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.object') }}</span>
                                </div>
                                <select class="form-control" id="mark_sid_new" name="mark_sid" required>
                                    <option value="0" selected>{{ __('str.not_select') }}</option>
                                    @foreach($stages as $stage)
                                        <option value="{{$stage->id}}">{{$stage->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.block') }}</span>
                                </div>
                                <select class="form-control" id="mark_bid_new" name="mark_bid" >
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- <br> -->
                    <button onclick="createEquAction()" class="btn btn-success" style="width: 200px">{{ __('str.equ_add_base') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-screen2" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tabs">
                        <input type="radio" name="inset" value="" id="tab_1" checked>
                        <label for="tab_1"><h4 class="modal-title" id="modalLabelHeader"><h4 class="modal-title" id="modalLabelHeader">{{ __('str.equ_edit') }}</h4></h4></label>

                        <input type="radio" name="inset" value="" id="tab_2">
                        <label for="tab_2"><h4 class="modal-title" id="modalLabelHeader2"><h4 class="modal-title" id="modalLabelHeader">{{ __('str.equ_soti') }}</h4></h4></label>

                        <div id="txt_1">
                            <input type="number" id="e_id" name="id" value="" style="visibility: hidden">
                            <input type="text" id="estat" value="" style="visibility: hidden">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.equ_type') }}</span>
                                        </div>
                                        <select class="form-control" id="e_tid" name="tid" required>
                                            <option value="" selected disabled>{{ __('str.equ_type_need') }}</option>
                                            @foreach($etypes as $etype)
                                                <option value="{{$etype->id}}">
                                                    {{$etype->tname . " " . $etype->tmark . " " . $etype->tmodel . ($etype->is_comp ? " " . __('str.et_comp_txt') : "")}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.table_state2') }}</span>
                                        </div>
                                        <select class="form-control" id="custom_estate" name="custom_estate">
                                        <!-- onchange="equ_state_onchange();" -->
                                            <option value="-1" disabled>{{ __('str.table_archive') }}</option>
                                            <option value="-2" disabled>{{ __('str.table_not_working') }}</option>
                                            <option value="-3" >{{ __('str.table_is_free') }}</option>
                                            <option value="-4" disabled>{{ __('str.table_on_box') }}</option>
                                            <option value="-5" disabled>{{ __('str.table_in_hand') }}</option>
                                            <option value="-7" >{{ __('str.table_in_renovation') }}</option>
                                            <option value="-8" disabled>{{ __('str.table_in_temp_return') }}</option>
                                            @foreach($equ_state as $state)
                                                <option value="{{$state->id}}">{{ $state->descr }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.equ_label') }}</span>
                                        </div>
                                        <input type="text" id="e_label" name="label" value="" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.equ_ser_num') }}</span>
                                        </div>
                                        <input type="text" id="e_ser_num" name="ser_num" value="" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.equ_inv_num') }}</span>
                                        </div>
                                        <input type="text" id="e_inv_num" name="inv_num" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.equ_mac') }}</span>
                                        </div>
                                        <input type="text" id="e_mac" name="mac" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.state') }}</span>
                                        </div>
                                        <select class="form-control" id="e_is_work" name="is_work">
                                            <option value="1" >{{ __('str.equ_state_working') }}</option>
                                            <option value="0" >{{ __('str.equ_state_not_working') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend" >

                                                <span class="input-group-text"
                                                      id="inputGroup-sizing-default"
                                                      style="width: 150px;"><div style="width: 126px;text-align: left">{{ __('str.equ_archiving') }}</div><img data-toggle="tooltip"
                                                                                                                                                               title="{{ __('str.hint_select_equ_archive_status') }}"
                                                                                                                                                               @if ($thema) src="/img/help_outline_white_24dp.svg" @else src="/img/help_outline_black_24dp.svg" @endif
                                                                                                                                                               style="width : 24px; height: 24px; padding: 4px;"/></span>



                                        </div>
                                        <select class="form-control" id="e_archive" name="archive" >
                                            <option value="1" >{{ __('str.equ_is_archived') }}</option>
                                            <option value="0" >{{ __('str.equ_not_archived') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.object') }}</span>
                                        </div>
                                        <select class="form-control" id="mark_sid" name="mark_sid" required>
                                            <option value="0" selected>{{ __('str.not_select') }}</option>
                                            @foreach($stages as $stage)
                                                <option value="{{$stage->id}}">{{$stage->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend" >
                                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.block') }}</span>
                                        </div>
                                        <select class="form-control" id="mark_bid" name="mark_bid" >
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" style="width: 150px; height: 80px">{{ __('str.equ_description') }}</span>
                                </div>
                                <textarea id="e_description" name="description" class="form-control" aria-label="With textarea"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button onclick="saveEquChangesAction()" class="btn btn-success" style="width: 100%">{{ __('str.equ_save') }}</button>
                                </div>
                                <div class="col-md-6">
                                    <button onclick="equEjectCurrent()" class="btn btn-success" style="width: 100%">{{ __('str.equ_eject') }}</button>
                                </div>
                            </div>
                            <!-- <br><br> -->

                        </div>
                        <div id="txt_2">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_mac') }}</span>
                                </div>
                                <input type="text" id="e_mac2" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                            </div>
                            <br>
                            <button onclick="rdpConnect()" class="btn btn-success" style="width: 200px">{{ __('str.equ_rdp_connect') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-screen3" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div id="report-screen-header">

                    </div>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_date_start') }}</span>
                        </div>
                        <input type="date" id="rep_date_start" name="rep_date_start" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_date_end') }}</span>
                        </div>
                        <input type="date" id="rep_date_end" name="rep_date_end" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.events_mens') }}</span>
                        </div>
                        <select class="form-control" id="rep_all_men" name="rep_all_men" onchange="onRepAllMenChange(this)">
                            <option value="0" selected >{{ __('str.equ_all') }}</option>
                            <option value="1" >{{ __('str.equ_select') }}</option>
                        </select>
                    </div>
                    <div class="row" id="selected-users-container" style="visibility: hidden">
                        <div class="col-md-10">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_selected_users') }}</span>
                                </div>
                                <textarea id="selected-users-list" name="selected-users-list" style="height: 80px" class="form-control" aria-label="With textarea" readonly></textarea>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info" id="rep-create" style="width: 100%;height: 80px" onclick='$("#modal-users2").modal("show");'>{{ __('str.equ_select_men') }}</button>
                        </div>
                    </div>
                    <br>
                    <button type="button" class="btn btn-success" id="rep-create" style="margin-right: 16px" onclick="repCreate()">{{ __('str.equ_create_report') }}</button>
                    <br>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-users2" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.equ_select_men2') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h5 id="headerToastInfo" style="height: 16px; text-align: right"></h5>
                    <table class="table table-bordered table-striped" id="men-table2" data-page-length="12"></table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-screen4" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    {{__('str.equ_import2')}}
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/equipment-import-action" method="POST" enctype="multipart/form-data" style="width: 100%">
                        @csrf
                        <input type="file" name="file" class="form-control" required="required" style="padding: 16px;height: 64px">
                        <br>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_name') }}</span>
                            </div>
                            <select class="form-control" name="tid" required>
                                <option value="" disabled selected>{{ __('str.equ_type_need') }}</option>
                                @foreach($etypes->all() as $etype)
                                    <option value="{{$etype->id}}">{{$etype->tname . " " . $etype->tmark . " " . $etype->tmodel}}</option>
                                @endforeach
                            </select>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-6">
                                <button class="btn btn-success" style="width: 100%;">{{ __('str.equ_excel_import') }}</button>
                            </div>
                            <div class="col-6">
                                <button class="btn btn-info" id="equipment-archive" style="width: 100%;" onclick="window.location='/doc/equ-import.xlsx'">{{ __('str.equ_load_file_example') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal alert data -->
    <div class="modal fade" id="modal__data" tabindex="-1" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content" style="border-radius:20px;overflow: hidden;">
                <div class="modal-header" style="justify-content:center;border-top-right-radius:20px;border-top-left-radius:20px;">
                    <h4 class="modal-title" style="font-weight:500;">{{ __('str.content_modal_header') }}</h4>
                </div>
                <div class="modal-body" style="font-size:20px;margin:0;text-align:center;font-weight:500;">
                </div>
                <div class="modal-footer" style="justify-content:center;">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal" style="border-radius:8px;">
                        <h4 style="font-size:20px;margin:0">{{ __('str.content_modal_footer') }}</h4>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">
            <div class="card-body">
                <div class="modal-header" style="z-index:3200;">
                    <h4 class="modal-title text-nowrap" style="margin-top: 0px;">{{ $table_header }}</h4>
                    <div class="btn-group dropstart align-self-end">
                        <button class="btn-set-visible dropstart" type="button" data-bs-toggle="dropdown" aria-expanded="false"></button>
                        <ul class="dropdown-menu" id="grpChkBoxEquipment">
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxMetka" type="checkbox"> {{ __('str.equ_label') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxSN" type="checkbox"> {{ __('str.equ_ser_num') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxInv" type="checkbox"> {{ __('str.rep_xls_header_inv_num') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxMac" type="checkbox"> {{ __('str.equ_mac') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxArchive" type="checkbox"> {{ __('str.equ_archiving') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxBlock" type="checkbox"> {{ __('str.block') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxObject" type="checkbox"> {{ __('str.object') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxDescription" type="checkbox"> {{ __('str.equ_description') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxType" type="checkbox"> {{ __('str.equ_type') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxModel" type="checkbox"> {{ __('str.et_model') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxStatus" type="checkbox"> {{ __('str.equ_states') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxLocation" type="checkbox"> {{ __('str.table_place') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxOwner" type="checkbox"> {{ __('str.table_owner') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxConnected" type="checkbox"> {{ __('str.table_linkin') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxTypeBreakdown" type="checkbox"> {{ __('str.table_failure_type') }}</label></li>
                        </ul>
                    </div>
                </div>
                {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                    "data-page-length" => "100",
                    "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}
            </div>

        </div>
    </div>

@endsection

<style>

</style>

@push('scripts')

    {{$dataTable->scripts()}}

    <script src="{{ asset('/js/tableSelectors.js') }}"></script>
    <script src="/js/page-settings-io.js"></script>

    <script type="text/javascript">

        let mens               =  null;
        let blocks             =  null;
        let repMenSelectedId   =  null;
        let intervalId         =  null;
        let equ                =  null;
        let rigitTableIsInit   = false;
        let menTableIsInit     = false;
        let menTable2IsInit    = false;
        let selectedMens2      =    [];
        let selectedMens2Names =    [];

        let page   = 'equipment';
        let userId = parseInt("{{Auth::user()->id}}");

        let isNotWorkingEquUrl = "{{  Request::url() }}".lastIndexOf("equipment-not-working") >= 0;
        let isArchiveEquUrl = "{{  Request::url() }}".lastIndexOf("equipment-archive") >= 0;
        let isAllEquUrl = !isNotWorkingEquUrl && !isArchiveEquUrl;
        let enableEditEqu = "1" === "{{ auth()->user()->enableEditEqu() }}";

        function repCreate(){
            let ds = $("#rep_date_start").val();
            if (ds.length === 0){
                ds = "0";
            }
            let de = $("#rep_date_end").val();
            if (de.length === 0){
                de = "0";
            }
            let isAllMen = parseInt($("#rep_all_men").val()) === 0;
            let isAllEqu = false;
            let str = "";
            if (!isAllMen){
                for (let i = 0; i < selectedMens2.length; i++){
                    str += str.length > 0 ? "," + selectedMens2[i] : selectedMens2[i];
                }
            }
            if (str.length === 0 && !isAllMen){
                alert("{{ __('str.equ_need_select_men') }}");
                return;
            }
            let equ_id = equ.id;
            if (("" + equ_id).length === 0 && !isAllEqu){
                alert("{{ __('str.equ_need_select_equ') }}");
                return;
            }
            if (str.length === 0){
                str = "0";
            }
            if (isAllEqu || ("" + equ_id).length === 0){
                equ_id = "0";
            }
            window.location = "rep/" + ds + "/" + de + "/" + str + "/" + equ_id;
        }

        function onEquClick(data){
            equ = JSON.parse(decodeURIComponent(data));
            menDataUpdate(equ.id)
            $('#modal-users').modal('show');
        }
/*
        function show_comp_items(data){
            let equ = JSON.parse(decodeURIComponent(data));
            let _id = "comp-scr-" + equ['id'];
            let  id = "#" + _id;
            let url = 'equ-kit-items/' + equ['id'];
            $.get(url, function(_data, status) {
                if (status === 'success'){
                    let func = null;
                    func = function(e){
                        if (!document.getElementById(_id).contains(e.target)){
                            window.removeEventListener('click',func);
                            $(id).css('display','none');
                        }
                    }
                    window.addEventListener('click',func);
                    $(id).empty();
                    let data = JSON.parse(_data);
                    let str =
                        '<table style="width : 100%;">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>{{ __('str.equipment2') }}</th>' +
                        '<th>{{ __('str.table_label') }}</th>' +
                        '<th>{{ __('str.table_ser') }}</th>' +
                        '<th>{{ __('str.table_inv_num') }}</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>';
                    for (let i = 0; i < data.length; i++){
                        let strRow = "<tr>";
                        strRow += "<td>" + data[i]['tname'] + " " + data[i]['tmark'] + " " + data[i]['tmodel']   + "</td>";
                        strRow += "<td>" + data[i]['label']   + "</td>";
                        strRow += "<td>" + data[i]['ser_num'] + "</td>";
                        strRow += "<td>" + data[i]['inv_num'] + "</td>";
                        strRow += "<tr>";
                        str += strRow;
                    }
                    str += '</tbody></table>';
                    $(id).append(str);
                    $(id).css('display','block');
                }
            });
        }
*/
        function on_take_apart_click(data){
            let equ = JSON.parse(decodeURIComponent(data));
            let equIsFree = equ['estat'] === -3;
            if (!equIsFree){
                alert("{{ __('str.equ_take_apart_impossible') }}");
                return;
            }
            let url = 'equ-take-apart/' + equ['id'];
            $.get( url, function(_data, status) {
                if (status === 'success'){
                    if ('true' === _data){
                        $('#equ-table').DataTable().ajax.reload();
                        alert("{{ __('str.equ_take_apart_complete') }}");
                    }else{
                        console.log(_data);
                        alert(_data);
                    }
                }
            });
        }

        function menDataUpdate(eid){
            if (menTableIsInit){
                $('#men-table').DataTable().ajax.url('/men-data/' + eid).load();
            }else{
                menTableIsInit = true;
                $('#men-table').DataTable({
                    processing: true,
                    serverSide: true,
                    dom: "frtip",
                    ajax: '/men-data/' + eid,
                    columns: [
                        { data: 'is_checked',   title:'{{ __('str.table_linkin2') }}', className: 'text-center', searchable : false,  },
                        { data: 'post', name: 'men.post', title:'{{ __('str.table_post') }}' },
                        { data: 'msurname', name: 'men.msurname', title:'{{ __('str.table_surname') }}' },
                        { data: 'mname', name: 'men.mname', title:'{{ __('str.table_name2') }}' },
                        { data: 'mpatronymic', name: 'men.mpatronymic', title:'{{ __('str.mens_patronymic') }}'  },
                    ]
                }).on('click', 'tbody td', function() {
                    let tr  = $(this).closest('tr');
                    let row = $('#men-table').DataTable().row(tr);
                    onMenClick(row.data());
                }).on('dblclick', 'tbody td', function() {
                });
            }
        }

        function onMenTable2Click(data){
            if (selectedMens2.includes(data.id)){
                selectedMens2.splice(selectedMens2.indexOf(data.id), 1);
                selectedMens2Names.splice(selectedMens2Names.indexOf(data.id), 1);
            }else{
                selectedMens2.push(data.id);
                selectedMens2Names[data.id] = data.msurname + " " + data.mname;
            }
            menDataUpdate2();
        }

        function menDataUpdate2(){
            let str2 = "";
            selectedMens2Names.forEach(function callback(value, index) {
                str2 += str2.length > 0 ? ", " + value : value;
            });
            $('#selected-users-list').text(str2);
            let str = ",";
            for (let i = 0; i < selectedMens2.length; i++){
                str += selectedMens2[i] + ",";
            }
            if (menTable2IsInit){
                $('#men-table2').DataTable().ajax.url('/men-select/' + str).load();
            }else{
                menTable2IsInit = true;
                $('#men-table2').DataTable({
                    processing: true,
                    serverSide: true,
                    stateSave: true,
                    dom: "frtip",
                    ajax: '/men-select/' + str,
                    columns: [
                        { data: 'is_checked',   title:'{{ __('str.table_linkin2') }}', className: 'text-center',searchable: false,  },
                        { data: 'post', name: 'men.post', title:'{{ __('str.table_post') }}' },
                        { data: 'msurname', name: 'men.msurname', title:'{{ __('str.table_surname') }}' },
                        { data: 'mname', name: 'men.mname', title:'{{ __('str.table_name2') }}' },
                        { data: 'mpatronymic', name: 'men.mpatronymic', title:'{{ __('str.mens_patronymic') }}'  },
                    ]
                }).on('click', 'tbody td', function() {
                    let tr  = $(this).closest('tr');
                    let row = $('#men-table2').DataTable().row(tr);
                    onMenTable2Click(row.data());
                }).on('dblclick', 'tbody td', function() {
                });
            }
        }

        function onMenClick(data){
            if (equ != null){
                let isChecked = data.is_checked.length > 0;
                let url = isChecked ? "/me-rigid-remove2/" + equ.id + "/" + data.id :
                    "/me-rigid-add2/" + equ.id + "/" + data.id;
                $.get( url, function(_data, status) {
                    if (status === 'success'){
                        if ('true' === _data){
                            $('#equ-table').DataTable().ajax.reload();
                            $('#men-table').DataTable().ajax.reload();
                            let toast = isChecked ? "{{ __('str.rigit_links_men_remove_ok') }}" : "{{ __('str.rigit_links_men_add_ok') }}";
                            toast = data.mname + " " + data.msurname + " " + toast;
                            showToast(toast);
                        }else{
                            console.log(_data);
                            alert(_data);
                        }
                    }
                });
            }
        }

        function showToast(txt){
            if (intervalId != null){
                clearInterval(intervalId);
                intervalId = null;
            }
            $("#headerToastInfo").text(txt);
            intervalId = setTimeout(function (){
                $("#headerToastInfo").text("");
                intervalId = null;
            },3000);
        }

        function on_edit_click(data){
            equ = JSON.parse(decodeURIComponent(data));
            fillModalScreen2(equ);
            $('#modal-screen2').modal('show');
        }
/*
        function equ_state_onchange(){
            let id = $('#custom_estate').val();
            let msg = null;
            if (id == ""){
                msg = "{{ __('str.equ_set_status_reset_message') }}";
                id = -3;
            }else{
                let txt = $( "#custom_estate option:selected" ).text();
                msg = "{{ __('str.equ_set_status_message') }}" + " " + txt + "?";
            }
            if (confirm(msg)){
                let equ_id = $("#e_id").val();
                if (("" + equ_id).length > 0){
                    $.get( '/equ-update-estat/' + equ_id + '/' + id, function(data, status) {
                        if (status === 'success'){
                            if ('true' === data){
                                window.location.reload();
                            }else{
                                alert(data);
                                console.log(data);
                            }
                        }
                    });
                }
            }
        }
*/
        function equEjectCurrent(){
            let equ_id = $("#e_id").val();
            if (("" + equ_id).length > 0 && confirm("{{ __('str.equ_eject_message') }}")){
                equEject(equ_id);
            }
        }

        function equEject(id){
            $.get( "/equ-eject/" + id, function(data, status) {
                if (status === 'success'){
                    if ('true' === data){
                        window.location.reload();
                    }else{
                        alert(data);
                        console.log(data);
                    }
                }
            });
        }

        function onInitComplete(dataTable) {
            let equEstateDesQuerySelectorParams = isArchiveEquUrl ? "archive = 1" : "archive = 0";
            createTableSelectors(dataTable,["etypes.tname",
                    "failure_types.name", "equipment.estat_des", "stages.name","blocks2.name"],
                {"stages.name" : "is_arc = false","blocks2.name" : "is_arc = false",
                    "etypes.tname" : "hide = false,is_cons = false", "equipment.estat_des" : equEstateDesQuerySelectorParams},
                null,null,{"blocks2" : "blocks"});
        }

        // функция вызывается в момент готовности строки таблицы
        function onRowCreated(row){
            if (row.childNodes[0].querySelector('.compscr') != null){
                const kitTable = row.childNodes[0].querySelector('.compscr');
                let isSetHeight = false;
                new ResizeObserver(function (){
                    if (!isSetHeight){
                        isSetHeight = true;
                        let offsetMargin = -10;
                        let rootH = row.getBoundingClientRect().height;
                        row.style.height = rootH + kitTable.getBoundingClientRect().height + offsetMargin;
                    }
                    kitTable.style.width = row.getBoundingClientRect().width;
                }).observe(row);
            }
        }

        function fillSearchMenSelector(){
            setTimeout(function() {
                let str = ("" + $('#rep_man_name_search').val()).trim().toLowerCase();
                $('#rep_all_men_filtered').find('option').remove().end();
                for (let i = 0; i < mens.length; i++){
                    if (str.length === 0 || mens[i].msurname.toLowerCase().includes(str) ||
                        mens[i].mname.toLowerCase().includes(str) ||
                        mens[i].mpatronymic.toLowerCase().includes(str)){
                        $('#rep_all_men_filtered').append('<option value="' + mens[i].id + '">' + ' (id=' + mens[i].id + ') ' +
                            mens[i].msurname + ' ' + mens[i].mname+ ' ' + mens[i].mpatronymic + '</option>');
                    }
                }
            },300);
        }

        function onRepAllMenChange(selector){
            if (selector.selectedIndex === 0) {
                $("#modal-users2").modal("hide");
                $("#selected-users-container").css('visibility','hidden');
            }else{
                menDataUpdate2();
                $("#modal-users2").modal("show");
                $("#selected-users-container").css('visibility','visible');
            }
        }

        function onReportClick(data){
            equ = JSON.parse(decodeURIComponent(data));
            $("#report-screen-header").text("{{__('str.table_report2')}}" + ": " + equ.tname + " " +
                equ.tmodel + "   n: " + equ.id + "   s/n: " + equ.ser_num + "   inn: " + equ.inv_num);
            $("#rep_all_men").val(0);
            $("#rep_equ_container").show();
            $("#modal-users2").modal("hide");
            $("#rep_man_name_search").val("");
            $("#selected-users-container").css('visibility','hidden');
            fillSearchMenSelector();
            $('#modal-screen3').modal('show');
        }

        function rdpConnect(){
            let mac = $('#e_mac2').val();
            if (mac.length == 0){
                alert("{{ __('str.equ_mac_is_empty') }}");
                return;
            }
            let post ={};
            post['_token'     ] = "{{csrf_token()}}";
            post['mac'        ] = mac;
            $.post( "/equ-find-rdp-link", post, function(data, status) {
                if (status === 'success'){
                    if (data.startsWith("https://")){
                        window.open(data, '_blank').focus();
                    }else{
                        alert("{{ __('str.equ_rdp_connect_error') }}" + ": " + data);
                    }
                }
            });
        }

        function saveEquChangesAction(){
            let post ={};
            post['_token'     ] = "{{csrf_token()}}";
            post['label'      ] = $('#e_label').val();
            post['tid'        ] = $('#e_tid').val();
            post['ser_num'    ] = $('#e_ser_num').val();
            post['description'] = $('#e_description').val();
            post['is_work'    ] = $('#e_is_work').val();
            post['inv_num'    ] = $('#e_inv_num').val();
            post['archive'    ] = $('#e_archive').val();
            post['mac'        ] = $('#e_mac').val();
            post['id'         ] = $('#e_id').val();
            post['mark_sid'   ] = $('#mark_sid').val();
            post['mark_bid'   ] = $('#mark_bid').val();
            post['estat'      ] = $('#custom_estate').val();
            $.post( "/equ-edit-web-action", post, function(data, status) {
                //console.log(data);
                if (status === 'success'){
                    if ("true" === data){
                        $('#modal-screen2').modal('hide');
                        $('#equ-table').DataTable().ajax.reload();
                    }else{
                        alert(data);
                    }
                }
            });
        }

        function showCreateModal(){
            equ = null;
            $('#mark_sid_new').val(0);
            $('#tid').val("");
            $('#label').val("");
            $('#ser_num').val("");
            $('#inv_num').val("");
            $('#mac').val("");
            initBidSelectorNew()
            $('#modal-screen').modal('show');
        }

        function fillModalScreen2(data){
            $('#e_id').val(data.id);
            $('#e_label').val(data.label);
            $('#e_description').val(data.description);
            $('#e_is_work').val(data.is_work);
            $('#e_archive').val(data.archive);
            $('#e_ser_num').val(data.ser_num);
            $('#e_inv_num').val(data.inv_num);
            $('#e_mac').val(data.mac);
            $('#e_mac2').val(data.mac);
            $('#e_tid').val(data.tid);
            $('#estat').val(data.estat);
            let estat = parseInt(data.estat);
            //console.log("estat=" + estat);
            let enable_custom_state_selector = estat >= 0 || estat == -3 || estat == -7;
            if (estat === -6){
                $('#custom_estate').val(0);
            }else{
                $('#custom_estate').val(estat);
            }
            $('#custom_estate').prop('disabled', !enable_custom_state_selector);
            let equIsFree = estat == -3 || estat == -1;
            $('#e_archive').prop('disabled', !equIsFree);
            $('#mark_sid').val(data.mark_sid);
            initBidSelector();
        }

        function createEquAction(){
            let tid = $('#tid').val();
            if (tid == null){
                alert("{{ __('str.equ_need_type') }}");
                return;
            }
            let post = {};
            post['_token' ] = "{{csrf_token()}}";
            post['tid'    ] = tid;
            post['label'  ] = $('#label').val();
            post['inv_num'] = $('#inv_num').val();
            post['ser_num'] = $('#ser_num').val();
            post['mac'    ] = $('#mac').val();
            post['mark_sid'] = $('#mark_sid_new').val();
            post['mark_bid'] = $('#mark_bid_new').val();
            $.post( "/equ-create-web-action", post, function(data, status) {
                if (status === 'success'){
                    if ("true" === data){
                        $('#modal-screen').modal('hide');
                        $('#equ-table').DataTable().ajax.reload();
                        alert('Устройство успешно создано');
                    }else{
                        const elemModal = document.querySelector('#modal__data');
                        const modal = new bootstrap.Modal(elemModal, {backdrop: 'static', keyboard: false, focus: true});
                        let html = data;
                        elemModal.querySelector('.modal-body').innerHTML = html;
                        modal.show();
                    }
                }
            });
        }

        function initBidSelector(){
            initBidSelectorId('mark_sid','mark_bid');
        }

        function initBidSelectorNew(){
            initBidSelectorId('mark_sid_new','mark_bid_new');
        }

        function initBidSelectorId(id_sid,id_bid){
            let v = $('#' + id_bid).find('option').remove().end();
            v.append('<option value="0">' + "{{__('str.not_select')}}" + '</option>');
            let sid = parseInt($('#' + id_sid).val());
            if (sid == 0){
                v.val(0);
            }else{
                for (let i = 0; i < blocks.length; i++){
                    if (blocks[i].st_id == sid){
                        let selectedTxt = equ != null && blocks[i].id == equ.mark_bid ? " selected" : "";
                        v.append('<option value="' + blocks[i].id + '"' + selectedTxt + '>' + blocks[i].name + '</option>');
                    }
                }
            }
        }

        $(document).ready(function(){

            let button = document.getElementById("equipment-import");
            if (isAllEquUrl){
                $("#equipment-all").css('display','none');
                button.onclick = function (){
                    $('#modal-screen4').modal('show');
                }
            }else{
                button.style.display = 'none';
                if (enableEditEqu) {
                    $("#add-new").css('display','none');
                }
                $("#equipment-archive").css('display','none');
                $("#equipment-show-not-working").css('display','none');
            }
            //--- обработка таблицы ------------------------------------------------------------------------------------
            $('#equ-table').css('min-height','220px');
            //----------------------------------------------------------------------------------------------------------
            mens = @json($mens);
            blocks = @json($blocks);
            fillSearchMenSelector();

            $( "#mark_sid" ).change(initBidSelector);
            $( "#mark_sid_new" ).change(initBidSelectorNew);

            //--------------------------Выбор отображаемых колонок в таблице блока--------------------------------------
            loadAnyPageData(page,userId,function (pageSettings, bSaveSetiingsUser){
                let checkBoxNames = ['chkBoxInv','chkBoxSN','chkBoxMac','chkBoxBlock','chkBoxObject',
                    'chkBoxArchive','chkBoxType','chkBoxModel','chkBoxStatus','chkBoxMetka',
                    'chkBoxLocation','chkBoxOwner','chkBoxConnected','chkBoxTypeBreakdown','chkBoxDescription'];
                let columnNums    = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
                let columnDefault = [true,true,false,false,false,false,true,true,true,true,true,true,true,true,false];
                let dt = $('#equ-table').DataTable();

                //--- некоторые колонки по умолчанию при первой загрузке у нового пользователя не выбираются -----------
                if (bSaveSetiingsUser){
                    for (let i = 0; i < columnNums.length; i++){
                        let isChecked = columnDefault[i];
                        $('#' + checkBoxNames[i]).prop('checked', isChecked);
                        dt.column(columnNums[i]).visible(isChecked);
                        $('#' + checkBoxNames[i]).change(function() {
                            dt.column(columnNums[i]).visible(this.checked);
                            saveUserSettings(page,userId,checkBoxNames[i],this.checked,"{{csrf_token()}}");
                        });
                    }
                }else{
                    for (let i = 0; i < checkBoxNames.length; i++){
                        let isChecked = 'false' !== pageSettings[checkBoxNames[i]];
                        $('#' + checkBoxNames[i]).prop('checked', isChecked);
                        dt.column(columnNums[i]).visible(isChecked);
                        $('#' + checkBoxNames[i]).change(function() {
                            dt.column(columnNums[i]).visible(this.checked);
                            saveUserSettings(page,userId,checkBoxNames[i],this.checked,"{{csrf_token()}}");
                        });
                    }
                }
            });
            //----------------------------------------------------------------------------------------------------------
        });
    </script>
@endpush
