@extends('layouts.app')

@section('third_party_stylesheets')
    <link href="{{ asset('/dist/css/tabulator.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                <p id="header"></p>
                <div id="equ-table" style="margin-top: 16px"></div>
                <br>
                <p>{{ __('str.equ_tsd_report') }}</p>
                <div id="cur-equ-table" style="margin-top: 16px"></div>
                <br>
                <button class="btn btn-success" onclick="window.location='/rep-xls/{{$urlstr}}'" style="margin: 2px 2px 2px 2px;">Excel Export</button>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script src="{{ asset('/dist/js/tabulator.min.js') }}"></script>

    <script>

        let curEquData = @json($equ);

        function xlsxexport(){
            alert("{{$urlstr}}");
        }

        function menFormatter(cell){
            return cell.getRow().getData().msurname + " " + cell.getRow().getData().mname;
        }

        function typeFormatter(cell){
            return cell.getRow().getData().tname + " " + cell.getRow().getData().tmark + " " + cell.getRow().getData().tmodel;
        }

        function takeFormatter(cell){
            return timeConvert(cell.getRow().getData().created_at);
        }

        function returnFormatter(cell){
            return timeConvert(cell.getRow().getData().ret);
        }

        function numFormatter(cell){
            return cell.getRow().getPosition(true) + 1;
        }

        function placeFormatter(cell){
            return placeFormatter2(cell.getRow().getData());
        }

        function placeFormatter2(data){
            if (data.uid > 0){
                return data.msurname + " " + data.mname;
            }
            if (data.cid > 0) {
                return "{{ __('str.block') }}" + " " + data.bname + ", " + "{{ __('str.cell') }}" + " " + data.block_pos;
            }
            return "{{ __('str.equ_no_place') }}";
        }

        function placeFormatter3(cell){
            let id = cell.getRow().getData().eid;
            let str = "";
            for (let i = 0; i < curEquData.length; i++){
                if (curEquData[i].id == id){
                    str = placeFormatter2(curEquData[i]);
                    break;
                }
            }
            return str;
        }

        function timeConvert(t){
            if (typeof t === 'string'){
                let strArr = t.replace(' ','T').split(".")[0].split("T");
                let parts = strArr[0].split("-");
                let parts2 = strArr[1].split(":");
                return parts[2] + "." + parts[1] + "." + parts[0] + " " + parts2[0] + ":" + parts2[1];
            }else{
                return "";
            }
        }

        $(document).ready(function(){
            let equTable = new Tabulator("#equ-table", {
                virtualDomHoz:true,
                layout:"fitColumns",
                groupStartOpen:false,
                columns:[
                    {title:"№",  hozAlign:"center", editor:"input", editable:false, width:50, formatter:numFormatter},
                    {title:"{{ __('str.equ_label') }}",  hozAlign:"center", editor:"input", editable:false, width:100, field:"label"},
                    {title:"{{ __('str.table_inv_num') }}",  hozAlign:"center", editor:"input", editable:false, width:100, field:"inv_num"},
                    {title:"{{ __('str.equ_equ') }}",  hozAlign:"center", field:"tname", editor:"input", editable:true, width:300, formatter:typeFormatter},
                    {title:"{{ __('str.equipment_take_time') }}",  hozAlign:"center", field:"created_at",  editor:"input", editable:false, width:150, formatter:takeFormatter},
                    {title:"{{ __('str.equipment_ret_time') }}",  hozAlign:"center", field:"ret",  editor:"input", editable:false, width:150, formatter:returnFormatter},
                    {title:"{{ __('str.mens_worker') }}",  field:"msurname",  editable:false, formatter:menFormatter},
                    {title:"{{ __('str.equ_place_header') }}",  field:"msurname",  editable:false, formatter:placeFormatter3},
                ],
            });
            let data = @json($items);
            //console.log(JSON.stringify(data));
            let t1 = timeConvert(data[0].created_at);
            let t2 = timeConvert(data[data.length - 1].ret);
            if (t2.length == 0){
                t2 = timeConvert(data[data.length - 1].created_at);
            }
            equTable.setData(data);
            var _header = "{{ __('str.equ_period') }}" + " " + t1 + " - " + t2;
            $("#header").text(_header);
            let curEquTable = new Tabulator("#cur-equ-table", {
                virtualDomHoz:true,
                layout:"fitColumns",
                groupStartOpen:false,
                columns:[
                    {title:"№",  hozAlign:"center", editor:"input", editable:false, width:50, formatter:numFormatter},
                    {title:"{{ __('str.equ_label') }}",  hozAlign:"center", editor:"input", editable:false, width:100, field:"label"},
                    {title:"{{ __('str.table_inv_num') }}",  hozAlign:"center", editor:"input", editable:false, width:100, field:"inv_num"},
                    {title:"{{ __('str.equ_equ') }}",  hozAlign:"center", field:"tname", editor:"input", editable:true, width:300, formatter:typeFormatter},
                    {title:"{{ __('str.equ_place_header') }}",  field:"msurname",  editable:false, formatter:placeFormatter},
                ],
            });
            curEquTable.setData(curEquData);
        });
    </script>

@endpush

