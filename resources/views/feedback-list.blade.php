@extends('layouts.app')

@section('nav-bar-container')
    <button class="btn btn-success" id="add-new" onclick="$('#modal-feedback').modal('show')">+</button>
@endsection

@section('content')

    <div class="modal fade" id="modal-feedback" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 1200px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.feedback_header') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form method="post" id="form-feedback" action="http://90.156.203.30/api/add-feedback">
                        <input type="text" name="oname" value="{{ $org_name }}" style="visibility: hidden">
                        <input type="text" name="uname" value="{{ \Illuminate\Support\Facades\Auth::user()->name }}" style="visibility: hidden">
                        <input type="text" name="email" value="{{ \Illuminate\Support\Facades\Auth::user()->email }}" style="visibility: hidden">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.feedback_name_fb') }}</span>
                            </div>
                            <input type="text" id="namefb" name="name" placeholder="{{ __('str.feedback_name_fb') }}"  class="form-control" >
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.feedback_type') }}</span>
                            </div>
                            <select class="form-control" id="fe_type" name="fe_type" required>
                                <option value="1">{{ __('str.feedback_type_error') }}</option>
                                <option value="2">{{ __('str.feedback_type_revision') }}</option>
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" style="width: 200px; height: 200px">{{ __('str.description') }}:</span>
                            </div>
                            <textarea id="bodyfb" name="body" class="form-control" aria-label="With textarea"></textarea>
                        </div>
                        <br>
                        <input type="file" name="file[]" id="filefb" class="form-control"  style="padding: 16px;height: 64px">
                        <br>
                        <button type="button" onclick="sendFeedbackAction()" class="btn btn-primary" style="width: 200px">{{ __('str.settings_save') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-screen2" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 1300px;">
            <div class="modal-content" >
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader2" style="color: #9d9d9d;">{{ __('str.header_feedback_reports') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-5">
                            <br><br>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.table_feedback_type') }}</span>
                                </div>
                                <input type="text" id="fe_type_des" class="form-control" readonly disabled>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.table_author') }}</span>
                                </div>
                                <input type="text" id="author_des" class="form-control" readonly disabled>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.table_feedback_created') }}</span>
                                </div>
                                <input type="text" id="created_sec_des" class="form-control" readonly disabled>
                            </div>
                            <div class="input-group mb-3" style="margin-top: 10px">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.table_name') }}</span>
                                </div>
                                <input type="text" id="name_des" class="form-control" readonly disabled>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" style="width: 200px">{{ __('str.description') }}:</span>
                                </div>
                                <textarea id="des_des" class="form-control" aria-label="With textarea"  onkeyup="onChangeDesDes(this.value)"></textarea>
                            </div>
                            <button type="button" id="bUpdateFeedbackDes" onclick="updateFeedbackDesArea()" style="width: 100%" class="btn btn-primary">{{ __('str.feedback_save_feedback_des') }}</button>
                        </div>
                        <div class="col-lg-7">
                            <table class="table table-bordered table-striped" id="fb-reps-table" data-page-length="6" style="width: 100%"></table>
                            <input type="hidden" id="fid" name="fid" value="">
                            <input type="text" id="uname" name="uname" value="{{ \Illuminate\Support\Facades\Auth::user()->name }}" style="visibility: hidden">
                            <input type="text" id="email" name="email" value="{{ \Illuminate\Support\Facades\Auth::user()->email }}" style="visibility: hidden">
                        </div>
                    </div>
                    <hr><br>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" style="width: 200px">{{ __('str.table_text_body') }}:</span>
                                </div>
                                <input type="text" class="form-control"  id="body" name="body" aria-label="Default" aria-describedby="inputGroup-sizing-default" required >
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="input-group mb-3" style="margin-top: 8px">
                                <input type="file" name="file[]" id="file">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <button type="button" onclick="sendReportAction()" style="width: 100%;margin-top: 8px" class="btn btn-primary">{{ __('str.table_feedback_send') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">
            <div class="card-body">
                <table class="table table-striped">
                    <thead class="text-center">
                    <tr>
                        <th>{{ __('str.table_color') }}</th>
                        <th>{{ __('str.table_descr') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($color_scheme as $txt => $color)
                        <tr>
                            <td style="width: 150px">
                                <div style="
                                    overflow:hidden;
                                    width:100%;
                                    height:32px;
                                    border-radius:16px;"
                                >
                                    <input
                                        type="color" value="{{ substr($color,0,7) }}"
                                        style="
                                            padding:0;
                                            margin:-5px;
                                            width:150%;
                                            height:150%;"
                                        disabled
                                    >
                                </div>
                            </td>
                            <td>{{ $txt }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <br>
                <table class="table table-bordered table-striped" id="feedbacks-table" data-page-length="50"></table>
                <br>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script>

        let repTableIsInit = false;
        let currentFid = 0;
        let org_name = "{{$org_name}}".replaceAll("&amp;","\"").
            replaceAll("&quot;","\"").
            replaceAll("&#039;&#039;","\"").
            replaceAll("&#039;","\"");
        let currendRepDes = '';

        function updateFeedbackDesArea(){
            //alert($("#des_des").val());
            let des = $("#des_des").val();
            let post ={};
            post['id' ] = currentFid;
            post['des'] = des;
            $.post( "http://90.156.203.30/api/upd-fb-des", post, function(data, status) {
                if (status === 'success'){
                    if ("true" === data){
                        $('#feedbacks-table').DataTable().ajax.reload(null, false);
                        let str = 'Пользователь ' + $("#uname").val() + ' отредактировал обращение с "' +
                            currendRepDes + '" на "' + des + '"';
                        sendFeedBackMessage(str);
                        currendRepDes = des;
                    }else{
                        console.log(data);
                    }
                }
            });
        }

        function onChangeDesDes(str){
            $("#bUpdateFeedbackDes").css('visibility', 'visible');
        }

        function createTableSelectors(dataTable,names,params = null,
                                      onSelectorCreated = null,onSelectorChange = null){
            let tableName = dataTable[0].id;
            let cols = $('#' + tableName).DataTable().settings().init().columns;
            let selectorArray = {};
            dataTable.api().columns().every(function () {
                let name = cols[this[0]].name
                let parts = name.split(".");
                if (parts.length !== 2 || (!names.includes(name))){
                    //console.log('name=' + name);
                    return
                }
                let title = cols[this[0]].title;
                let column = this;
                let url = "http://90.156.203.30/api/any-cols/" + parts[0] + '/' + parts[1];
                if (params != null){
                    url += "/" + encodeURI(params[parts[1]]);
                }
                //console.log(url);
                $.get( url, function(_data, status) {
                    if (status === 'success'){
                        //console.log(_data);
                        let data = JSON.parse(_data);
                        let input = document.createElement("select");
                        input.id = name.replaceAll('.','-');
                        input.classList.add('form-control');
                        input.classList.add('select-search');
                        let opt = document.createElement('option');
                        opt.value = '';
                        opt.innerHTML = title;
                        input.appendChild(opt);
                        let vals = [];
                        for (let i = 0; i < data.length; i++) {
                            opt = document.createElement('option');
                            let txt = "" + data[i];
                            if (txt.length > 0 && !vals.includes(txt)){
                                opt.value = txt;
                                opt.innerHTML = txt;
                                input.appendChild(opt);
                                vals.push(txt);
                            }
                        }
                        $(input).on('change', function () {
                            column.search($(this).val(), false, false, true).draw();
                            if (onSelectorChange != null){
                                onSelectorChange(this);
                            }
                        });
                        if (onSelectorCreated != null){
                            onSelectorCreated(input);
                        }
                        selectorArray[name] = input;
                        if (Object.keys(selectorArray).length === names.length){
                            names.forEach(n => {
                                let i = selectorArray[n];
                                $(i).appendTo('.dataTables_filter');
                            });
                        }
                    }
                });
            });
        }

        function sendFeedbackAction(){
            let name = $("#namefb").val();
            if (name.length === 0){
                return;
            }
            let formData = new FormData();
            formData.append('fid', fid);
            formData.append('oname', org_name);
            formData.append('uname', $("#uname").val());
            formData.append('name', name);
            formData.append('fe_type', $("#fe_type").val());
            formData.append('email', $("#email").val());
            formData.append('body', $("#bodyfb").val());
            formData.append('file', document.getElementById("filefb").files[0]);
            fetch('http://90.156.203.30/api/add-feedback', {
                method: 'POST',
                body: formData
            })
                .then((response) => response.text())
                .then((text) => {
                    console.log(text);
                    if (text.endsWith("true")){
                        window.location.reload();
                    }})
                .catch(
                    error => console.log(error) // Handle the error response object
                );
        }

        function sendFeedBackMessage(message){
            let fid = $("#fid").val();
            let formData = new FormData();
            formData.append('_token', "{{csrf_token()}}");
            formData.append('fid', fid);
            formData.append('uname', $("#uname").val());
            formData.append('email', $("#email").val());
            formData.append('body', message);
            formData.append('file', document.getElementById("file").files[0]);
            fetch('http://90.156.203.30/api/add-fb-rep', {
                method: 'POST',
                body: formData
            })
                .then((response) => response.text())
                .then((text) => {
                    //console.log(text);
                    if (text.endsWith("true")){
                        $("#body").val('');
                        $("#file").val('')
                        initReportTable(fid)
                    }})
                .catch(
                    error => console.log(error) // Handle the error response object
                );
        }

        function sendReportAction(){
            let body = $("#body").val();
            if (body.length > 0){
                sendFeedBackMessage(body);
            }
        }

        function initReportTable(id){
            if (currentFid === id){
                $('#fb-reps-table').DataTable().ajax.reload(null, false);
            }else if (repTableIsInit){
                currentFid = id;
                $('#fb-reps-table').DataTable().ajax.url('http://90.156.203.30/api/load-fb-reps/' + id).load();
            }else{
                currentFid = id;
                repTableIsInit = true;
                $('#fb-reps-table').DataTable({
                    processing: true,
                    serverSide: true,
                    autoWidth: false,
                    dom: "frtip",
                    ajax: 'http://90.156.203.30/api/load-fb-reps/' + id,
                    columns: [
                        { data: 'created_at', name: 'created_at', 'width':'80px', title:'{{ __('str.table_created_at') }}'  },
                        { data: 'email', name: 'fb_reports.email', 'width':'80px', title:'{{ __('str.table_feedback_email') }}' },
                        { data: 'uname', name: 'uname', 'width':'80px', title:'{{ __('str.table_feedback_name') }}' },
                        { data: 'path', name: 'path', 'width':'80px', title:'{{ __('str.table_link') }}'  },
                        { data: 'body', name: 'body', title:'{{ __('str.table_des') }}'  },
                    ]
                }).on('click', 'tbody td', function() {
                }).on('dblclick', 'tbody td', function() {
                });
            }
        }

        $(document).ready(function(){
            let url = 'http://90.156.203.30/api/load-fb-list/' + encodeURI(org_name);
            //console.log("org_name=" + org_name + " url=" + url);
            $('#feedbacks-table').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                order: [[ 0, 'desc' ]],
                dom: "frtip",
                initComplete : function () {
                    let curTable = this;
                    $.get( 'http://90.156.203.30/api/myid/' + org_name, function(_data, status) {
                        if (status === 'success') {
                            let pc_id = parseInt(_data);
                            //console.log("id=" + pc_id);
                            //pc_id = 4;
                            createTableSelectors(curTable,['feedback.state','feedback.fe_type','feedback.aut'],{"aut" : "pc_id = " + pc_id},function (selector) {
                                //console.log(selector.id);
                                if (selector.id === "feedback-state"){
                                    let ops = selector.options;
                                    for (let i = 0; i < ops.length; i++){
                                        if (ops[i].value === '0' || ops[i].value === '-1') ops[i].text = "Открыто";
                                        if (ops[i].value === '-2') ops[i].text = "В работе";
                                        if (ops[i].value === '-3') ops[i].text = "На анализе";
                                        if (ops[i].value === '-4') ops[i].text = "Отложено";
                                        if (ops[i].value === '-5') ops[i].text = "Выполнено";
                                    }
                                }
                                if (selector.id === "feedback-fe_type"){
                                    let ops = selector.options;
                                    for (let i = 0; i < ops.length; i++){
                                        if (ops[i].value === '1') ops[i].text = "Ошибка";
                                        if (ops[i].value === '2') ops[i].text = "Предложение";
                                    }
                                }
                            }, null)
                        }
                    });
                },
                ajax: url,
                columns: [
                    { data: 'id', name: 'id', 'width':'60px', title:'№'},
                    { data: 'created_sec', name: 'created_sec', 'width':'120px', title:'{{ __('str.table_feedback_created') }}'},
                    { data: 'state',   name: 'feedback.state', 'width':'120px', title:'{{ __('str.table_feedback_state') }}'},
                    { data: 'fe_type', name: 'feedback.fe_type', 'width':'100px', title:'{{ __('str.table_feedback_type') }}', className: 'text-center'   },
                    { data: 'name',    name: 'name', title:'{{ __('str.table_name') }}'  },
                    { data: 'aut',  name: 'feedback.aut', title:'{{ __('str.table_author') }}', 'width':'120px'},
                ]
            }).on('click', 'tbody td', function() {
                let tr  = $(this).closest('tr');
                let row = $('#feedbacks-table').DataTable().row(tr);
                initReportTable(row.data().id);
                $('#fid').val(row.data().id);
                $('#modalLabelHeader2').text("№" + row.data().id);
                $('#name_des').val(row.data().name);
                $('#author_des').val(row.data().aut);
                $('#created_sec_des').val(row.data().created_sec);
                $('#fe_type_des').val(row.data().fe_type);
                $('#des_des').val(row.data().des);
                $('#modal-screen2').modal('show');
                currendRepDes = row.data().des;
            });
            //----------------------------------------------------------------------------------------------------------
            setInterval(function() {
                let screenIsOpen = $('#modal-screen2').is(":visible");
                if (screenIsOpen){
                    initReportTable(currentFid);
                }
            }, 5000);
            //----------------------------------------------------------------------------------------------------------
            $('#bUpdateFeedbackDes').css('visibility', 'hidden');
        });
    </script>
@endpush
