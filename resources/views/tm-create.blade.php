@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                <form method="post" action="/tm-create-action">
                    @csrf
                    <input type="number" name="tid" value="{{$tid}}" style="visibility: hidden">
                    <input type="text" name="title" value="{{ __('str.on_message') }}!" style="visibility: hidden">
                    <div class="input-group">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" style="width: 200px; height: 100px">Текст</span>
                        </div>
                        <textarea name="body" class="form-control" aria-label="With textarea"></textarea>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary" style="width: 200px">{{ __('str.send_message') }}</button>
                </form>
                <br>
                {{$dataTable->table(["style"=>"width:100%;height:400px"])}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{$dataTable->scripts()}}
@endpush
