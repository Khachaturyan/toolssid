@extends('layouts.app')

@section('content')

    <div class="modal fade" id="modal-screen1" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <button class="btn btn-success" onclick="selectAllActItemsAction()">{{ __('str.select_all') }}</button>
                    <button class="btn btn-success" onclick="removeAllActItemsAction()">{{ __('str.unselect_all') }}</button>
                    <table class="table table-bordered table-striped" id="equ-repair-table" data-page-length="12"></table>
                    <button class="btn btn-success" onclick="saveActAction()">{{ __('str.rep_act_create') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                <div>
                    <button class="btn btn-success" onclick="createAct()">+</button>
                </div>
                <table class="table table-bordered table-striped" id="act-table" data-page-length="50"></table>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

    <script>

        var actId = null;
        var equTableIsInited = false;

        function selectAllActItemsAction(){
            $.get( "/act-select-all/" + actId, function(data, status) {
                if (status === 'success'){
                    if ('true' == data){
                        updateEquTable();
                    }else{
                        alert(data);
                        console.log(data);
                    }
                }
            });
        }

        function removeAllActItemsAction(){
            $.get( "/act-unselect-all/" + actId, function(data, status) {
                if (status === 'success'){
                    if ('true' == data){
                        updateEquTable();
                    }else{
                        alert(data);
                        console.log(data);
                    }
                }
            });
        }

        function saveActAction(){
            $.get( "/act-save/" + actId, function(data, status) {
                if (status === 'success'){
                    if ('true' == data){
                        window.location.reload();
                    }else{
                        alert(data);
                        console.log(data);
                    }
                }
            });
        }

        function onEquClick(eid){
            $.get( "/act-change/" + actId + "/" + eid, function(data, status) {
                if (status === 'success'){
                    if ('true' == data){
                        updateEquTable();
                    }else{
                        alert(data);
                        console.log(data);
                    }
                }
            });
        }

        function updateEquTable(){
            if (equTableIsInited){
                $('#equ-repair-table').DataTable().ajax.reload(null, false);
            }else{
                equTableIsInited = true;
                $('#equ-repair-table').DataTable({
                    processing: true,
                    serverSide: true,
                    dom: "frtip",
                    ajax: '/act-equ-lst/' + actId,
                    columns: [
                        { data: 'is_checked',   title:'', className: 'text-center', width: "50"  },
                        { data: 'ser_num', name: 'equipment.ser_num', title:'{{ __('str.table_ser') }}' },
                        { data: 'inv_num', name: 'equipment.inv_num', title:'{{ __('str.table_inv_num') }}' },
                        { data: 'tname', name: 'etypes.tname', title:'{{ __('str.table_mark') }}' },
                        { data: 'tmark', name: 'etypes.tmark', title:'{{ __('str.table_model') }}' },
                        { data: 'tmodel', name: 'etypes.tmodel', title:'{{ __('str.table_type') }}' },
                        { data: 'tmodel', name: 'etypes.tmodel', title:'{{ __('str.table_type') }}' },
                    ]
                }).on('click', 'tbody td', function() {
                    let tr  = $(this).closest('tr');
                    let row = $('#equ-repair-table').DataTable().row(tr);
                    if (this.cellIndex === 0){
                        onEquClick(row.data().id);
                    }
                }).on('dblclick', 'tbody td', function() {
                });
            }
        }

        function createAct(){
            $.get( "/act-create", function(data, status) {
                if (status === 'success'){
                    if (data.startsWith('true')){
                        actId = parseInt(data.replace("true",""));
                        updateEquTable();
                        $('#modal-screen1').modal('show');
                    }else{
                        alert(data);
                        console.log(data);
                    }
                }
            });
        }

        $(document).ready(function(){
            $('#act-table').DataTable({
                processing: true,
                serverSide: true,
                dom: "frtip",
                ajax: '/act-list',
                columns: [
                    { data: 'export',  title:'{{ __('str.table_export_xls') }}', className: 'text-center', width: "50" },
                    { data: 'num', name: 'num', title:'{{ __('str.table_num_act') }}', className: 'text-center', width: "100"  },
                    { data: 'created_at', name: 'created_at', title:'{{ __('str.table_created2') }}' },
                    { data: 'count', name: 'count', title:'{{ __('str.table_count_act') }}' },
                ]
            });
        })

    </script>
@endpush

