@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('str.add_remove') }} {{$etype->tname}} {{$etype->tmark}} {{$etype->tmodel}} {{ __('str.in_roles') }}</h4>
                </div>
                <form method="post" action="/etype-attache-action">
                    @csrf
                    <input type="number" name="id" value="{{$etype->id}}" style="visibility: hidden">
                    <div class="input-group mb-3">
                        <table class="table">
                            @foreach ($roles as $role)
                                <tr>
                                    <td style="padding-bottom: 12px; padding-left: 12px;">
                                        <input type="checkbox" name="role{{$role->id}}" value="1"
                                               @if (str_contains($role->etypes, '#' . $etype->id . '#')) checked @endif>
                                        {{$role->rname}}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary" style="width: 200px">{{ __('str.update') }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection
