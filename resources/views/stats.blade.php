@extends('layouts.app')

@section('nav-bar-container')
    @if ($men != null)
        {{ __('str.stat_worker') }}: {{$men->msurname . " " . $men->mname . " " . $men->mpatronymic}}
    @endif
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">
            <div class="card-body">
                {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                    "data-page-length" => "100",
                    "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{$dataTable->scripts()}}
    <script>
        $(document).ready(function(){

        });
    </script>
@endpush
