@extends('layouts.app')

@section('third_party_stylesheets')
    <style>
        input[type="datetime-local"]::-webkit-calendar-picker-indicator {
            background: transparent;
            bottom: 0;
            color: transparent;
            cursor: pointer;
            height: auto;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            width: auto;
        }
        input[type="date"]::-webkit-calendar-picker-indicator {
            background: transparent;
            bottom: 0;
            color: transparent;
            cursor: pointer;
            height: auto;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            width: auto;
        }
        /* Card-Move */
        .card-move__inner {
            display: flex;
            width: 100%;
            height: auto;
            position: relative;
            background-color: #c7c7c7;
            border-radius: 16px;
            filter: drop-shadow(2px 2px 3px gray);
        }
        .dark-layout .card-move__inner {
            background-color: rgba(199, 199, 199, 0.3);
        }
        .card-move__inner:hover .card-move__text {
            opacity: 1;
        }
        .card-move__inner:hover .card-move__img {
            transform: translate3d(-10px, -10px, 0);
        }
        .card-move__img {
            display: flex;
            flex-direction: column;
            width: 100%;
            height: 250px;
            background: linear-gradient(to bottom, #ffffff, #f4f4f4);
            transition: transform .1s linear;
            border-radius: 16px;
        }
        .dark-layout .card-move__img {
            background: #161d31;
        }
        .card-move__text {
            width: 100%;
            opacity: 0;
            position: absolute;
            top: 72%;
            left: 90%;
            z-index: 2;
            transform: translate3d(-10%, -28%, 0);
            transition: opacity .4s linear;
        }
        .card-move__title {
            padding: 16px 0 0 14px;
            margin-bottom: 0;
            font-weight: 600;
            font-size: 18px;
        }
        .card-box__images {
            display: flex;
            margin-top: auto;
            padding-bottom: 16px;
        }
        .card-move__content {
            padding: 0 14px 0 14px;
            margin-bottom: 0;
            font-weight: 600;
            font-size: 14px;
        }
        .card-move__img-inner {
            padding: 12px;
            width: 12%;
            height: auto;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-move__inner" onclick="rep_not_working_click();">
                        <div class="card-move__img">
                            <p class="card-move__title" id="modalLabelHeader">{{ __('str.rep_not_working') }}</p>
                            <hr>
                            <p class="card-move__content">{{ __('str.rep_des_not_working') }}</p>
                            <div class="card-box__images">
                                <img class="card-move__img-inner" src="/img/warehouse-green.svg" alt="warehouse" data-toggle="tooltip" title="Выбор объекта">
                                <img class="card-move__img-inner" src="/img/calendar-green.svg" alt="calendar" data-toggle="tooltip" title="Выбор даты/времени">
                            </div>
                        </div>
                        <div class="card-move__text"> <img class="card-move__img-inner" src="/img/printer-green.svg" alt="printer"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-move__inner" onclick="rep_type_load_click();">
                        <div class="card-move__img">
                            <p class="card-move__title" id="modalLabelHeader">{{ __('str.rep_type_load') }}</p>
                            <hr>
                            <p class="card-move__content">{{ __('str.rep_des_used_equ') }}</p>
                            <div class="card-box__images">
                                <img class="card-move__img-inner" src="/img/warehouse-green.svg" alt="warehouse" data-toggle="tooltip" title="Выбор объекта">
                                <img class="card-move__img-inner" src="/img/calendar-green.svg" alt="calendar" data-toggle="tooltip" title="Выбор даты/времени">
                            </div>
                        </div>
                        <div class="card-move__text"> <img class="card-move__img-inner" src="/img/printer-green.svg" alt="printer"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-move__inner" onclick="rep_equ_states_click();">
                        <div class="card-move__img">
                            <p class="card-move__title" id="modalLabelHeader">{{ __('str.rep_equ_states') }}</p>
                            <hr>
                            <p class="card-move__content">{{ __('str.rep_des_states') }}</p>
                            <div class="card-box__images">
                                <img class="card-move__img-inner" src="/img/warehouse-green.svg" alt="warehouse" data-toggle="tooltip" title="Выбор объекта">
                            </div>
                        </div>
                        <div class="card-move__text"> <img class="card-move__img-inner" src="/img/printer-green.svg" alt="printer"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-move__inner" onclick="rep_men_list_click();">
                        <div class="card-move__img">
                            <p class="card-move__title" id="modalLabelHeader">{{ __('str.rep_men_list') }}</p>
                            <hr>
                            <p class="card-move__content">{{ __('str.rep_des_men') }}</p>
                            <div class="card-box__images">
                                <img class="card-move__img-inner" src="/img/arrow-down_report-green.svg" alt="arrow-down" data-toggle="tooltip" title="Моментальная выгрузка">
                            </div>
                        </div>
                        <div class="card-move__text"> <img class="card-move__img-inner" src="/img/printer-green.svg" alt="printer"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-move__inner" onclick="rep_day_log_click();">
                        <div class="card-move__img">
                            <p class="card-move__title" id="modalLabelHeader">{{ __('str.rep_day_log_out') }}</p>
                            <hr>
                            <p class="card-move__content">{{ __('str.rep_des_day_logs') }}</p>
                            <div class="card-box__images">
                                <img class="card-move__img-inner" src="/img/warehouse-green.svg" alt="warehouse" data-toggle="tooltip" title="Выбор объекта">
                                <img class="card-move__img-inner" src="/img/calendar-green.svg" alt="calendar" data-toggle="tooltip" title="Выбор даты/времени">
                            </div>
                        </div>
                        <div class="card-move__text"> <img class="card-move__img-inner" src="/img/printer-green.svg" alt="printer"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-move__inner" onclick="rep_acts_click();">
                        <div class="card-move__img">
                            <p class="card-move__title" id="modalLabelHeader">{{ __('str.rep_acts') }}</p>
                            <hr>
                            <p class="card-move__content">{{ __('str.rep_des_acts') }}</p>
                            <div class="card-box__images">
                                <img class="card-move__img-inner" src="/img/table-grid_green.svg" alt="table-grid" data-toggle="tooltip" title="Дополнительное окно">
                            </div>
                        </div>
                        <div class="card-move__text"> <img class="card-move__img-inner" src="/img/printer-green.svg" alt="printer"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card card__modificator" style="margin-top: 20px;">
                    <div class="card-move__inner" onclick="rep_archive_logs_click();">
                        <div class="card-move__img">
                            <p class="card-move__title" id="modalLabelHeader">{{ __('str.rep_archive_logs') }}</p>
                            <hr>
                            <p class="card-move__content">{{ __('str.rep_des_archive_logs') }}</p>
                            <div class="card-box__images">
                                <img class="card-move__img-inner" src="/img/table-grid_green.svg" alt="table-grid" data-toggle="tooltip" title="Дополнительное окно">
                            </div>
                        </div>
                        <div class="card-move__text"> <img class="card-move__img-inner" src="/img/printer-green.svg" alt="printer"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-not-working" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.rep_select_period') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3" @if (count($stages) === 0) style="display: none" @endif >
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.block_stage_not_select2') }}</span>
                        </div>
                        <select class="form-control" id="not_working_stage" >
                            <option value="0" selected >Все объекты</option>
                            @foreach($stages as $stage)
                                <option value="{{$stage->id}}">{{$stage->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_date_start') }}</span>
                        </div>
                        <input type="datetime-local" id="rep_date_start1" name="rep_date_start1" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_date_end') }}</span>
                        </div>
                        <input type="datetime-local" id="rep_date_end1" name="rep_date_end1" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
                    </div>
                    <button type="button" class="btn btn-primary" style="width: 200px" onclick="export_xlsx_not_working();">{{ __('str.equ_create_report') }}</button>
                    <button type="button" class="btn btn-secondary" style="width: 200px" onclick="hideModal();">{{ __('str.rep_cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-type-load" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.rep_select_period') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3" @if (count($stages) === 0) style="display: none" @endif >
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.block_stage_not_select2') }}</span>
                        </div>
                        <select class="form-control" id="type_load_stage" >
                            <option value="0" selected >Все объекты</option>
                            @foreach($stages as $stage)
                                <option value="{{$stage->id}}">{{$stage->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_date_start') }}</span>
                        </div>
                        <input type="datetime-local" id="rep_date_start" name="rep_date_start" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_date_end') }}</span>
                        </div>
                        <input type="datetime-local" id="rep_date_end" name="rep_date_end" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
                    </div>
                    <br>
                    <button type="button" class="btn btn-primary" id="rep-create" onclick="export_xlsx_all_equ()">{{ __('str.equ_create_report') }}</button>
                    <button type="button" class="btn btn-secondary" style="width: 200px" onclick="hideModal();">{{ __('str.rep_cancel') }}</button>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-log-day" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.rep_day_log_out') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3" @if (count($stages) === 0) style="display: none" @endif >
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.block_stage_not_select2') }}</span>
                        </div>
                        <select class="form-control" id="log_stage" >
                            <option value="0" selected >Все объекты</option>
                            @foreach($stages as $stage)
                                <option value="{{$stage->id}}">{{$stage->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.rep_date') }}</span>
                        </div>
                        <input type="date" id="rep_log_date" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" />
                    </div>
                    <br>
                    <button type="button" class="btn btn-primary" id="rep-create" onclick="export_xlsx_log_day()">{{ __('str.equ_create_report') }}</button>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-equ-state" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.rep_equ_states') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3" @if (count($stages) === 0) style="display: none" @endif >
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.block_stage_not_select2') }}</span>
                        </div>
                        <select class="form-control" id="equ_states_stage" >
                            <option value="0" selected >Все объекты</option>
                            @foreach($stages as $stage)
                                <option value="{{$stage->id}}">{{$stage->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                    <button type="button" class="btn btn-primary" id="rep-create" onclick="equ_states_create()">{{ __('str.equ_create_report') }}</button>
                    <button type="button" class="btn btn-secondary" style="width: 200px" onclick="$('#modal-equ-state').modal('hide');">{{ __('str.rep_cancel') }}</button>
                    <br>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

    <script>

        function rep_acts_click(){
            window.location = "/acts";
        }

        function rep_day_log_click(){
            $('#modal-log-day').modal('show');
        }

        function export_xlsx_log_day(){
            $('#modal-log-day').modal('hide');
            let d = $("#rep_log_date").val();
            if (d.length == 0){
                d = new Date();
            }
            let stageId = $("#log_stage").val();
            window.location = "/rep-day-log/" + d + "/" + stageId;
        }

        function export_xlsx_not_working(){
            let ds = $("#rep_date_start1").val();
            if (ds.length == 0){
                ds = "0";
            }
            let de = $("#rep_date_end1").val();
            if (de.length == 0){
                de = "0";
            }
            let stageId = $("#not_working_stage").val();
            window.location="/rep-not-wrk/" + ds + "/" + de + "/" + stageId;
        }

        function export_xlsx_all_equ(){
            let ds = $("#rep_date_start").val();
            if (ds.length == 0){
                ds = "0";
            }
            let de = $("#rep_date_end").val();
            if (de.length == 0){
                de = "0";
            }
            let stageId = $("#type_load_stage").val();
            window.location = "/rep-equ-xls/" + ds + "/" + de + '/' + stageId;
        }

        function hideModal(){
            $('#modal-not-working').modal('hide');
            $('#modal-type-load').modal('hide');
        }

        function rep_not_working_click(){
            $('#modal-not-working').modal('show');
        }

        function rep_type_load_click(){
            $('#modal-type-load').modal('show');
        }

        function rep_men_list_click(){
            window.location = "/rep-men";
        }

        function rep_archive_logs_click(){
            window.location = "/archive-logs";
        }

        function rep_equ_states_click(){
            $('#modal-equ-state').modal('show');
        }

        function equ_states_create(){
            let stageId = $("#equ_states_stage").val();
            window.location = "/rep-equ-xls3/" + stageId;
        }

        $(document).ready(function(){
            document.getElementById('rep_log_date').valueAsDate = new Date();
        });

    </script>
@endpush
