@extends('layouts.app')

@section('third_party_stylesheets')
    <style>
        .avatar{
            background-color: #f8f8f8!important;
            margin-right: 10px;
        }
        .blocksy{
            height: 300px;
            overflow-y: auto;
            overflow-x: auto;
        }
        input[type="datetime-local"]::-webkit-calendar-picker-indicator {
            background: transparent;
            bottom: 0;
            color: transparent;
            cursor: pointer;
            height: auto;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            width: auto;
        }
    </style>
@endsection

@section('content')

    <div class="modal fade" id="modal-screen" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader" style="margin-top: 0px;">{{ __('str.equ_states_create') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="/equ-states-item-edit">
                        @csrf
                        <input type="hidden" id="id" name="id" value="">

                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.table_color') }}</span>
                            </div>
                            <input type="color" id="color" name="color" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.table_name') }}</span>
                            </div>
                            <input type="text" id="descr" name="descr" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.table_descr') }}</span>
                            </div>
                            <input type="text" id="des2" name="des2" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                        </div>

                        <br>
                        <button type="submit" class="btn btn-success" style="width: 200px">{{ __('str.save') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">
            <div class="card-body">
                <div class="card__title-bg">
                    <h4 class="modal-title">{{ __('str.equipment_status') }}</h4>
                </div>
                <table class="table table-bordered table-striped">
                    <thead class="text-center">
                    <tr>
                        <th>{{ __('str.table_color') }}</th>
                        <th>{{ __('str.table_descr') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($stat_items as $item)
                        <tr>
                            <td style="width: 150px"><input type="color" value="{{ substr($item['color'],0,7) }}" style="width:100%;" disabled></td>
                            <td>{{ $item['descr'] . '. ' . $item['desfu'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">
            <div class="card-body">
                <button class="btn btn-success" id="add-new" onclick="showModal()">+</button>
                <table class="table table-bordered table-striped" id="equ-states-table" data-page-length="50"></table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        function showModal(){
            fillModal(null);
            $('#modal-screen').modal('show');
        }

        function fillModal(data){
            if (data == null){
                $('#modalLabelHeader').text('{{ __('str.equ_states_create') }}');
                $('#id').val("");
                $('#color').val("");
                $('#descr').val("");
                $('#des2').val("");
            }else{
                $('#modalLabelHeader').text('{{ __('str.equ_states_edit') }}');
                $('#id').val(data.id);
                $('#color').val(data.color);
                $('#descr').val(data.descr);
                $('#des2').val(data.des2);
            }
        }

        function removeStatus(id){
            $.get( "/equ-states-item-del/" + id, function(data, status) {
                if (status === 'success'){
                    if ('true' === data){
                        window.location.reload();
                    }else{
                        alert(data);
                        console.log(data);
                    }
                }
            });
        }

        $(document).ready(function(){

            $('#equ-states-table').DataTable({
                processing: true,
                serverSide: true,
                dom: "frtip",
                ajax: '/equ-states-items',
                columns: [
                    { data: 'delete', title:'{{ __('str.table_remove') }}', 'width':'50px', className: 'text-center', searchable: false, },
                    { data: 'action', title:'{{ __('str.table_edit') }}', 'width':'50px', className: 'text-center', searchable: false, },
                    { data: 'color2', name: 'color', title:'{{ __('str.table_color') }}', 'width':'50px', searchable: false, },
                    { data: 'descr', name: 'descr', title:'{{ __('str.table_name') }}', 'width':'200px' },
                    { data: 'des2', name: 'des2', title:'{{ __('str.table_descr') }}' },
                ]
            }).on('click', 'tbody td', function() {
                let tr  = $(this).closest('tr');
                let row = $('#equ-states-table').DataTable().row(tr);
                if (this.cellIndex == 0){
                    if (row.data().count == 0){
                        if (confirm('{{ __('str.alert_status_need_delete') }}' +  ' ' + row.data().descr + '?')){
                            removeStatus(row.data().id);
                        }
                    }else{
                        alert('{{ __('str.alert_status_equ_not_empty') }}');
                    }
                } else if (this.cellIndex == 1){
                    if (row.data().count == 0){
                        fillModal(row.data());
                        $('#modal-screen').modal('show');
                    }else{
                        alert('{{ __('str.alert_status_equ_not_empty') }}');
                    }
                }
            });
        })
    </script>
@endpush

