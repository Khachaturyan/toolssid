@extends('layouts.app')

@section('third_party_stylesheets')
    <style>
        @keyframes donut-spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .donut {
            display: inline-block;
            border: 4px solid rgba(0, 0, 0, 0.1);
            border-left-color: #7983ff;
            border-radius: 50%;
            width: 30px;
            height: 30px;
            animation: donut-spin 1.2s linear infinite;
        }
        div.container6 {
            display: flex;
            align-items: center;
            justify-content: center
        }
        div.container6 p {
            margin: 0
        }
        /* ----Выбор отображаемых колонок в таблице блока---- */
        /* ключевые кадры */
        @keyframes rotate_shesterenka {
            0% {
                transform: scale(1.1);
            }
            25% {
                transform: rotate(60deg);
            }
            50% {
                transform: scale(1.1);
            }
            75% {
                transform: rotate(0deg);
            }
            100% {
                transform: scale(1);
            }
        }
        .btn-set-visible {
            width: 22px;
            height: 22px;
            border-radius: 50%;
            border:none;
            background-color: rgba(255, 255, 255, 0);
            background-image: url('/img/new-des_setting-1.svg');
            background-repeat: no-repeat;
            background-position: center;
            padding: 0;
            cursor: pointer;
            transition: transform 0.2s linear;
        }
        .btn-set-visible:hover {
            /* transform: translateY(-3px); */
            animation-name: rotate_shesterenka;
            animation-duration: 1.5s;
            animation-timing-function: ease;
            animation-iteration-count: 1;
            animation-direction: normal;
            animation-play-state: running;
            animation-delay: 0s;
            animation-fill-mode: none;
        }
        .set-mb-pl {
            padding: 0 10px;
        }
    </style>
@endsection

@section('nav-bar-container')
    <div class="text-center">
        @if(Auth::user()->create_user())
            <button class="btn btn-success" id="add-new" onclick="createMen()" >+</button>
        @endif
        <button class="btn btn-success" id="ext-import" style="margin: 2px 2px 2px 16px;">{{ __('str.mens_import') }}</button>
        <button class="btn btn-success" onclick="window.location='/stats-all'" style="margin: 2px 2px 2px 16px;">{{ __('str.mens_statistic') }}</button>
        @if (strpos(Request::url(),'men-equ-rigid') !== false)
            <button class="btn btn-success" onclick="window.location.href='/me-rigid-remove/{{$eid}}'" style="margin: 2px 2px 2px 12px;">{{ __('str.mens_remove_bundle') }} {{$equ_text}}</button>
        @endif
    </div>
@endsection

@section('content')

    <div class="modal fade" id="modal-screen" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="margin-top:0">
                    <h4 class="modal-title" style="margin-top:0" id="modalLabelHeader">{{ __('str.mens_worker') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form id="form-men-data" method="post"  action="/man-action">
                        @csrf
                        <input type="number" id="id" name="id" value="" style="visibility: hidden">
                        <input type="text" id="old_mail" name="old_mail" value="" style="visibility: hidden">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group input__group-modificator">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 220px">{{ __('str.mens_id_in_export') }}</span>
                                    </div>
                                    <input type="text" id="str_ext_id" name="str_ext_id" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group input__group-modificator">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 220px">{{ __('str.mens_login_in_export') }}</span>
                                    </div>
                                    <input type="text" id="str_ext_login" name="str_ext_login" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 220px">{{ __('str.mens_surname') }}</span>
                                    </div>
                                    <input type="text" id="msurname" name="msurname" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 220px">{{ __('str.mens_men_status') }}</span>
                                    </div>
                                    <select class="form-control" id="fired" name="fired" required>
                                        <option value="" disabled selected>{{ __('str.mens_select_status') }}</option>
                                        <option value="1">{{ __('str.mens_status_fired') }}</option>
                                        <option value="0">{{ __('str.mens_status_in_state') }}</option>
                                        <option value="2">{{ __('str.mens_status_outsource') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 220px">{{ __('str.mens_name') }}</span>
                                    </div>
                                    <input type="text" id="mname" name="mname" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 220px">{{ __('str.mens_label') }}</span>
                                    </div>
                                    <input type="text" id="mlabel" name="mlabel" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 220px">{{ __('str.mens_patronymic') }}</span>
                                    </div>
                                    <input type="text" id="mpatronymic" name="mpatronymic" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 220px">{{ __('str.mens_post') }}</span>
                                    </div>
                                    <input type="text" id="post" name="post" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 220px">{{ __('str.mens_role') }}</span>
                                    </div>
                                    <select class="form-control" id="rid" name="rid" required>
                                        <option value="" disabled selected>{{ __('str.mens_role_select') }}</option>
                                        <?php
                                        $needSeparator = false;
                                        for ($i = 0; $i < count($roles); $i++){
                                            $needSeparator = $i > 0 && $roles[$i - 1]->perm_index < 0 && $roles[$i]->perm_index >= 0;
                                            if ($needSeparator){
                                                echo '<option disabled>---------------------------</option>';
                                            }
                                            echo '<option value="' . $roles[$i]->id . '">' . $roles[$i]->rname . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 220px">{{ __('str.login') }}</span>
                                    </div>
                                    <input type="text" id="email" name="email" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-success" style="width: 100%">{{ __('str.mens_save') }}</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-success" onclick="sendTestMail()" style="width: 100%">{{ __('str.mens_test_email') }}</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-success" onclick="genLabelUni()" style="width: 100%">{{ __('str.mens_gen_label') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-screen2" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader" style="margin-top: 0px;">{{ __('str.mens_import') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form action="/men-excel-import" id='mForm' method="POST" enctype="multipart/form-data" style="width: 100%">
                            @csrf
                        <input type="hidden" id="perco-data" name="perco-data" value="">
                        <input type="file" name="file" id="usr_file" class="form-control" required="required" style="padding: 16px;height: 54px">
                        <div class=container6 id="donat-container" style="visibility: hidden">
                            <div class="donut" ></div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.mens_encode_label_algorithm') }}</span>
                            </div>
                            <select class="form-control" id="lab_encode" name="lab_encode" required>
                                <option value="" disabled selected>{{ __('str.mens_encode_label_algorithm_select') }}</option>
                                <option value="0">{{ __('str.mens_encode_label_no') }}</option>
                                <option value="1">{{ __('str.mens_encode_label_alg_1') }}</option>
                                <option value="2">{{ __('str.mens_encode_label_alg_2') }}</option>
                                <option value="3">{{ __('str.mens_encode_label_alg_3') }}</option>
                                <option value="4">{{ __('str.mens_encode_label_alg_4') }}</option>
                                <option value="5">{{ __('str.mens_encode_label_alg_5') }}</option>
                                <option value="6">{{ __('str.mens_encode_label_alg_6') }}</option>
                            </select>
                        </div>
                        <br>
                        <div class="icheck-primary">
                            <input  type="checkbox" name="remove_absent">
                            <label for="remember">{{ __('str.mens_remove_absent') }}</label>
                        </div>
                        <br>
                        <div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button class="btn btn-success" id="men-import-action" style="margin-right: 16px;width:100%;">{{ __('str.mens_excel_import') }}</button>
                                </div>
                                <div class="col-md-6 mb-2">
                                    <button class="btn btn-success" id="men-archive" style="margin-right: 16px;width:100%;" onclick="window.location='/doc/canon.xlsx'">{{ __('str.equ_load_file_example') }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- <br><br> -->
                </div>
            </div>
        </div>
    </div>

    <!-- Modal alert data -->
    <div class="modal fade" style="z-index:3300" id="modal__data-mans" tabindex="-1" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content" style="border-radius:20px;overflow:hidden;">
                <div class="modal-header" style="justify-content:center;border-top-right-radius:20px;border-top-left-radius:20px;">
                    <h4 class="modal-title" style="margin-top: 0px;font-weight:500;">{{ __('str.content_modal_header') }}</h4>
                </div>
                <div class="modal-body" style="font-size:20px;margin:0;text-align:center;font-weight:500;">
                </div>
                <div class="modal-footer" style="justify-content:center;">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal" style="border-radius:8px;">
                        <h4 style="font-size:20px;margin:0">{{ __('str.content_modal_footer') }}</h4>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">
            <div class="card-body">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li> {{$error}} </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="d-flex justify-content-end">
                    <div class="btn-group dropstart">
                        <button class="btn-set-visible dropstart" type="button" data-bs-toggle="dropdown" aria-expanded="false"></button>
                        <ul class="dropdown-menu" id="grpChkBoxMans">
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxNumber" type="checkbox"> {{ __('str.table_num') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxFio" type="checkbox"> {{ __('str.table_fio') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxStatus" type="checkbox"> {{ __('str.table_state2') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxMetka" type="checkbox"> {{ __('str.table_label') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxRole" type="checkbox"> {{ __('str.table_role') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxEmail" type="checkbox"> {{ __('str.table_feedback_email') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxPost" type="checkbox"> {{ __('str.table_post') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxLock" type="checkbox"> {{ __('str.table_orion_lock') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chk-box-registr" type="checkbox"> {{ __('str.table_user_create_date') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxEqu" type="checkbox"> {{ __('str.equipment') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chkBoxDia" type="checkbox"> {{ __('str.table_diagnostic') }}</label></li>
                            <li class="set-mb-pl text-nowrap"><label><input id="chk-box-login_in_exp" type="checkbox"> {{ __('str.mens_login_in_export') }}</label></li>
                        </ul>
                    </div>
                </div>
                {{$dataTable->table([ "style"=>"width:100%; scrollX:true;",  "class" => "table table-bordered table-striped",
                    "data-page-length" => "100",
                    "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}

            </div>
        </div>
    </div>

@endsection

@push('scripts')

    {{ $dataTable->scripts() }}

    <script src="/js/page-settings-io.js"></script>
    <script src="{{ asset('/js/tableSelectors.js') }}"></script>

    <script>

        let page    = 'mans';
        let curData = null;
        let userId  = parseInt("{{Auth::user()->id}}");
        let mid     = parseInt("{{Auth::user()->mid()}}");
        let rid     = parseInt("{{Auth::user()->rid()}}");

        function sendTestMail(){
            let id = '' + $('#id').val();
            let mail = '' + $('#email').val();
            if (id.length > 0 && mail.length > 0){
                let post ={};
                post['_token'] = "{{csrf_token()}}";
                post['adr'   ] = mail;
                post['sbj'   ] = 'tools-id проверка почты';
                post['bod'   ] = 'Привет, если вы получили это письмо - значит сервис отправки почты работает.';
                $.post( "/ps-mail-send", post, function(data, status) {
                    if (status === 'success'){
                        if ('true' === data){
                            alert("{{ __('str.settings_send_mail_ok') }}");
                        }else{
                            alert(data);
                        }
                    }
                });
            }
        }

        function createMen(){
            fillMenData(null);
            $('#modal-screen').modal('show');
        }

        function fillMenData(data){
            if (data == null){
                $('#id').val("");
                $('#mlabel').val("");
                $('#mname').val("");
                $('#msurname').val("");
                $('#mpatronymic').val("");
                $('#rid').val("");
                $('#pin').val("");
                $('#lim_items').val("");
                $('#fired').val("");
                $('#post').val("");
                $('#str_ext_id').val("");
                $('#email').val("");
                $('#str_ext_login').val("");
                $('#old_mail').val("");
            }else{
                $('#id').val(data.id);
                $('#mlabel').val(data.mlabel);
                $('#mname').val(data.mname);
                $('#msurname').val(data.msurname);
                $('#mpatronymic').val(data.mpatronymic);
                $('#rid').val(data.rid);
                $('#pin').val(data.pin);
                $('#lim_items').val(data.lim_items == 1 ? 0 : 1);
                $('#fired').val(data.fired);
                $('#post').val(data.post);
                $('#str_ext_id').val(data.str_ext_id);
                $('#str_ext_login').val(data.str_ext_login);
                $('#email').val(data.email);
                $('#old_mail').val(data.email);
            }
            curData = data;
        }

        function changeDiagnosticEnableStateMode(data){
            let msg = data.can_dia === 0 ? "{{ __('str.mens_need_select_diagnostic') }}" : "{{ __('str.mens_need_unselect_diagnostic') }}";
            msg += " {{ __('str.mens_for') }} " + data.mname + " " + data.msurname + " " + data.mpatronymic + "?";
            if (!confirm(msg)){
                return;
            }
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            post['id'     ] = data.id;
            $.post( "/men-revert-dia-state", post, function(data, status) {
                if (status === 'success' && "true" === data){
                    window.location.reload();
                }
            });
        }

        function genLabelUni(){
            $.get( "/men-gen-lab", function(data, status) {
                if (status === 'success'){
                    //alert(data);
                    $('#mlabel').val(data);
                }
            });
        }

        function onInitComplete(dataTable) {
            let is_fired_only = '1' === "{{$is_fired_only}}";
            let postValue = is_fired_only ? "fired == 1" : "fired != 1";
            createTableSelectors(dataTable,["roles.rname","men.post","men.fired"],
                {"roles.rname" : "is_arc = false", "men.post" : postValue},
                function (selector) {
                    if (selector.id === "men-fired"){
                        let ops = selector.options;
                        for (let i = 0; i < ops.length; i++){
                            if (ops[i].value === '0') ops[i].text = "В штате";
                            if (ops[i].value === '1') ops[i].text = "Уволен";
                            if (ops[i].value === '2') ops[i].text = "Аутсорс";
                        }
                    }
                }, null, null);
        }

        window.onload = function(){
            let isMansUrl = "{{  Request::url() }}".lastIndexOf("mans") >= 0;
            if (isMansUrl){
                document.getElementById("ext-import").onclick = function (){
                    $('#modal-screen2').modal('show');
                }
            }else{
                document.getElementById("ext-import").style.display = 'none';
            }
            //--- обработка кликов по таблице --------------------------------------------------------------------------
            $('#men-table').on('click', 'tbody td', function() {
                let tr  = $(this).closest('tr');
                let row = $('#men-table').DataTable().row(tr);
                @if (Auth::user()->create_user())
                    if (this.cellIndex === 0){
                        fillMenData(row.data());
                        $('#modal-screen').modal('show');
                    }else if (this.cellIndex === 9){
                        changeDiagnosticEnableStateMode(row.data());
                    }
                @endif

            });
            //----------------------------------------------------------------------------------------------------------
            $("#men-import-action").click(function(){
                $("#donat-container").css('visibility','visible');
            });
            $("#form-men-data").submit(function(e) {
                let needLogout = curData != null &&
                    parseInt($("#id").val()) === mid && parseInt($("#rid").val()) !== rid;
                e.preventDefault(); // avoid to execute the actual submit of the form.
                let form = $(this);
                let actionUrl = form.attr('action');
                $.ajax({
                    type: "POST",
                    url: actionUrl,
                    data: form.serialize(), // serializes the form's elements.
                    success: function(data)
                    {
                        if ('true' === data){
                            if (needLogout){
                                event.preventDefault();
                                document.getElementById('logout-form').submit();
                            }else{
                                $('#modal-screen').modal('hide');
                                $('#men-table').DataTable().ajax.reload(null, false);
                            }
                        }else{
                            //alert(data);
                            const elemModal = document.querySelector('#modal__data-mans');
                            const modal = new bootstrap.Modal(elemModal, {backdrop: 'static', keyboard: false, focus: true});
                            let html = data;
                            elemModal.querySelector('.modal-body').innerHTML = html;
                            modal.show();
                        }
                    }
                });
            });
            //--------------------------Выбор отображаемых колонок в таблице блока--------------------------------------
            loadAnyPageData(page,userId,function (pageSettings,bSaveSettingsUser){
                let checkBoxNames = ['chkBoxNumber','chkBoxFio','chkBoxStatus','chkBoxMetka','chkBoxRole',
                    'chkBoxEmail','chkBoxPost','chkBoxLock','chk-box-registr','chkBoxEqu','chkBoxDia','chk-box-login_in_exp'];
                let columnNums    = [1,2,3,6,7,8,9,10,11,12,13,14];
                let columnDefault = [false,true,true,true,true,true,true,false,true,true,false,true];
                let dt = $('#men-table').DataTable();

                //--- некоторые колонки по умолчанию при первой загрузке у нового пользователя не выбираются -----------
                if (bSaveSettingsUser){
                    for (let i = 0; i < columnNums.length; i++){
                        let isChecked = columnDefault[i];
                        $('#' + checkBoxNames[i]).prop('checked', isChecked);
                        dt.column(columnNums[i]).visible(isChecked);
                        $('#' + checkBoxNames[i]).change(function() {
                            dt.column(columnNums[i]).visible(this.checked);
                            saveUserSettings(page,userId,checkBoxNames[i],this.checked,"{{csrf_token()}}");
                        });
                    }
                }else{
                    for (let i = 0; i < checkBoxNames.length; i++){
                        let isChecked = 'false' !== pageSettings[checkBoxNames[i]];
                        $('#' + checkBoxNames[i]).prop('checked', isChecked);
                        dt.column(columnNums[i]).visible(isChecked);
                        $('#' + checkBoxNames[i]).change(function() {
                            dt.column(columnNums[i]).visible(this.checked);
                            saveUserSettings(page,userId,checkBoxNames[i],this.checked,"{{csrf_token()}}");
                        });
                    }
                }
            });
            //----------------------------------------------------------------------------------------------------------

        }

    </script>
@endpush
