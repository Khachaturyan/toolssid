@extends('layouts.app')

@section('third_party_stylesheets')
    <link href="{{ asset('/css/tabulator.css') }}" rel="stylesheet">
    <style>
        input[type="time"]::-webkit-calendar-picker-indicator {
            background: transparent;
            bottom: 0;
            color: transparent;
            cursor: pointer;
            height: auto;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            width: auto;
        }
    </style>
@endsection

@section('content')

    <div class="modal fade" id="modal-alerts" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal-alerts-header"></h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="alert_uid"   name="alert_uid"   value="">
                    <input type="hidden" id="alert_email" name="alert_email" value="">
                    <div class="row">
                        <div class="col-3">
                            <button class="btn btn-success" style="width: 100%;" onclick="$('#select_alert_type').css('visibility','visible');">{{ __('str.settings_personal_alert_add') }}</button>
                        </div>
                        <div class="col-3">
                            <select class="form-control"
                                    id="select_alert_type"
                                    name="select_alert_type"
                                    style="visibility: hidden"
                                    onchange="$('#personal_alert_time_container').css('visibility','visible');"
                                    required>
                                <option value="" selected disabled>{{ __('str.settings_select_alert_type') }}</option>
                                <option value="meeih">{{ __('str.settings_mail_estate_meeih') }}</option>
                                <option value="meces">{{ __('str.settings_mail_estate_meces') }}</option>
                            </select>
                        </div>
                        <div class="col-3">
                            <div class="input-group mb-3" id="personal_alert_time_container" style="visibility: hidden;margin-top: 0px">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 100px">{{ __('str.table_time') }}</span>
                                </div>
                                <input type="time"
                                       id="alert_time"
                                       onchange="$('#personal_alert_save').css('visibility','visible');"
                                       class="form-control"
                                       aria-label="Default"
                                       aria-describedby="inputGroup-sizing-default" />
                            </div>
                        </div>
                        <div class="col-3">
                            <button class="btn btn-success"
                                    id="personal_alert_save"
                                    style="width: 100%;visibility: hidden;"
                                    onclick="onCreatePersonalAlertClick()">{{ __('str.settings_save') }}</button>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped" id="men-alert-table" data-page-length="12"></table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-users" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.settings_select_email') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped" id="men-table" data-page-length="12"></table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-users2" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.settings_select_email') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped" id="men-table2" data-page-length="12"></table>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">
            <div class="card-body">
                <div class="row">
                    <div class="col-3">
                        <button class="btn btn-success" style="width: 100%;" onclick="addMail()">{{ __('str.settings_post_add') }}</button>
                    </div>
                    <div class="col-3">
                        <input type="text" id="mail" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                    </div>
                </div>
                <br><br>
                <h4 style="display:inline">{{ __('str.settings_day_equ_report') }}
                    <img data-toggle="tooltip"
                         data-placement="bottom"
                         title="{{ __('str.hint_alert_equ_day_report') }}"
                         src="/img/help_outline_gray_24dp.svg"
                         style="width : 24px; height: 24px; padding: 2px"/>
                </h4>
                <div class="row">
                    <div class="col-md-3">
                        <button class="btn btn-success"
                                style="width: 100%;margin-top: 20px"
                                onclick="showUserModalScreen('meces')">
                            {{ __('str.settings_recipient') }}
                        </button>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group mb-3">
                            <select class="form-control" id="day_equ_report" name="day_equ_report" required>
                                <option value="0">{{ __('str.table_no') }}</option>
                                <option value="1">{{ __('str.table_yes') }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text"
                                      id="inputGroup-sizing-default"
                                      style="width: 250px">{{ __('str.table_time') }}</span>
                            </div>
                            <input type="time"
                                   id="day_rep_time"
                                   name="day_rep_time"
                                   class="form-control"
                                   aria-label="Default"
                                   aria-describedby="inputGroup-sizing-default"/>
                        </div>
                    </div>
                </div>

                <br><br>

                <h4 style="display:inline">{{ __('str.settings_return_equipment_in_cell_specified_time') }}
                            <img data-toggle="tooltip"
                                 data-placement="bottom"
                                 title="{{ __('str.hint_alert_equ_in_work') }}"
                                 src="/img/help_outline_gray_24dp.svg"
                                 style="width : 24px; height: 24px; padding: 2px"/>
                </h4>

                <div class="row">
                    <div class="col-md-3">
                        <button class="btn btn-success"
                                style="width: 100%;margin-top: 20px"
                                onclick="showUserModalScreen('meeih')">
                            {{ __('str.settings_recipient') }}
                        </button>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group mb-3">
                            <select class="form-control"
                                    id="day_equ_report_equ_work"
                                    name="day_equ_report_equ_work" required>
                                <option value="0">{{ __('str.table_no') }}</option>
                                <option value="1">{{ __('str.table_yes') }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text"
                                      id="inputGroup-sizing-default"
                                      style="width: 250px">{{ __('str.table_time') }}</span>
                            </div>
                            <input type="time"
                                   id="day_rep_time_equ_work"
                                   name="day_rep_time_equ_work"
                                   class="form-control"
                                   aria-label="Default"
                                   aria-describedby="inputGroup-sizing-default"/>
                        </div>
                    </div>
                </div>
                <br><br>

                <div class="row">
                    <div class="col-md-3">
                        <button class="btn btn-success" style="width: 100%;margin-top: 24px" onclick="showPersonalAlertDialogScreen()">{{ __('str.settings_personal_alert') }}</button>
                    </div>
                    <div class="col-3">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_mail_use_custom') }}</span>
                            </div>
                            <select class="form-control" id="mail_custom" name="mail_custom" required>
                                <option value="0">{{ __('str.settings_mail_off') }}</option>
                                <option value="1">{{ __('str.settings_mail_on') }}</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="mail_settings_container" style="display: none">
                    <form method="post" action="/ps-save-mail-action">
                        @csrf
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_mail_port') }}</span>
                            </div>
                            <input type="number" id="mail_port" name="mail_port" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_mail_host') }}</span>
                            </div>
                            <input type="text" id="mail_host" name="mail_host" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_mail_user') }}</span>
                            </div>
                            <input type="email" id="mail_user" name="mail_user" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required value="">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.settings_mail_pass') }}</span>
                            </div>
                            <input type="password" id="mail_pass" name="mail_pass" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                        </div>
                    <br>
                    <button type="submit" class="btn btn-success" style="width: 200px">{{ __('str.settings_save') }}</button>
                </form>
                </div>
                <br><br><br>
                <div id="mail-table" style="margin-top: 16px"></div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script src="{{ asset('/js/tabulator.min.js') }}"></script>

    <script>

        let mailTable          =  null;
        let userTableIsInited  = false;
        let userTable2IsInited = false;
        let alertTableIsInited = false;

        function onCreatePersonalAlertClick(){
            let uid = $('#alert_uid').val();
            let post ={};
            post['_token'] = "{{csrf_token()}}";
            post['uid'   ] = uid;
            post['time'  ] = $('#alert_time').val();
            post['email' ] = $('#alert_email').val();
            post['estate'] = $('#select_alert_type').val();
            $.post( "/personal-alert-create", post, function(data, status) {
                if (status === 'success'){
                    $('#select_alert_type').css('visibility','hidden');
                    $('#personal_alert_time_container').css('visibility','hidden');
                    $('#personal_alert_save').css('visibility','hidden');
                    if ("true" === data){
                        $('#men-alert-table').DataTable().ajax.url('/men-for-alert-person/' + uid).load();
                    }else{
                        alert(data);
                    }
                }
            });
        }

        function removePersonalAlert(id,uid){
            let post ={};
            post['_token'] = "{{csrf_token()}}";
            post['id'    ] = id;
            $.post( "/personal-alert-remove", post, function(data, status) {
                if (status === 'success'){
                    if ("true" === data){
                        $('#men-alert-table').DataTable().ajax.url('/men-for-alert-person/' + uid).load();
                    }else{
                        alert(data);
                    }
                }
            });
        }

        function showMenAlertDialogScreen(uid, email){
            $('#alert_uid').val(uid);
            $('#alert_email').val(email);
            $('#modal-alerts').modal('show');
            $('#modal-users2').modal('hide');
            $('#modal-alerts-header').text(email);
            if (alertTableIsInited){
                $('#men-alert-table').DataTable().ajax.url('/men-for-alert-person/' + uid).load();
            }else{
                alertTableIsInited = true;
                $('#men-alert-table').DataTable({
                    processing: true,
                    searching: false,
                    serverSide: true,
                    paging: false,
                    dom: "frtip",
                    ajax: '/men-for-alert-person/' + uid,
                    columns: [
                        { data: 'remove',     title:'{{ __('str.table_remove') }}', className: 'text-center'  },
                        { data: 'time_des',   name: 'men.post', title:'{{ __('str.table_time') }}' },
                        { data: 'estate_des', name: 'users.email', title:'{{ __('str.table_descr') }}' },
                    ]
                }).on('click', 'tbody td', function() {
                    let tr  = $(this).closest('tr');
                    let row = $('#men-alert-table').DataTable().row(tr);
                    if (this.cellIndex == 0){
                        removePersonalAlert(row.data().id,$('#alert_uid').val());
                    }
                });
            }
        }

        function showPersonalAlertDialogScreen(){
            $('#modal-users2').modal('show');
            if (userTable2IsInited){
                $('#men-table2').DataTable().ajax.url('/men-for-alert-person').load();
            }else{
                userTable2IsInited = true;
                $('#men-table2').DataTable({
                    processing: true,
                    serverSide: true,
                    dom: "frtip",
                    ajax: '/men-for-alert-person',
                    columns: [
                        { data: 'time_des',   title:'{{ __('str.settings_personal_alert2') }}', className: 'text-center'  },
                        { data: 'email', name: 'users.email', title:'{{ __('str.table_feedback_email') }}' },
                        { data: 'post', name: 'men.post', title:'{{ __('str.table_post') }}' },
                        { data: 'msurname', name: 'men.msurname', title:'{{ __('str.table_surname') }}' },
                        { data: 'mname', name: 'men.mname', title:'{{ __('str.table_name2') }}' },
                        { data: 'mpatronymic', name: 'men.mpatronymic', title:'{{ __('str.mens_patronymic') }}'  },
                    ]
                }).on('click', 'tbody td', function() {
                    let tr  = $(this).closest('tr');
                    let row = $('#men-table2').DataTable().row(tr);
                    showMenAlertDialogScreen(row.data().web_uid, row.data().email);
                });
            }
        }

        //--- функция сохраняет настройки ------------------------------------------------------------------------------
        function saveSetting(key,value){
            let post ={};
            post['_token'] = "{{csrf_token()}}";
            post['label' ] = key;
            post['data'  ] = value;
            $.post( "/set-any-data", post, function(data, status) {
                if (status === 'success'){
                    //console.log(data);
                    if ("true" !== data){
                        console.log(data);
                    }
                }
            });
        }
        //--------------------------------------------------------------------------------------------------------------
        function showUserModalScreen(estate){
            $('#modal-users').prop('estate',estate);
            $('#modal-users').modal('show');
            if (userTableIsInited){
                $('#men-table').DataTable().ajax.url('/men-for-alert/' + estate).load();
            }else{
                userTableIsInited = true;
                $('#men-table').DataTable({
                    processing: true,
                    serverSide: true,
                    dom: "frtip",
                    ajax: '/men-for-alert/' + estate,
                    columns: [
                        { data: 'is_checked',   title:'{{ __('str.table_linkin2') }}', className: 'text-center'  },
                        { data: 'post', name: 'men.post', title:'{{ __('str.table_post') }}' },
                        { data: 'email', name: 'users.email', title:'{{ __('str.table_feedback_email') }}' },
                        { data: 'msurname', name: 'men.msurname', title:'{{ __('str.table_surname') }}' },
                        { data: 'mname', name: 'men.mname', title:'{{ __('str.table_name2') }}' },
                        { data: 'mpatronymic', name: 'men.mpatronymic', title:'{{ __('str.mens_patronymic') }}'  },
                    ]
                }).on('click', 'tbody td', function() {
                    let tr  = $(this).closest('tr');
                    let row = $('#men-table').DataTable().row(tr);
                    let isChecked = row.data().is_checked.length > 0;
                    isChecked = !isChecked;
                    let _estate = $('#modal-users').prop('estate');
                    changeStateChecked(_estate,row.data().email,isChecked,function (){
                        showUserModalScreen(_estate);
                    })
                });
            }
        }
        //--------------------------------------------------------------------------------------------------------------

        function updateDayRepTimeControlEnable(needSaveServer){
            $('#day_rep_time').prop('disabled', $('#day_equ_report').val() == 0);
            if (needSaveServer){
                saveSetting('day_equ_report',$('#day_equ_report').val());
            }
        }

        function updateDayRepTimeEquWorkControlEnable(needSaveServer){
            $('#day_rep_time_equ_work').prop('disabled', $('#day_equ_report_equ_work').val() == 0);
            if (needSaveServer){
                saveSetting('day_equ_report_equ_work',$('#day_equ_report_equ_work').val());
            }
        }

        function isEmail(email) {
            let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        function updateAlertMails(){
            $.get( "/alert-mails", function(d, status) {
                if (status === 'success'){
                    //console.log(d);
                    mailTable.setData(JSON.parse(d));
                }
            });
        }

        function updateMailSettingsContainer(needSaveServer){
            let isAuto = parseInt($("#mail_custom").val()) === 0;
            $("#mail_settings_container").css("display",isAuto ? "none" : "block");
            if (needSaveServer){
                saveSetting('set_mail_custom',$("#mail_custom").val());
            }
        }

        function stateFormatter(estateKey,estate){
            if ('mespb' === estateKey){
                return '<img src="/img/done_gray_24dp.svg" style="width : 24px; height: 24px; padding: 6px"/>';
            }
            return estate.includes(estateKey) ?
                '<img src="/img/done_green_24dp.svg" style="width : 24px; height: 24px; padding: 6px"/>' : "";
        }

        function addMail(){
            let mail = $('#mail').val();
            if (!isEmail(mail)){
                alert("{{ __('str.settings_post_wrong') }}");
                return
            }
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            post['mail'   ] = mail;
            $.post( "/add-new-mail-alert", post, function(data, status) {
                if (status === 'success'){
                    if ("true" === data){
                        $('#mail').val('');
                        updateAlertMails();
                    }else{
                        alert("{{ __('str.settings_add_post_error') }} - " + data);
                    }
                }
            });
        }

        function removeMail(id){
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            post['id'   ] = id;
            $.post( "/remove-mail-alert", post, function(data, status) {
                if (status === 'success'){
                    //console.log(data);
                    if ("true" === data){
                        $('#mail').val('');
                        updateAlertMails();
                        //alert("{{ __('str.settings_remove_post_ok') }}");
                    }else{
                        alert("{{ __('str.settings_remove_post_error') }} - " + data);
                    }
                }
            });
        }

        //--- изменение состояния рассылки -----------------------------------------------------------------------------
        function changeStateChecked(estateKey,mail,isChecked,trueCallBack){
            if (mail == null || mail.length === 0){
                console.log("mail is empty!")
                return;
            }
            let post = {};
            post['_token' ] = "{{csrf_token()}}";
            post['mail'   ] = mail;
            post['estate' ] = estateKey;
            post['checked'] = isChecked ? 'true' : 'false';
            $.post( "/alert-mail-update-state", post, function(data, status) {
                if (status === 'success'){
                    //console.log(data);
                    if ("true" === data){
                        trueCallBack();
                    }else{
                        alert("{{ __('str.settings_update_post_error') }} - " + data);
                    }
                }
            });
        }
        function changeState(estateKey, row, trueCallBack){
            changeStateChecked(estateKey,row.getData().mail, row.getData().estate.includes(estateKey) ? false : true, trueCallBack);
        }
        //--------------------------------------------------------------------------------------------------------------

        $(document).ready(function(){
            mailTable = new Tabulator("#mail-table", {
                layout:"fitColumns",
                columns:[
                    {title:"", hozAlign:"center", editor:"input", editable:false, cellClick:function(e, cell){ removeMail(cell.getRow().getData().id); }, formatter: function(cell) {return '<img src="img/delete_black_24dp.svg" style="width: 16px; height: 16px">'}, width:56},
                    {title:"№", hozAlign:"center", editor:"input", field:"id", editable:false, width:56},
                    {title:"{{ __('str.settings_mail') }}",  field:"mail", editor:"input", editable:false},
                    {title:"{{ __('str.settings_mail_estate_mernwe') }}",  hozAlign:"center", field:"estate",  editor:"input", cellClick:function(e, cell){ changeState("mernwe", cell.getRow(), updateAlertMails); }, editable:false, formatter:function(cell) { return stateFormatter("mernwe", cell.getRow().getData().estate); }},
                    {title:"{{ __('str.settings_mail_estate_meuue') }}",   hozAlign:"center", field:"estate",  editor:"input", cellClick:function(e, cell){ changeState("meuue",  cell.getRow(), updateAlertMails); }, editable:false, formatter:function(cell) { return stateFormatter("meuue",  cell.getRow().getData().estate); }},
                    //{title:"{{ __('str.settings_mail_estate_meces') }}",   hozAlign:"center", field:"estate",  editor:"input", cellClick:function(e, cell){ changeState("meces",  cell.getRow(), updateAlertMails); }, editable:false, formatter:function(cell) { return stateFormatter("meces",  cell.getRow().getData().estate); }},
                    {title:"{{ __('str.settings_mail_estate_mespb') }}",   hozAlign:"center", field:"estate",  editor:"input", /*cellClick:function(e, cell){ changeState("mespb",  cell.getRow()); },*/ editable:false, formatter:function(cell) { return stateFormatter("mespb",  cell.getRow().getData().estate); }},
                ],
            });
            $('#mail_custom').val("{{$set_mail_custom}}");
            $('#mail_port').val("{{$set_mail_port}}");
            $('#mail_host').val("{{$set_mail_host}}");
            $('#mail_user').val("{{$set_mail_user}}");
            $('#mail_pass').val("{{$set_mail_pass}}");
            updateAlertMails();
            //----------------------------------------------------------------------------------------------------------
            $('#day_equ_report').val("{{$day_equ_report}}");
            updateDayRepTimeControlEnable(false);
            $("#day_equ_report").change(function() {
                updateDayRepTimeControlEnable(true);
            });
            $('#day_rep_time').val("{{$day_rep_time}}");
            $("#day_rep_time").change(function() {
                saveSetting('day_rep_time',$('#day_rep_time').val());
            });
            //----------------------------------------------------------------------------------------------------------
            $('#day_equ_report_equ_work').val("{{$day_equ_report_equ_work}}");
            updateDayRepTimeEquWorkControlEnable(false);
            $("#day_equ_report_equ_work").change(function() {
                updateDayRepTimeEquWorkControlEnable(true);
            });
            $('#day_rep_time_equ_work').val("{{$day_rep_time_equ_work}}");
            $("#day_rep_time_equ_work").change(function() {
                saveSetting('day_rep_time_equ_work',$('#day_rep_time_equ_work').val());
            });
            //----------------------------------------------------------------------------------------------------------
            $("#mail_custom").val("{{$set_mail_custom}}");
            updateMailSettingsContainer(false);
            $("#mail_custom").change(function() {
                updateMailSettingsContainer(true);
            });
            //----------------------------------------------------------------------------------------------------------
            $('#modal-alerts').on('hidden.bs.modal', function (e) {
                showPersonalAlertDialogScreen();
            })
        });
    </script>
@endpush
