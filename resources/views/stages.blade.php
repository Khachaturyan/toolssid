@extends('layouts.app')

@section('nav-bar-container')
    <button class="btn btn-success" id="add-new" onclick="showModal()">+</button>
    @if (Route::currentRouteName() == 'stages')
        <button class="btn btn-success" onclick="window.location.href='/stages-arc'" style="margin-left: 12px">{{ __('str.stage_show_archive') }}</button>
    @else
        <button class="btn btn-success" onclick="window.location.href='/stages'" style="margin-left: 12px">{{ __('str.stage_show_current') }}</button>
    @endif
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">

            <div class="modal fade" id="modal-screen" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modalLabelHeader" style="margin-top: 0px;">{{ __('str.stage_object') }}</h4>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="/stage-action">
                                @csrf

                                <input type="hidden" id="id" name="id" value="">

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.stage_name') }}</span>
                                    </div>
                                    <input type="text" id="name" name="name" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.stage_address') }}</span>
                                    </div>
                                    <input type="text" id="adr" name="adr" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.stage_in_archive') }}</span>
                                    </div>
                                    <select class="form-control" id="is_arc" name="is_arc" required>
                                        <option value="" selected disabled>{{ __('str.stage_select_status') }}</option>
                                        <option value="0">{{ __('str.stage_not_archive') }}</option>
                                        <option value="1">{{ __('str.stage_in_archive2') }}</option>
                                    </select>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-success" style="width: 200px">{{ __('str.stage_save') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="modal-header">
                    <h4 class="modal-title">{{ $table_header }}</h4>
                </div>
                {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                    "data-page-length" => "100",
                    "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{$dataTable->scripts()}}

    <script>
        function fillModal(data){
            if (data == null){
                $('#id').val("");
                $('#name').val("");
                $('#adr').val("");
                $('#is_arc').val(0);
            }else{
                $('#id').val(data.id);
                $('#name').val(data.name);
                $('#adr').val(data.adr);
                $('#is_arc').val(data.is_arc);
            }
        }
        function showModal(){
            fillModal(null);
            $('#modal-screen').modal('show');
        }
        $(document).ready(function(){
            //--- обработка кликов по таблице --------------------------------------------------------------------------
            $('#block-table').on('click', 'tbody td', function() {
                let tr  = $(this).closest('tr');
                let row = $('#block-table').DataTable().row(tr);
                if (this.cellIndex == 0){
                    fillModal(row.data());
                    $('#modal-screen').modal('show');
                }
            });
            //----------------------------------------------------------------------------------------------------------
        });
    </script>

@endpush
