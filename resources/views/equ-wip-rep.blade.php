@extends('layouts.app')

@section('third_party_stylesheets')
    <link href="{{ asset('/dist/css/tabulator.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
                <p>{{ __('str.equ_tsd_report') }}</p>
                <div id="cur-equ-table" style="margin-top: 16px"></div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script src="{{ asset('/dist/js/tabulator.min.js') }}"></script>

    <script>

        function menFormatter(cell){
            return cell.getRow().getData().msurname + " " + cell.getRow().getData().mname;
        }

        function typeFormatter(cell){
            return cell.getRow().getData().tname + " " + cell.getRow().getData().tmark + " " + cell.getRow().getData().tmodel;
        }

        function takeFormatter(cell){
            return timeConvert(cell.getRow().getData().created_at);
        }

        function returnFormatter(cell){
            return timeConvert(cell.getRow().getData().ret);
        }

        function numFormatter(cell){
            return cell.getRow().getPosition(true) + 1;
        }

        function placeFormatter(cell){
            return placeFormatter2(cell.getRow().getData());
        }

        function stateFormatter(cell){
            let state = cell.getRow().getData().state;
            if (state == 0) {
                return "{{ __('str.equ_state_0') }}";
            }
            if (state == 1) {
                cell.getElement().style.color= "#00FF00";
                return "{{ __('str.equ_state_1') }}";
            }
            cell.getElement().style.color= "#FF0000";
            return "{{ __('str.equ_state_2') }}";
        }

        function placeFormatter2(data){
            if (data.uid > 0){
                return data.msurname + " " + data.mname;
            }
            if (data.cid > 0) {
                return "{{ __('str.block') }}" + " " + data.bname + ", " + "{{ __('str.cell') }}" + " " + data.block_pos;
            }
            return "{{ __('str.equ_no_place') }}";
        }

        function placeFormatter3(cell){
            let id = cell.getRow().getData().eid;
            let str = "";
            for (let i = 0; i < curEquData.length; i++){
                if (curEquData[i].id == id){
                    str = placeFormatter2(curEquData[i]);
                    break;
                }
            }
            return str;
        }

        function timeConvert(t){
            if (typeof t === 'string'){
                let strArr = t.split(".")[0].split("T");
                let parts = strArr[0].split("-");
                let parts2 = strArr[1].split(":");
                return parts[2] + "." + parts[1] + "." + parts[0] + " " + parts2[0] + ":" + parts2[1];
            }else{
                return "";
            }
        }

        $(document).ready(function(){
            let curEquTable = new Tabulator("#cur-equ-table", {
                virtualDomHoz:true,
                layout:"fitColumns",
                groupStartOpen:false,
                columns:[
                    {title:"№",  hozAlign:"center", editor:"input", editable:false, width:50, formatter:numFormatter},
                    {title:"{{ __('str.equipment_take_time') }}",  hozAlign:"center", field:"created_at",  editor:"input", editable:false, width:150, formatter:takeFormatter},
                    {title:"{{ __('str.table_state2') }}",  hozAlign:"center", editor:"input", editable:false, width:100, field:"state", formatter:stateFormatter},
                    {title:"{{ __('str.table_inv_num') }}",  hozAlign:"center", editor:"input", editable:false, width:100, field:"inv_num"},
                    {title:"{{ __('str.mens_worker') }}",  field:"msurname",  editable:false, width:300, formatter:menFormatter},
                    {title:"{{ __('str.equ_equ') }}",  field:"tname", editor:"input", editable:true,  formatter:typeFormatter},
                    ],
            });
            curEquTable.setData(@json($items));
        });
    </script>

@endpush

