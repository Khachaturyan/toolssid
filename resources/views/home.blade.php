@extends('layouts.app')

@section('third_party_stylesheets')
    <style>
        input[type="date"]::-webkit-calendar-picker-indicator {
            background: transparent;
            bottom: 0;
            color: transparent;
            cursor: pointer;
            height: auto;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            width: auto;
        }
    </style>
@endsection

@section('nav-bar-container')
    <button class="btn btn-success" onclick="remote()">{{ __('str.home_control') }}</button>
    <button class="btn btn-success" onclick="window.location.href='/events-all'">{{ __('str.home_all_events') }}</button>
    <button class="btn btn-success" onclick="openClearLogScreen()">{{ __('str.home_logs_clear') }}</button>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">

            <div class="row" style="display: none">
                <div class="col-md-4 col-sm-6 col-12">
                    <div style="margin: 12px">
                        <div class="info-box">
                            <span class="info-box-icon bg-info"><i class="far fa-bookmark"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">{{ __('str.home_total') }}:</span>
                                <span class="info-box-text">{{ __('str.home_in_work') }}: {{ $info[-1]['inWork']     }}</span>
                                <span class="info-box-text">{{ __('str.home_busy') }}:  {{ $info[-1]['busy']       }}</span>
                                <span class="info-box-text">{{ __('str.home_cons') }}:  {{ $info[-1]['consCount']  }}</span>
                                <span class="info-box-text">{{ __('str.home_for_renovation') }}: {{ $info[-1]['notWorking'] }}</span>
                                <span class="info-box-text">_</span>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach ($blocks as $block)
                    @if ($block->id > 0)
                        <div class="col-md-4 col-sm-6 col-12">
                            <div style="margin: 12px">
                                <div class="info-box bg-gradient-warning">
                                    <span class="info-box-icon"><i class="far fa-calendar-alt"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">{{ __('str.home_block') }}: "{{ $block->name }}"</span>
                                        <span class="info-box-text">{{ __('str.home_cells') }}: {{ $block->cell_count }}</span>
                                        <span class="info-box-text">{{ __('str.home_in_work') }}: {{ $info[$block->id]['inWork'] }}</span>
                                        <span class="info-box-text">{{ __('str.home_busy') }}: {{ $info[$block->id]['busy'] }}</span>
                                        <span class="info-box-text">{{ __('str.home_cons') }}: {{ $info[$block->id]['consCount'] }}</span>
                                        <span class="info-box-text">{{ __('str.home_for_renovation') }}: {{ $info[$block->id]['notWorking'] }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="row">
                <div class="col-6">
                    <canvas id="myChart1" width="400" height="0"></canvas>
                </div>
                <div class="col-6">
                    <canvas id="myChart2" width="400" height="0"></canvas>
                </div>
            </div>
            <div class="row">
                <div class="card-body">
                    {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                                "data-page-length" => "100",
                                "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-screen" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.home_alert') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.home_block') }}</span>
                        </div>
                        <select class="form-control" id="bid" name="bid" required>
                            <option value="" selected disabled>{{ __('str.home_block_select') }}</option>
                            @foreach($blocks as $block)
                                @if ($block->id > 0) <option value="{{$block->id}}">{{$block->name}}</option> @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.home_command') }}</span>
                        </div>
                        <input type="text" id="com" name="com" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                    </div>
                    <button type="submit" class="btn btn-primary" style="width: 200px" onclick="sendCom()">{{ __('str.home_send') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="clear-logs-screen" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.home_logs_clear') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend" >
                            <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.home_period_before') }}</span>
                        </div>
                        <input type="date" id="log_clear_date" name="log_clear_date" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
                    </div>
                </div>

                <button type="button" class="btn btn-primary" style="width: 200px; margin: 32px;" onclick="beginClearLogs()">{{ __('str.home_clear') }}</button>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    {{$dataTable->scripts()}}

    <script src="{{ asset('/js/tableSelectors.js') }}"></script>

    <script>

        $( document ).ajaxError(function( event, jqxhr, settings, thrownError ) {
            console.log("ajaxError: settings=" + settings + " jqxhr=" + jqxhr + " thrownError=" + thrownError);
            if (typeof settings === "string" && settings.indexOf('401') > -1) {
                //you can probably do something here
            }
        });

        function onInitComplete(dataTable) {

            // включение данных с инпута cells-block_pos  в запрос ajax,
            // до отправки запроса с помощью обработчика событий preXhr.dt
            const table = $('#log-table');
            table.on('preXhr.dt', function (e, settings, data) {
                data.block_pos = $('#cells-block_pos').val();
                //console.log(data.block_pos);
            });
            $('#cells-block_pos').on('change', function() {
                table.DataTable().ajax.reload();
                return false;
            });
            // ---------------------------------------------------------------------------------------------------------

            createTableSelectors(dataTable,["stages.name", "blocks.name", "cells.block_pos"],
                {"stages.name" : "is_arc = false","blocks.name" : "is_arc = false"},
                function (selector) {
                    if (selector.id === "cells-block_pos"){
                        selector.style.display = "none";
                    }
                },
                function (selector) {
                    if ($(selector).prop('id') == "blocks-name"){
                        if ($(selector).val().length == 0){
                            $('#cells-block_pos').val("");
                            $('#cells-block_pos').css("display", "none");
                            $('#cells-block_pos').change();
                        }else{
                            $('#cells-block_pos').css("display", "block");
                        }
                    }
                },null
            );
        }

        function beginClearLogs(){
            let date = $('#log_clear_date').val();
            if (date.length == 0){
                alert('{{ __('str.home_no_date') }}');
                return
            }
            if (confirm('{{ __('str.home_log_clear_ask') }}' + " " + date + "?")){
                $.get( '/logs-clear/' + date, function(data, status) {
                    if (status === 'success'){
                        if ('true' === data){
                            window.location.reload();
                        }else{
                            alert(data);
                            console.log(data);
                        }
                    }
                });
            }
        }

        function remote(){
            $('#modal-screen').modal('show');
        }

        function openClearLogScreen(){
            $('#clear-logs-screen').modal('show');
        }

        function sendCom(){
            let bid = $('#bid').val();
            if (bid == null || bid.length == 0){
                alert('{{ __('str.home_need_block') }}');
                return;
            }
            let com =  $('#com').val();
            if (com.length == 0){
                alert('{{ __('str.home_need_command') }}');
                return;
            }
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            post['bid'    ] = bid;
            post['com'    ] = com;
            $.post( "/and-comm", post, function(data, status) {
                if (status === 'success'){
                    if ("true" === data){
                        $('#bid').val("");
                        $('#com').val("");
                        $('#modal-screen').modal('hide');
                    }else{
                        console.log(data);
                    }
                }
            });
        }
    </script>
@endpush
