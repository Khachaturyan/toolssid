@extends('layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="" style="margin-top: 20px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card mt-1 mb-1" onclick="openBlock('all')">
                                        <div>
                                            <div class="row">
                                                <div class="col-6">
                                                    <h5><b>Всего по блокам</b></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="blockall" class="mt-1 blocksy">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Тип</th>
                                                    <th>Исправное</th>
                                                    <th>Неисправное</th>
                                                    <th>В работе</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($types as $item)
                                                    <tr>
                                                        <td>
                                                            <div class="d-flex align-items-center">
                                                                <div class="avatar rounded">
                                                                    <div class="avatar-content">
                                                                        <img src="../../../app-assets/images/icons/toolbox.svg" alt="Toolbar svg">
                                                                    </div>
                                                                </div>
                                                                <div>
                                                                    <div >{{$item->tname}} {{$item->tmark}} {{$item->tmodel}}</div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            {{$item->work($item->id, 1)->count()}}
                                                        </td>
                                                        <td>
                                                            {{$item->work($item->id, 0)->count()}}
                                                        </td>
                                                        <td>
                                                            {{$item->count_men}}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        @foreach($blocks as $block)
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card mt-1 mb-1" onclick="openBlock({{$block->id}})">
                                            <div>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <h5><b>Блок {{$block->st_id}} {{$block->name}}</b></h5>
                                                        <p style="margin-bottom: 10px">Всего {{$block->cell_count}} ячеек</p>
                                                    </div>
                                                    <div class="col-6" style="text-align:right;">
                                                        <p class="mb-0">Занято <b>{{$block->items($block->id)->count()}}</b></p>
                                                        <p class="mb-0">Неисправно <b>{{$block->itemsWarning($block->id)->count()}}</b></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="block{{$block->id}}" class="mt-1 blocksy">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>Тип</th>
                                                        <th>Исправное</th>
                                                        <th>Неисправное</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($block->items($block->id)->unique('tid') as $item)
                                                        <tr>
                                                            <td>
                                                                <div class="d-flex align-items-center">
                                                                    <div class="avatar rounded">
                                                                        <div class="avatar-content">
                                                                            <img src="../../../app-assets/images/icons/toolbox.svg" alt="Toolbar svg">
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <div >{{$item->tip($item->tid)->tname}} {{$item->tip($item->tid)->tmodel}}</div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                {{$item->work($item->tid, $block->id, 1)->count()}}
                                                            </td>
                                                            <td>
                                                                {{$item->work($item->tid, $block->id, 0)->count()}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    </div>
        </div>
    </div>
@endsection

<style>
    .avatar{
        background-color: #f8f8f8!important;
        margin-right: 10px;
    }
    .blocksy{
        height: 300px;
        overflow-y: auto;
        overflow-x: auto;
    }
</style>

@push('scripts')
    <script>
        function openBlock(id){
            if(document.getElementById('block'+id).style.display == 'none'){
                $('#block'+id).css({'display': 'block'});
            }
            else{
                $('#block'+id).css({'display': 'none'});
            }
        }
    </script>
@endpush
