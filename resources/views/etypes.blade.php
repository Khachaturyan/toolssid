@extends('layouts.app')

@section('nav-bar-container')
    <button class="btn btn-success" id="add-new" onclick="showCreateTypeModalScreen()">+</button>
    <button class="btn btn-success" id="etype-show-hide" style="margin-left: 16px" onclick="window.location='etypes-all'">{{ __('str.etypes_hide_main_header') }}</button>
    <button class="btn btn-success" id="etype-show-all" style="margin-left: 16px" onclick="window.location='etypes'">{{ __('str.et_show_all') }}</button>
@endsection

@section('content')

    <div class="modal fade" id="modal-screen" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader" style="margin-top:0">{{ __('str.et_equ_type') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body" >
                    <form id="e-type-form" method="post" action="/etype-create-action" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="comp_data" id="comp_data" value="">
                        <div class="input-group">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.et_type') }}</span>
                            </div>
                            <input type="text" id="tname" name="tname" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.et_mark') }}</span>
                            </div>
                            <input type="text" id="tmark" name="tmark" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.et_model') }}</span>
                            </div>
                            <input type="text" id="tmodel" name="tmodel" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.et_is_cons') }}</span>
                            </div>
                            <select class="form-control" name="is_cons" id="is_cons" required>
                                <option value="" selected disabled>{{ __('str.et_select_yes_no') }}</option>
                                <option value="1">{{ __('str.et_yes') }}</option>
                                <option value="0">{{ __('str.et_no') }}</option>
                            </select>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.et_is_comp') }}</span>
                                    </div>
                                    <select class="form-control" name="is_comp" id="is_comp"
                                            onchange="on_is_comp_select_change(this);" required>
                                        <option value="" selected disabled>{{ __('str.et_select_yes_no') }}</option>
                                        <option value="0">{{ __('str.et_no') }}</option>
                                        <option value="1">{{ __('str.et_yes') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 mt-4">
                                <button type="button" id="bShowSelectSubtypesScreen" class="btn btn-success" style="width: 100%;height: 38px; visibility: hidden" onclick="showSelectSubtypesScreen();">{{ __('str.et_comp_edit') }}</button>
                            </div>
                        </div>
                        <label id="kit-description" style="visibility: hidden"></label>
                        <!-- <br><br> -->
                        <div class="input-group">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.size') }}</span>
                            </div>
                            <select class="form-control" id="csid" name="csid" required>
                                <option value="0" selected>{{ __('str.size_default') }}</option>
                                @foreach($cellSizes as $cellSize)
                                    <option value="{{$cellSize->id}}">{{$cellSize->name}} {{$cellSize->des}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.et_is_hide_type') }}</span>
                            </div>
                            <select class="form-control" name="hide" id="hide" required>
                                <option value="" selected disabled>{{ __('str.et_select_yes_no') }}</option>
                                <option value="1">{{ __('str.et_yes') }}</option>
                                <option value="0">{{ __('str.et_no') }}</option>
                            </select>
                        </div>
                        <br>
                        <input type="file" name="file" class="form-control mb-3" style="padding: 16px;height: 54px">
                        <!-- <br> -->
                        <button type="button" class="btn btn-success" style="width: 200px" onclick="formValidation();">{{ __('str.et_save') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="select-type-screen" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="selectTipeScreenHeader">{{ __('str.et_equ_type') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped" id="etypes-table" data-page-length="10"></table>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">
            <div class="card-body">
                <div class="modal-header">
                    <h4 class="modal-title" style="margin-top:0">{{ $table_header }}</h4>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li> {{$error}} </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                    "data-page-length" => "100",
                    "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}
            </div>
        </div>
    </div>

@endsection

@push('scripts')

    {{$dataTable->scripts()}}

    <script>

        let etypesTableIsInit = false;

        function initUnitModalScreenVisibleListener(){
            let oldState = null;
            setInterval(function () {
                    let newState = $('#select-type-screen').is(':visible');
                    if (oldState != null && newState != oldState){
                        if (newState == false){
                            $('#modal-screen').show();
                        }
                    }
                    oldState = newState;
            },500);
        }

        function on_is_comp_select_change(v){
            if (v.value === '1'){
                updateCompDataDescription();
            }else{
                $('#kit-description').text('');
            }
            $('#bShowSelectSubtypesScreen').css('visibility',v.value === '1' ? 'visible' : 'hidden');
            $('#kit-description').css('visibility',v.value === '1' ? 'visible' : 'hidden');
        }

        function updateCompDataDescription(){
            let comp_data = $('#comp_data').val();
            if (comp_data == null || comp_data.length == 0) {
                $('#kit-description').text('');
                return;
            }
            $.get( "/equ-types-des/" + comp_data, function(data, status) {
                if (status === 'success') {
                    $('#kit-description').text(data);
                }
            });
        }

        function changeCompDataValue(id,is_checked){
            let str = $('#comp_data').val();
            let arr1 = [];
            let arr2 = str.split(",");
            for (let i  = 0; i < arr2.length; i++){
                if (arr2[i].length > 0){
                    arr1.push(parseInt(arr2[i]));
                }
            }
            if (is_checked){
                let index = arr1.indexOf(id);
                if (index !== -1) {
                    arr1.splice(index, 1);
                }
            }else{
                if (!arr1.includes(id)) arr1.push(id);
            }
            str = "";
            for (let i  = 0; i < arr1.length; i++){
                if (str.length > 0) str += ",";
                str += arr1[i];
            }
            $('#comp_data').val(str);
            if (str.length == 0) str += ",";
            $('#etypes-table').DataTable().ajax.url('/et-select/' + str).load();
            updateCompDataDescription();
        }

        function showSelectSubtypesScreen(){
            let str = $('#comp_data').val();
            if (str.length == 0) str += ",";
            $('#selectTipeScreenHeader').text("{{__('str.et_equ_type')}}" + " " + "{{__('str.et_comp_for')}}" +
                " " + $('#tname').val() + " " + $('#tmark').val() + " " + $('#tmodel').val());
            if (etypesTableIsInit){
                $('#etypes-table').DataTable().ajax.url('/et-select/' + str).load();
            }else{
                etypesTableIsInit = true;
                $('#etypes-table').DataTable({
                    processing: true,
                    serverSide: true,
                    dom: "frtip",
                    ajax: '/et-select/' + str,
                    columns: [
                        { data: 'is_checked', title:'{{ __('str.equ_select') }}', className: 'text-center'             },
                        { data: 'tname', name: 'tname', title:'{{ __('str.table_type') }}', className: 'text-center'   },
                        { data: 'tmark', name: 'tmark', title:'{{ __('str.table_mark') }}', className: 'text-center'   },
                        { data: 'tmodel', name: 'tmodel', title:'{{ __('str.table_model') }}', className: 'text-center'},
                    ]
                }).on('click', 'tbody td', function() {
                    let tr  = $(this).closest('tr');
                    let row = $('#etypes-table').DataTable().row(tr);
                    let is_checked = row.data().is_checked.length > 0;
                    changeCompDataValue(row.data().id,is_checked);
                    //alert(row.data().id + " " + is_checked);
                }).on('dblclick', 'tbody td', function() {
                });
            }
            $('#select-type-screen').modal('show');
            $('#modal-screen').hide();
        }

        function formValidation(form){
            let id = $('#id').val();
            let isHide = $('#hide').val() == "1";
            if (isHide && id.length > 0){
                $.get( "/type-has-equ/" + id, function(data, status) {
                    if (status === 'success'){
                        if ('true' == data){
                            alert("Тип устройства не может быть скрытым так как имеются несписанные устройства принадлежащие этому типу.");
                        }else if ('false' == data){
                            $("#e-type-form").submit();
                        }else{
                            alert(data);
                            console.log(data);
                        }
                    }
                });
            }else{
                $("#e-type-form").submit();
            }
        }

        function fillModal(data){
            if (data == null){
                $('#id').val("");
                $('#tname').val("");
                $('#tmark').val("");
                $('#tmodel').val("");
                $('#is_cons').val("0");
                $('#csid').val("0");
                $('#hide').val("0");
                $('#is_comp').val("0");
                $('#comp_data').val("");
                $('#bShowSelectSubtypesScreen').css('visibility','hidden');
                $('#kit-description').css('visibility','hidden');
            }else{
                $('#id').val(data.id);
                $('#tname').val(data.tname);
                $('#tmark').val(data.tmark);
                $('#tmodel').val(data.tmodel);
                $('#is_cons').val(data.is_cons);
                $('#csid').val(data.csid);
                $('#hide').val(data.hide);
                $('#is_comp').val(data.is_comp);
                $('#comp_data').val(data.comp_data);
                $('#bShowSelectSubtypesScreen').css('visibility',data.is_comp ? 'visible' : 'hidden');
                $('#kit-description').css('visibility',data.is_comp ? 'visible' : 'hidden');
            }
            updateCompDataDescription();
        }

        function showCreateTypeModalScreen(){
            fillModal(null);
            $('#modal-screen').modal('show');
        }

        $(document).ready(function(){
            let isNotEtypesUrl = "{{  Request::url() }}".lastIndexOf("etypes-all") >= 0;
            if (isNotEtypesUrl){
                $("#add-new").css('display','none');
                $("#etype-show-hide").css('display','none');
            }else{
                $("#etype-show-all").css('display','none');
            }
            //--- обработка кликов по таблице --------------------------------------------------------------------------
            $('#block-table').on('click', 'tbody td', function() {
                let tr  = $(this).closest('tr');
                let row = $('#block-table').DataTable().row(tr);
                if (this.cellIndex == 1){
                    fillModal(row.data());
                    $('#modal-screen').modal('show');
                }
            });
            //----------------------------------------------------------------------------------------------------------
            initUnitModalScreenVisibleListener();
        });
    </script>
@endpush
