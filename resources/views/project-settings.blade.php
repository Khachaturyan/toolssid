@extends('layouts.app')

@section('third_party_stylesheets')
    <link href="{{ asset('/css/tabulator.css') }}" rel="stylesheet">
    <style>
        input[type="time"]::-webkit-calendar-picker-indicator {
            background: transparent;
            bottom: 0;
            color: transparent;
            cursor: pointer;
            height: auto;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            width: auto;
        }
        /* Стили для поля "режим отладки" */
        .form-control__switch-dark {
            border-color: #3b4253;
        }
        body.dark-layout .form-control__switch-dark {
            background-color: #283046;
        }
        .form-control__switch {
            border-color: #ced4da;
        }
    </style>
@endsection

@section('nav-bar-container')
    <div class="text-center" style="width: 100%;">

        <button class="btn btn-success" id="change-reg-lock-mode">
            @if (\App\Models\CustomData::where('label','reg')->where('data','false')->first() != null)
                {{ __('str.settings_reg_enable') }}
            @else
                {{ __('str.settings_reg_disable') }}
            @endif
        </button>

            <button class="btn btn-success" id="test-api" style="margin: 2px 2px 2px 16px;" onclick="testApi();">{{ __('str.settings_test_api') }}</button>
            <button class="btn btn-success" style="margin: 2px 2px 2px 16px;" onclick="changeScale()">{{ __('str.settings_change_scale') }}</button>

        <div class="row" style="width: 800px; margin: 0 auto;">
            <div class="col-lg-4" style="vertical-align: center;">
                <div class="input-group mb-3" id="sync_tim_en_container" style="visibility: hidden;margin: 16px 16px 16px 16px;">
                    <div class="input-group-prepend" >
                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.settings_sync_tim') }}</span>
                    </div>
                    <select class="form-control" id="sync_tim_en" name="sync_tim_en" onchange="changeSyncTim(this)">
                        <option value="0" @if ($sync_tim_en == '0') selected @endif>{{ __('str.settings_off') }}</option>
                        <option value="1" @if ($sync_tim_en != '0') selected @endif>{{ __('str.settings_on') }}</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4">
                <button class="btn btn-success" style="width: 100%;display: none;margin: 16px 16px 16px 16px;" onclick="clearBaseAction()" id="b-base-clear">{{ __('str.settings_clear_base') }}</button>
            </div>
            <div class="col-lg-4">
                <button class="btn btn-success" style="width: 100%;display: none;margin: 16px 16px 16px 16px;" onclick="createDist()" id="b-pack-project">{{ __('str.settings_create_dist') }}</button>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">
            <div class="card-body">
                <br>
                <form method="post" action="/p-settings2-save-action">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_timezone') }}</span>
                                </div>
                                <select class="form-control" name="time_offset" required>
                                    @foreach($offsets as $key => $value)
                                        <option value="{{$key}}"
                                                @if ($timeOffset == intval($key)) selected="selected" @endif
                                        >{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_org_name') }}</span>
                                </div>
                                <input type="text" id="org_name" placeholder="{{ __('str.settings_org_name_hint') }}" name="org_name"  class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_usage_limit') }}</span>
                                </div>
                                <input type="number" id="equ_expired" placeholder="{{ __('str.equ_expired_hint') }}" name="equ_expired"  class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_org_yr_name') }}</span>
                                </div>
                                <input type="text" id="org_yr_name" name="org_yr_name" placeholder="{{ __('str.settings_org_yr_name_hint') }}"  class="form-control" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_min_tenure_time') }}</span>
                                </div>
                                <select class="form-control" id="min_own_time" name="min_own_time" required>
                                    <option value="0" selected>{{ __('str.settings_missing') }}</option>
                                    <option value="300">{{ __('str.settings_min5') }}</option>
                                    <option value="600">{{ __('str.settings_min10') }}</option>
                                    <option value="900">{{ __('str.settings_min15') }}</option>
                                    <option value="1800">{{ __('str.settings_min30') }}</option>
                                    <option value="3600">{{ __('str.settings_clock1') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_org_yr_adr') }}</span>
                                </div>
                                <input type="text" id="org_yr_adr" name="org_yr_adr" placeholder="{{ __('str.settings_org_yr_adr_hint') }}"  class="form-control" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_max_return_time') }}</span>
                                </div>
                                <select class="form-control" id="max_ret_time" name="max_ret_time" required>
                                    <option value="0" selected>{{ __('str.settings_missing') }}</option>
                                    <option value="600">{{ __('str.settings_min10') }}</option>
                                    <option value="900">{{ __('str.settings_min15') }}</option>
                                    <option value="1800">{{ __('str.settings_min30') }}</option>
                                    <option value="3600">{{ __('str.settings_clock1') }}</option>
                                    <option value="7200">{{ __('str.settings_clock2') }}</option>
                                    <option value="14400">{{ __('str.settings_clock4') }}</option>
                                    <option value="28800">{{ __('str.settings_clock8') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_org_yr_inn') }}</span>
                                </div>
                                <input type="text" id="org_yr_inn" name="org_yr_inn" placeholder="{{ __('str.settings_org_yr_inn_hint') }}"  class="form-control" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px;">{{ __('str.settings_debug_mode') }}</span>
                                </div>
                                <div class="form-control form-control__switch" data-themes tabindex="0">
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="debug_mode" name="debug_mode" checked
                                               onchange="onChangeDebugSetting(this);">
                                        <label class="form-check-label" for="debug_mode" id="debug_mode_txt"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_org_yr_kpp') }}</span>
                                </div>
                                <input type="text" id="org_yr_kpp" name="org_yr_kpp" placeholder="{{ __('str.settings_org_yr_kpp_hint') }}"  class="form-control" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_language') }}</span>
                                </div>
                                <select class="form-control" id="ui_lang" name="ui_lang" required>
                                    <option value="auto" selected>{{ __('str.settings_auto') }}</option>
                                    <option value="en">{{ __('str.settings_en') }}</option>
                                    <option value="ru">{{ __('str.settings_ru') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_ret_any_block') }}</span>
                                </div>
                                <select class="form-control" id="ret_any_block" name="ret_any_block" required>
                                    <option value="0">{{ __('str.settings_disable') }}</option>
                                    <option value="1">{{ __('str.settings_enable') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.settings_server_address') }}</span>
                                </div>
                                <input type="text" id="srv_adr" placeholder="{{ __('str.settings_server_address_hint') }}" name="srv_adr"  class="form-control" >
                            </div>
                        </div>
                        <div class="col-lg-6">
                        </div>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-success" style="width: 200px">{{ __('str.settings_save') }}</button>
                    <br>
                    <label style="float: right">{{ Config::get('app.web_ver') }}</label>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script src="{{ asset('/js/tabulator.min.js') }}"></script>

    <script>

        let changeDebugStateCount = 0;

        function changeSyncTim(v){
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            post['sync_tim_en' ] = v.value;
            $.post( "/set-sync-tim", post, function(data, status) {
                if (status === 'success'){
                    //console.log(data);
                    if ("true" === data){
                        window.location.reload();
                    }else{
                        alert(data);
                    }
                }
            });
        }

        function createDist(){
            $.get( "/pack-prj", function(data, status) {
                if (status === 'success'){
                    if ('true' === data){
                        alert('Упаковка проекта прошла успешно.')
                    }else{
                        console.log(data);
                        alert(data);
                    }
                }
            });
        }

        function clearBaseAction(){
            if (confirm("{{ __('str.settings_clear_base_ask') }}")){
                let post = {};
                post['_token'  ] = "{{csrf_token()}}";
                $.post( "/clear-base", post, function(data, status) {
                    if (status === 'success'){
                        if ("true" === data){
                            window.location.href='/login';
                        }else{
                            alert("{{ __('str.settings_update_post_error') }} - " + data);
                        }
                    }
                });
            }
        }

        function onChangeDebugSetting(v){
            changeDebugStateCount++;
            if (changeDebugStateCount === 32){
                $('#b-base-clear').css( "display", "block");
                $('#b-pack-project').css( "display", "block");
                $('#sync_tim_en_container').css( "visibility", "visible");
            }
            $('#debug_mode_txt').text(v.checked ? '{{ __('str.table_yes') }}' : '{{ __('str.table_no') }}');
        }

        function changeScale(){
            alert("{{ __('str.settings_change_scale_message') }}");
        }

        function testApi(){
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            $.post( "/api-test", post, function(data, status) {
                if (status === 'success'){
                    //console.log(data);
                    if ("true" === data){
                        alert("{{ __('str.settings_api_ok') }}");
                    }else{
                        alert("{{ __('str.settings_api_error') }} - " + data);
                    }
                }
            });
        }

        window.onload = function(){
            document.getElementById("change-reg-lock-mode").onclick = function() {
                window.location='{{ url("/change-reg-lock-mode-action") }}'
            }
            $('#equ_expired').val("{{$equ_expired}}");
            $('#min_own_time').val("{{$min_own_time}}");
            $('#max_ret_time').val("{{$max_ret_time}}");
            $('#ui_lang').val("{{$ui_lang}}");
            $('#ret_any_block').val("{{$ret_any_block}}");
            //----------------------------------------------------------------------------------------------------------
            $('#debug_mode').prop('checked',"1" === "{{$debug_mode}}");
            $('#debug_mode_txt').text("1" === "{{$debug_mode}}" ? '{{ __('str.table_yes') }}' : '{{ __('str.table_no') }}');
            //----------------------------------------------------------------------------------------------------------
            //--------Переключение стилей для поля "режим отладки" в зависимости от светлой или темной темы-------------
            if($("body").hasClass("dark-layout")) {
                $('[data-themes]').removeClass('form-control__switch').addClass('form-control__switch-dark');
            } else {
                $('[data-themes]').removeClass('form-control__switch-dark').addClass('form-control__switch');
            }
            //----------------------------------------------------------------------------------------------------------
            $('#org_name').val("{{$org_name}}".replaceAll("&amp;","\"").
                replaceAll("&quot;","\"").
                replaceAll("&#039;&#039;","\"").
                replaceAll("&#039;","\""));
            $('#org_yr_name').val("{{$org_yr_name}}".replaceAll("&amp;","\"").
                replaceAll("&quot;","\"").
                replaceAll("&#039;&#039;","\"").
                replaceAll("&#039;","\""));
            $('#org_yr_adr').val("{{$org_yr_adr}}");
            $('#org_yr_inn').val("{{$org_yr_inn}}");
            $('#org_yr_kpp').val("{{$org_yr_kpp}}");
            $('#srv_adr').val("{{$srv_adr}}");
        }
    </script>

@endpush
