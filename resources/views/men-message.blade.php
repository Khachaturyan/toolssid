@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                    "data-page-length" => "100",
                    "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    {{$dataTable->scripts()}}

    <script>

        const mens = [];

        function getSelectedMens(){
            let ids = "";
            for (let i = 0; i < mens.length; i++) {
                if (ids.length > 0) ids += ",";
                ids += mens[i];
            }
            return ids;
        }

        function handleClick(cb) {
            if (cb.checked){
                mens.push(cb.value);
            }else{
                mens.splice(mens.indexOf(cb.value), 1);
            }
        }

        window.onload = function(){

            document.getElementById("bSendAll").onclick = function() {
                window.location='{{ url("/tm-men-send-all") }}'
            }

            document.getElementById("bSendSelected").onclick = function() {
                if (mens.length > 0){
                    let ids = getSelectedMens();
                    window.location='{{ url("/tm-men-send-sel") }}' + "/" + ids;
                }
            }

        }


    </script>
@endpush
