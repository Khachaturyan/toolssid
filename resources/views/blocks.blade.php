@extends('layouts.app')

@section('nav-bar-container')

@endsection

@section('third_party_stylesheets')
    <style>
        .avatar{
            background-color: #f8f8f8!important;
            margin-right: 10px;
        }
        .blocksy{
            height: 300px;
            overflow-y: auto;
            overflow-x: auto;
        }
        input[type="time"]::-webkit-calendar-picker-indicator {
            background: transparent;
            bottom: 0;
            color: transparent;
            cursor: pointer;
            height: auto;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            width: auto;
        }
    </style>
@endsection

@section('content')

    <div class="modal fade" id="modal-screen" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 1200px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader" style="margin-top: 0px;">{{ __('str.block') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="id" name="id" value="">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.name') }}</span>
                                </div>
                                <input type="text" id="name" name="name" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.block_pin') }}</span>
                                </div>
                                <input type="text" id="pin" name="pin" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.object') }}</span>
                                </div>
                                <select class="form-control" id="st_id" name="st_id" required>
                                    <option value="" selected disabled>{{ __('str.select_object') }}</option>
                                    @foreach ($stages as $stage)
                                        <option value="{{ $stage->id }}">{{ $stage->name . " " . $stage->adr }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.cells_count') }}</span>
                                </div>
                                <input type="number" id="cell_count" name="cell_count" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.block_type') }}</span>
                                </div>
                                <select class="form-control" id="btype" name="btype" required>
                                    <option value="" selected disabled>{{ __('str.block_type_select') }}</option>
                                    <option value="0">{{ __('str.block_type_tools') }}</option>
                                    <option value="1">{{ __('str.block_type_products') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.in_archive') }}</span>
                                </div>
                                <select class="form-control" id="is_arc" name="is_arc" required>
                                    <option value="" selected disabled>{{ __('str.select_status') }}</option>
                                    <option value="0">{{ __('str.block_no') }}</option>
                                    <option value="1">{{ __('str.block_yes') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.block_show_win_sel_sizes') }}</span>
                                </div>
                                <select class="form-control" id="show_win_sel_sizes" name="show_win_sel_sizes" required>
                                    <option value="" selected disabled>{{ __('str.select_status') }}</option>
                                    <option value="0">{{ __('str.block_no') }}</option>
                                    <option value="1">{{ __('str.block_yes') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.block_time_ret') }}</span>
                                </div>
                                <select class="form-control" id="time_ret" name="time_ret" required>
                                    <option value="" selected disabled>{{ __('str.select_status') }}</option>
                                    <option value="0">{{ __('str.block_no') }}</option>
                                    <option value="1">{{ __('str.block_yes') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.block_orion_lab_encode') }}</span>
                                </div>
                                <select class="form-control" id="orion_lab_encode" name="orion_lab_encode" required>
                                    <option value="" selected disabled>{{ __('str.select_status') }}</option>
                                    <option value="0">{{ __('str.block_no') }}</option>
                                    <option value="1">{{ __('str.block_yes') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.block_encode_alg') }}</span>
                                </div>
                                <select class="form-control" id="encode_alg" name="encode_alg" required>
                                    <option value="0" selected>{{ __('str.block_encode_alg_0') }}</option>
                                    <option value="2">{{ __('str.block_encode_alg_2') }}</option>
                                    <option value="1">{{ __('str.block_encode_alg_1') }}</option>
                                    <option value="3">{{ __('str.block_encode_alg_3') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 300px">{{ __('str.block_ret_any_cell') }}</span>
                                </div>
                                <select class="form-control" id="ret_any_cell" name="ret_any_cell" required>
                                    <option value="" selected disabled>{{ __('str.select_status') }}</option>
                                    <option value="0">{{ __('str.block_no') }}</option>
                                    <option value="1">{{ __('str.block_yes') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div style="width : 100%; margin:20px auto 18px auto; text-align: center">
                                <button type="button" class="btn btn-success" onclick="onWorkRestClick();" style="width: 300px" onclick="">{{ __('str.block_blocking_periods') }}</button>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group" style="margin-top:5px">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" style="width: 300px; height: 100px">{{ __('str.description') }}</span>
                                </div>
                                <textarea id="description" name="description" class="form-control" aria-label="With textarea"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <textarea  style="height: 100px;margin-top:5px" id="work_rest_str" name="work_rest_str" class="form-control" aria-label="With textarea" readonly>
                            </textarea>
                        </div>
                    </div>
                        <!-- <br> -->
                        <button type="submit" class="btn btn-success" style="width: 200px;margin-top:15px" onclick="saveBlockAction()">{{ __('str.save') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-screen2" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader" style="margin-top: 0px;">{{ __('str.set_cell_size') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="/cell-sizes-all-action">
                        @csrf
                        <input type="hidden" id="bid" name="bid" value="">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.type_size') }}</span>
                            </div>
                            <select class="form-control" id="csid" name="csid" required>
                                <option value="0" selected>{{ __('str.type_default') }}</option>
                                @foreach($cellSizes as $cellSize)
                                    <option value="{{$cellSize->id}}">{{$cellSize->name}} {{$cellSize->des}}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-success" style="width: 200px">{{ __('str.apply') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-screen3" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 900px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader" style="margin-top: 0px;">{{ __('str.block_blocking_periods') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" onclick="onCloseBlockingPeriodScreen()">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="button" class="btn btn-success" style="width: 100%" onclick="onBlockingPeriodNewClick()">{{ __('str.block_blocking_periods_new') }}</button>
                            <br><br>
                            <div class="input-group mb-3" id="blocking-period-type-container" style="visibility: hidden">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.block_blocking_periods_type') }}</span>
                                </div>
                                <select class="form-control" id="blocking_period_type" name="blocking_period_type" onchange="onBlockingPeriodTypeChange()">
                                    <option value="" selected disabled>{{ __('str.block_blocking_periods_type_select') }}</option>
                                    <option value="0">{{ __('str.block_blocking_periods_type_0') }}</option>
                                    <option value="1">{{ __('str.block_blocking_periods_type_1') }}</option>
                                    <option value="2">{{ __('str.block_blocking_periods_type_2') }}</option>
                                </select>
                            </div>
                            <br>
                            <div class="input-group mb-3" id="blocking-period-time-begin-container" style="visibility: hidden">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.block_blocking_period_begin') }}</span>
                                </div>
                                <input type="time" id="blocking_period_time_begin" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" onchange="onBlockingPeriodTimeBeginChange()"/>
                            </div>
                            <br>
                            <div class="input-group mb-3" id="blocking-period-time-end-container" style="visibility: hidden">
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" id="inputGroup-sizing-default" style="width: 150px">{{ __('str.block_blocking_period_end') }}</span>
                                </div>
                                <input type="time" id="blocking_period_time_end" onchange="onBlockingPeriodTimeEndChange()" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" />
                            </div>
                            <br>
                            <button type="button" id="blocking-period-time-create-action" class="btn btn-success" style="width: 100%;visibility: hidden" onclick="onBlockingPeriodTimeCreateActionClick()">{{ __('str.block_blocking_period_create') }}</button>
                            <br>
                        </div>
                        <div class="col-lg-6">
                            <div id="blocking-period-item-container"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;margin-bottom: 20px;">
            <div class="card-body">
                <h4 class="modal-title">
                    {{ __('str.blocks') }}
                </h4>
                <br>
                <div>
                    <button class="btn btn-success" id="add-new" onclick="showModal()">+</button>
                    <button class="btn btn-success" onclick="changeArchViewBlock(this);" style="margin-left: 12px">{{ __('str.show_archived') }}</button>
                </div>
                <table class="table table-bordered table-striped" id="block-table"></table>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="" style="margin-top: 20px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card__modificator" style="margin-top: 20px;margin-bottom: 20px;">
                        <div class="card-body">
                            <div class="card mt-1 mb-1" onclick="openBlock('all')">
                                <div>
                                    <div class="row">
                                        <div class="col-6">
                                            <h5><b>Всего по блокам</b></h5>
                                        </div>
                                    </div>
                                </div>
                                <div id="blockall" class="mt-1 blocksy">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Тип</th>
                                            <th>Исправное</th>
                                            <th>Неисправное</th>
                                            <th>В работе</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($types as $item)
                                            <tr>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div class="avatar rounded">
                                                            <div class="avatar-content">
                                                                <img src="../../../app-assets/images/icons/toolbox.svg" alt="Toolbar svg">
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div >{{$item->tname}} {{$item->tmark}} {{$item->tmodel}}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    {{$item->work($item->id, 1)->count()}}
                                                </td>
                                                <td>
                                                    {{$item->work($item->id, 0)->count()}}
                                                </td>
                                                <td>
                                                    {{$item->count_men}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach($blocks as $block)
                    <div class="col-md-6">
                        <div class="card card__modificator" style="margin-bottom: 20px;">
                            <div class="card-body">
                                <div class="card mt-1 mb-1" onclick="openBlock({{$block->id}})">
                                    <div>
                                        <div class="row">
                                            <div class="col-6">
                                                <h5><b>Блок {{$block->st_id}} {{$block->name}}</b></h5>
                                                <p style="margin-bottom: 10px">Всего {{$block->cell_count}} ячеек</p>
                                            </div>
                                            <div class="col-6" style="text-align:right;">
                                                <p class="mb-0">Занято <b>{{$block->items($block->id)->count()}}</b></p>
                                                <p class="mb-0">Неисправно <b>{{$block->itemsWarning($block->id)->count()}}</b></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="block{{$block->id}}" class="mt-1 blocksy">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Тип</th>
                                                <th>Исправное</th>
                                                <th>Неисправное</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($block->items($block->id)->unique('tid') as $item)
                                                <tr>
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            <div class="avatar rounded">
                                                                <div class="avatar-content">
                                                                    <img src="../../../app-assets/images/icons/toolbox.svg" alt="Toolbar svg">
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div >{{$item->tip($item->tid)->tname}} {{$item->tip($item->tid)->tmodel}}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        {{$item->work($item->tid, $block->id, 1)->count()}}
                                                    </td>
                                                    <td>
                                                        {{$item->work($item->tid, $block->id, 0)->count()}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection

@push('scripts')

    <script>

        let archive = 0;
        let current_work_rest = null;

        function onCloseBlockingPeriodScreen(){
            $('#modal-screen').modal('show');
        }

        function onBlockingPeriodTimeCreateActionClick(){
            $('#blocking-period-time-create-action').css( "visibility", "hidden");
            $('#blocking-period-time-end-container').css( "visibility", "hidden");
            $('#blocking-period-time-begin-container').css( "visibility", "hidden");
            $('#blocking-period-type-container').css( "visibility", "hidden");
            let tBegin = $('#blocking_period_time_begin').val();
            let tEnd = $('#blocking_period_time_end').val();
            let tType = $('#blocking_period_type').val();
            $('#blocking_period_time_begin').val("");
            $('#blocking_period_time_end').val("");
            $('#blocking_period_type').val("");
            let item = tType + "#" + tBegin + "#" + tEnd;
            if (current_work_rest.length > 0){
                current_work_rest += " ";
            }
            current_work_rest += item;
            fillBlockingPeriodItemContainer();
        }

        function onBlockingPeriodTimeEndChange(){
            $('#blocking-period-time-create-action').css( "visibility", "visible");
        }

        function onBlockingPeriodTimeBeginChange(){
            $('#blocking-period-time-end-container').css( "visibility", "visible");
        }

        function onBlockingPeriodTypeChange(){
            $('#blocking-period-time-begin-container').css( "visibility", "visible");
        }

        function onBlockingPeriodNewClick(){
            if (current_work_rest.split(" ").length < 3){
                $('#blocking-period-type-container').css( "visibility", "visible");
            }else{
                alert('{{ __('str.block_blocking_limit_alert') }}');
            }
        }

        function deleteBlockingPeriodItem(n){
            let str = '';
            let arr = current_work_rest.split(' ');
            for (let i = 0; i < arr.length; i++){
                if (i !== parseInt(n)){
                    if (str.length > 0) str += " ";
                    str += arr[i];
                }
            }
            current_work_rest = str;
            fillBlockingPeriodItemContainer();
        }

        function fillBlockingPeriodItemContainer(){
            $('#blocking-period-item-container').empty();
            let strDes = "";
            if (current_work_rest.length > 0){
                let arr = current_work_rest.split(' ');
                for (let i = 0; i < arr.length; i++){
                    let arr2 = arr[i].split('#');
                    if (arr2.length === 3){
                        let tType  = arr2[0];
                        if (tType === '0') {
                            tType = '{{ __('str.block_blocking_periods_type_0') }}';
                        } else if (tType === '1') {
                            tType = '{{ __('str.block_blocking_periods_type_1') }}';
                        } else if (tType === '2') {
                            tType = '{{ __('str.block_blocking_periods_type_2') }}';
                        }
                        let tBegin = arr2[1];
                        let tEnd   = arr2[2];
                        let strDef = " " + (i + 1) + ". " +
                            '{{ __('str.block_blocking') }}' + " " + tType + " " +
                            '{{ __('str.block_blocking_unit') }}' + " с " + tBegin + " по " + tEnd;
                        let img = '<img src="/img/delete_gray_24dp.svg" alt="del" onclick="deleteBlockingPeriodItem(' + "'" + i + "'" + ')">';

                        $('#blocking-period-item-container').append('<p>' + img + strDef + '</p>');
                        if (strDes.length > 0) strDes += "\n";
                        strDes += strDef;
                    }
                }
            }
            $('#work_rest_str').val(strDes);
        }

        function onWorkRestClick(){
            $('#modal-screen').modal('hide');
            $('#modal-screen3').modal('show');
        }

        function sendBlockUpdateCom(bid){
            if (bid == null || bid.length === 0){
                alert('{{ __('str.home_need_block') }}');
                return;
            }
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            post['bid'    ] = bid;
            post['com'    ] = 'block-update';
            $.post( "/and-comm", post, function(data, status) {
                if (status === 'success'){
                    if ("true" === data){
                        window.location.reload();
                    }else{
                        console.log(data);
                    }
                }
            });
        }

        function saveBlockAction(){
            let st_id = $('#st_id').val();
            if (st_id == null || st_id.length == 0){
                alert('{{ __('str.block_stage_not_select') }}');
                return;
            }
            let name = $('#name').val();
            if (name == null || name.length == 0){
                alert('{{ __('str.block_name_empty') }}');
                return;
            }
            let cell_count = $('#cell_count').val();
            if (cell_count == null || cell_count.length == 0){
                alert('{{ __('str.block_cell_count_empty') }}');
                return;
            }
            let btype = $('#btype').val();
            if (btype == null || btype.length == 0){
                alert('{{ __('str.block_btype_empty') }}');
                return;
            }
            let id = $('#id').val();
            let post ={};
            post['_token'             ] = "{{csrf_token()}}";
            post['id'                 ] = id;
            post['name'               ] = name;
            post['st_id'              ] = st_id;
            post['cell_count'         ] = cell_count;
            post['description'        ] = $('#description').val();
            post['btype'              ] = btype;
            post['is_arc'             ] = $('#is_arc').val();
            post['pin'                ] = $('#pin').val();
            post['show_win_sel_sizes' ] = $('#show_win_sel_sizes').val();
            post['time_ret'           ] = $('#time_ret').val();
            post['orion_lab_encode'   ] = $('#orion_lab_encode').val();
            post['encode_alg'         ] = $('#encode_alg').val();
            post['ret_any_cell'       ] = $('#ret_any_cell').val();
            post['work_rest'          ] = current_work_rest;
            $.post( "/block-action", post, function(data, status) {
                if (status === 'success'){
                    if ("true" === data){
                        if (id.length > 0){
                            sendBlockUpdateCom(id)
                        }else{
                            window.location.reload();
                        }
                    }else{
                        alert(data);
                    }
                }
            });
        }

        function changeArchViewBlock(v){
            if (archive == 0){
                archive = 1;
                v.innerHTML = '{{ __('str.show_current') }}';
            }else{
                archive = 0;
                v.innerHTML = '{{ __('str.show_archived') }}';
            }
            let ajax = $('#block-table').DataTable().ajax;
            ajax.url('/block-data/' + archive);
            ajax.reload();
        }

        function fillModal(data){
            if (data == null){
                $('#id').val("");
                $('#name').val("");
                $('#st_id').val("");
                $('#cell_count').val("");
                $('#description').val("");
                $('#btype').val(0);
                $('#is_arc').val(0);
                $("#cell_count").prop('disabled', false);
                $("#is_arc").prop('disabled', true);
                $('#pin').val("99998");
                $('#show_win_sel_sizes').val(0);
                $('#time_ret').val(1);
                $('#orion_lab_encode').val(0);
                $('#encode_alg').val(0);
                $('#ret_any_cell').val(0);
                current_work_rest = "";
            }else{
                $('#id').val(data.id);
                $('#name').val(data.name);
                $('#st_id').val(data.st_id);
                $('#cell_count').val(data.cell_count);
                $('#btype').val(data.btype);
                $('#description').val(data.description);
                $('#is_arc').val(data.is_arc);
                $("#cell_count").prop('disabled', true);
                $("#is_arc").prop('disabled', false);
                $('#pin').val(data.pin);
                $('#show_win_sel_sizes').val(data.show_win_sel_sizes);
                $('#time_ret').val(data.time_ret);
                $('#orion_lab_encode').val(data.orion_lab_encode);
                $('#encode_alg').val(data.encode_alg);
                $('#ret_any_cell').val(data.ret_any_cell);
                current_work_rest = data.work_rest;
            }
            fillBlockingPeriodItemContainer();
        }

        function showModal(){
            fillModal(null);
            $('#modal-screen').modal('show');
        }

        function openBlock(id){
            if(document.getElementById('block'+id).style.display == 'none'){
                $('#block'+id).css({'display': 'block'});
            }
            else{
                $('#block'+id).css({'display': 'none'});
            }
        }

        $(document).ready(function(){
            //----------------------------------------------------------------------------------------------------------
            $('#block-table').DataTable({
                processing: true,
                serverSide: true,
                dom: "frtip",
                ajax: '/block-data/' + archive,
                columns: [
                    { data: 'action', title:'{{ __('str.table_edit') }}', className: 'text-center' },
                    { data: 'action_size', title:'{{ __('str.table_size') }}', className: 'text-center' },
                    { data: 'id', name: 'id', title:'{{ __('str.table_num') }}', className: 'text-center' },
                    { data: 'sname', name: 'stages.name', title:'{{ __('str.table_object') }}' },
                    { data: 'name', title:'{{ __('str.table_name') }}'  },
                    { data: 'cell_count', title:'{{ __('str.table_cell_count') }}' },
                    { data: 'adr', name: 'stages.adr', title:'{{ __('str.table_address') }}' },
                    { data: 'description', title:'{{ __('str.table_description') }}'},
                    { data: 'btype2', name: 'blocks.btype', title:'{{ __('str.block_type') }}' },
                    { data: 'android_ver', name: 'blocks.android_ver', className: 'text-right', title:'{{ __('str.block_ver') }}' },
                ]
            }).on('click', 'tbody td', function() {
                let tr  = $(this).closest('tr');
                let row = $('#block-table').DataTable().row(tr);
                if (this.cellIndex == 0){
                    fillModal(row.data());
                    $('#modal-screen').modal('show');
                } else if (this.cellIndex == 1){
                    $('#bid').val(row.data().id);
                    $('#modal-screen2').modal('show');
                }
            });
            //----------------------------------------------------------------------------------------------------------
        });
    </script>
@endpush
