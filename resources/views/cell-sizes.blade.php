@extends('layouts.app')

@section('nav-bar-container')
    <button type="button" class="btn btn-success" id="bAddNew" style="margin-right: 16px">+</button>
    <button type="button" class="btn btn-success" onclick="window.location='cells'" id="bAddNew" style="margin-right: 16px">{{ __('str.cells') }}</button>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="modal fade" id="cell-size-modal" tabindex="-1" role="dialog" aria-labelledby="CellSizesModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modalTitleHeader"></h4>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form action="/cell-size-create-action" method="POST" enctype="multipart/form-data" style="width: 100%">
                            @csrf
                            <input type="hidden" id="id" name="id" value="">
                            <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('str.name') }}" required>
                            <br>
                            <input type="text" class="form-control" id="des" name="des" placeholder="{{ __('str.description') }}">
                            <br>
                            <button class="btn btn-success">{{ __('str.save') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card__modificator" style="margin-top: 20px;">
            <div class="card-body">
                <div class="modal-header">
                    <h4 class="modal-title">{{ $table_header }}</h4>
                </div>
                {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                    "data-page-length" => "100",
                    "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{$dataTable->scripts()}}
    <script>
        $(document).ready(function(){
            $('#bAddNew').click(function () {
                $('#modalTitleHeader').text("{{ __('str.add_size') }}");
                $('#id').val("");
                $('#name').val("");
                $('#des').val("");
                $('#cell-size-modal').modal('show');
            });
            //----------------------------------------------------------------------------------------------------------
            $('#cell-size-table').on('click', 'tbody td', function() {
                if (this.cellIndex == 0){
                    let data =  $('#cell-size-table').DataTable().row($(this).closest('tr')).data();
                    $('#modalTitleHeader').text("{{ __('str.change_size') }}");
                    $('#id').val(data['id']);
                    $('#name').val(data['name']);
                    $('#des').val(data['des']);
                    $('#cell-size-modal').modal('show');
                }
            })
            //----------------------------------------------------------------------------------------------------------
        });
    </script>
@endpush
