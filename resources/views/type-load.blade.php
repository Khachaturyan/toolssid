@extends('layouts.app')

@section('third_party_stylesheets')
    <style>
        .avatar{
            background-color: #f8f8f8!important;
            margin-right: 10px;
        }
        .blocksy{
            height: 300px;
            overflow-y: auto;
            overflow-x: auto;
        }
    </style>
@endsection

@section('content')

<?php
    //$hex1 = 0x36CD7E3;
    //$hex1 = $hex1 >> 1;
    //$hex1 = $hex1 | (1 << 25);
    //echo "RES=" . $hex1;
?>

    <div class="modal fade" id="modal-screen2" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.set_cell_size') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="/cell-sizes-all-action">
                        @csrf
                        <input type="hidden" id="bid" name="bid" value="">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.size') }}</span>
                            </div>
                            <select class="form-control" id="csid" name="csid" required>
                                <option value="0" selected>{{ __('str.size_default') }}</option>
                                @foreach($cellSizes as $cellSize)
                                    <option value="{{$cellSize->id}}">{{$cellSize->name}} {{$cellSize->des}}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary" style="width: 200px">{{ __('str.apply') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;margin-bottom: 20px;">
            <div class="card-body">
                {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                    "data-page-length" => "100",
                    "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}

            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;margin-bottom: 20px;">
            <div class="card-body">
                <h4 class="modal-title">
                    {{ __('str.unlim') }}
                </h4>
                <br>
                <table class="table table-bordered table-striped" id="unlim-table" data-page-length="100"></table>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="" style="margin-top: 20px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card__modificator" style="margin-top: 20px;margin-bottom: 20px;">
                        <div class="card-body">
                            <div class="card mt-1 mb-1" onclick="openBlock('all')">
                                <div>
                                    <div class="row">
                                        <div class="col-6">
                                            <h5><b>Всего по блокам</b></h5>
                                        </div>
                                    </div>
                                </div>
                                <div id="blockall" class="mt-1 blocksy">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Тип</th>
                                            <th>Исправное</th>
                                            <th>Неисправное</th>
                                            <th>В работе</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($types as $item)
                                            <tr>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div class="avatar rounded">
                                                            <div class="avatar-content">
                                                                <img src="../../../app-assets/images/icons/toolbox.svg" alt="Toolbar svg">
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div >{{$item->tname}} {{$item->tmark}} {{$item->tmodel}}</div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    {{$item->work($item->id, 1)->count()}}
                                                </td>
                                                <td>
                                                    {{$item->work($item->id, 0)->count()}}
                                                </td>
                                                <td>
                                                    {{$item->count_men}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                @foreach($blocks as $block)
                    <div class="col-md-6">
                        <div class="card card__modificator" style="margin-bottom: 20px;">
                            <div class="card-body">
                                <div class="card mt-1 mb-1" onclick="openBlock({{$block->id}})">
                                    <div>
                                        <div class="row">
                                            <div class="col-6">
                                                <h5><b>Блок {{$block->st_id}} {{$block->name}}</b></h5>
                                                <p style="margin-bottom: 10px">Всего {{$block->cell_count}} ячеек</p>
                                            </div>
                                            <div class="col-6" style="text-align:right;">
                                                <p class="mb-0">Занято <b>{{$block->items($block->id)->count()}}</b></p>
                                                <p class="mb-0">Неисправно <b>{{$block->itemsWarning($block->id)->count()}}</b></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="block{{$block->id}}" class="mt-1 blocksy">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Тип</th>
                                                <th>Исправное</th>
                                                <th>Неисправное</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($block->items($block->id)->unique('tid') as $item)
                                                <tr>
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            <div class="avatar rounded">
                                                                <div class="avatar-content">
                                                                    <img src="../../../app-assets/images/icons/toolbox.svg" alt="Toolbar svg">
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div >{{$item->tip($item->tid)->tname}} {{$item->tip($item->tid)->tmodel}}</div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        {{$item->work($item->tid, $block->id, 1)->count()}}
                                                    </td>
                                                    <td>
                                                        {{$item->work($item->tid, $block->id, 0)->count()}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection



@push('scripts')

    {{$dataTable->scripts()}}

    <script src="/js/chart3.js"></script>

    <script>

        let chart1  = null;
        let chart2  = null;
        let archive = 0;

        function changeArchViewBlock(v){
            if (archive == 0){
                archive = 1;
                v.innerHTML = '{{ __('str.show_current') }}';
            }else{
                archive = 0;
                v.innerHTML = '{{ __('str.show_archived') }}';
            }
            let ajax = $('#block-table').DataTable().ajax;
            ajax.url('/block-data/' + archive);
            ajax.reload();
        }

        function createDataSet(lab,bcgr,tid,items){
            let arr = [];
            for (let i = 0; i < items.length; i++){
                if (items[i].tid == tid){
                    let count_all = items[i].count_all;
                    let count_men = items[i].count_men;
                    let percent   = 100 * (count_men) / count_all;
                    arr.push(percent);
                }
            }
            let dataSet = {
                label: lab,
                    borderColor: bcgr.replace('0.2','1.0'),
                backgroundColor: bcgr.replace('0.2','0.5'),
                data: arr,
                borderWidth: 1
            }
            return dataSet;
        }

        function secondsToDate(sec){
            let res = new Date(sec * 1000).toISOString().substring(5,16);
            let arr = res.replace("T"," ").split(" ");
            let arr2 = arr[0].split("-");
            return arr2[1] + "." + arr2[0] + " " + arr[1];
        }

        function openBlock(id){
            if(document.getElementById('block'+id).style.display == 'none'){
                $('#block'+id).css({'display': 'block'});
            }
            else{
                $('#block'+id).css({'display': 'none'});
            }
        }

        $(document).ready(function(){
            //----------------------------------------------------------------------------------------------------------
            $('#unlim-table').DataTable({
                processing: true,
                serverSide: true,
                dom: "frtip",
                ajax: '/type-unlim-list/0',
                columns: [
                    { data: 'time', name: 'equ_expireds.time', title:'{{ __('str.equipment_take_time') }}' },
                    { data: 'fio', name: 'men.msurname', title:'{{ __('str.mens_name') }}', 'width':'50%' },
                    { data: 'mname', name: 'men.mname', visible: false },
                    { data: 'mpatronymic', name: 'men.mpatronymic', visible: false },
                    { data: 'tname', name: 'etypes.tname', title:'{{ __('str.et_type') }}' },
                    { data: 'tmark', name: 'etypes.tmark', title:'{{ __('str.et_mark') }}' },
                    { data: 'tmodel', name: 'etypes.tmodel', title:'{{ __('str.et_model') }}' },
                ]
            }).on('click', 'tbody td', function() {
                let tr  = $(this).closest('tr');
                let row = $('#block-table').DataTable().row(tr);
                if (this.cellIndex == 1){
                    $('#bid').val(row.data().id);
                    $('#modal-screen2').modal('show');
                }
            });
            //----------------------------------------------------------------------------------------------------------
        })

    </script>
@endpush
