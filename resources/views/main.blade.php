@extends('layouts.app')

@section('third_party_stylesheets')
    <style>
        .hscroll {
            width: 100%;
            overflow-x: scroll;
            padding-bottom: 16px;
        }
        .avatar{
            background-color: #f8f8f8!important;
            margin-right: 10px;
        }
        .blocksy{
            height: 300px;
            overflow-y: auto;
            overflow-x: auto;
        }
        input[type="datetime-local"]::-webkit-calendar-picker-indicator {
            background: transparent;
            bottom: 0;
            color: transparent;
            cursor: pointer;
            height: auto;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            width: auto;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="/css/custom-main.css">
@endsection

@section('content')

    @if (count($stages) > 1)
        <ul class="nav nav-tabs small justify-content-end" id="debug-mode">
            <li class="nav-item">
                <a  @if ($current_stage_id <= 0)
                        class="nav-link active" aria-current="page"
                    @else
                        class="nav-link"
                    @endif
                    href="/main">Все объекты</a>
            </li>
            @foreach($stages as $stage)
                <li class="nav-item">
                    <a  @if ($current_stage_id == $stage->id)
                            class="nav-link active" aria-current="page"
                        @else
                            class="nav-link"
                        @endif
                        href="/main/{{$stage->id}}">{{$stage->name}}</a>
                </li>
            @endforeach
        </ul>
    @endif

    <!-- Выбор отображаемых блоков -->
    <div class="dropdown">
        <button class="btn__show-hidden-blocks" type="button" data-bs-toggle="dropdown" aria-expanded="false"></button>
        <ul class="dropdown-menu">
            <li><h6 class="dropdown-header">Показать или скрыть блок на странице</h6></li>
            <li><hr class="dropdown-divider"></li>
            <li class="set-mb-pl"><label><input id="chkBlock_f0" type="checkbox"> {{ __('str.equipment_status') }} ({{ __('str.diagrams') }})</label></li>
            <li class="set-mb-pl"><label><input id="chkBlock_f1" type="checkbox"> {{ __('str.equipment_status') }}</label></li>
            <li class="set-mb-pl"><label><input id="chkBlock_f2" type="checkbox"> {{ __('str.type_load2') }}</label></li>
            <li class="set-mb-pl"><label><input id="chkBlock_f3" type="checkbox"> {{ __('str.table_breaking_type') }}</label></li>
            <li class="set-mb-pl"><label><input id="chkBlock_f4" type="checkbox"> {{ __('str.unlim') }}</label></li>
            <li class="set-mb-pl"><label><input id="chkBlock_f5" type="checkbox"> {{ __('str.table_not_working2') }}</label></li>
            <li class="set-mb-pl"><label><input id="chkBlock_f6" type="checkbox"> {{ __('str.table_hardworking_users') }}</label></li>
            <li class="set-mb-pl"><label><input id="chkBlock_f7" type="checkbox"> {{ __('str.table_breaking_users') }}</label></li>
            <li class="set-mb-pl"><label><input id="chkBlock_f8" type="checkbox"> {{ __('str.main_graph_title2') }}</label></li>
        </ul>
    </div>

    <!-- Отключение перемещения блоков -->
    <div class="btn__dnd-switch-box">
        <button class="btn__dnd-switch" type="button"></button>
    </div>

    <div id="c4" class="container__dad">

        <div id="f0" class="dnd-sortable-item" data-id="0" style="display: none">
            <div class="container-fluid container-fluid__modificator" style="display: flex">
                <div class="card card__modificator" style="width: calc(50% - 4px);margin-right: 4px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="modal-title__modificator">{{ __('str.equipment_status') }}</div>
                                <div class="in-total-val-container">
                                    <div class="in-total-val"></div>
                                </div>
                                <div class="canvas__container">
                                    <div class="chartCard">
                                        <div class="chartBox">
                                            <canvas id="myChart4" width="250" height="250"></canvas>
                                        </div>
                                    </div>
                                    <div class="legend">
                                        <div class="caption-list">
                                            @foreach ($stat_items[''] as $item)
                                                <div class="caption-item_container">
                                                    <div class="unit" data-descr="{{ $item['descr'] }}" data-count="{{ $item['count'] }}" data-color="{{ $item['color'] }}" style="display: none"></div>
                                                    <button data-toggle-item="" class="caption-item_before" style="background-color: {{ $item['color'] }}"></button>
                                                    <div class="caption-item_descr"><h6>{{ $item['descr'] }}</h6></div>
                                                    <div class="caption-item_count">{{ $item['count'] }}</div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card__modificator" style="width: calc(50% - 4px);margin-left: 4px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="modal-title__modificator">{{ __('str.equipment_status') }}</div>
                                <div class="in-total-val-container">
                                    <div class="in-total-val"></div>
                                </div>
                                <div class="chartCard1">
                                    <div class="chartBox1">
                                        <canvas id="myChart5" width="600" height="250"></canvas>
                                    </div>
                                    <div class="list__count-vertical">
                                        <ul class="ui__list"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="f1" class="dnd-sortable-item" data-id="1" style="display: none">
            <div class="container-fluid container-fluid__modificator">
                <div class="card card__modificator">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="modal-title__modificator">{{ __('str.equipment_status') }}</div>
                            </div>
                        </div>
                        <br>
                        <div @if (count($stat_items['']) > 5) class="hscroll" @endif>
                            <table class="table table-bordered table__new">
                                <thead class="text-center">
                                <tr>
                                    @foreach ($stat_items[''] as $item)
                                        <th>{{ $item['descr'] }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="text-center" id="all-estate-row-main">
                                    @foreach ($stat_items[''] as $item)
                                        <td style="background: {{ $item['color'] }};@if ($item['id'] == -6) width: 300px; @endif">
                                            {{ $item['count'] }}
                                            @if ($item['id'] == -6)
                                                <div class="div__open-table" style="background: {{ $item['color'] }}">
                                                    <div class="but__open-table but__arrow-down"></div>
                                                    <div class="title__open-table">{{ __('str.button_title') }}</div>
                                                </div>
                                            @endif
                                        </td>
                                    @endforeach
                                </tr>
                                @foreach($stat_items as $key => $value)
                                    @if (strlen($key) > 0)
                                        <tr id="type-state-row" class="text-center" style="visibility: collapse;">
                                            @foreach ($stat_items[$key] as $item)
                                                <td>
                                                    @if ($item['id'] == -6)
                                                        <div class="row">
                                                            <div class="col-lg-10" style="text-align: left">
                                                                {{ $key }}
                                                            </div>
                                                            <div class="col-lg-2" style="align-self: center;">
                                                                {{ $item['count'] }}
                                                            </div>
                                                        </div>
                                                    @else
                                                        {{ $item['count'] }}
                                                    @endif
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="f2" class="dnd-sortable-item dnd-sortable-item-wide" data-id="2" style="display: none">
            <div class="container-fluid container-fluid__modificator">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card__modificator">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="modal-title" id="modalLabelHeader">{{ __('str.type_load2') }}</div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                            <select class="form-control form-control_modificator" id="chart_period" required>
                                                <option value="1">{{ __('str.type_load_period1') }}</option>
                                                <option value="6">{{ __('str.type_load_period6') }}</option>
                                                <option value="3" selected>{{ __('str.type_load_period3') }}</option>
                                                <option value="7">{{ __('str.type_load_period7') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <canvas id="myChart1" width="400" height="170"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="f3" class="dnd-sortable-item dnd-sortable-item-wide" data-id="3" style="display: none">
            <div class="container-fluid container-fluid__modificator">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card__modificator">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="modal-title" id="modalLabelHeader">
                                            {{ __('str.table_breaking_type') }}
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                            <!-- <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default">{{ __('str.type_load_period') }}</span>
                                            </div> -->
                                            <select class="form-control form-control_modificator input-group_modificator" id="chart_period_breaking_state" required onchange="loadBreakingStateChartData(this.value);">
                                                <option value="4" selected>{{ __('str.type_load_period4') }}</option>
                                                <option value="5">{{ __('str.type_load_period5') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <canvas id="myChart3" width="400" height="170"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="f4" class="dnd-sortable-item dnd-sortable-item-wide" data-id="4" style="display: none">
            <div class="container-fluid container-fluid__modificator">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card__modificator">
                            <div class="card-body">
                                <div class="col-lg-12">
                                    <div class="modal-title">{{ __('str.unlim') }}</div>
                                </div>
                                <div class="row">
                                    <div class="btn-group dropstart">
                                        <button class="btn-set-visible" type="button" data-bs-toggle="dropdown" aria-expanded="false"></button>
                                        <ul class="dropdown-menu" id="grpChkBoxUpLimit">
                                            <li class="set-mb-pl"><label><input id="chkBoxMetka" type="checkbox"> {{ __('str.equ_label') }}</label></li>
                                            <li class="set-mb-pl"><label><input id="chkBoxSN" type="checkbox"> {{ __('str.rep_ser_num') }}</label></li>
                                            <li class="set-mb-pl"><label><input id="chkBoxInv" type="checkbox"> {{ __('str.rep_xls_header_inv_num') }}</label></li>
                                            <li class="set-mb-pl"><label><input id="chk-box-block" type="checkbox"> {{ __('str.block') }}</label></li>
                                            <li class="set-mb-pl"><label><input id="chk-box-stage" type="checkbox"> {{ __('str.object') }}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered table__modificator" id="unlim-table" data-page-length="10"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="f5" class="dnd-sortable-item dnd-sortable-item-wide" data-id="5" style="display: none">
            <div class="container-fluid container-fluid__modificator">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card__modificator">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="modal-title">{{ __('str.table_not_working2') }}</div>
                                    </div>
                                    <div class="btn-group dropstart">
                                        <button class="btn-set-visible" type="button" data-bs-toggle="dropdown" aria-expanded="false"></button>
                                        <ul class="dropdown-menu" id="grpChkBoxNotWorkEqu">
                                            <li class="set-mb-pl"><label><input id="checkbox-metka" type="checkbox"> {{ __('str.equ_label') }}</label></li>
                                            <li class="set-mb-pl"><label><input id="checkbox-sn" type="checkbox"> {{ __('str.rep_ser_num') }}</label></li>
                                            <li class="set-mb-pl"><label><input id="checkbox-inv" type="checkbox"> {{ __('str.rep_xls_header_inv_num') }}</label></li>
                                            <li class="set-mb-pl"><label><input id="checkbox-model" type="checkbox"> {{ __('str.et_model') }}</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="not-working-table" data-page-length="10"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="f6" class="dnd-sortable-item dnd-sortable-item-wide" data-id="6" style="display: none">
            <div class="container-fluid container-fluid__modificator">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card__modificator">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="modal-title">
                                            {{ __('str.table_hardworking_users') }}
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="input-group mb-3">
                                            <!-- <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.type_load_period') }}</span>
                                            </div> -->
                                            <select class="form-control form-control_modificator" id="hardworking_period" required onchange="hardworking_period_onchange(this);">
                                                <option value="3" selected>{{ __('str.type_load_period3') }}</option>
                                                <option value="4" >{{ __('str.type_load_period4') }}</option>
                                                <option value="5">{{ __('str.type_load_period5') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="hardworking-users-table" data-page-length="10"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="f7" class="dnd-sortable-item dnd-sortable-item-wide" data-id="7" style="display: none">
            <div class="container-fluid container-fluid__modificator">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card__modificator">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="modal-title">
                                            {{ __('str.table_breaking_users') }}
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="input-group mb-3">
                                            <!-- <div class="input-group-prepend" >
                                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.type_load_period') }}</span>
                                            </div> -->
                                            <select class="form-control form-control_modificator" id="breaking_period" required onchange="breaking_period_onchange(this);">
                                                <option value="4" selected>{{ __('str.type_load_period4') }}</option>
                                                <option value="5" >{{ __('str.type_load_period5') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="breaking-users-table" data-page-length="10"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="f8" class="dnd-sortable-item" data-id="8" style="display: none">
            <div class="container-fluid container-fluid__modificator">
                <div class="card card__modificator">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="modal-title" id="modalLabelHeader">{{ __('str.main_graph_title2') }}</h4>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-12 d-flex justify-content-end">
                                <p class="title__datetime">Выбрать период</p>
                                <div class="input-group form-control__modificator">
                                    <!-- <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_date_start') }}</span>
                                    </div> -->
                                    <input type="datetime-local" id="rep_date_start" name="rep_date_start" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
                                </div>
                                <!-- </div> -->
                                <!-- <div class="col-lg-6"> -->
                                <div class="input-group form-control__modificator">
                                    <!-- <div class="input-group-prepend" >
                                        <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.equ_date_end') }}</span>
                                    </div> -->
                                    <input type="datetime-local" id="rep_date_end" name="rep_date_end" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"/>
                                </div>
                            </div>
                        </div>
                        <canvas id="myChart2" width="800" height="150"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <label id="agent"></label>
    </div>

@endsection

@push('scripts')

    <script src="/js/page-settings-io.js"></script>
    <script src="/js/areasortable.js"></script>
    <script src="/js/chart3.js"></script>


    <script>

        let chart1  = null;
        let chart2  = null;
        let chart3  = null;

        let page   = 'main';
        let userId = parseInt("{{Auth::user()->id}}");
        let sid    = "{{$current_stage_id}}";

        let bcgr = [
            'rgba(128,  128, 128, 0.2)',
            'rgba(255,    0,   0, 0.2)',
            'rgba(255,  128,   0, 0.2)',
            'rgba(0,      0, 255, 0.2)',
            'rgba(0,    255,   0, 0.2)',
        ];

        //--- drag and drop --------------------------------------------------------------------------------------------

        AreaSortable('unrestricted', {
            container: 'c4', // container element or id
            animationMs: 880,
            onStart: function(item){
                var scrollTop = window.pageYOffset || document.documentElement.scrollTop || 0,
                    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft || 0;
                window.onscroll = function() {window.scrollTo(scrollLeft, scrollTop);};
            },
            onEnd: function(item){
                window.onscroll = function() {};
                var sort = [];
                $('#c4').children('div').each(function () {
                    sort.push($(this).attr('id'));
                });
                //console.log(JSON.stringify(sort));
                saveUserSettings(page,userId,"sort-block",JSON.stringify(sort),"{{csrf_token()}}");
            }
        });
        //dispose
        //instance4.dispose();
        //--------------------------------------------------------------------------------------------------------------

        function onClickOfCustomStatesByTypes(){
            let counter = 0;
            $('tr[id="type-state-row"]').each(function() {
                let row = this;
                setTimeout(function(){
                    let displayStateVisible = row.style.visibility == 'collapse';
                    row.style.setProperty('visibility', displayStateVisible ? 'visible' : 'collapse');
                }, counter);
                counter += 20;
            });
        }

        function createDataSetBreakingState(bcgr,items){
            let arr = [];
            let lab = [];
            for (let i = 0; i < items.length; i++){
                lab.push(items[i].name);
                arr.push(items[i].cnt);
            }
            let dataSet = {
                label: lab,
                borderColor: bcgr.replace('0.2','1.0'),
                backgroundColor: bcgr.replace('0.2','0.5'),
                data: arr,
                borderWidth: 1
            }
            return dataSet;
        }

        function createLineDataStatStates(bcgr, stateIndex, items){
            let arr = [];
            for (let i = 0; i < items.length; i++){
                let v = 0;
                if (stateIndex == 0){
                    v = items[i].full;
                }else if (stateIndex == 1){
                    v = items[i].not_working;
                }else if (stateIndex == 2){
                    v = items[i].free;
                }else if (stateIndex == 3){
                    v = items[i].in_hand;
                }else if (stateIndex == 4){
                    v = items[i].in_cell;
                }
                arr.push(v);
            }
            let lab = "";
            if (stateIndex == 0){
                lab = '{{ __('str.home_total') }}';
            }else if (stateIndex == 1){
                lab = '{{ __('str.table_not_working') }}';
            }else if (stateIndex == 2){
                lab = '{{ __('str.table_is_free') }}';
            }else if (stateIndex == 3){
                lab = '{{ __('str.table_in_hand') }}';
            }else{
                lab = '{{ __('str.table_on_box') }}';
            }
            let dataSet = {
                label: lab,
                borderColor: bcgr.replace('0.2','1.0'),
                backgroundColor: bcgr.replace('0.2','0.5'),
                data: arr,
                borderWidth: 1
            }
            return dataSet;
        }

        function secondsToDate(sec){
            let res = new Date(sec * 1000).toISOString().substring(5,16);
            let arr = res.replace("T"," ").split(" ");
            let arr2 = arr[0].split("-");
            return arr2[1] + "." + arr2[0] + " " + arr[1];
        }

        function renderBreakingTypeStateChart(items){
            if (chart3 != null){
                chart3.destroy();
            }
            // let bcgr2 = [
            //     'rgba(250,   95,     67, 0.6)',
            // ];
            ctx3 = document.getElementById('myChart3').getContext('2d');
            const gradientBg = ctx3.createLinearGradient(0, 0, 0, 250);
            gradientBg.addColorStop(0, '#892DD1');
            gradientBg.addColorStop(1, 'rgba(205, 163, 239, 0.3)');
            let dat  = [];
            let lab  = [];
            let data = [];
            for (let i = 0; i < items.length; i++){
                lab.push(items[i].name);
                dat.push(items[i].cnt);
            }
            data.push({
                label: '{{ __('str.table_breaking_type') }}',
                backgroundColor: gradientBg,
                borderColor: gradientBg,
                data: dat,
                borderWidth: 1,
                //barPercentage: 0.5,
                //barThickness: 96,
                maxBarThickness: 96,
                borderRadius: 10
            });
            chart3 = new Chart(document.getElementById('myChart3').getContext('2d'), {
                type: 'bar',
                data: {
                    labels: lab,
                    datasets: data,
                },
                options: {
                    scales: {
                        x: {
                            stacked: true,
                            grid: {
                                display: false
                            }
                        },
                        y: {
                            stacked: true,
                            ticks: {
                                precision: 0
                            },
                        }
                    },
                    plugins: {
                        title: {
                            display: false,
                            text: ''
                        },
                        legend: {
                            display: false
                        }
                    }
                }
            });
        }

        function renderStatStateChart(items){
            if (chart2 != null){
                chart2.destroy();
            }
            let timeLabels   = [];
            for (let i = 0; i < items.length; i++){
                timeLabels.push(items[i].created_at.split('T')[0]);
            }
            let dataLines = [];
            for (let i = 0; i < 5; i++){
                let res = createLineDataStatStates(bcgr[i % bcgr.length],i,items);
                dataLines.push(res);
            }
            chart2 = new Chart(document.getElementById('myChart2').getContext('2d'), {
                type: 'line',
                data: {
                    labels: timeLabels,
                    datasets: dataLines,
                },
                options: {
                    animation: false,
                    elements: {
                        point:{
                            radius: 0
                        }
                    },
                    plugins: {
                        legend: {
                            display: true,
                            position: 'left',
                            align: 'start',
                            labels: {
                                textAlign: 'top',
                                padding: 16,
                                font: {
                                    family: "'Manrope','Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                                    size: 12,
                                    lineHeight: 1.65
                                },
                                usePointStyle: false,
                                boxWidth: 16,
                                boxHeight: 16,
                                useBorderRadius: true,
                                borderRadius: 3,
                            }
                        }
                    },
                    scales: {
                        y: {
                            beginAtZero: true,
                            //max: max * 1.05
                        }
                    },
                    layout: {
                        padding: {
                            top: 0,
                            left: 0,
                            right: 16,
                            bottom: 16
                        }
                    }
                }
            });
        }

        function renderChartUnitCount(items){
            if (items == null || items.length === 0){
                return
            }
            let maxTimePoint  =  0;
            let timeLabels    = [];
            let typeIds       = [];
            let typeTimeArray = [];
            for (let i = 0; i < items.length; i++){
                if (items[i].created_at > maxTimePoint) maxTimePoint = items[i].created_at;
                if (!timeLabels.includes(items[i].created_at)) {
                    timeLabels.push(items[i].created_at);
                    typeTimeArray[items[i].created_at] = [];
                }
                if (!typeIds.includes(items[i].tid)) typeIds.push(items[i].tid);
            }
            timeLabels.sort(function(a,b){
                return new Date(a.plantingDate) - new Date(b.plantingDate)
            });
            let timeLabelsStr   = [];
            for (let i = 0; i < timeLabels.length; i++){
                timeLabelsStr.push(secondsToDate(timeLabels[i]));
            }
            for (let i = 0; i < items.length; i++){
                typeTimeArray[items[i].created_at][items[i].tid] = items[i];
            }
            let datasetsArray = [];
            for (let j = 0; j < typeIds.length; j++){
                let arr = [];
                let lab = "";
                for (let i = 0; i < timeLabels.length; i++){
                    let hasValueInTimeLabel = typeIds[j] in typeTimeArray[timeLabels[i]];
                    if (hasValueInTimeLabel){
                        let item = typeTimeArray[timeLabels[i]][typeIds[j]];
                        if (lab.length == 0){
                            lab = item.tname + " " + item.tmark + " " + item.tmodel;
                        }
                        let count_all = item.count_all;
                        let count_men = item.count_men <= count_all ? item.count_men : count_all;
                        let percent   = 100 * (count_men) / count_all;
                        arr.push(percent);
                    }else{
                        arr.push(0);
                    }
                }
                let dataSet = {
                    label: lab,
                    borderColor: bcgr[j % bcgr.length].replace('0.2','1.0'),
                    backgroundColor: bcgr[j % bcgr.length].replace('0.2','0.5'),
                    data: arr,
                    borderWidth: 1
                }
                datasetsArray.push(dataSet);
            }
            if (chart1 != null){
                chart1.destroy();
            }
            chart1 = new Chart(document.getElementById('myChart1').getContext('2d'), {
                type: 'line',
                data: {
                    labels: timeLabelsStr,
                    datasets: datasetsArray,
                },
                options: {

                    plugins: {
                        tooltip: {
                            callbacks: {
                                label: function(context) {
                                    var label = context.dataset.label || '';
                                    if (context.parsed.y !== null) {
                                        label += ': ' + Math.round(context.parsed.y) + "%";
                                    }
                                    return label;
                                }
                            }
                        },
                        legend: {
                            display: true,
                            position: 'right',
                            align: 'start',
                            labels: {
                                textAlign: 'left',
                                padding: 4,
                            }
                        }
                    },
                    scales: {
                        y: {
                            beginAtZero: true,
                            max: 100
                        }
                    },
                    layout: {
                        padding: 10
                    }
                }
            });

        }

        function loadBreakingStateChartData(timeFrame){
            $.get( "/breaking-stats/" + timeFrame + "/" + sid, function(data, status) {
                if (status === 'success'){
                    //console.log(data);
                    renderBreakingTypeStateChart(JSON.parse(data));
                }
            });
        }

        function loadChartData(timeFrame){
            $.get( "/type-load-list/" + timeFrame + "/" + sid, function(data, status) {
                if (status === 'success'){
                    //console.log("time2=" + Date.now());
                    //console.log("data=" + data);
                    renderChartUnitCount(JSON.parse(data));
                }
            });
        }

        function loadStatStatesChartData(){
            let ds = $("#rep_date_start").val();
            if (ds.length === 0){
                ds = "0";
            }
            let de = $("#rep_date_end").val();
            if (de.length === 0){
                de = "0";
            }
            $.get( "/stst-items/" + ds + "/" + de + "/" + sid, function(data, status) {
                if (status === 'success'){
                    //console.log(data);
                    renderStatStateChart(JSON.parse(data));
                }
            });
        }

        function showUnlimTableHint1(v){
            $('#hint1').tooltip("show");
        }

        function hardworking_period_onchange(v){
            $('#hardworking-users-table').DataTable().ajax.url('/hw-men-data/' + v.value + "/" + sid).load();
        }

        function breaking_period_onchange(v){
            $('#breaking-users-table').DataTable().ajax.url('/b-men-data/' + v.value + "/" + sid).load();
        }

        //--------------------------------------------------------------------------------------------------------------
        $(document).ready(function(){

            // remove the default 'Search' text for all DataTable search boxes
            $.extend(true, $.fn.dataTable.defaults, {
                language: {
                    search: ""
                }
            });

            $('#chart_period').on('change', function() {
                loadChartData(this.value);
            });
            //----------------------------------------------------------------------------------------------------------
            let today = new Date().toISOString().slice(0, 10);
            $("#rep_date_start").val(today);
            $("#rep_date_end").val(today);
            $("#rep_date_start").on("input", function() {
                loadStatStatesChartData();
            });
            $("#rep_date_end").on("input", function() {
                loadStatStatesChartData();
            });
            //loadStatStatesChartData();
            //----------------------------------------------------------------------------------------------------------
            loadBreakingStateChartData(4);
            //----------------------------------------------------------------------------------------------------------
            $('#unlim-table').DataTable({
                processing: true,
                serverSide: true,
                dom: "frtip",
                ajax: '/type-unlim-list/' + sid,
                columns: [
                    { data: 'fio', name: 'men.msurname', title:'{{ __('str.table_fio') }}' },
                    { data: 'bname', name: 'blocks.name', title:'{{ __('str.table_block') }}' },
                    { data: 'sname', name: 'stages.name', title:'{{ __('str.table_object') }}' },
                    { data: 'mname', name: 'men.mname', visible: false, searchable : true },
                    { data: 'mpatronymic', name: 'men.mpatronymic', visible: false, searchable : true },
                    { data: 'tname', name: 'etypes.tname', title:'{{ __('str.et_type') }}' },
                    { data: 'ser_num', name: 'equipment.ser_num', title:'{{ __('str.rep_ser_num') }}', visible: true, searchable : true },
                    { data: 'label', name: 'equipment.label', title:'{{ __('str.et_label') }}', searchable : true },
                    { data: 'tmark', name: 'etypes.tmark', title:'{{ __('str.et_mark') }}', searchable : true, visible: false },
                    { data: 'tmodel', name: 'etypes.tmodel', title:'{{ __('str.et_model') }}', searchable : true, visible: false },
                    { data: 'inv_num', name: 'equipment.inv_num', title:'{{ __('str.table_inv_num') }}', searchable : true },
                    { data: 'time_last', name: 'equipment.inv_num', title:'{{ __('str.equipment_take_time') }}' + " / " + '{{ __('str.table_elapsed_time') }}', searchable : true },
                ]
            });
            $("#unlim-table thead tr th").each(function( index ) {
                if (index === 7){
                    $(this).append('<img data-toggle="tooltip" data-placement="bottom" title="{{ __('str.hint_table_time_take') }}" ' +
                        'src="/img/help_outline_black_24dp.svg" style="width : 24px; height: 24px; padding: 6px"/>');
                }
            });
            //----------------------------------------------------------------------------------------------------------
            $('#not-working-table').DataTable({
                processing: true,
                serverSide: true,
                dom: "frtip",
                ajax: '/not-working-list/' + sid,
                columns: [
                    { data: 'time', title:'{{ __('str.equipment_ret_time') }}' },
                    { data: 'fio', name: 'men.msurname', title:'{{ __('str.table_fio') }}' },
                    { data: 'mname', name: 'men.mname', visible: false, searchable : true },
                    { data: 'mpatronymic', name: 'men.mpatronymic', visible: false, searchable : true },
                    { data: 'tname', name: 'etypes.tname', title:'{{ __('str.et_type') }}' },
                    { data: 'ser_num', name: 'equipment.ser_num', title:'{{ __('str.rep_ser_num') }}', visible: true, searchable : true },
                    { data: 'label', name: 'equipment.label', title:'{{ __('str.et_label') }}', searchable : true },
                    { data: 'tmark', name: 'etypes.tmark', title:'{{ __('str.et_mark') }}', searchable : true, visible: false },
                    { data: 'tmodel', name: 'etypes.tmodel', title:'{{ __('str.et_model') }}', searchable : true, visible: true },
                    { data: 'ft_name', name: 'failure_types.name', title:'{{ __('str.table_failure_type') }}', searchable : true },
                    { data: 'inv_num', name: 'equipment.inv_num', title:'{{ __('str.table_inv_num') }}', searchable : true },
                ]
            });
            //----------------------------------------------------------------------------------------------------------
            $('#hardworking-users-table').DataTable({
                processing: true,
                serverSide: true,
                dom: "frtip",
                ajax: '/hw-men-data/' + $('#hardworking_period').val() + "/" + sid,
                columns: [
                    { data: 'msurname', name: 'men.msurname', title:'{{ __('str.table_fio') }}' },
                    { data: 'mname', name: 'men.mname', title:'{{ __('str.table_fio') }}', visible: false  },
                    { data: 'mpatronymic', name: 'men.mpatronymic', title:'{{ __('str.mens_patronymic') }}', visible: false  },
                    { data: 'mlabel', name: 'men.post', title:'{{ __('str.table_label_men') }}' },
                    { data: 'cnt', title:'{{ __('str.table_take_count') }}', searchable: false, className: 'text-center' },
                    { data: 'pin', searchable: false, title:'{{ __('str.table_profile_men_link') }}' },
                ]
            }).on('click', 'tbody td', function() {
                let tr  = $(this).closest('tr');
                let row = $('#hardworking-users-table').DataTable().row(tr);
            }).on('dblclick', 'tbody td', function() {
            });

            $('#breaking-users-table').DataTable({
                processing: true,
                serverSide: true,
                dom: "frtip",
                ajax: '/b-men-data/' + $('#breaking_period').val() + "/" +sid,
                columns: [
                    { data: 'msurname', name: 'men.msurname', title:'{{ __('str.table_fio') }}' },
                    { data: 'mname', name: 'men.mname', title:'{{ __('str.table_fio') }}', visible: false },
                    { data: 'mpatronymic', name: 'men.mpatronymic', title:'{{ __('str.mens_patronymic') }}', visible: false  },
                    { data: 'mlabel', name: 'men.post', title:'{{ __('str.table_label_men') }}' },
                    { data: 'cnt', title:'{{ __('str.table_break_count') }}', searchable: false, className: 'text-center' },
                    { data: 'pin', searchable: false, title:'{{ __('str.table_profile_men_link') }}' },
                ]
            }).on('click', 'tbody td', function() {
                let tr  = $(this).closest('tr');
                let row = $('#breaking-users-table').DataTable().row(tr);
            }).on('dblclick', 'tbody td', function() {
            });

            // Custom стиль для Search boxes
            $('[type=search]').each(function () {
                $(this).attr("placeholder", "Поиск");
            });

            //--------------------------Выбор отображаемых блоков-------------------------------------------------------
            loadAnyPageData(page,userId,function (pageSettings, bSaveSetiingsUser){
                //--- загрузка порядка блоков на странице --------------------------------------------------------------
                let sortStr = typeof pageSettings["sort-block"] !== 'undefined' ? pageSettings["sort-block"] : '[]';
                let bIndexes = JSON.parse(sortStr);
                for (let i = 0; i < bIndexes.length; i++){
                    $('#c4').append($('#' + bIndexes[i]));
                }
                //--- загрузка видимости блоков ------------------------------------------------------------------------
                let blockNames = ["f0","f1","f2","f3","f4","f5","f6","f7","f8"];
                let checkBoxBlockNames = ['chkBlock_f0','chkBlock_f1','chkBlock_f2','chkBlock_f3','chkBlock_f4',
                    'chkBlock_f5','chkBlock_f6','chkBlock_f7','chkBlock_f8'];
                let blockCallback = function(isChecked,blockName) {
                    //let delay = blockName === 'f8' ? 100 : 200;
                    let delay = 100;
                    if (isChecked){
                        $('#' + blockName).show(delay);
                    }else{
                        $('#' + blockName).hide(delay);
                    }
                }
                for (let i = 0; i < checkBoxBlockNames.length; i++){
                    let isChecked = !('false' === pageSettings[checkBoxBlockNames[i]]);
                    $('#' + checkBoxBlockNames[i]).prop('checked', isChecked);
                    blockCallback(isChecked,blockNames[i]);
                    //--блок диаграммы по умолчанию при первой загрузке у нового пользователя не выбирается-------------
                    if (bSaveSetiingsUser) {
                        $('#' + checkBoxBlockNames[0]).prop('checked', false);
                        blockCallback(false,'f0');
                    }
                    //--------------------------------------------------------------------------------------------------
                    $('#' + checkBoxBlockNames[i]).change(function() {
                        blockCallback(this.checked, blockNames[i]);
                        saveUserSettings(page,userId,checkBoxBlockNames[i],this.checked,"{{csrf_token()}}");
                    });
                }
                //--------------------------Выбор отображаемых колонок в таблице блока----------------------------------
                let checkBoxUnlim = ['chkBoxSN','chkBoxMetka','chkBoxInv','chk-box-block','chk-box-stage'];
                let columnUnlim   = [6,7,10,1,2];
                let unlimTable = $('#unlim-table').DataTable();
                let tableCallback = function (isChecked,columnNum,table){
                    table.column(columnNum).visible(isChecked);
                }
                for (let i = 0; i < checkBoxUnlim.length; i++){
                    let isChecked = !('false' === pageSettings[checkBoxUnlim[i]]);
                    $('#' + checkBoxUnlim[i]).prop('checked', isChecked);
                    tableCallback(isChecked,columnUnlim[i],unlimTable);
                    $('#' + checkBoxUnlim[i]).change(function() {
                        tableCallback(this.checked, columnUnlim[i], unlimTable);
                        saveUserSettings(page,userId,checkBoxUnlim[i],this.checked,"{{csrf_token()}}");
                    });
                }
                let checkBoxNotWorking = ['checkbox-sn','checkbox-metka','checkbox-inv','checkbox-model'];
                let columnNotWorking   = [5,6,10,8];
                let tableNotWorking = $('#not-working-table').DataTable();
                for (let i = 0; i < checkBoxNotWorking.length; i++){
                    let isChecked = !('false' === pageSettings[checkBoxNotWorking[i]]);
                    $('#' + checkBoxNotWorking[i]).prop('checked', isChecked);
                    tableCallback(isChecked,columnNotWorking[i],tableNotWorking);
                    $('#' + checkBoxNotWorking[i]).change(function() {
                        tableCallback(this.checked, columnNotWorking[i], tableNotWorking);
                        saveUserSettings(page,userId,checkBoxNotWorking[i],this.checked,"{{csrf_token()}}");
                    });
                }
            });
            //-----Переключение стилей для кнопки выпадающего списка отображаемых блоков в зависимости от режима--------
            if ($("#debug-mode").length) {
                $('.btn__show-hidden-blocks').removeClass('btn__show-hidden-blocks__without-debug_mode');
            } else {
                $('.btn__show-hidden-blocks').addClass('btn__show-hidden-blocks__without-debug_mode');
            }
            //----------------------------------------------------------------------------------------------------------
            loadChartData(3);
            //--Установка даты по умолчанию после загрузки страницы-----------------------------------------------------
            let dStart = new Date(new Date()-7*24*60*60*1000).toISOString().slice(0,-5);
            let dEnd = new Date().toISOString().slice(0,-5);
            $("#rep_date_start").val(dStart);
            $("#rep_date_end").val(dEnd);
            loadStatStatesChartData();
            //--Переключение блокировки перемещения блоков на странице--------------------------------------------------
            $(".btn__dnd-switch").click(function(){
                $(this).css('background-image', "url('/img/lock-close.png')");
                $(".container__dad > div").each(function(i,elem) {
                    if ($(this).hasClass("dnd-sortable-handle")) {
                        $(this).removeClass("dnd-sortable-handle");
                    } else {
                        $(this).addClass("dnd-sortable-handle");
                        $(".btn__dnd-switch").css('background-image', "url('/img/lock-open.png')");
                    }
                });
            });
            //--Вращение треугольника на кнопке "Подробнее" таблицы статусов оборудования-------------------------------
            $(".div__open-table").click(function(){
                let arrowTriangle = $(".but__open-table");
                if (arrowTriangle.hasClass("but__arrow-down")) {
                    arrowTriangle.removeClass('but__arrow-down').addClass('but__arrow-up');
                } else {
                    arrowTriangle.removeClass('but__arrow-up').addClass('but__arrow-down');
                }
            });
            //----------------------------------------------------------------------------------------------------------
        })
    </script>
    <!-- Диаграммы и таблица Статусы оборудования -->
    <script src="/js/chartjs-plugin-datalabels.min.js"></script>
    <script src="/js/diagrams.js"></script>
@endpush
