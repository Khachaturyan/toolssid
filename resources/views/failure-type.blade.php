@extends('layouts.app')

@section('nav-bar-container')
    @if(Route::currentRouteName() == 'failure-type')
        <button class="btn btn-success" id="add-new">+</button>
        <button class="btn btn-success" onclick="window.location='/failure-type-arc'" style="margin-left: 16px">{{ __('str.failure_type_archive') }}</button>
    @endif
    @if(Route::currentRouteName() == 'failure-type-arc')
        <button class="btn btn-success" onclick="window.location='/failure-type'" >{{ __('str.failure_type') }}</button>
    @endif
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">

            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="CellSizesModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modalTitleHeader"></h4>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <form action="/failure-type-action" method="POST" enctype="multipart/form-data" style="width: 100%">
                                @csrf
                                <input type="hidden" id="id" name="id" value="">
                                <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('str.name') }}" required>
                                <br>
                                <button class="btn btn-success">{{ __('str.save') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                    "data-page-length" => "100",
                    "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    {{$dataTable->scripts()}}

    <script>

        function onChangeArchiveStatus(id){
            $.get( "/failure-change-arc-state/" + id, function(data, status) {
                if (status === 'success'){
                    if ('true' == data){
                        $('#failure-type-table').DataTable().ajax.reload();
                    }else{
                        alert(data);
                        console.log(data);
                    }
                }
            });
        }

        $(document).ready(function(){
            @if(Route::currentRouteName() == 'failure-type')
                $('#add-new').click(function () {
                    $('#modalTitleHeader').text("{{ __('str.create') }}");
                    $('#id').val("");
                    $('#name').val("");
                    $('#modal').modal('show');
                });
            @endif
            //--- обработка кликов по таблице --------------------------------------------------------------------------
            $('#failure-type-table').on('click', 'tbody td', function() {
                if (this.cellIndex > 0){
                    let tr  = $(this).closest('tr');
                    let row = $('#failure-type-table').DataTable().row(tr).data();
                    $('#modalTitleHeader').text("{{ __('str.edit') }}");
                    $('#id').val(row.id);
                    $('#name').val(row.name);
                    $('#modal').modal('show');
                }
            });
            //----------------------------------------------------------------------------------------------------------
        });
    </script>
@endpush
