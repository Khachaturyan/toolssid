<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
          integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
          crossorigin="anonymous"/>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom-table.css') }}" rel="stylesheet">

    @yield('third_party_stylesheets')

    @stack('page_css')
</head>

<body class="hold-transition sidebar-mini layout-fixed">


    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                <button class="btn btn-success" onclick="window.location.href='/'">{{ __('str.login_to_admin') }}</button>
                <br><br>
                <!--
                Установка:
                <br>
                1. Убедитесь что у вас есть доступ к серверу по SSH.
                <br>
                2. скачайте и распакуйте этот <a href="/doc/doc.rar" class="d-none d-md-inline">архив</a> в домашнюю папку сервера
                <br>
                3. ввести команду: sudo chmod +x ~/vps-install
                <br>
                4. ввести команду: sudo bash vps-install; везде отвечать где спросят y, где попросят нажать Enter - нажать Enter, в конфигураторе MYSQL выбрать apache2, пароль при настройке любой, можно root
                <br>
                5. ввести команду: sudo java -jar autoinstall_jar.jar -all; или ввести команду: sudo java -jar autoinstall_jar.jar -all -https; если используете SSL, не забудьте проверить наличие файлов сертификата /etc/ssl/domain_name.crt, /etc/ssl/private.key, /etc/ssl/chain.crt
                <br>
                6. ввести команду: sudo chmod +x /var/www/html/.htaccess;
                <br>
                7. ввести команду: sudo chmod +x /var/www/html/update.sh;
                <br>
                8. ввести команду: sudo systemctl restart apache2
                <br>
                9. для обновления кода проекта использовать команду: sudo /var/www/html/./update.sh;
                <br>
                10. <a href="https://gitlab.com/component17/backend-box" class="d-none d-md-inline">Исходный код проекта</a>
                <br>
                11. --> {{ __('str.files') }}:
                <br>
                <?php
                    function printFiles($path,$linkPath = null){
                        if (is_dir($path)){
                            $files = File::files($path);
                            foreach ($files as $f){
                                $fileName = basename($f);
                                $linkPath = $linkPath != null ? $linkPath : $path;
                                echo '<button type="submit" class="btn btn-success" style="margin: 8px" onclick="window.location.href=' . "'" . $linkPath . '/' . $fileName . "'" . '">' . $fileName . '</button><br>';
                            }
                        }//else{
                            //die($path);
                        //}
                    }
                    printFiles(public_path() . "/files", "/files");
                    printFiles(public_path() . "/bac", "/bac");
                    printFiles(Storage::path('public/apk'),'storage/apk');
                ?>
                <br>
<!--
                Мануал для андроид:
                <br>
                <div id="manual-container">
                    <br>
                    {!!  nl2br(e($manual)) !!}
                </div>
                <div id="manual-edit-container" style="display: none">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li> {{$error}} </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <br>
                    <form method="post"  action="/manual-android-save">
                        @csrf
                        <div class="input-group">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" style="width: 200px; height: 300px">Описание</span>
                            </div>
                            <textarea name="manual-android" class="form-control" aria-label="With textarea">{{$manual}}</textarea>

                        </div>
                        <br>
                        <button type="submit" class="btn btn-success" style="width: 200px">Сохранить мануал</button>
                    </form>
                </div>

                @if ($isAuth)
                    <br><br>
                    <button class="btn btn-success" onclick="beginEditAndroidManual();">Редактировать мануал для Андроид. </button>
                    <br>
                @endif
-->
            </div>
        </div>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
    <script>
        function beginEditAndroidManual(){
            let c = document.getElementById("manual-container");
            if (c.style.display === "none") {
                c.style.display = "block";
                document.getElementById("manual-edit-container").style.display = "none";
            } else {
                c.style.display = "none";
                document.getElementById("manual-edit-container").style.display = "block";
            }
        }
    </script>
</body>
</html>


