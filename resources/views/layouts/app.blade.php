<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>TOOLSiD</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Alegreya+SC:ital,wght@0,400;1,700&family=IBM+Plex+Serif:ital,wght@1,500&family=Manrope:wght@400;600&family=Montserrat:wght@700&family=Roboto:ital,wght@0,400;1,400;1,700&family=Vollkorn+SC:wght@900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="/css/font-awesome5140.min.css"/>
    <link rel="stylesheet" href="/css/fonts.googleapis.css">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->

    <link rel="stylesheet" type="text/css"
          href="/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
          href="/app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css">

    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap-extended.css">

    <link rel="stylesheet" type="text/css" href="/bootstrap-5.1.3-dist/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/dark-layout.css">

    <link rel="stylesheet" type="text/css" href="/css/tabulator.css">

    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/semi-dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/pages/app-user.css">
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <!-- END: Page CSS-->
    <!--
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    -->
    <link rel="stylesheet" type="text/css" href="/css/style.css">

    @yield('third_party_stylesheets')

    @stack('page_css')
    <style>
        .dt-buttons{
            display: inline-block;
            margin-right: 10px;
        }
        .dataTables_length{
            display: inline-block;
        }
        .dataTables_filter{
            display: inline-flex;
            float: right;
        }
        .add-new-create{
            border-color: #7367f0 !important;
            background-color: #7367f0 !important;
        }
        table thead th select{
            background: none;
            border: none;
            text-transform: uppercase;
            font-size: 0.857rem;
            letter-spacing: 0.5px;
            font-family: "Montserrat", Helvetica, Arial, serif;
            font-weight: 600;
            color: #5f5c68;
        }

        div.dataTables_wrapper div.dataTables_filter {
            text-align: left;
            float: left;
        }

        .select-search{
            width: auto!important;
            padding: 0 20px 0 10px!important;
            margin: 0px 0 0 15px!important;
            height: 30px!important;
            position: relative;
            top: 14px;
        }

        .layout-fixed .select-search {
            width: auto!important;
            margin: 0px 0 0 15px!important;
            height: 31px!important;
            position: relative;
            top: 0px;
            display: inline-block!important;
            padding: 0 0.75rem!important;
        }

        table.dataTable td {
            font-size: 0.9em;
        }

        table.dataTable tbody td {
            vertical-align: top;
        }

        .dt-buttons button{
            height: 29px;
            padding: 0;
            width: 45px;
        }

        div.dataTables_wrapper div.dataTables_filter select, div.dataTables_wrapper div.dataTables_length select{
            margin: 0;
        }

        a {
            text-decoration: none;
        }

        .modal-dialog {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%) !important;
            z-index:3200;
        }

        @media (min-width: 1500px) {
            .modal-lg {
                width: 1500px;
                z-index:3200;
            }
        }

        .circle {
            width: 20px;
            height: 20px;
            padding: 4px;
            line-height: 2px;
            border-radius: 50%;
            font-size: 12px;
            color: #fff;
            text-align: center;
            background: #fc0000
        }

        /* ----Выбор отображаемых блоков---- */
        /* ключевые кадры */
        @keyframes rotate_shesterenka {
            0% {
                transform: scale(1.1);
            }
            25% {
                transform: rotate(60deg);
            }
            50% {
                transform: scale(1.1);
            }
            75% {
                transform: rotate(0deg);
            }
            100% {
                transform: scale(1);
            }
        }
        .btn__show-hidden-blocks {
            width: 24px;
            height: 24px;
            border-radius: 50%;
            border: none;
            background-color: rgba(255, 255, 255, 0);
            background-image: url('/img/new-des_setting-1.svg');
            background-repeat: no-repeat;
            background-position: center;
            /* margin-top: 6px; */
            /* -webkit-transition: -webkit-transform 0.7s ease-in-out;
            transition: transform 0.7s ease-in-out; */
        }
        .btn__show-hidden-blocks:hover {
            /* -webkit-transform: rotate(60deg);
            transform: rotate(60deg); */
            animation-name: rotate_shesterenka;
            animation-duration: 1.5s;
            animation-timing-function: ease;
            animation-iteration-count: 1;
            animation-direction: normal;
            animation-play-state: running;
            animation-delay: 0s;
            animation-fill-mode: none;
        }
        /* .btn__show-hidden-blocks:hover {
            transition: transform 0.2s line;
            transform: scale(1.1);
        }  */
        .btn__show-hidden-blocks__without-debug_mode {
            top: 0px;
        }
        /*.show-hidden-bloks_title {
            margin-bottom: 0;
            margin-right: 10px;
            font-family: 'Manrope';
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 1.5;
            letter-spacing: 0.01em;
            color: #1A1A1A;
        }*/
        /* ----Стили навбара---- */
        .header_company_name {
            margin: 0 10px;
            font-family: 'Manrope';
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 1.5;
            letter-spacing: 0.01em;
            color: #666666;
        }
        .dark-layout .header_company_name {
            color: #b4b7bd;
        }
        .avatar__box {
            display: flex;
            width : 48px;
            height : 48px;
            border-radius: 50%;
            flex-direction: column;
            text-align: center;
            justify-content: center;
            background-color: #E6E6E6;
        }
        .dark-layout .avatar__box {
            background-color: #a8adc2;
        }
        .avatar__content {
            font-family: 'Manrope';
            font-weight: 600;
            font-size: 24px;
            color: #666666;
            text-transform: uppercase;
        }
        .avatar__name {
            font-family: 'Manrope';
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 1.5;
            letter-spacing: 0.01em;
            color: #666666;
        }
        .dark-layout .avatar__name {
            color: #b4b7bd;
        }
        .event__hover-item:hover {
            transition: transform 0.4s line;
            box-shadow: 0px 1px 0px #ccc, 0px -1px 0px #ccc, 1px 0px 0px #ccc, -1px 0px 0px #ccc;
            border-radius: 50%;
        }
        /* Стили модального окна оповещения о новостях */
        .but__title_footer {
            vertical-align: middle;
            font-size: 16px;
            margin: 0;
            padding: 6px 12px;
            /* font-family: 'Manrope'; */
            font-size: 18px;
            font-weight: 600;
            color: #fff;
        }
        .dark-layout .but__title_footer {
            color: #000;
        }
        .but__title_body {
            /* font-family: 'Manrope'; */
            font-size: 16px;
        }
    </style>
</head>

<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static @if($thema) dark-layout @endif"
      data-open="click" data-menu="vertical-menu-modern" data-col="">

@php
    $lang = \App\Http\Controllers\ProjectSettingsController::getUiLang()->data;
    $lang = $lang === 'auto' ? Request::getPreferredLanguage(Config::get('app.available_locales')) : $lang;
    $lang = $lang != null ? $lang : 'en';
    App::setLocale($lang);
    //Config::set('app.locale', $lang);
    $currentRouteName  = Route::currentRouteName();
@endphp

<div class="modal fade" id="modal-faq" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog" role="document" style="max-width: 1500px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabelHeader">{{ __('str.menu_manuals') }}</h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="main-faq-body"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-news" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog" role="document" style="max-width: 1500px;">
        <div class="modal-content">
            <div class="modal-header">
                <div style="width: 100%">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.menu_news') }}</h4>
                    @if (Auth::user()->enableUpdateWeb())
                        <button type="button" class="close" onclick="updateWebLk()"
                                style="float:right;margin-right: 16px;margin-bottom: 16px;padding: 8px;font-size: medium">
                            {{ __('str.main_check_update') }}
                        </button>
                    @endif
                </div>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="main-news-body" style="width: 1500px;height:600px;overflow:scroll;overflow-x:hidden;">
            </div>
        </div>
    </div>
</div>

<!-- Modal alert news center -->
<div class="modal fade" id="modal-mess" tabindex="-1" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content " style="border-radius:20px;overflow: hidden;">
            <div class="modal-header" style="justify-content:center;border-top-right-radius:20px;border-top-left-radius:20px;">
                <h4 class="modal-title" style="margin-top:0px;font-weight:700;">{{ __('str.content_modal_header') }}</h4>
            </div>
            <div class="modal-body">
                <div style="text-align: center;">
                    <h4 style="font-weight:700;">{{ __('str.main_has_system_messages') }}</h4>
                </div>
                <div style="text-align: center;">
                    <p class="but__title_body">{{ __('str.content_modal_body') }}</p>
                </div>
            </div>
            <div class="modal-footer" style="justify-content:center;">
                <button type="button" class="btn-success" data-dismiss="modal">
                    <p class="but__title_footer">{{ __('str.content_modal_footer') }}</p>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Main Header -->
<nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow">
    <div class="navbar-container d-flex content">
        <div class="bookmark-wrapper d-flex align-items-center">
            <ul class="nav navbar-nav d-xl-none">
                <li class="nav-item"><a class="nav-link menu-toggle" href="javascript:void(0);"><i
                            class="fas fa-bars"></i></a></li>
            </ul>

            @if($currentRouteName != 'home')
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item">
                        <svg style="padding-bottom:4px" width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9.02 2.84004L3.63 7.04004C2.73 7.74004 2 9.23004 2 10.36V17.77C2 20.09 3.89 21.99 6.21 21.99H17.79C20.11 21.99 22 20.09 22 17.78V10.5C22 9.29004 21.19 7.74004 20.2 7.05004L14.02 2.72004C12.62 1.74004 10.37 1.79004 9.02 2.84004Z" stroke="#2DC7D1" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M12 17.99V14.99" stroke="#2DC7D1" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        <a href="/">Главная</a>
                    </li>
                    <li class="breadcrumb-item active">
                        @if ($currentRouteName == 'stages') {{ __('str.objects') }} @endif
                        @if ($currentRouteName == 'stages-arc') {{ __('str.objects') }} архивные @endif
                        @if ($currentRouteName == 'blocks') {{ __('str.blocks') }} @endif
                        @if ($currentRouteName == 'blocks-arc') {{ __('str.blocks') }} архивные @endif
                        @if ($currentRouteName == 'cells') {{ __('str.cells') }} @endif
                        @if ($currentRouteName == 'roles') {{ __('str.roles') }} @endif
                        @if ($currentRouteName == 'roles-arc') {{ __('str.roles') }} архивные @endif
                        @if ($currentRouteName == 'mans') В штате @endif
                        @if ($currentRouteName == 'men-fired') Уволенные @endif
                        @if ($currentRouteName == 'man-item') Информация, о сотруднике @endif
                        @if ($currentRouteName == 'etypes') {{ __('str.equ_types') }} @endif
                        @if ($currentRouteName == 'equipment') {{ __('str.equipment') }} @endif
                        @if ($currentRouteName == 'tm-men') {{ __('str.messages') }} @endif
                        @if ($currentRouteName == 'blocks-equipment') Информация, о блоках @endif
                        @if ($currentRouteName == 'project-settings2') {{ __('str.project_settings') }} @endif
                    </li>
                </ol>
            @endif
            @yield('head-buttons')
        </div>
        <ul class="nav navbar-nav align-items-center ml-auto">

            <li class="nav-item header_company_name ">
                @if(isset($header_company_name))
                    {{ $header_company_name }}
                @endif
            </li>

            <li class="nav-item">
                <!-- <i class="ficon" data-feather="info" onclick="onManualClick();"></i> -->
                <span onclick="onManualClick();">
                    <img
                        @if ($thema)
                            src="/img/new-des_info-circle-dark.svg"
                        @else
                            src="/img/new-des_info-circle.svg"
                        @endif class="event__hover-item"
                    >
                </span>
                <span id="count_new_manual" class="circle" style="margin-left: -15px;visibility: hidden" onclick="onManualClick();" >0</span>
            </li>

            <li class="nav-item">
                <img
                    @if ($thema)
                        src="/img/new-des_news-dark.svg"
                    @else
                        src="/img/new-des_news.svg"
                    @endif  onclick="onNewsClick();" class="event__hover-item"
                />
                <span id="count_new_release_notes" class="circle" style="margin-left: -15px;visibility: hidden" onclick="onNewsClick();" >0</span>
            </li>

            <li class="nav-item dropdown dropdown-user ml-1">
                <a class="nav-link dropdown-toggle dropdown-user-link"
                   id="dropdown-user" href="javascript:void(0);"
                   data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <div class="avatar__box">
                        <!-- <span class="avatar-status-online"></span> -->
                        <div class="avatar__content"></div>
                    </div>
                    <div class="user-nav d-sm-flex d-none mr-2">
                        <span class="user-name avatar__name ml-2 mb-0" id="first__letter-account">{{ Auth::user()->name }}</span>
                        <!-- <span class="user-status">{{ Auth::user()->created_at->format('M. Y') }}</span> -->
                        <!-- <span>
                            <img src="img//new-des_arrow-down.svg" alt="arrow-down">
                        </span> -->
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                    <a href="#" class="btn btn-default btn-flat float-right"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Выйти
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('user-thema') }}">
                    <!-- <i class="ficon1" data-feather="moon"></i> -->
                    <!-- <div class="switch__themes"></div> -->
                    <img
                        @if ($thema)
                            src="/img/new-des_sun-dark.svg"
                        @else
                            src="/img/new-des_moon1.svg"
                        @endif style="width:48px;height:48px;"
                    />
                </a>
            </li>
        </ul>
    </div>
</nav>

<!-- Left side column. contains the logo and sidebar -->
@include('layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="app-content content ">
    <div class="content-wrapper">
        <div id="nav-bar-container">
            @yield('nav-bar-container')

            @if ($currentRouteName == 'tm-men')
                <button type="button" class="btn btn-primary" id="bSendAll" style="margin-right: 16px">Отправить сообщение всем</button>
                <button type="button" class="btn btn-primary" id="bSendSelected">Отправить сообщение выбранным</button>
            @endif

        </div>
        <section class="">
            <div class="row">
                @yield('content')
            </div>
        </section>
    </div>
</div>


<script src="/bootstrap-5.1.3-dist/js/bootstrap.bundle.min.js"></script>

<script src="/app-assets/vendors/js/vendors.min.js"></script>
<script src="/app-assets/js/scripts/components/components-popovers.js"></script>
<script src="/app-assets/js/core/app-menu.js"></script>
<script src="/app-assets/js/core/app.js"></script>

<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>

@stack('scripts')

@yield('third_party_scripts')

@stack('page_scripts')

<script>

    let faqIsInit           = false;
    let rnTableIsInited     = false;
    let maxReleaseNoteId    = parseInt("{{$last_news_id}}");
    let maxManualId         = parseInt("{{$last_manual_id}}");
    let isNewUser           = maxManualId === 0 && maxReleaseNoteId === 0;
    let defMaxReleaseNoteId = maxReleaseNoteId;
    let defMaxManualId      = maxManualId;
    let isAdmin             = 1 == parseInt("{{\Illuminate\Support\Facades\Auth::user()->needShowAlertAboutNewSystemMessages()}}");
    let isMainPage          = 1 == parseInt("{{$currentRouteName == 'main'}}");
    let isAlertWasOpen      = false; // алерт о новых сообщениях уже был показан

    function formatDate(dat){
        let arr = dat.replace("T"," ").split(" ");
        let arr2 = arr[0].split("-");
        return arr2[2] + "." + arr2[1] + "." + arr2[0];
    }

    function updateWebLk(){
        $.get( "/set-autoupdate/true", function(data, status) {
            if (status === 'success'){
                if ('true' === data){
                    alert("{{ __('str.main_lk_will_update') }}");
                }
            }
        });
    }

    function initNewInfoData(){
        let url = "http://90.156.203.30/api/info-count/" + defMaxReleaseNoteId + "/" + defMaxManualId;
        $.get( url, function(_data, status) {
            if (status === 'success'){
                let d = JSON.parse(_data);
                let newNewsCount = d.count_rn;
                maxReleaseNoteId = d.max_rn;
                if (newNewsCount > 0 && !isNewUser){
                    if (newNewsCount > 99) newNewsCount = 99;
                    newNewsCount = "" + newNewsCount;
                    if (newNewsCount.length = 1) newNewsCount = "0" + newNewsCount;
                    $('#count_new_release_notes').text(newNewsCount);
                    $('#count_new_release_notes').css('visibility','visible');
                }else{
                    $('#count_new_release_notes').css('visibility','hidden');
                }
                let newManualCount = d.count_m;
                maxManualId = d.max_m;
                if (newManualCount > 0 && !isNewUser){
                    if (newManualCount > 99) newManualCount = 99;
                    newManualCount = "" + newManualCount;
                    if (newManualCount.length = 1) newManualCount = "0" + newManualCount;
                    $('#count_new_manual').text(newManualCount);
                    $('#count_new_manual').css('visibility','visible');
                }else{
                    $('#count_new_manual').css('visibility','hidden');
                }
                let needAlertAboutNewSystemMessages = isMainPage && d.sys_count_rn > 0 &&
                    isAdmin && !isAlertWasOpen && !isNewUser;
                if (needAlertAboutNewSystemMessages){
                    isAlertWasOpen = true;
                    // элемент, содержащий контент модального окна
                    const elemModal = document.querySelector('#modal-mess');
                    const modal = new bootstrap.Modal(elemModal, {
                        backdrop: true,
                        keyboard: true,
                        focus: true
                    });
                    modal.show();
                }
                if (isNewUser){
                    saveMaxManualId();
                    saveMaxReleaseNoteId();
                }
            }
        });
    }

    function saveMaxReleaseNoteId(){
        let post ={};
        post['_token' ] = "{{csrf_token()}}";
        post['id'     ] = maxReleaseNoteId;
        post['uid'    ] = {{ Auth::user()->id }};
        $.post( "/update-release-note-max-id", post, function(data, status) {
            if (status === 'success'){
                console.log(data);
            }
        });
    }

    function saveMaxManualId(){
        let post ={};
        post['_token' ] = "{{csrf_token()}}";
        post['id'     ] = maxManualId;
        post['uid'    ] = {{ Auth::user()->id }};
        $.post( "/update-manual-max-id", post, function(data, status) {
            if (status === 'success'){
                if ("true" === data){
                    console.log(data);
                }
            }
        });
    }

    function onNewsClick(){

        if (maxReleaseNoteId != null){
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            post['id'     ] = maxReleaseNoteId;
            post['uid'    ] = {{ Auth::user()->id }};
            $.post( "/update-release-note-max-id", post, function(data, status) {
                //console.log(data);
                if (status === 'success'){
                    if ("true" === data){
                        defMaxReleaseNoteId = maxReleaseNoteId;
                        initNewInfoData();
                    }else{
                        console.log(data);
                    }
                }
            });
        }

        if (!rnTableIsInited){
            rnTableIsInited = true;
            let host = "http://90.156.203.30/";
            let url = host + "api/release-note-list2";
            $.get( url, function(_data, status) {
                if (status === 'success'){
                    let data = JSON.parse(_data);
                    let tmp = '<div class="container-fluid"><div class="card" style="margin-top: 20px;">' +
                        '<div class="card-body"><table style="width: 100%"><tr><th>##</th><th style="width: 50px;">$$</th></tr></table></div></div></div>';
                    for (let i = 0; i < data.length; i++){
                        let str1 = "<p>" + data[i].name + "</p>";
                        str1 += "<p>" + data[i].des + "</p>";
                        //str1 += "<p>" + data[i].author + "</p>";
                        str1 += "<p>" + formatDate(data[i].created_at) + "</p>";
                        let str2 = "";
                        if (data[i].link.length > 0) {
                            str2 += '<a href="' + host + data[i].link + '" target="_blank"><img style="float: right;width:48px;height:48px;" src="/img/picture_as_pdf_black_24dp.svg" alt="img"></a>';
                        }
                        $("#main-news-body").append(tmp.replaceAll('##',str1).replaceAll('$$',str2));
                    }
                    $('#modal-news').modal('show');
                }
            });
        }else{
            $('#modal-news').modal('show');
        }
    }

    function onManualClick(){

        if (maxManualId != null){
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            post['id'     ] = maxManualId;
            post['uid'    ] = {{ Auth::user()->id }};
            $.post( "/update-manual-max-id", post, function(data, status) {
                //console.log(data);
                if (status === 'success'){
                    if ("true" === data){
                        defMaxManualId = maxManualId;
                        initNewInfoData();
                    }else{
                        console.log(data);
                    }
                }
            });
        }

        if (!faqIsInit){
            let url = "http://90.156.203.30/api/manuals";
            $.get( url, function(_data, status) {
                if (status === 'success'){
                    faqIsInit = true;
                    let data = JSON.parse(_data);
                    let tmp = '<div class="container-fluid"><div class="card" style="margin-top: 20px;">' +
                        '<div class="card-body"><table style="width: 100%"><tr><th>##</th><th style="width: 50px;">$$</th></tr></table></div></div></div>';
                    for (let i = 0; i < data.length; i++){
                        let str1 = "<p>" + data[i].name + "</p>";
                        if (data[i].des.length > 0) {
                            str1 += "<p>" + data[i].des + "</p>";
                        }
                        let str2 = "";
                        if (data[i].link.length > 0) {
                            str2 += '<a href="' + data[i].link + '" target="_blank"><img style="float: right;width:48px;height:48px;" src="/img/picture_as_pdf_black_24dp.svg" alt="img"></a>';
                        }
                        $( "#main-faq-body" ).append(tmp.replaceAll('##',str1).replaceAll('$$',str2));
                    }
                    $('#modal-faq').modal('show');
                }
            });
        }else{
            $('#modal-faq').modal('show');
        }
    }

    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            });
        }
        //--------------------------------------------------------------------------------------------------------------
        setInterval(function() {
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            $.post( "/check-auth", post)
                .done(function(msg){
                    //console.log(msg);
                })
                .fail(function(xhr, status, error) {
                    //console.log(xhr.status);
                    if (xhr.status == 419){
                        window.location.href = "/login";
                    }
                });
        }, 5000);
        //--------------------------------------------------------------------------------------------------------------
        initNewInfoData();
    })

    //------------ Первая буква имени в иконке аккаунта ----------------------------------------------------------------
    let strFirstLetter = $("#first__letter-account").text();
    strFirstLetter = strFirstLetter.substring(0, 1);
    $(".avatar__content").text(strFirstLetter);
    //------------------------------------------------------------------------------------------------------------------

</script>

</body>
</html>

