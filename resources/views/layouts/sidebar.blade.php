<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header" id="b-expandsidebar">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto mt-2">
                <a class="navbar-brand" href="/">
                    <h2 class="brand-text">TOOLSID</h2>
                </a>
            </li>
            <li class="nav-item nav-toggle">
                <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                    <!-- <i class="d-block d-xl-none text-primary fas fa-times font-medium-4" data-feather="x"></i> -->
                    <!-- <i class="d-none d-xl-block font-medium-4  text-primary fas fa-bars"  data-feather="disc" data-ticon="disc"></i> -->
                    <div class="sidebar__arrow-left">
                        <!-- <img src="/img/arrow-left.svg" alt="setting"> -->
                    </div>
                </a>
            </li>
        </ul>
    </div>
    <hr class="hr__sidebar">
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            @include('layouts.menu')
        </ul>
    </div>
</div>
