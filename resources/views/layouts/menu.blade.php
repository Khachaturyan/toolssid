
@if (Auth::user()->enableAuthWeb())
    <li class="nav-item @if ($currentRouteName == 'main') active @endif">
        <a href="{{ route('main') }}" class='d-flex align-items-center'>
            <!-- <i data-feather='settings'></i> -->
            <!-- <img class="ss" style="padding-right:16px" src="/img/new-des_home-2.svg" alt="setting"> -->
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M9.02 2.84004L3.63 7.04004C2.73 7.74004 2 9.23004 2 10.36V17.77C2 20.09 3.89 21.99 6.21 21.99H17.79C20.11 21.99 22 20.09 22 17.78V10.5C22 9.29004 21.19 7.74004 20.2 7.05004L14.02 2.72004C12.62 1.74004 10.37 1.79004 9.02 2.84004Z" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M12 17.99V14.99" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
            </svg>
            <span>{{ __('str.home') }}</span>
        </a>
    </li>

    <li class="nav-item @if ($currentRouteName == 'type-load') active @endif"
        @if ($debug_mode_var != 'true')  style="display: none"  @endif>
        <a href="{{ route('type-load') }}" class='d-flex align-items-center' >
            <!-- <i data-feather='archive'></i> -->
            <!-- <img style="padding-right:16px" src="/img/new-des_gift.svg" alt="setting"> -->
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M20.4697 9H4.46973V17C4.46973 20 5.46973 21 8.46973 21H16.4697C19.4697 21 20.4697 20 20.4697 17V9Z" stroke="#666666" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M22 6V7C22 8.1 21.47 9 20 9H5C3.47 9 3 8.1 3 7V6C3 4.9 3.47 4 5 4H20C21.47 4 22 4.9 22 6Z" stroke="#666666" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M9.5 12H15.53" stroke="#666666" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
            </svg>
            <span>{{ __('str.type_load') }}</span>
        </a>
    </li>

    <li class=" nav-item">
        <a class="d-flex align-items-center" href="#">
            <!-- <i data-feather='map-pin'></i> -->
            <!-- <img style="padding-right:16px" src="/img/new-des_location.svg" alt="location"> -->
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M11.9999 13.4299C13.723 13.4299 15.1199 12.0331 15.1199 10.3099C15.1199 8.58681 13.723 7.18994 11.9999 7.18994C10.2768 7.18994 8.87988 8.58681 8.87988 10.3099C8.87988 12.0331 10.2768 13.4299 11.9999 13.4299Z" stroke="#666666" stroke-width="1.5" class="svg-change__stroke"/>
                <path d="M3.6202 8.49C5.5902 -0.169998 18.4202 -0.159997 20.3802 8.5C21.5302 13.58 18.3702 17.88 15.6002 20.54C13.5902 22.48 10.4102 22.48 8.3902 20.54C5.6302 17.88 2.4702 13.57 3.6202 8.49Z" stroke="#666666" stroke-width="1.5" class="svg-change__stroke"/>
            </svg>
            <span>{{ __('str.sectors') }}</span>
        </a>
        <ul class="menu-content">

            <li class="nav-item @if ($currentRouteName == 'stages') active @endif"
                @if (!Auth::user()->change_object()) style="display: none"  @endif>
                <a href="{{ route('stages') }}" class='d-flex align-items-center'>
                    <!-- <i data-feather='circle'></i> -->
                    <span>{{ __('str.objects') }}</span>
                </a>
            </li>

            <li class="nav-item @if ($currentRouteName == 'blocks') active @endif"
                @if (!Auth::user()->change_box()) style="display: none"  @endif>
                <a href="{{ route('blocks') }}"  class='d-flex align-items-center' >
                    <!-- <i data-feather='circle'></i> -->
                    <span>{{ __('str.blocks') }}</span>
                </a>
            </li>

            <li class="nav-item @if ($currentRouteName == 'cells') active @endif"
                @if (!Auth::user()->change_cell()) style="display: none"  @endif>
                <a href="{{ route('cells') }}" class='d-flex align-items-center' >
                    <!-- <i data-feather='circle'></i> -->
                    <span>{{ __('str.cells') }}</span>
                </a>
            </li>

        </ul>
    </li>

    <li class=" nav-item">
        <a class="d-flex align-items-center" href="#">
            <!-- <i data-feather='user-check'></i> -->
            <!-- <img style="padding-right:16px" src="/img/new-des_user-tick.svg" alt="setting"> -->
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 12C14.7614 12 17 9.76142 17 7C17 4.23858 14.7614 2 12 2C9.23858 2 7 4.23858 7 7C7 9.76142 9.23858 12 12 12Z" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M3.41016 22C3.41016 18.13 7.26015 15 12.0002 15C12.9602 15 13.8902 15.13 14.7602 15.37" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M22 18C22 18.75 21.79 19.46 21.42 20.06C21.21 20.42 20.94 20.74 20.63 21C19.93 21.63 19.01 22 18 22C16.54 22 15.27 21.22 14.58 20.06C14.21 19.46 14 18.75 14 18C14 16.74 14.58 15.61 15.5 14.88C16.19 14.33 17.06 14 18 14C20.21 14 22 15.79 22 18Z" stroke="#666666" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M16.4399 18L17.4299 18.99L19.5599 17.02" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
            </svg>
            <span>{{ __('str.workers') }}</span>
        </a>
        <ul class="menu-content">
            <li class="@if ($currentRouteName == 'mans') active @endif">
                <a class="d-flex align-items-center" href="{{ route('mans') }}">
                    <!-- <i data-feather='circle'></i> -->
                    <span class="menu-item text-truncate" data-i18n="List">{{ __('str.mens_in_state') }}</span>
                </a>
            </li>
            <li class="@if ($currentRouteName == 'men-fired') active @endif">
                <a class="d-flex align-items-center" href="/men-fired">
                    <!-- <i data-feather='circle'></i> -->
                    <span class="menu-item text-truncate" data-i18n="List">{{ __('str.mens_dismissed') }}</span>
                </a>
            </li>
            <li class="nav-item @if ($currentRouteName == 'roles') active @endif">
                <a class="d-flex align-items-center" href="/roles">
                    <!-- <i data-feather='circle'></i> -->
                    <span class="menu-item text-truncate" data-i18n="List">{{ __('str.roles') }}</span>
                </a>
            </li>
        </ul>
    </li>

    <li class=" nav-item">
        <a class="d-flex align-items-center" href="#">
            <!-- <i data-feather='video'></i> -->
            <!-- <img style="padding-right:16px" src="/img/new-des_video.svg" alt="equipment"> -->
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.53 20.42H6.21C3.05 20.42 2 18.32 2 16.21V7.78996C2 4.62996 3.05 3.57996 6.21 3.57996H12.53C15.69 3.57996 16.74 4.62996 16.74 7.78996V16.21C16.74 19.37 15.68 20.42 12.53 20.42Z" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M19.5202 17.1L16.7402 15.15V8.84001L19.5202 6.89001C20.8802 5.94001 22.0002 6.52001 22.0002 8.19001V15.81C22.0002 17.48 20.8802 18.06 19.5202 17.1Z" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M11.5 11C12.3284 11 13 10.3284 13 9.5C13 8.67157 12.3284 8 11.5 8C10.6716 8 10 8.67157 10 9.5C10 10.3284 10.6716 11 11.5 11Z" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
            </svg>
            <span>{{ __('str.equipment') }}</span>
        </a>
        <ul class="menu-content">
            <li class="nav-item @if ($currentRouteName == 'etypes') active @endif"
                @if (!Auth::user()->create_unit_type()) style="display: none"  @endif>
                <a href="{{ route('etypes') }}" class='d-flex align-items-center'>
                    <!-- <i data-feather='circle'></i> -->
                    <span>{{ __('str.equ_types') }}</span>
                </a>
            </li>
            <li class="nav-item @if ($currentRouteName == 'equipment') active @endif">
                <a href="{{ route('equipment') }}" class='d-flex align-items-center' >
                    <!-- <i data-feather='circle'></i> -->
                    <span>{{ __('str.equipment2') }}</span>
                </a>
            </li>
            <li class="nav-item @if ($currentRouteName == 'failure-type') active @endif"
                @if (!Auth::user()->create_failure_types()) style="display: none"  @endif>
                <a href="{{ route('failure-type') }}" class='d-flex align-items-center' >
                    <!-- <i data-feather='circle'></i> -->
                    <span>{{ __('str.failure_type') }}</span>
                </a>
            </li>
            <li class="nav-item @if ($currentRouteName == 'equ-states') active @endif"
                @if (!Auth::user()->change_status()) style="display: none"  @endif>
                <a href="{{ route('equ-states') }}" class='d-flex align-items-center' >
                    <!-- <i data-feather='circle'></i> -->
                    <span>{{ __('str.equ_states') }}</span>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item @if ($currentRouteName == 'home') active @endif"
        @if (!Auth::user()->show_logs()) style="display: none"  @endif>
        <a href="{{ route('home') }}" class='d-flex align-items-center' >
            <!-- <i data-feather='edit'></i> -->
            <!-- <img style="padding-right:16px" src="/img/new-des_message-edit.svg" alt="log"> -->
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M16 2H8C4 2 2 4 2 8V21C2 21.55 2.45 22 3 22H16C20 22 22 20 22 16V8C22 4 20 2 16 2Z" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M12.9098 7.84003L7.71979 13.03C7.51979 13.23 7.3298 13.62 7.2898 13.9L7.0098 15.88C6.9098 16.6 7.40979 17.1 8.12979 17L10.1098 16.72C10.3898 16.68 10.7798 16.49 10.9798 16.29L16.1698 11.1C17.0598 10.21 17.4898 9.17003 16.1698 7.85003C14.8498 6.52003 13.8098 6.94003 12.9098 7.84003Z" stroke="#666666" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M12.1699 8.58008C12.6099 10.1501 13.8399 11.3901 15.4199 11.8301" stroke="#666666" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
            </svg>
            <span>{{ __('str.logs') }}</span>
        </a>
    </li>

    <li class=" nav-item">
        <a class="d-flex align-items-center" href="#">
            <!-- <i data-feather='settings'></i> -->
            <!-- <img style="padding-right:16px" src="/img/new-des_setting-2.svg" alt="setting"> -->
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 15C13.6569 15 15 13.6569 15 12C15 10.3431 13.6569 9 12 9C10.3431 9 9 10.3431 9 12C9 13.6569 10.3431 15 12 15Z" stroke="#666666" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M2 12.8801V11.1201C2 10.0801 2.85 9.22006 3.9 9.22006C5.71 9.22006 6.45 7.94006 5.54 6.37006C5.02 5.47006 5.33 4.30006 6.24 3.78006L7.97 2.79006C8.76 2.32006 9.78 2.60006 10.25 3.39006L10.36 3.58006C11.26 5.15006 12.74 5.15006 13.65 3.58006L13.76 3.39006C14.23 2.60006 15.25 2.32006 16.04 2.79006L17.77 3.78006C18.68 4.30006 18.99 5.47006 18.47 6.37006C17.56 7.94006 18.3 9.22006 20.11 9.22006C21.15 9.22006 22.01 10.0701 22.01 11.1201V12.8801C22.01 13.9201 21.16 14.7801 20.11 14.7801C18.3 14.7801 17.56 16.0601 18.47 17.6301C18.99 18.5401 18.68 19.7001 17.77 20.2201L16.04 21.2101C15.25 21.6801 14.23 21.4001 13.76 20.6101L13.65 20.4201C12.75 18.8501 11.27 18.8501 10.36 20.4201L10.25 20.6101C9.78 21.4001 8.76 21.6801 7.97 21.2101L6.24 20.2201C5.33 19.7001 5.02 18.5301 5.54 17.6301C6.45 16.0601 5.71 14.7801 3.9 14.7801C2.85 14.7801 2 13.9201 2 12.8801Z" stroke="#666666" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
            </svg>
            <span>{{ __('str.settings') }}</span>
        </a>
        <ul class="menu-content">
            <li class="nav-item @if ($currentRouteName == 'project-settings2') active @endif"  @if (!Auth::user()->access_settings()) style="display: none"  @endif>
                <a href="{{ route('project-settings2') }}" class='d-flex align-items-center'>
                    <!-- <i data-feather='circle'></i> -->
                    <span>{{ __('str.menu_project') }}</span>
                </a>
            </li>
            <li class="nav-item @if ($currentRouteName == 'mail-settings') active @endif"  @if (!Auth::user()->access_settings()) style="display: none"  @endif>
                <a href="{{ route('mail-settings') }}" class='d-flex align-items-center'>
                    <!-- <i data-feather='circle'></i> -->
                    <span>{{ __('str.menu_mail') }}</span>
                </a>
            </li>
            <li class="nav-item @if ($currentRouteName == 'sync') active @endif" @if (!Auth::user()->access_settings()) style="display: none"  @endif>
                <a href="{{ route('sync') }}" class='d-flex align-items-center' >
                    <!-- <i data-feather='circle'></i> -->
                    <span>{{ __('str.menu_sync') }}</span>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item @if ($currentRouteName == 'reports') active @endif"   @if (!Auth::user()->export_report()) style="display: none"  @endif>
        <a href="{{ route('reports') }}" class='d-flex align-items-center'>
            <!-- <i data-feather='file-text'></i> -->
            <!-- <img style="padding-right:16px" src="/img/new-des_note.svg" alt="reports"> -->
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M20 8.25V18C20 21 18.21 22 16 22H8C5.79 22 4 21 4 18V8.25C4 5 5.79 4.25 8 4.25C8 4.87 8.24997 5.43 8.65997 5.84C9.06997 6.25 9.63 6.5 10.25 6.5H13.75C14.99 6.5 16 5.49 16 4.25C18.21 4.25 20 5 20 8.25Z" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M16 4.25C16 5.49 14.99 6.5 13.75 6.5H10.25C9.63 6.5 9.06997 6.25 8.65997 5.84C8.24997 5.43 8 4.87 8 4.25C8 3.01 9.01 2 10.25 2H13.75C14.37 2 14.93 2.25 15.34 2.66C15.75 3.07 16 3.63 16 4.25Z" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M8 13H12" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M8 17H16" stroke="#666666" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
            </svg>
            <span>{{ __('str.reports') }}</span>
        </a>
    </li>

    <li class="nav-item @if ($currentRouteName == 'feedbacks') active @endif"
        @if ($debug_mode_var != 'true')  style="display: none"  @endif>
        <a href="{{ route('feedbacks') }}" class='d-flex align-items-center'>
            <!-- <i data-feather='mail'></i> -->
            <!-- <img style="padding-right:16px" src="/img/new-des_sms.svg" alt="feedbacks"> -->
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M17 20.5H7C4 20.5 2 19 2 15.5V8.5C2 5 4 3.5 7 3.5H17C20 3.5 22 5 22 8.5V15.5C22 19 20 20.5 17 20.5Z" stroke="#666666" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
                <path d="M17 9L13.87 11.5C12.84 12.32 11.15 12.32 10.12 11.5L7 9" stroke="#666666" stroke-width="1.5" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" class="svg-change__stroke"/>
            </svg>
            <span>{{ __('str.settings_appeals') }}</span>
        </a>
    </li>
@endif










