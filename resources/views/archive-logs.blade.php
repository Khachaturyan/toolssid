@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="card" style="margin-top: 20px;">
            <div class="card-body">
                <table class="table table-bordered table-striped table__new">
                    <thead class="text-center">
                    <tr style="border-top-style:none">
                        <th>
                            {{ __('str.table_file') }}
                        </th>
                        <th>
                            {{ __('str.table_num') }}
                        </th>
                        <th>
                            {{ __('str.table_created2') }}
                        </th>
                        <th>
                            {{ __('str.table_min_date') }}
                        </th>
                        <th>
                            {{ __('str.table_max_date') }}
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                            <tr class="text-center">
                                <td>
                                    <a href="{{$item['url']}}" target="_blank"><img src="/img/icons8-export-excel-48.png"/></a>
                                </td>
                                <td>
                                    {{$item['num']}}
                                </td>
                                <td>
                                    {{$item['create']}}
                                </td>
                                <td>
                                    {{$item['min']}}
                                </td>
                                <td>
                                    {{$item['max']}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

