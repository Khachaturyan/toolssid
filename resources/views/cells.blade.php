@extends('layouts.app')

@section('nav-bar-container')
    <button class="btn btn-success" id="add-new" onclick="window.location='cell-sizes'">{{ __('str.sizes') }}</button>
@endsection

@section('content')

    <div class="modal fade" id="modal-users" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelHeader">{{ __('str.equ_select_men2') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped" id="men-table" data-page-length="12"></table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-screen" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="cellModalLabelHeader" style="margin-top: 0px;">{{ __('str.cell') }}</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                        <input type="number" id="id" name="id" value="" style="visibility: hidden">
                        <div class="input-group" style="margin-top: 0px;">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.size') }}</span>
                            </div>
                            <select class="form-control" id="csid" name="csid" required>
                                <option value="0" selected>{{ __('str.size_default') }}</option>
                                @foreach($cellSizes as $cellSize)
                                    <option value="{{$cellSize->id}}">{{$cellSize->name}} {{$cellSize->des}}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- <br> -->
                        <div class="input-group">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.blocking') }}</span>
                            </div>
                            <select class="form-control" id="is_lock" name="is_lock" required>
                                <option value="" disabled selected>{{ __('str.state') }}</option>
                                <option value="0">{{ __('str.state_unlock') }}</option>
                                <option value="1">{{ __('str.state_lock') }}</option>
                            </select>
                        </div>
                        <!-- <br> -->
                        <div class="input-group" style="display: none">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" id="inputGroup-sizing-default" style="width: 200px">{{ __('str.cell_dst_state') }}</span>
                            </div>
                            <select class="form-control" id="dst" name="dst" required>
                                <option value="1">{{ __('str.cell_dst_open') }}</option>
                                <option value="2">{{ __('str.cell_dst_close') }}</option>
                                <option value="3">{{ __('str.cell_dst_open_hard') }}</option>
                                <option value="4">{{ __('str.cell_dst_opening') }}</option>
                                <option value="5">{{ __('str.cell_dst_opening_error') }}</option>
                                <option value="0">{{ __('str.cell_dst_unknown') }}</option>
                            </select>
                        </div>
                        <!-- <br> -->
                        <div class="input-group">
                            <div class="input-group-prepend" >
                                <span class="input-group-text" style="width: 200px; height: 300px">{{ __('str.description') }}:</span>
                            </div>
                            <textarea id="des" name="des" class="form-control" aria-label="With textarea"></textarea>
                        </div>
                        <br>
                        <button type="button" class="btn btn-success" style="width: 200px" onclick="onUpdateCellClick();">{{ __('str.save') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card card__modificator" style="margin-top: 20px;">
            <div class="card-body">
                {{$dataTable->table([ "style"=>"width:100%",  "class" => "table table-bordered table-striped",
                    "data-page-length" => "100",
                    "dom" => "<'row'<'col-sm-6'f><'col-sm-6'l>>" ])}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    {{$dataTable->scripts()}}

    <script src="{{ asset('/js/tableSelectors.js') }}"></script>

    <script>

        var menTableIsInit  = false;
        var selectedCell    =  null;
        let blocks = @json($blocks);
        let eTypes = @json($etypesAll);

        function onUpdateCellClick(){
            let post ={};
            post['_token' ] = "{{csrf_token()}}";
            post['id'     ] = $('#id').val();
            post['csid'   ] = $('#csid').val();
            post['is_lock'] = $('#is_lock').val();
            post['dst'    ] = $('#dst').val();
            post['des'    ] = $('#des').val();
            $.post( "/cell-edit-action", post, function(data, status) {
                if (status === 'success'){
                    if ("true" === data){
                        $('#modal-screen').modal('hide');
                        $('#cell-table').DataTable().ajax.reload();
                    }else{
                        console.log(data);
                    }
                }
            });
        }

        function getBlockCellCount(blockName){
            let res = 0;
            for (let i = 0; i < blocks.length; i++){
                if (blocks[i].name == blockName){
                    res = blocks[i].cell_count;
                    break;
                }
            }
            return res;
        }

        function on_edit_click(rowData){
            fillModal(JSON.parse(decodeURIComponent(rowData)));
            $('#modal-screen').modal('show');
        }

        function onMenClick(data){
            if (selectedCell != null){
                let mid = data.id;
                let cid = selectedCell.id;
                let isChecked = data.is_checked.length > 0;
                let url = !isChecked ? "/mce-add/" + mid + "/" + cid : "/mce-remove/" + mid + "/" + cid;
                $.get( url, function(_data, status) {
                    if (status === 'success'){
                        if ('true' === _data){
                            $('#men-table').DataTable().ajax.reload();
                            $('#cell-table').DataTable().ajax.reload();
                        }else{
                            console.log(_data);
                        }
                    }
                });
            }
        }

        function on_bind_click(rowData){
            selectedCell = JSON.parse(decodeURIComponent(rowData));
            let cid = selectedCell.id;
            $('#modal-users').modal('show');
            if (menTableIsInit){
                $('#men-table').DataTable().ajax.url('/men-data-cell/' + cid).load();
            }else{
                menTableIsInit = true;
                $('#men-table').DataTable({
                    processing: true,
                    serverSide: true,
                    dom: "frtip",
                    ajax: '/men-data-cell/' + cid,
                    columns: [
                        { data: 'is_checked',   title:'{{ __('str.table_linkin2') }}', className: 'text-center'  },
                        { data: 'post', name: 'men.post', title:'{{ __('str.table_post') }}' },
                        { data: 'msurname', name: 'men.msurname', title:'{{ __('str.table_surname') }}' },
                        { data: 'mname', name: 'men.mname', title:'{{ __('str.table_name2') }}' },
                        { data: 'mpatronymic', name: 'men.mpatronymic', title:'{{ __('str.mens_patronymic') }}'  },
                    ]
                }).on('click', 'tbody td', function() {
                    let tr  = $(this).closest('tr');
                    let row = $('#men-table').DataTable().row(tr);
                    onMenClick(row.data());
                });
            }
        }

        function on_unbind_click(rowData){
            if (confirm("{{ __('str.cell_unlink_message') }}")){
                let id = JSON.parse(decodeURIComponent(rowData)).id;
                $.get( "/cell-clear/" + id, function(data, status) {
                    if (status === 'success'){
                        if ('true' === data){
                            $('#cell-table').DataTable().ajax.reload();
                        }else{
                            alert(data);
                            console.log(data);
                        }
                    }
                });
            }
        }

        function fillModal(data){
            let header = ("{{ __('str.cell') }}:" + data.block_pos + ", " +
                                "{{ __('str.block') }}:" + data.name).replaceAll('+',' ');
            $('#cellModalLabelHeader').text(header);
            $('#id').val(data.id);
            $('#csid').val(data.csid);
            $('#is_lock').val(data.is_lock);
            $('#des').val(data.des);
            $('#dst').val(data.dst);
        }

        function onInitComplete(dataTable) {

            // включение данных с инпута cells-block_pos  в запрос ajax
            // до отправки запроса с помощью обработчика событий preXhr.dt
            $('#cell-table').on('preXhr.dt', function (e, settings, data) {
                data.block_pos = $('#cells-block_pos').val();
            });

            createTableSelectors(dataTable,["blocks.name", "cells.block_pos","etypes.tname","etypes.tmark"],
                {"blocks.name" : "is_arc = false", "etypes.tmark" : "hide = false"},

                //селектор создан
                function (selector) {
                    if (selector.id == "cells-block_pos"){
                        selector.style.display = "none";
                    }
                    if (selector.id === 'etypes-tname'){
                        while (selector.options.length > 1) {
                            selector.remove(selector.options.length - 1);
                        }
                        for (let i = 0; i < eTypes.length; i++){
                            if (i > 0 && eTypes[i - 1].is_cons === 0 && eTypes[i].is_cons === 1){
                                let opt = document.createElement('option');
                                opt.innerHTML = "---------------------------------------------------";
                                opt.disabled = true;
                                selector.appendChild(opt);
                            }
                            let opt = document.createElement('option');
                            opt.innerHTML = eTypes[i].tname;
                            opt.value = eTypes[i].tname;
                            selector.appendChild(opt);
                        }
                    }
                },

                //селектор изменён
                function (selector) {
                    if ($(selector).prop('id') == "blocks-name"){
                        $("#cells-block_pos").val("");
                        $("#cells-block_pos").change();
                        if ($(selector).val().length == 0){
                            $('#cells-block_pos').css("display", "none");
                        }else{
                            $("#cells-block_pos > option").each(function() {
                                if (this.value.length > 0) this.remove();
                            });
                            let cellCont = getBlockCellCount($("#blocks-name").val());
                            for (let i = 1; i <= cellCont; i++){
                                $("#cells-block_pos").append($('<option>', {
                                    value: '' + i,
                                    text: '' + i
                                }));
                            }
                            $('#cells-block_pos').css("display", "block");
                        }
                    }
                },null);
        }

        // функция вызывается в момент готовности строки таблицы
        function onRowCreated(row){
            if (row.childNodes[0].querySelector('.compscr') != null){
                const kitTable = row.childNodes[0].querySelector('.compscr');
                let isSetHeight = false;
                new ResizeObserver(function (){
                    if (!isSetHeight){
                        isSetHeight = true;
                        let offsetMargin = -10;
                        let rootH = row.getBoundingClientRect().height;
                        row.style.height = rootH + kitTable.getBoundingClientRect().height + offsetMargin;
                    }
                    kitTable.style.width = row.getBoundingClientRect().width;
                }).observe(row);
            }
        }

        $(document).ready(function(){
            //console.log(@json($blocks));
            //--- обработка таблицы ------------------------------------------------------------------------------------
            $('#cell-table').css('min-height','220px');
            //----------------------------------------------------------------------------------------------------------
        });
    </script>

@endpush
