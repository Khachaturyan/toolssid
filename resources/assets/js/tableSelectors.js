

//----------------------------------------------------------------------------------------------------------------------
// параметр params является массивом и служит для дополнительных
// параметров выборки селектора где ключ - имя колонки например для таблицы блоков:
// {"name" : "is_arc = false"}
// калбек onSelectorCreated отрабатывает когда селектор был создан
// калбек onSelectorChange отрабатывает когда значения селектора меняются, после обновления данных таблицы
// каждый селектор имеет свой ID который получается путём замены в поле name символа . на -
// массив tableOriginalNames содержит пары ключ-значение где ключ - имя таблицы фронта,
// значение - имя оригинальной таблицы в базе, применяется когда таблице присваивается специфическое имя при выборке
//----------------------------------------------------------------------------------------------------------------------

function createTableSelectors(dataTable,names,params,onSelectorCreated,onSelectorChange,tableOriginalNames){
    let tableName = dataTable[0].id;
    let cols = $('#' + tableName).DataTable().settings().init().columns;
    let selectorArray = {};
    dataTable.api().columns().every(function () {
        let name = cols[this[0]].name
        let parts = name.split(".");
        if (parts.length != 2 || (!names.includes(name))){
            return
        }
        let title = cols[this[0]].title;
        let column = this;
        let queryTable = parts[0];
        if (tableOriginalNames != null && typeof tableOriginalNames[parts[0]] !== "undefined"){
            queryTable = tableOriginalNames[parts[0]];
        }
        let url = "/any-cols/" + queryTable + '/' + parts[1];
        if (params != null){
            let queryParam = params[name];
            if (typeof queryParam !== "undefined") {
                url += "/" + encodeURI(queryParam);
            }
        }
        //console.log(url);
        $.get( url, function(_data, status) {
            if (status === 'success'){
                //console.log(_data);
                let data = JSON.parse(_data);
                let input = document.createElement("select");
                input.id = name.replaceAll('.','-');
                input.classList.add('form-control');
                input.classList.add('select-search');
                let opt = document.createElement('option');
                opt.value = '';
                opt.innerHTML = title;
                input.appendChild(opt);
                let vals = [];
                for (let i = 0; i < data.length; i++) {
                    opt = document.createElement('option');
                    let txt = "" + data[i];
                    if (txt.length > 0 && !vals.includes(txt)){
                        opt.value = txt;
                        opt.innerHTML = txt;
                        input.appendChild(opt);
                        vals.push(txt);
                    }
                }
                $(input).on('change', function () {
                    column.search($(this).val(), false, false, true).draw();
                    if (onSelectorChange != null){
                        onSelectorChange(this);
                    }
                });
                if (onSelectorCreated != null){
                    onSelectorCreated(input);
                }
                selectorArray[name] = input;
                if (Object.keys(selectorArray).length == names.length){
                    names.forEach(n => {
                        let i = selectorArray[n];
                        $(i).appendTo('.dataTables_filter');
                    });
                }
            }
        });
    });
}
